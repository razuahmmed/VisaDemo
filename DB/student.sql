/*
SQLyog Ultimate v8.55 
MySQL - 5.0.22-community-nt : Database - student_visa
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`student_visa` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `student_visa`;

/*Table structure for table `sv_company` */

DROP TABLE IF EXISTS `sv_company`;

CREATE TABLE `sv_company` (
  `COMPANY_CODE` int(11) NOT NULL auto_increment,
  `COMPANY_NAME` tinytext,
  `COMPANY_ADDRESS` tinytext,
  `CONTACT_NUMBER` tinytext,
  `COMPANY_STATUS` char(1) default NULL,
  `INSERT_BY` tinytext,
  `INSERT_DATE` date default NULL,
  `UPDATE_BY` tinytext,
  `UPDATE_DATE` date default NULL,
  PRIMARY KEY  (`COMPANY_CODE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `sv_company` */

insert  into `sv_company`(`COMPANY_CODE`,`COMPANY_NAME`,`COMPANY_ADDRESS`,`CONTACT_NUMBER`,`COMPANY_STATUS`,`INSERT_BY`,`INSERT_DATE`,`UPDATE_BY`,`UPDATE_DATE`) values (1,'Brand Bangla IT','Dhaka',NULL,'Y',NULL,NULL,NULL,NULL);

/*Table structure for table `sv_user` */

DROP TABLE IF EXISTS `sv_user`;

CREATE TABLE `sv_user` (
  `USER_ID` bigint(20) NOT NULL auto_increment,
  `USER_NAME` tinytext NOT NULL,
  `USER_PASSWORD` tinytext,
  `USER_STATUS` char(1) default 'Y',
  `DISPLAY_NAME` tinytext,
  `GROUP_ID` int(11) default NULL,
  `INSERT_BY` tinytext,
  `INSERT_DATE` datetime default NULL,
  `UPDATED_BY` tinytext,
  `UPDATED_DATE` datetime default NULL,
  `COMPANY_CODE` int(11) default NULL,
  PRIMARY KEY  (`USER_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `sv_user` */

insert  into `sv_user`(`USER_ID`,`USER_NAME`,`USER_PASSWORD`,`USER_STATUS`,`DISPLAY_NAME`,`GROUP_ID`,`INSERT_BY`,`INSERT_DATE`,`UPDATED_BY`,`UPDATED_DATE`,`COMPANY_CODE`) values (1,'admin','123','Y','Admin',1,NULL,NULL,NULL,NULL,1);

/*Table structure for table `sv_user_group` */

DROP TABLE IF EXISTS `sv_user_group`;

CREATE TABLE `sv_user_group` (
  `GROUP_ID` int(11) NOT NULL auto_increment,
  `GROUP_NAME` tinytext,
  `GROUP_TYPE` tinytext,
  `GROUP_STATUS` char(1) default 'Y',
  `INSERT_BY` tinytext,
  `INSERT_DATE` datetime default NULL,
  `UPDATED_BY` tinytext,
  `UPDATED_DATE` datetime default NULL,
  PRIMARY KEY  (`GROUP_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `sv_user_group` */

insert  into `sv_user_group`(`GROUP_ID`,`GROUP_NAME`,`GROUP_TYPE`,`GROUP_STATUS`,`INSERT_BY`,`INSERT_DATE`,`UPDATED_BY`,`UPDATED_DATE`) values (1,'Admin',NULL,'Y',NULL,NULL,NULL,NULL),(2,'Agent',NULL,'Y',NULL,NULL,NULL,NULL),(3,'Student',NULL,'Y',NULL,NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
