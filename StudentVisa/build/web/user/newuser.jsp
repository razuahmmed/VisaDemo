<%--
    Document   : edituser
    Created on : Jan 6, 2013, 5:01:14 AM
    Author     : MIS
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sx" uri="/struts-dojo-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <head>
            <script type="text/javascript"  src="<%= request.getContextPath() %>/resources/js/jquery-1.8.3.js"></script>
            <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/resources/css/common.css" />
            <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/resources/css/style.css" />
            <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/resources/css/navi.css" />
            <script type="text/javascript"   src="<%= request.getContextPath() %>/resources/js/jquery-1.8.3.js"></script>
            <script type="text/javascript"  src="<%= request.getContextPath() %>/resources/js/jquery-ui.js"></script>
            <link rel="stylesheet" href="<%= request.getContextPath() %>/resources/css/jquery-ui.css" />
            <link rel="shortcut icon" href="<%= request.getContextPath() %>/resources/ico/favicon.png">
            <script type="text/javascript">
              $(window).load(function() { $("#spinner").fadeOut("slow"); })
              $(function() {
                $( "#datepicker" ).datepicker();
                    $(".box .h_title").not(this).next("ul").hide("normal");
                    $(".box .h_title").not(this).next("#home").show("normal");
                    $(".box").children(".h_title").click( function() { $(this).next("ul").slideToggle(); });
                    $("#nextId").live("click",function(){
                    var isValidate = true;
                    if($("#userLevelId").val().length == 0){
        //                alert("Please fill up the input box.");
                        $("#errorMessageDiv").html("<div class=\"n_error\"><p>Warning: Select a valid user level</p></div>");
                        isValidate = false;
                    }
                    else if($("#saoId").val().length == 0){
//                        alert("Please fill up the input box.");
                        $("#errorMessageDiv").html("<div class=\"n_error\"><p>Warning: Select a valid Code</p></div>");
                        isValidate = false;
                    }
                    if(isValidate){
                        $('#newUserFormId').submit();
                    }
                });

              });
            </script>
            <sx:head/>
            <s:head/>
            <script type="text/javascript">
                dojo.addOnLoad(function() {
                        dojo.widget.createWidget("struts:BindEvent", {
                            "sources": "userLevelId",
                            "events": "onchange",
                            "formId": "newUserFormId",
                            "href": "GetSAOCodeByUserLevel",
                            "targets": "saoDivId",
                            "showError": true,
                            "validate": false,
                            "ajaxAfterValidation": false,
                            "scriptSeparation": true,
                            "parseContent": true
                        });

                });
            </script>
    </head>
    <body>
      <%
        boolean login = (request.getSession().getAttribute("logged-in")!=null  && request.getSession().getAttribute("logged-in").toString().trim().equals("true")) ? true: false;
        if(!login){%>
          <jsp:forward page="/index.jsp"></jsp:forward>
        <%
        }else {
      %>
 <div class="wrap">
        <s:include value="/menu.jsp"></s:include>
        <div id="content">
            <div id="sidebar">
                <div class="box">
                    <div class="h_title"><b>&#8250;&nbsp;User Info</b></div>
                    <ul id="home">
                        <li class="b1"><a class="icon add_user" href="NewUserInfoStep1">New User</a></li>
                        <li class="b1"><a class="icon add_user" href="NewAdminUserInfo">New Admin User</a></li>
                        <li class="b1"><a class="icon add_user" href="NewOtherUserInfoStep1">New Other User</a></li>
                        <li class="b1"><a class="icon users" href="GetAllUsers">Show All Users</a></li>
                        <li class="b1"><a class="icon search" href="SearchUser">Search</a></li>
                    </ul>
                </div>
                <div class="box">
                    <div class="h_title"><b>Calendar</b></div>
                    <ul id="home">
                    <div id="datepicker"></div>
                    </ul>
                </div>
            </div>
            <div id="main">
                <div class="full_w">
                    <div class="h_title"><b>New User Information</b></div>
                    <div id="spinner"></div>
                <br/>
                <div style="color:red; width: 50%" align="left">
                    <s:actionerror/>
                </div>
                <div id="errorMessageDiv"></div>
                <s:form id="newUserFormId" action="NewUserInfoStep2" method="post">
                <s:token/>
                <table style="padding-left:2%" border="0">
                    <tr><td>
                    <table>
                        <s:select id="userLevelId" theme="xhtml" list="rtlUserLevels" listKey="levelId" listValue="levelName" name="userLevel.levelId" label="User Role" cssStyle="width:175px;" emptyOption="true" required="true" requiredposition="left"/>
                    </table>
                    </td></tr><tr><td>
                        <sx:div id="saoDivId" executeScripts="true" separateScripts="true">
                        </sx:div>
                    </td></tr>
                </table>
                <div class="divcenter" style="margin-left: 50%;">
                    <s:a  href="#" id="nextId" cssClass="button" onclick="" ><span>next >></span></s:a>
                </div>
                </s:form>
                </div>
            </div>
            <div class="clear"></div>
	</div>

	<s:include value="/footer.jsp"/>
    </div>
    <% } %>
    </body>
</html>
