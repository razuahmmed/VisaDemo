<%--
    Document   : operation
    Created on : Dec 27, 2012, 3:28:32 PM
    Author     : MIS-Monjur
--%>

<%@page import="java.util.Map"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sx" uri="/struts-dojo-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE html>
<html>
    <head>
        <script type="text/javascript"  src="<%= request.getContextPath()%>/resources/js/jquery-1.8.3.js"></script>
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/common.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/style.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/navi.css" />
        <script type="text/javascript"   src="<%= request.getContextPath()%>/resources/js/jquery-1.8.3.js"></script>
        <script type="text/javascript"  src="<%= request.getContextPath()%>/resources/js/jquery-ui.js"></script>
        <link rel="stylesheet" href="<%= request.getContextPath()%>/resources/css/jquery-ui.css" />
        <link rel="STYLESHEET" type="text/css" href="<%= request.getContextPath()%>/resources/codebase/dhtmlxgrid.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/codebase/skins/dhtmlxgrid_dhx_skyblue.css">
        <script type="text/javascript"   src="<%= request.getContextPath()%>/resources/codebase/dhtmlxcommon.js"></script>
        <script type="text/javascript"   src="<%= request.getContextPath()%>/resources/codebase/dhtmlxgrid.js"></script>
        <script type="text/javascript"   src="<%= request.getContextPath()%>/resources/codebase/dhtmlxgridcell.js"></script>
        <script type="text/javascript"   src="<%= request.getContextPath()%>/resources/codebase/ext/dhtmlxgrid_start.js"></script>
        <link rel="shortcut icon" href="<%= request.getContextPath()%>/resources/ico/favicon.png">
        <%
                    String hasSuccessful = request.getSession().getAttribute("hasSuccessful") != null ? request.getSession().getAttribute("hasSuccessful").toString().trim() : null;
                    String message = request.getSession().getAttribute("message") != null ? request.getSession().getAttribute("message").toString().trim() : null;
                    if (hasSuccessful != null) {
                        request.getSession().removeAttribute("hasSuccessful");
                        request.getSession().removeAttribute("message");
                    }
        %>
        <script type="text/javascript">
            $(window).load(function() { $("#spinner").fadeOut("slow"); })
            $(function() {

            <% if (hasSuccessful != null && (hasSuccessful.equalsIgnoreCase("true") || hasSuccessful.equalsIgnoreCase("false"))) {%>
                    $('#dialog').html('<p><%out.print(message);%></p>');
                    $('#dialog').dialog({
                        title:"Message",
                        bgiframe: true,
                        height: 'auto',
                        width: 'auto',
                        modal: true,autoOpen: true,
                        buttons: { "Ok": function() {
                                $(this).dialog("close");}}
                    });

            <% }%>
                    $( "#datepicker" ).datepicker();
                    $(".box .h_title").not(this).next("ul").hide("normal");
                    $(".box .h_title").not(this).next("#home").show("normal");
                    $(".box").children(".h_title").click( function() { $(this).next("ul").slideToggle(); });
                });
        </script>
        <sx:head/>
        <s:head/>
        <script type="text/javascript"  >
            dhtmlx.skin = "dhx_skyblue";
        </script>
        <script type="text/javascript">
            $(document).ready(function($){

                $("#search").live("click",function(){
                    var isValidate = true;
                    if($("#userNameId").val().length == 0){
                        alert("Please type a user name.");
                        isValidate = false;
                    }
                    if(isValidate){
                        $('#userSearchFormId').submit();
                    }
                });
            });
        </script>
    </head>
    <body>
        <%
                    boolean login = (request.getSession().getAttribute("logged-in") != null && request.getSession().getAttribute("logged-in").toString().trim().equals("true")) ? true : false;
                    boolean isAdmin = (request.getSession().getAttribute("isAdmin") != null && request.getSession().getAttribute("isAdmin").toString().equalsIgnoreCase("true")) ? true : false;
                    boolean isStore = (request.getSession().getAttribute("isStore") != null && request.getSession().getAttribute("isStore").toString().equalsIgnoreCase("true")) ? true : false;
                    boolean isArea = (request.getSession().getAttribute("isArea") != null && request.getSession().getAttribute("isArea").toString().equalsIgnoreCase("true")) ? true : false;
                    boolean isOperation = (request.getSession().getAttribute("isOperation") != null && request.getSession().getAttribute("isOperation").toString().equalsIgnoreCase("true")) ? true : false;
                    boolean isMIS = (request.getSession().getAttribute("isMIS") != null && request.getSession().getAttribute("isMIS").toString().equalsIgnoreCase("true")) ? true : false;
                    if (!login) {%>
        <jsp:forward page="/index.jsp"></jsp:forward>
        <%            } else {
        %>
        <div class="wrap">
            <s:include value="/menu.jsp"></s:include>
            <div id="content">
                <div id="sidebar">
                    <%if (isAdmin || isMIS) {%>
                    <div class="box">
                        <div class="h_title"><b>&#8250;&nbsp;User Info</b></div>
                        <ul id="home">
                            <li class="b1"><a class="icon add_user" href="NewUserInfoStep1">New User</a></li>
                            <li class="b1"><a class="icon add_user" href="NewAdminUserInfo">New Admin User</a></li>
                            <li class="b1"><a class="icon add_user" href="NewOtherUserInfoStep1">New Other User</a></li>
                            <li class="b1"><a class="icon users" href="GetAllUsers">Show All Users</a></li>
                            <li class="b1"><a class="icon search" href="SearchUser">Search</a></li>
                        </ul>
                    </div>
                    <% }%>
                    <div class="box">
                        <div class="h_title"><b>Calendar</b></div>
                        <ul id="home">
                            <div id="datepicker"></div>
                        </ul>
                    </div>
                </div>
                <div id="main">
                    <div class="full_w">
                        <div class="h_title"><b>User Info</b></div>
                        <div id="spinner"></div>
                        <br/>
                        <div class="clear"></div>
                        <div id="dialog"></div>
                        <sx:div>
                            <center>
                                <table class="dhtmlxGrid" gridHeight="500px" name="grid2" imgpath="resources/codebase/imgs/" style="width:80%" lightnavigation="true">
                                    <tr>
                                        <td type="ro"><center>User Name</center></td>
                                        <td type="ro"><center>User Level</center></td>
                                        <td type="ro"><center>Active</center></td>
                                        <td type="ro"></td>
                                        <%if (isAdmin || isMIS)%>
                                        <td type="ro"></td>
                                    </tr>
                                    <s:if test="userInfos != null">
                                        <s:iterator value="userInfos">
                                            <tr>
                                                <td style="text-align: left"><s:property value="userName"/></td>
                                                <td style="text-align: left"><s:property value="rtlUserLevel.levelName" /></td>
                                                <s:if test="status == 'Y'|| status == 'y'">
                                                    <td style="text-align: center">Yes</td>
                                                </s:if>
                                                <s:else>
                                                    <td style="text-align: center">No</td>
                                                </s:else>
                                                <td style="text-align: left">
                                                    <form id="E<s:property value="userId"/>" action="FindUserInfo" method="POST">
                                                        <s:hidden name="user.userId" value="%{userId}"></s:hidden>
                                                        <s:hidden name="option" value="details"/>
                                                    </form>
                                                    <a class="table-icon archive"></a>
                                                    <a href="#" onclick="$('#E<s:property value="userId"/>').submit();">details</a>
                                                </td>
                                                <%if (isAdmin || isMIS) {%>
                                                <td style="text-align: left">
                                                    <form id="UPD<s:property value="userId"/>" action="FindUserInfo" method="POST">
                                                        <s:hidden name="user.userId" value="%{userId}"></s:hidden>
                                                        <s:hidden name="option" value="update"/>
                                                    </form>
                                                    <a class="table-icon edit"></a>
                                                    <a href="#" onclick="var answer =$('#UPD<s:property value="userId"/>').submit();">update</a>
                                                </td>
                                                <% }%>
                                            </tr>
                                        </s:iterator>
                                    </s:if>
                                </table>
                            </center>
                        </sx:div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>

            <s:include value="/footer.jsp"/>
        </div>
        <% }%>

    </body>
</html>

