<%-- 
    Document   : newadminuser
    Created on : Mar 7, 2013, 11:12:17 AM
    Author     : MIS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sx" uri="/struts-dojo-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>New Admin User Information</title>
        <script type="text/javascript"  src="<%= request.getContextPath() %>/resources/js/jquery-1.8.3.js"></script>
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/resources/css/common.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/resources/css/style.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/resources/css/navi.css" />
        <script type="text/javascript"   src="<%= request.getContextPath() %>/resources/js/jquery-1.8.3.js"></script>
        <script type="text/javascript"  src="<%= request.getContextPath() %>/resources/js/jquery-ui.js"></script>
        <link rel="stylesheet" href="<%= request.getContextPath() %>/resources/css/jquery-ui.css" />
        <link rel="shortcut icon" href="<%= request.getContextPath() %>/resources/ico/favicon.png">
        <script type="text/javascript">
          $(window).load(function() { $("#spinner").fadeOut("slow"); })
          $(function() {
            $( "#datepicker" ).datepicker();
                $(".box .h_title").not(this).next("ul").hide("normal");
                $(".box .h_title").not(this).next("#home").show("normal");
                $(".box").children(".h_title").click( function() { $(this).next("ul").slideToggle(); });
          });
        </script>
        <sx:head/>
        <s:head/>

        <script type="text/javascript">
            $(document).ready(function($){

                $("#save").live("click",function(){
                    var isValidate = true;
                    $("#error1").html("");
                    $("#error2").html("");
                    $("#error3").html("");
                    if($("#id1").val().length == 0){
                        $("#error1").html("Mandatory field must be filled up");
                        isValidate = false;
                    }
                    if($("#id2").val().length == 0){
                        $("#error2").html("Mandatory field must be filled up");
                        isValidate = false;
                    }
                    if($("#id3").val().length == 0){
                        $("#error3").html("Mandatory field must be filled up");
                        isValidate = false;
                    }
                    if($("#id2").val().length > 0 && $("#id2").val().length <6 || $("#id3").val().length > 0 && $("#id3").val().length <6){
                        alert("Password length must be 6.");
                        $("#id2").val("");
                        $("#id3").val("");
                        isValidate = false;
                    }
                    if(isValidate){
                        var answer=confirm('Do you want to save it?'); if(answer==true){
                            $('#newUserFormId').submit();
                        }
                    }
                });
            });
        </script>
        <s:head/>
    </head>
    <body>
      <%
        boolean login = (request.getSession().getAttribute("logged-in")!=null  && request.getSession().getAttribute("logged-in").toString().trim().equals("true")) ? true: false;
        if(!login){%>
          <jsp:forward page="/index.jsp"></jsp:forward>
        <%
        }else {
      %>
    <div class="wrap">
        <s:include value="/menu.jsp"></s:include>
        <div id="content">
            <div id="sidebar">
                <div class="box">
                    <div class="h_title"><b>&#8250;&nbsp;User Info</b></div>
                    <ul id="home">
                        <li class="b1"><a class="icon add_user" href="NewUserInfoStep1">New User</a></li>
                        <li class="b1"><a class="icon add_user" href="NewAdminUserInfo">New Admin User</a></li>
                        <li class="b1"><a class="icon add_user" href="NewOtherUserInfoStep1">New Other User</a></li>
                        <li class="b1"><a class="icon users" href="GetAllUsers">Show All Users</a></li>
                        <li class="b1"><a class="icon search" href="SearchUser">Search</a></li>
                    </ul>
                </div>
                <div class="box">
                    <div class="h_title"><b>Calendar</b></div>
                    <ul id="home">
                    <div id="datepicker"></div>
                    </ul>
                </div>
            </div>
            <div id="main">
                <div class="full_w">
                    <div class="h_title"><b>New Admin User Info</b></div>
                    <div id="spinner"></div>
                    <br/>
                    <div style="color:red; width: 50%" align="left">
                        <s:actionerror/>
                    </div>
                    <form id="cancelFormId" action="ListUser" method="POST">
                    </form>
                    <s:form id="newUserFormId" action="AddUserInfo" method="post">
                        <s:hidden name="userLevel.levelId" value="%{userLevel.levelId}"/>
                    <s:token/>
                    <table style="padding-left:2%" border="0">
                        <s:textfield id="id1" theme="xhtml" name="user.userName" label="User Name" required="true" size="40" requiredposition="left"/>
                        <tr><td></td><td><span id="error1" style="color:red; font-style: italic;"></span></td></tr>
                        <s:textfield theme="xhtml" name="userLevel.levelName" label="User Role"  size="40" readonly="true"/>
                        <s:textfield theme="xhtml" name="user.saoCode" label="%{userLevel.levelName} Code" size="40" readonly="true"/>
                        <s:password id="id2" theme="xhtml" name="user.userPassword" label="Password"  required="true" size="40" requiredposition="left"/>
                        <tr><td></td><td><span id="error2" style="color:red; font-style: italic;"></span></td></tr>
                        <s:password id="id3" theme="xhtml" name="password" label="Re-type Password"  required="true" size="40" requiredposition="left"/>
                        <tr><td></td><td><span id="error3" style="color:red; font-style: italic;"></span></td></tr>
                        <s:textarea theme="xhtml" name="user.description" label="Description" rows="6" cols="40" cssStyle="resize:none;"/>
                    </table>
                    <div class="divcenter" style="margin-left: 50%;">
                        <a id="save" href="#" class="button"><span>Save</span></a>
                        <a href="#" onclick="var answer=confirm('Do you want to cancel it?'); if(answer==true){$('#cancelFormId').submit();}" class="button"><span>Cancel</span></a>
                    </div>
                    </s:form>
                </div>
            </div>
            <div class="clear"></div>
	</div>

	<s:include value="/footer.jsp"/>
    </div>
    <% } %>
    </body>
</html>
