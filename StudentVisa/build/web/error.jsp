<%--
    Document   : home
    Created on : Feb 9, 2013, 9:20:09 PM
    Author     : MIS
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sx" uri="/struts-dojo-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/common.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/style.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/navi.css" />
        <script type="text/javascript"   src="<%= request.getContextPath()%>/resources/js/jquery-1.8.3.js"></script>
        <script type="text/javascript"  src="<%= request.getContextPath()%>/resources/js/jquery-ui.js"></script>
        <link rel="stylesheet" href="<%= request.getContextPath()%>/resources/css/jquery-ui.css" />
        <link rel="shortcut icon" href="<%= request.getContextPath()%>/resources/ico/favicon.png">
        <%
                    String hasSuccessful = request.getSession().getAttribute("hasSuccessful") != null ? request.getSession().getAttribute("hasSuccessful").toString().trim() : null;
                    String message = request.getSession().getAttribute("message") != null ? request.getSession().getAttribute("message").toString().trim() : null;
                    if (hasSuccessful != null) {
                        request.getSession().removeAttribute("hasSuccessful");
                        request.getSession().removeAttribute("message");
                    }
        %>
        <sx:head/>
        <s:head/>
    </head>
    <body>
        <%
                    boolean login = (request.getSession().getAttribute("logged-in") != null && request.getSession().getAttribute("logged-in").toString().trim().equals("true")) ? true : false;
                    if (!login) {%>
        <jsp:forward page="/index.jsp"></jsp:forward>
        <%                    } else {
        %>
        <div class="wrap">

            <s:include value="/top_menu.jsp"></s:include>

            <div id="content">
                <!-- side bar sub menu div -->
                <div id="sidebar">

                </div>
                <!-- main div -->
                <div id="main">
                    <div class="full_w">
                        <div class="h_title"><b>Error</b></div>
                        <s:actionerror/>
                    </div>

                </div>
                <div class="clear"></div>
            </div>

            <s:include value="/footer.jsp"/>
        </div>
        <% }%>
    </body>
</html>