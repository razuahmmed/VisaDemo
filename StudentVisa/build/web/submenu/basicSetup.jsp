<%@page import="mis.drd.action.ParamUtil"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%
            // Basic Setup Menu
            boolean PM12CM01BS001 = (request.getSession().getAttribute("PM12CM01BS001") != null && request.getSession().getAttribute("PM12CM01BS001").toString().equalsIgnoreCase("T")) ? true : false;
            boolean PM12CM01BS002 = (request.getSession().getAttribute("PM12CM01BS002") != null && request.getSession().getAttribute("PM12CM01BS002").toString().equalsIgnoreCase("T")) ? true : false;
            boolean PM12CM01BS003 = (request.getSession().getAttribute("PM12CM01BS003") != null && request.getSession().getAttribute("PM12CM01BS003").toString().equalsIgnoreCase("T")) ? true : false;
%>

<div class="h_title"><b>&#8250;&nbsp;Basic Setup</b></div>

<ul id="home">

    <%if (PM12CM01BS001) {%>
    <li class="b1"><a class="icon view_page" title="Company Info" href="companyInfoViewBACSET.bata">Company Info</a></li>
    <% }%>
    <li class="b1"><a class="icon view_page" title="Brand" href="brandViewBACSET.bata">Brand</a></li>
    <li class="b1"><a class="icon view_page" title="Category Group" href="#">Category Group</a></li>
    <li class="b1"><a class="icon view_page" title="Category Gender" href="#">Category Gender</a></li>
    <li class="b1"><a class="icon view_page" title="Category" href="#">Category</a></li>
    <li class="b1"><a class="icon view_page" title="Sub Category" href="#">Sub Category</a></li>
    <li class="b1"><a class="icon view_page" title="Size Group" href="#">Size Group</a></li>
    <li class="b1"><a class="icon view_page" title="Color" href="#">Color</a></li>
    <li class="b1"><a class="icon view_page" title="Supplier" href="#">Supplier</a></li>
    <li class="b1"><a class="icon view_page" title="Buyer" href="#">Buyer</a></li>
    <li class="b1"><a class="icon view_page" title="Project" href="#">Project</a></li>
    <li class="b1"><a class="icon view_page" title="Production Process" href="#">Production Process</a></li>
    
</ul>