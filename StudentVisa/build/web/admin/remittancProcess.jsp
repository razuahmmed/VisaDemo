
<%@page import="mis.drd.action.ParamUtil"%>
<!DOCTYPE html>
<%@taglib  prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html;" charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <title>FootIn</title>

        <link href="<%= request.getContextPath()%>/resources/css/style.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/bootstrap.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/yamm.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/style_menu.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/resources/css/jquery.dataTables.css" rel="stylesheet" />

    </head>

    <style type="text/css">
        <!--
        #example th {
            /* color: #E06C0A; 009999 */
            background: none repeat scroll 0 0 #E06C0A;
            color: #FFFFFF;
            font-size: 12px;
            height: 22px;
            vertical-align: middle;
            padding: 2px;
            text-align: center;
            border: 1px solid #FFFFFF;
        }

        #example td {
            vertical-align: middle;
        }

        -->
    </style>


    <body>

        <jsp:include page="/top_menu.jsp" flush="true"></jsp:include>

        <div class="body" style="padding-left: 15px; padding-right: 15px; height: 270px; vertical-align: middle;">

            <table width="100%">
                <tr>
                    <td><a href="newRemittanceProcessOdr.footin" class="btn btn-primary">Remittance Upload</a></td>
                </tr>
            </table>

            <br/>

            <table id="example" class="table table-striped" cellspacing="0" width="100%">

                <thead>

                    <tr>
                        <th>Process Date</th>
                        <th>AWB</th>
                        <th>Amount</th>
                        <th>MR-NO</th>
                        <th>MR-DATE</th>
                        <th>Collected Amount</th>
                        <th>Pickup Date</th>
                        <th>Shipper Name</th>
                        <th>Remarks</th>
                        <th>Process By</th>
                    </tr>

                </thead>

                <tfoot>
                    <tr>
                        <th colspan="5" style="text-align: right;">Total:</th>
                        <th colspan="5" style="text-align: left;">Total:</th>
                    </tr>
                </tfoot>

                <tbody>

                    <s:if test="ftnRemittanceList != null">
                        <s:if test="ftnRemittanceList.size()!=0">
                            <s:iterator value="ftnRemittanceList">
                                <tr>
                                    <td><s:property value="processDate"/></td>
                                    <td><s:property value="awb"/></td>
                                    <td style="text-align: right; padding-right: 2px;"><s:property value="amount"/></td>
                                    <td><s:property value="mrno"/></td>
                                    <td><s:property value="mrdate"/></td>
                                    <td style="text-align: right; padding-right: 2px;"><s:property value="collectedAmount"/></td>
                                    <td><s:property value="pickupDate"/></td>
                                    <td><s:property value="shipperName"/></td>
                                    <td><s:property value="remarks"/></td>
                                    <td><s:property value="processBy"/></td>

                                </tr>

                            </s:iterator>
                        </s:if>
                    </s:if>
                </tbody>

            </table>

        </div>


        <jsp:include page="/footer.jsp" flush="true"></jsp:include>

        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery-1.8.3.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery-ui.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/bootstrap.min.js"></script>


        <script type="text/javascript">
            
            $(document).ready(function($) {               
                onloadFunction();
            });

            function onloadFunction(){

                var total = 0;
                var pageTotal =0;

                // DataTable
                $('#example').DataTable({
                    "aoColumnDefs": [{'bSortable': false, 'aTargets': [0]}],
                    "order": [[ 3, "desc" ]],
                    "pageLength": 25,
                    "footerCallback": function ( row, data, start, end, display ) {
                        var api = this.api(), data;

                        // Remove the formatting to get integer data for summation
                        var intVal = function ( i ) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '')*1 :
                                typeof i === 'number' ?
                                i : 0;
                        };

                        // Total over all pages
                        total = api.column(5).data().reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                        // Total over this page
                        pageTotal = api.column(5, { page: 'current'}).data().reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                        // Update footer
                        $( api.column(5).footer()).html(''+pageTotal +' ( '+ total +' total)');
                    }
                });

            }


        </script>

    </body>

</html>
