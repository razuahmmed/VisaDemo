
<%@page import="mis.drd.action.ParamUtil"%>
<!DOCTYPE html>
<%@taglib  prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html;" charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <title>FootIn</title>

        <link href="<%= request.getContextPath()%>/resources/css/style.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/bootstrap.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/yamm.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/style_menu.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/resources/css/jquery.dataTables.css" rel="stylesheet" />

    </head>

    <style type="text/css">
        <!--
        #example th {
            /* color: #E06C0A; 009999 */
            background: none repeat scroll 0 0 #E06C0A;
            color: #FFFFFF;
            font-size: 12px;
            height: 22px;
            vertical-align: middle;
            padding: 2px;
            text-align: center;
            border: 1px solid #FFFFFF;
        }

        #example td {
            vertical-align: middle;
        }

        -->
    </style>


    <body>

        <jsp:include page="/top_menu.jsp" flush="true"></jsp:include>

        <div class="body" style="padding-left: 15px; padding-right: 15px; height: 270px; vertical-align: middle;">

            <div id="successMessageDiv" style="padding-left: 12px; font-size: 12px; color: red; text-align: center;">
                <s:property value="message"/>
            </div>

            <table style="width: 100%;">

                <tr>

                    <td valign="top" style="width: 35%;">
                        <form action="remittanceFileUploadOdr.footin" id="remittanceFileUploadFrom" enctype="multipart/form-data" method="post" accept-charset="utf-8">

                            <table id="example1" style="width: 100%;">

                                <tr>
                                    <td style="width: 40%; text-align: right; padding-right: 5px;"><label>Year</label></td>
                                    <td>
                                        <select id="yearList" name="yearList" class="form-control" style="width: 150px;" >
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: right; padding-right: 5px;"><label>Week</label></td>
                                    <td>
                                        <select id="weekList" name="weekList" class="form-control" style="width: 150px;" >
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: right; padding-right: 5px;"><label>Date</label></td>
                                    <td>
                                        <select id="dateList" name="dateList" class="form-control" style="width: 150px;" >
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: right; padding-right: 5px;"><label>Circular File</label></td>
                                    <td><input type="file" id="circularFileId" name="myFile" /></td>
                                </tr>

                                <tr>
                                    <td></td>
                                    <td align="left">
                                        <button class="btn btn-primary btn-green" type="submit">Upload</button>
                                        <a href="<%= request.getContextPath()%>/CIRCULAR_SAMPLE_FILE/Remittance_Sample.xlsx">Remittance Sample File</a>
                                    </td>
                                </tr>

                            </table>

                        </form>
                    </td>

                    <td valign="top" style="width: 65%;">

                        <table id="example" class="table table-striped" cellspacing="0" width="100%">

                            <thead>

                                <tr>
                                    <th>Process Date</th>
                                    <th>AWB</th>
                                    <th>Amount</th>
                                    <th>MR-NO</th>
                                    <th>MR-DATE</th>
                                    <th>Collected Amount</th>
                                    <th>Pickup Date</th>
                                    <th>Shipper Name</th>
                                    <th>Remarks</th>
                                    <th>Process By</th>
                                </tr>

                            </thead>

                            <tfoot>
                                <tr>
                                    <th colspan="5" style="text-align: right;">Total:</th>
                                    <th colspan="5" style="text-align: left;">Total:</th>
                                </tr>
                            </tfoot>

                            <tbody>

                                <s:if test="ftnRemittanceList != null">
                                    <s:if test="ftnRemittanceList.size()!=0">
                                        <s:iterator value="ftnRemittanceList">
                                            <tr>
                                                <td><s:property value="processDate"/></td>
                                                <td><s:property value="awb"/></td>
                                                <td style="text-align: right; padding-right: 2px;"><s:property value="amount"/></td>
                                                <td><s:property value="mrno"/></td>
                                                <td><s:property value="mrdate"/></td>
                                                <td style="text-align: right; padding-right: 2px;"><s:property value="collectedAmount"/></td>
                                                <td><s:property value="pickupDate"/></td>
                                                <td><s:property value="shipperName"/></td>
                                                <td><s:property value="remarks"/></td>
                                                <td><s:property value="processBy"/></td>

                                            </tr>

                                        </s:iterator>
                                    </s:if>
                                </s:if>
                            </tbody>

                        </table>

                    </td>

                </tr>

            </table>



        </div>


        <jsp:include page="/footer.jsp" flush="true"></jsp:include>

        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery-1.8.3.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery-ui.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/bootstrap.min.js"></script>


        <script type="text/javascript">

            $(function() {
                setTimeout(function() {$("#successMessageDiv").hide('blind', {}, 500)}, 3000);
            });
            
            function remittanceYear(){
                
                $.ajax({
                    type: "POST",
                    url: "remittanceYearOdr.footin",
                    success: function(data){                       
                        var select = $('#yearList');
                        select.find('option').remove();
                        var json = $.parseJSON(data);
                        $('<option>').val("0").text("SELECT").appendTo(select);
                        for (var i=0; i< json.length; i++){
                            $('<option>').val(json[i].year).text(json[i].year).appendTo(select);
                        }
                    }
                });
                        
            }


            $(document).ready(function($) {               

                remittanceYear();               

                $("#yearList").change(function(event) {

                    var year = $("select#yearList").val();

                    if(year =="0"){
                        var select = $('#weekList');
                        select.find('option').remove();
                        //
                        var select1 = $('#dateList');
                        select1.find('option').remove();
                    }else{

                        $.ajax({
                            type: "POST",
                            url: "remittanceWeekOdr.footin?ftnYear="+year,
                            success: function(data){
                             
                                var select = $('#weekList');
                                select.find('option').remove();
                                var json = $.parseJSON(data);
                                $('<option>').val("0").text("SELECT").appendTo(select);
                                for (var i=0; i< json.length; i++){
                                    $('<option>').val(json[i].yrweek).text(json[i].yrweek).appendTo(select);
                                }
                            }
                        });

                    }

                });

                // Week select options

                $("#weekList").change(function(event) {

                    var year = $("select#yearList").val();
                    var week = $("select#weekList").val();

                    if(week =="0"){
                        var select = $('#dateList');
                        select.find('option').remove();
                    }else{

                        $.ajax({
                            type: "POST",
                            url: "remittanceDateOdr.footin?ftnYear="+year+"&ftnWeek="+week,
                            success: function(data){

                                var select = $('#dateList');
                                select.find('option').remove();
                                var json = $.parseJSON(data);
                                $('<option>').val("0").text("SELECT").appendTo(select);
                                for (var i=0; i< json.length; i++){
                                    $('<option>').val(json[i].wkdate).text(json[i].wkdate).appendTo(select);
                                }
                            }
                        });

                    }

                });

                onloadFunction();


            });

            function onloadFunction(){

                var total = 0;
                var pageTotal =0;

                // DataTable
                var table =   $('#example').DataTable({
                    "aoColumnDefs": [{'bSortable': false, 'aTargets': [0]}],
                    "order": [[ 3, "desc" ]],
                    "pageLength": 25,
                    "footerCallback": function ( row, data, start, end, display ) {
                        var api = this.api(), data;

                        // Remove the formatting to get integer data for summation
                        var intVal = function ( i ) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '')*1 :
                                typeof i === 'number' ?
                                i : 0;
                        };

                        // Total over all pages
                        total = api.column(5).data().reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                        // Total over this page
                        pageTotal = api.column(5, { page: 'current'}).data().reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                        // Update footer
                        $( api.column(5).footer()).html(''+pageTotal +' ( '+ total +' total)');
                    }
                });

            }


        </script>

    </body>

</html>
