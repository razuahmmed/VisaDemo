<%@page import="mis.drd.action.ParamUtil"%>
<!DOCTYPE html>
<%@taglib  prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<html lang="en" >

    <head>
        <meta content="" charset="UTF-8" />
        <title>FootIn</title>
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/login.css" />       
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/common.css" />
    </head>


    <body>

        <div style="background-color: #E06C0A;  height: 40px;">&nbsp;</div>

        <div class="wrap">

            <div id="content">

                <div id="main">

                    <div class="full_w">

                        <table border="0" width="100%">

                            <tr>
                                <td>
                                    Error : 1001
                                </td>                               
                            </tr>

                        </table>

                    </div>

                </div>

            </div>

        </div>

        <jsp:include page="admin_footer.jsp" flush="true"></jsp:include>

    </body>

</html>