<!DOCTYPE html>
<%@page import="mis.drd.action.ParamUtil"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<html lang="en">

    <head>
        <meta content=""  charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Student Visa Management System</title>

        <link type="text/css" rel="stylesheet" href="<%= request.getContextPath()%>/resources/css/bootstrap.min.css" />
        <link type="text/css" rel="stylesheet" href="<%= request.getContextPath()%>/resources/css/bootstrap-theme.min.css" />
        <link type="text/css" rel="stylesheet" href="<%= request.getContextPath()%>/resources/css/font-awesome.min.css" />
        <link type="text/css" rel="stylesheet" href="<%= request.getContextPath()%>/resources/css/pogo-slider.css" />
        <link type="text/css" rel="stylesheet" href="<%= request.getContextPath()%>/resources/css/style.css" />
        <link type="text/css" rel="stylesheet" href="<%= request.getContextPath()%>/resources/css/responsive.css" />


    </head>


    <%
                if (ParamUtil.isSet(request.getSession().getAttribute("logged-in"))) {
                    request.getSession().removeAttribute("logged-in");
                    request.getSession().removeAttribute("userName");
                }
    %>

    <body>

        <div class="site-wrapper">
            <header class="header">
                <div class="tob-bar">
                    <div class="container">
                        <div class="row">
                            <div class="top-inner-warp">
                                <div class=""></div>
                                <div class="flot-left">
                                    <div class="contact-num">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="menu-wrapper">
                    <div class="container">
                        <div class="row">
                            <nav class="main-menu-warp">



                            </nav>
                        </div>
                    </div>
                </div>
            </header>
            <section class="main-content">
                <div class="fature-box">
                    <div class="container">
                        <div class="row">
                            <div class="panel panel-default loginbox">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Login You Account</h3>
                                </div>
                                <div class="panel-body">
                                    <form action="signInLog.student" method="post">
                                        <div style="padding-bottom:10px;" align="center" >
                                            <s:actionerror/>
                                        </div>
                                        <div class="form-group">
                                            <label for="username">Username</label>
                                            <input type="text" name="userName"  class="form-control text" required="required" maxlength="20" placeholder="Username">
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input type="password" name="password" class="form-control text" id="pass" placeholder="Password" required="required" maxlength="20">
                                        </div>
                                        <div class="form-group submit">
                                            <input type="submit" name="login" class="btn btn-primary ok" id="LId" value="login" />
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <footer class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-8">
                            <div class="copyright">
                                <span class="copy">� 2017 Student Visa Management System. All Rights Reserved.</span>
                            </div>
                        </div>

                    </div>
                </div>
            </footer>
        </div>

        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery.min.js" />
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/bootstrap.min.js" />
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery.pogo-slider.js" />
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/scripts.js" />


    </body>

</html>