<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>


<script type="text/javascript">

    function headerCheckBox(){
        if($("#headerChkId").is(':checked')){
            $('input:checkbox').attr('checked',true);
        } else{
            $('input:checkbox').attr('checked',false);
        }    
    }

    function L1CheckBox(chkBxId, l1, l2, l3){
        
        var chckbox=document.getElementById(chkBxId);

        if(chckbox.checked) {

            document.getElementById("headerChkId").checked = true;

            var l2String = l2.substr(0,2);
            var l2Value = l2.substr(2,2);
            var l2Index=parseInt(l2Value)+1;

            $('.CB'+l1).each(function(){
                this.checked = true;

                $('.CB'+l1+l2String+l2Index).each(function(){
                    this.checked = true;
                });

                l2Index++;

            });

        }else{
            
            var l2String = l2.substr(0,2);
            var l2Value = l2.substr(2,2);
            var l2Index=parseInt(l2Value)+1;

            $('.CB'+l1).each(function(){
                this.checked = false;

                $('.CB'+l1+l2String+l2Index).each(function(){
                    this.checked = false;
                });

                l2Index++;

            });

            if($("input:checkbox:checked").length==1) {
                document.getElementById("headerChkId").checked = false;
            }
        }
    }

    function L2CheckBox(chkBxId,l1, l2,l3){

        var chckbox=document.getElementById(chkBxId);

        if(chckbox.checked) {

            $('.CB'+l1+l2).each(function(){
                this.checked = true;
            });

            document.getElementById("headerChkId").checked = true;
            document.getElementById(l1+"L20L30").checked = true;

        }else{

            $('.CB'+l1+l2).each(function(){
                this.checked = false;
            });

            if($(".CB"+l1+":checked").length==0) {
                document.getElementById(l1+"L20L30").checked = false;
            }

            if($("input:checkbox:checked").length==1) {
                document.getElementById("headerChkId").checked = false;
            }
        }
        
    }

    function L3CheckBox(chkBxId,l1, l2,l3){

        var chckbox=document.getElementById(chkBxId);

        if(chckbox.checked) {
            document.getElementById("headerChkId").checked = true;
            document.getElementById(l1+"L20L30").checked = true;
            document.getElementById(l1+l2+"L30").checked = true;            
        }else{

            if($(".CB"+l1+l2+":checked").length==0) {
                document.getElementById(l1+l2+"L30").checked = false;
            }

            if($(".CB"+l1+":checked").length==0) {
                document.getElementById(l1+"L20L30").checked = false;
            }
            
            if($("input:checkbox:checked").length==1) {
                document.getElementById("headerChkId").checked = false;
            }
        }
    }

    function L2ExpandCollapse(parentBxClass, chdBxClass, l1, l2, l3){
       
        if ($('.'+parentBxClass).text() == '[+]') {
            $('.'+parentBxClass).text('[-]');
            $('.'+chdBxClass).show();
            
        } else {

            $('.'+parentBxClass).text('[+]');
            $('.'+chdBxClass).hide();

            var l=parseInt(l2);
            $('.'+chdBxClass).each(function(){
                l=l+1;
                $('.RWL1'+l1+'L2'+l+'L30').hide();
                $('.RWL1'+l1+'L2'+l).text('[+]');
            });
        }

    }

    function L3ExpandCollapse(parentBxClass, chdBxClass){

        if ($('.'+parentBxClass).text() == '[+]') {
            $('.'+parentBxClass).text('[-]');
            $('.'+chdBxClass).show();
        } else {
            $('.'+parentBxClass).text('[+]');
            $('.'+chdBxClass).hide();
        }

    }

    $(function() {
        
        if($("input:checkbox:checked").length>1) {
            document.getElementById("headerChkId").checked = true;
        }
      
    });

       

</script>

<table id="groupFormTbl" style="width: 80%;">

    <thead>
        <tr>
            <th style="width: 20px; text-align: center">
                <input type="checkbox" id="headerChkId" name="headerChkId" onclick="headerCheckBox();" >
            </th>
            <th style="width: 150px;">Menu</th>
            <th style="width: 250px;">Sub Menu</th>
            <th style="width: 350px;">Sub Child Menu</th>
            <th style="width: 40px;">&nbsp;</th>
        </tr>
    </thead>

    <s:if test="rtlMainMenuInfoList != null">

        <s:if test="rtlMainMenuInfoList.size()!=0">

            <tbody>

                <% int l1 = 0;%>
                <% int l2 = 0;%>
                <% int l3 = 0;%>

                <s:iterator value="rtlMainMenuInfoList">                    

                    <tr style="background-color: #336ea5; font-size: 12px; color: #ffffff" >

                        <td style="text-align: center;">
                            <s:if test="mainMenuStatus=='Y'">
                                <input type="checkbox" onclick="L1CheckBox('L1<%=l1%>L2<%=l2%>L3<%=l3%>','L1<%=l1%>','L2<%=l2%>','L3<%=l3%>');" id="L1<%=l1%>L2<%=l2%>L3<%=l3%>" name="L1<%=l1%>L2<%=l2%>L3<%=l3%>" checked>
                            </s:if>
                            <s:else>
                                <input type="checkbox" onclick="L1CheckBox('L1<%=l1%>L2<%=l2%>L3<%=l3%>','L1<%=l1%>','L2<%=l2%>','L3<%=l3%>');" id="L1<%=l1%>L2<%=l2%>L3<%=l3%>" name="L1<%=l1%>L2<%=l2%>L3<%=l3%>">
                            </s:else>
                        </td>
                        <td style="text-align: left; padding-left: 3px;">
                            <input type="hidden" id="TL1<%=l1%>L2<%=l2%>L3<%=l3%>" name="TL1<%=l1%>L2<%=l2%>L3<%=l3%>" value="<s:property value="mainMenuId"/>">
                            <s:property value="mainMenuName"/>
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <s:if test="rtlSubMenuList != null">
                            <s:if test="rtlSubMenuList.size()!=0">
                                <td style="cursor:pointer; text-align: center;" onclick="L2ExpandCollapse('RWL1<%=l1%>L2<%=l2%>','RWL1<%=l1%>L2<%=l2%>L3<%=l3%>',<%=l1%>, <%=l2%>,<%=l3%>);" class="RWL1<%=l1%>L2<%=l2%>">[-]</td>
                            </s:if>
                            <s:else>
                                <td></td>
                            </s:else>
                        </s:if>
                        <s:else>
                            <td></td>
                        </s:else>
                    </tr>

                    <% l2 = 1;%>

                    <s:iterator var="sub" value="rtlSubMenuList">

                        <tr class="RWL1<%=l1%>L20L30">
                            <td>&nbsp;</td>
                            <td style="text-align: right; padding-right: 5px;">
                                <s:if test="#sub.subMenuStatus=='Y'">
                                    <input type="checkbox" class="CBL1<%=l1%>" onclick="L2CheckBox('L1<%=l1%>L2<%=l2%>L3<%=l3%>','L1<%=l1%>','L2<%=l2%>','L3<%=l3%>');" id="L1<%=l1%>L2<%=l2%>L3<%=l3%>" name="L1<%=l1%>L2<%=l2%>L3<%=l3%>" checked>
                                </s:if>
                                <s:else>
                                    <input type="checkbox" class="CBL1<%=l1%>" onclick="L2CheckBox('L1<%=l1%>L2<%=l2%>L3<%=l3%>','L1<%=l1%>','L2<%=l2%>','L3<%=l3%>');" id="L1<%=l1%>L2<%=l2%>L3<%=l3%>" name="L1<%=l1%>L2<%=l2%>L3<%=l3%>">
                                </s:else>
                            </td>                            
                            <td style="text-align: left; padding-left: 3px;">
                                <input type="hidden" id="TL1<%=l1%>L2<%=l2%>L3<%=l3%>" name="TL1<%=l1%>L2<%=l2%>L3<%=l3%>" value="<s:property value="#sub.subMenuId"/>">
                                <s:property value="#sub.subMenuName"/>
                            </td>
                            <td>&nbsp;</td>
                            <s:if test="#sub.rtlSubChdMenuList != null">
                                <s:if test="#sub.rtlSubChdMenuList.size()!=0">
                                    <td style="cursor:pointer; text-align: center;" onclick="L3ExpandCollapse('RWL1<%=l1%>L2<%=l2%>','RWL1<%=l1%>L2<%=l2%>L3<%=l3%>');" class="RWL1<%=l1%>L2<%=l2%>">[-]</td>
                                </s:if>
                                <s:else>
                                    <td></td>
                                </s:else>
                            </s:if>
                            <s:else>
                                <td></td>  
                            </s:else>

                        </tr>

                        <% l3 = 1;%>

                        <s:iterator var="subchd" value="#sub.rtlSubChdMenuList">

                            <tr class="RWL1<%=l1%>L2<%=l2%>L30">
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td style="text-align: right; padding-right: 5px;">
                                    <s:if test="#subchd.subMenuStatus=='Y'">
                                        <input type="checkbox" class="CBL1<%=l1%>L2<%=l2%>" onclick="L3CheckBox('L1<%=l1%>L2<%=l2%>L3<%=l3%>','L1<%=l1%>','L2<%=l2%>','L3<%=l3%>');" id="L1<%=l1%>L2<%=l2%>L3<%=l3%>" name="L1<%=l1%>L2<%=l2%>L3<%=l3%>" checked>
                                    </s:if>
                                    <s:else>
                                        <input type="checkbox" class="CBL1<%=l1%>L2<%=l2%>" onclick="L3CheckBox('L1<%=l1%>L2<%=l2%>L3<%=l3%>','L1<%=l1%>','L2<%=l2%>','L3<%=l3%>');" id="L1<%=l1%>L2<%=l2%>L3<%=l3%>" name="L1<%=l1%>L2<%=l2%>L3<%=l3%>">
                                    </s:else>
                                </td>                                
                                <td style="text-align: left; padding-left: 3px;">
                                    <input type="hidden" id="TL1<%=l1%>L2<%=l2%>L3<%=l3%>" name="TL1<%=l1%>L2<%=l2%>L3<%=l3%>" value="<s:property value="#subchd.subMenuId"/>">
                                    <s:property value="#subchd.subMenuName"/>
                                </td>
                                <td>&nbsp;</td>

                            </tr>

                            <% l3++;%>

                        </s:iterator>

                        <% l3 = 0;%>
                        <% l2++;%>

                    </s:iterator>

                    <% l2 = 0;%>
                    <% l1++;%>

                </s:iterator>

            </tbody>

            <tfoot>
                <tr >
                    <td colspan="4" style="text-align: center;">
                        <input type="button" onclick="updateGroupForm()" id="saveButton" name="saveButton" value="Update">
                    </td>
                </tr>
            </tfoot>

        </s:if>
    </s:if>

</table>



