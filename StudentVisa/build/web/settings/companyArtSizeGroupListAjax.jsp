<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>


<script type="text/javascript">

    function onloadFunction(){
        // DataTable
        $('#example').DataTable({
            "aoColumnDefs": [{'bSortable': false, 'aTargets': [0]}],
            "order": [[ 1, "asc" ]]
        });
    }

    $(document).ready(function($) {
        onloadFunction();
    });

    function companyArtSizeGroupDelete(comId, sizeCode){
    
        var r = confirm("Do you want to delete?");

        if (r == true) {
            
            var dataString = 'companySizeCode='+sizeCode+'&comCode='+comId;

            document.getElementById('userDetailsViewImage').src='images/ajax-loader2.gif';

            $.ajax({
                type: "POST",
                url:'CompanyArtSizeGroupDelete',
                data: dataString,
                success: function(html){
                    alert(html);
                    document.getElementById('userDetailsViewImage').src='';
                    window.location.reload(true);
                }
            });
        }
    }



</script>


<s:if test="companyArticleSizeGroupInfoList != null">

    <s:if test="companyArticleSizeGroupInfoList.size()!=0">

        <form id="formId" action="#" method="post">

            <table id="example"  style="width: 100%;">

                <thead>
                    <tr>
                        <th style="width: 100px;">Com</th>
                        <th style="width: 50px;">Size</th>
                        <th style="width: 100px;">Group</th>
                        <th style="width: 70px;">S1</th>
                        <th style="width: 70px;">S2</th>
                        <th style="width: 70px;">S3</th>
                        <th style="width: 70px;">S4</th>
                        <th style="width: 70px;">S5</th>
                        <th style="width: 70px;">S6</th>
                        <th style="width: 70px;">S7</th>
                        <th style="width: 70px;">S8</th>
                        <th style="width: 70px;">S9</th>
                        <th style="width: 70px;">S10</th>
                        <th style="width: 70px;">S11</th>
                        <th style="width: 70px;">S12</th>
                        <th style="width: 70px;">S13</th>
                        <th style="width: 70px;">S14</th>
                        <th style="width: 70px;">S15</th>
                        <th style="width: 10px;"><img id="userDetailsViewImage" src="" alt="" /></th>
                    </tr>
                </thead>

                <tbody>

                    <s:iterator value="companyArticleSizeGroupInfoList">

                        <tr>
                            <td align="center"><s:property value="companyCode"/></td>
                            <td align="center"><s:property value="companySizeCode"/></td>
                            <td align="center"><s:property value="companySizeGroup"/></td>
                            <td align="center"><s:property value="comArtSizeR1"/></td>
                            <td align="center"><s:property value="comArtSizeR2"/></td>
                            <td align="center"><s:property value="comArtSizeR3"/></td>
                            <td align="center"><s:property value="comArtSizeR4"/></td>
                            <td align="center"><s:property value="comArtSizeR5"/></td>
                            <td align="center"><s:property value="comArtSizeR6"/></td>
                            <td align="center"><s:property value="comArtSizeR7"/></td>
                            <td align="center"><s:property value="comArtSizeR8"/></td>
                            <td align="center"><s:property value="comArtSizeR9"/></td>
                            <td align="center"><s:property value="comArtSizeR10"/></td>
                            <td align="center"><s:property value="comArtSizeR11"/></td>
                            <td align="center"><s:property value="comArtSizeR12"/></td>
                            <td align="center"><s:property value="comArtSizeR13"/></td>
                            <td align="center"><s:property value="comArtSizeR14"/></td>
                            <td align="center"><s:property value="comArtSizeR15"/></td>
                            <td align="center">
                                <a href="javascript:void(0)" onclick="companyArtSizeGroupDelete('<s:property value="companyCode"/>','<s:property value="companySizeCode"/>')" style="color: #0000FF;text-decoration: none;" >
                                    Del
                                </a>
                            </td>
                        </tr>
                    </s:iterator>

                </tbody>

            </table>

        </form>

    </s:if>
</s:if>