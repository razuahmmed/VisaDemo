<%--
    Document   : invoice
    Created on : Jun 20, 2013, 1:09:47 PM
    Author     : MIS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sx" uri="/struts-dojo-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/common.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/style.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/navi.css" />
        <script type="text/javascript"   src="<%= request.getContextPath()%>/resources/js/jquery-1.8.3.js"></script>
        <script type="text/javascript"  src="<%= request.getContextPath()%>/resources/js/jquery-ui.js"></script>
        <link rel="stylesheet" href="<%= request.getContextPath()%>/resources/css/jquery-ui.css" />
        <link rel="shortcut icon" href="<%= request.getContextPath()%>/resources/ico/favicon.png">

        <link rel="stylesheet" href="<%= request.getContextPath()%>/resources/css/bootstrap-3.1.1.min.css" type="text/css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/resources/css/bootstrap-multiselect.css" type="text/css" />
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/bootstrap-2.3.2.min.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/bootstrap-multiselect.js"></script>

        <link type="text/css" href="<%= request.getContextPath()%>/facebox/facebox.css" media="screen" rel="stylesheet" />
        <script type="text/javascript" src="<%= request.getContextPath()%>/facebox/facebox.js"></script>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/jquery.dataTables.css">
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery.dataTables.js"></script>


        <%
                    String hasSuccessful = request.getSession().getAttribute("hasSuccessful") != null ? request.getSession().getAttribute("hasSuccessful").toString().trim() : null;
                    String message = request.getSession().getAttribute("message") != null ? request.getSession().getAttribute("message").toString().trim() : null;
                    if (hasSuccessful != null) {
                        request.getSession().removeAttribute("hasSuccessful");
                        request.getSession().removeAttribute("message");
                    }
        %>
        <script type="text/javascript">

            $(function() {

            <% if (hasSuccessful != null && (hasSuccessful.equalsIgnoreCase("true") || hasSuccessful.equalsIgnoreCase("false"))) {%>
                    $('#dialog').html('<p><%out.print(message);%></p>');
                    $('#dialog').dialog({
                        title:"Result",
                        bgiframe: true,
                        height: 'auto',
                        width: 'auto',
                        modal: true,autoOpen: true,
                        buttons: { "Ok": function() {
                                $(this).dialog("close");}}
                    });

            <% }%>

                });
        </script>
        <sx:head/>
        <s:head/>

        <script type="text/javascript">

            function onloadFunction(){
                // DataTable
                $('#example').DataTable({
                    "aoColumnDefs": [{'bSortable': false, 'aTargets': [0]}],
                    "order": [[ 3, "desc" ]]
                });
            }


            $(document).ready(function($) {

                onloadFunction();


                $("#newUserId").keydown(function (e) {
                    // Allow: backspace, tab, enter, delete
                    if (($.inArray(e.keyCode, [8, 9, 13, 46]) !== -1 ||
                        // Allow: left, right
                    (e.keyCode >= 37 && e.keyCode <= 39))||
                        (e.keyCode >= 65 && e.keyCode <= 90))
                    {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if (((e.keyCode < 48 || e.keyCode > 57))
                        && (e.keyCode < 96 || e.keyCode > 105)
                        && (e.keyCode < 110 || e.keyCode > 112)
                        && (e.keyCode < 190 || e.keyCode > 192)
                ){
                        e.preventDefault();
                    }
                });

            });

            function checkValidateEmail() {

                var email=document.getElementById("newUserEmail").value;

                if(email.trim().length > 0){
                    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                   
                    if(!expr.test(email)){
                        alert("Invalid email address.");
                        document.getElementById("newUserEmail").value='';
                    }
                
                }
            };

            function checkDuplicateUser(){

                var newUserId=document.getElementById("newUserId").value;

                if(newUserId.trim().length > 0){

                    document.getElementById('createUserImageId').src='images/ajax-loader2.gif';

                    $.ajax({
                        url: "CheckDuplicateUserId.action?newUserId="+newUserId,
                        cache: false,
                        type: "POST"
                    }).done(function( html ) {                        
                        if(html.trim()=='1'){
                            document.getElementById("duplicateUserId").value='1';
                            alert("User already exist!");
                            document.getElementById('createUserImageId').src='images/deleteImg.jpeg';
                        }else{
                            document.getElementById("duplicateUserId").value='0';
                            document.getElementById('createUserImageId').src='images/okImg.jpeg';
                        }                        
                    });
                }
            }

            function createNewUser(){

                // $('#companyList').multiselect();

                

                var r = confirm("Do you want to create new user?");

                if (r == true) {
                    var duplicateUserId= document.getElementById("duplicateUserId").value;

                    if(duplicateUserId=='0'){

                        var newUserId=document.getElementById("newUserId").value;
                        var newUserPassword=document.getElementById("newUserPassword").value;
                        var newUserName=document.getElementById("newUserName").value;
                        var userEmail=document.getElementById("newUserEmail").value;
                        var userDescription=document.getElementById("userDescription").value;
                        // var comCode= $("#companyList" ).val();
                        var comCode="";
                        var groupCode= $("#groupList" ).val();
                        var userStatus= $("#statusList" ).val();

                        var fld = document.getElementById('companyList');


                        var flag=true;

                        if(newUserName.trim().length == 0){
                            flag=false;
                            alert('User name not found!');
                        }

                        if(flag){
                            if(newUserPassword.trim().length == 0){
                                flag=false;
                                alert('User password not found!');
                            }
                        }

                        if(flag){

                            // var values = [];
                            for (var i = 1; i < fld.options.length; i++) {
                                if (fld.options[i].selected) {
                                    // alert(fld.options[i].value);
                                    // values.push(fld.options[i].value);
                                    comCode += fld.options[i].value+"@";
                                }
                            }

                            if(comCode == ""){
                                flag=false;
                                alert('Company not found!');
                            }
                        }

                        if(flag){
                            if(groupCode == '-1'){
                                flag=false;
                                alert('Group not found!');
                            }
                        }
                        if(flag){

                            // alert(newUserId+' '+newUserName+' '+userEmail+' '+userDescription+' '+comCode+' '+groupCode+' '+userStatus);

                            var param='newUserId='+newUserId;
                            param+='&newUserPassword='+newUserPassword;
                            param+='&newUserName='+newUserName;
                            param+='&userEmail='+userEmail;
                            param+='&userDescription='+userDescription;
                            param+='&comCode='+comCode;
                            param+='&groupCode='+groupCode;
                            param+='&userStatus='+userStatus;
                            
                            document.getElementById('createUserImage').src='images/ajax-loader2.gif';

                            $.ajax({
                                url: "CreateNewUser.action?"+param,
                                cache: false,
                                type: "POST"
                            }).done(function( html ) {                        
                                alert(html);
                                document.getElementById('createUserImage').src='';
                                window.location.reload(true);
                            });
                        }
                        
                    }else{
                        alert("Duplicate user found!");
                    }
                
                }
            }
       
            function userDetailsView(userId){

                var dataString = 'newUserId='+userId;

                document.getElementById('userDetailsViewImage').src='images/ajax-loader2.gif';

                $.ajax({
                    type: "POST",
                    url:'UserDetailsView',
                    data: dataString,
                    success: function(data){
                        //alert(data);
                        document.getElementById('userDetailsViewImage').src='';
                        jQuery.facebox(data);
                        
                    }
                });

            }
           
            function userDelete(userId){

                var r = confirm("Do you want to delete?");

                if (r == true) {

                    var param='newUserId='+userId;

                    document.getElementById('userDetailsViewImage').src='images/ajax-loader2.gif';

                    $.ajax({
                        url: "DeleteUserDetails.action?"+param,
                        cache: false,
                        type: "POST"
                    }).done(function( html ) {
                        alert(html);
                        document.getElementById('userDetailsViewImage').src='';
                        window.location.reload(true);
                    });                
                }
            }

            $(function() {
                $('#companyList').multiselect({
                    includeSelectAllOption: true
                });
            });
        
        </script>


    </head>
    <body>
        <%
                    boolean login = (request.getSession().getAttribute("logged-in") != null && request.getSession().getAttribute("logged-in").toString().trim().equals("true")) ? true : false;

                    if (!login) {%>
        <jsp:forward page="/index.jsp"></jsp:forward>
        <%                    } else {
        %>

        <div class="wrap">

            <s:include value="/menu.jsp"></s:include>

            <div id="content">

                <div id="sidebar">
                    <div class="box">
                        <s:include value="/submenu/settings.jsp"></s:include>
                    </div>
                </div>

                <div id="main">

                    <div class="full_w">
                        <div class="h_title"><b>User Information</b></div>
                        <form id="formId" action="#" method="post">
                            <table width="100%">
                                <tr>                                    
                                    <td style="width: 15%; text-align: right">User Id:</td>
                                    <td style="width: 30%; text-align: left">
                                        <input type="hidden" id="duplicateUserId" name="duplicateUserId" value="" >
                                        <input onchange="checkDuplicateUser();" type="text" id="newUserId" name="newUserId" size="20" maxlength="10" >
                                        <label style="color: red;">*</label>
                                        <img id="createUserImageId" src="" alt="" />
                                    </td>
                                    <td style="width: 10%; text-align: right">Company:</td>
                                    <td style="width: 45%; text-align: left">
                                        <select id="companyList" multiple="multiple">
                                            <s:if test="bsoCompanyInfoList != null">
                                                <s:if test="bsoCompanyInfoList.size()!=0">
                                                    <s:iterator value="bsoCompanyInfoList">
                                                        <option value="<s:property value="companyCode"/>"><s:property value="companyName"/></option>
                                                    </s:iterator>
                                                </s:if>
                                            </s:if>
                                        </select>
                                        <label style="color: red;">*</label>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: right">Password:</td>
                                    <td style="text-align: left">
                                        <input type="password" id="newUserPassword" name="newUserPassword" size="25" maxlength="20" >
                                        <label style="color: red;">*</label>
                                    </td>
                                    <td style="text-align: right">User Group:</td>
                                    <td style="text-align: left">
                                        <select id="groupList" name="groupList" >
                                            <option value="-1">Select Group</option>
                                            <s:if test="bsoUserGroupInfoList != null">
                                                <s:if test="bsoUserGroupInfoList.size()!=0">
                                                    <s:iterator value="bsoUserGroupInfoList">
                                                        <option value="<s:property value="groupCode"/>"><s:property value="groupName"/></option>
                                                    </s:iterator>
                                                </s:if>
                                            </s:if>                                           
                                        </select>
                                         <label style="color: red;">*</label>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: right">User Name:</td>
                                    <td style="text-align: left">
                                        <input type="text" id="newUserName" name="newUserName" size="25" maxlength="20" >
                                        <label style="color: red;">*</label>
                                    </td>
                                    <td style="text-align: right">Status:</td>
                                    <td style="text-align: left">
                                        <select id="statusList" name="statusList" >
                                            <option value="Y">Active</option>
                                            <option value="N">Inactive</option>
                                        </select>
                                    </td>
                                </tr>

                                <tr>

                                    <td style="text-align: right">Email:</td>
                                    <td style="text-align: left">
                                        <input onchange="checkValidateEmail();" type="text" id="newUserEmail" name="newUserEmail" size="25" maxlength="100" >
                                    </td>
                                    <td style="text-align: right">Description:</td>
                                    <td colspan="3" style="text-align: left">
                                        <input type="text" id="userDescription" name="userDescription" size="50" maxlength="100" >
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="4" style="text-align: center">
                                        <input onclick="createNewUser();" type="button" id="saveButton" name="saveButton" value="Create" >
                                        <img id="createUserImage" src="" alt="" />
                                    </td>
                                </tr>

                            </table>
                        </form>

                        <div id="bsoUserDivId">

                            <form id="formId" action="#" method="post">

                                <table id="example"  style="width: 100%;">

                                    <thead>
                                        <tr>                                          
                                            <th style="width: 100px;">User Id</th>
                                            <th style="width: 150px;">User Name</th>
                                            <th style="width: 150px;">Group</th>
                                            <th style="width: 150px;">Email</th>
                                            <th style="width: 50px;">Status</th>
                                            <th style="width: 100px;"><img id="userDetailsViewImage" src="" alt="" /></th>
                                        </tr>
                                    </thead>

                                    <s:if test="bsoUserInfoList != null">

                                        <s:if test="bsoUserInfoList.size()!=0">

                                            <tbody>

                                                <s:iterator value="bsoUserInfoList">

                                                    <tr>
                                                        <td align="left"><s:property value="userId"/></td>
                                                        <td align="left"><s:property value="userName"/></td>
                                                        <td align="left"><s:property value="groupCodeInfo.groupName"/></td>
                                                        <td align="left"><s:property value="emailAddress"/></td>
                                                        <td align="left">
                                                            <s:if test="userStatus=='Y'">Active</s:if>
                                                            <s:else>Inactive</s:else>
                                                        </td>
                                                        <td align="center">
                                                            <a href="javascript:void(0)" onclick="userDetailsView('<s:property value="userId"/>')" style="color: #0000FF;text-decoration: none;" >
                                                                Edit
                                                            </a>
                                                            &nbsp;
                                                            <a href="javascript:void(0)" onclick="userDelete('<s:property value="userId"/>')" style="color: #0000FF;text-decoration: none;" >
                                                                Delete
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </s:iterator>
                                            </tbody>
                                        </s:if>
                                    </s:if>

                                </table>

                            </form>
                        </div>

                    </div>

                </div>
                <div class="clear"></div>

            </div>
            <s:include value="/footer.jsp"/>
        </div>

        <% }%>
    </body>
</html>
