<%--
    Document   : invoice
    Created on : Jun 20, 2013, 1:09:47 PM
    Author     : MIS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sx" uri="/struts-dojo-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/common.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/style.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/navi.css" />
        <script type="text/javascript"   src="<%= request.getContextPath()%>/resources/js/jquery-1.8.3.js"></script>
        <script type="text/javascript"  src="<%= request.getContextPath()%>/resources/js/jquery-ui.js"></script>
        <link rel="stylesheet" href="<%= request.getContextPath()%>/resources/css/jquery-ui.css" />
        <link rel="shortcut icon" href="<%= request.getContextPath()%>/resources/ico/favicon.png">

        <link type="text/css" href="<%= request.getContextPath()%>/facebox/facebox.css" media="screen" rel="stylesheet" />
        <script type="text/javascript" src="<%= request.getContextPath()%>/facebox/facebox.js"></script>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/jquery.dataTables.css">
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery.dataTables.js"></script>

        <sx:head/>
        <s:head/>

        <script type="text/javascript">
            
            $(document).ready(function() {
                onloadFunction();
            });

            function onloadFunction(){
                // DataTable
                $('#example').DataTable({
                    "pageLength": 50,
                    "order": [[ 0, "asc" ], [ 1, "asc" ]]
                });
            }

            function userDetailsView(userId){

                document.getElementById('userCodeImage').src='images/ajax-loader2.gif';

                $.ajax({
                    url: "userDetailsViewUSESET.bata?userId="+userId,
                    cache: false,
                    type: "POST"
                }).done(function( html ) {
                    document.getElementById('userCodeImage').src='';
                    jQuery.facebox(html);
                });
               
            }

            function userStoreView(userId){

                document.getElementById('userCodeImage').src='images/ajax-loader2.gif';

                $.ajax({
                    url: "userVsStoreViewUSESET.bata?userId="+userId,
                    cache: false,
                    type: "POST"
                }).done(function(html) {
                    document.getElementById('userCodeImage').src='';
                    $("#dialog").dialog({                       
                        modal: true,
                        title: "Store Information",     
                        height: 500,
                        width: 600,
                        position: 'center',
                        open: function () {
                            $(".ui-dialog-titlebar-close").hide();
                            $(this).html(html).show();
                        },
                        buttons: {
                            Update: function () {
                                updateUserVsStore();                                
                            },
                            Close: function () {
                                $(this).dialog("close");
                            }
                            
                        }
                    });       
                });

            }

            function updateUserVsStore(){

                var r = confirm("Do you want to update?");

                if (r == true) {

                    var userId=document.getElementById('userId').value;

                    var selected = new Array();
                    $('input:checkbox').each(function() {
                        selected.push($(this).attr('id'));
                    });

                    var stng="";
                    var str="";
                    var code = "";
                    var strCode="";

                    for(var i=1;i<selected.length;i++){

                        if(document.getElementById(selected[i]).checked){
                            str = document.getElementById('T'+selected[i]).value;
                            code = str.substring(0, 3);
                            strCode =str.substring(3);
                            // alert(code);
                            if(code=="STR"){
                                stng+=strCode+'/fd/Y/rd/'
                            }
                        }
                    }

                    //alert(stng);

                    if(stng.length!=0){

                        document.getElementById('userCodeImage3').src='images/ajax-loader2.gif';

                        $.ajax({
                            url: "userAssignedStoreSavedUSESET.bata?userId="+userId+"&userStoreString="+stng,
                            cache: false,
                            type: "POST"
                        }).done(function(html) {
                            alert(html);
                            document.getElementById('userCodeImage3').src='';
                            window.location.reload(true);
                        });
                    }

                }

            }


        </script>


    </head>
    <body>
        <%
                    boolean login = (request.getSession().getAttribute("logged-in") != null && request.getSession().getAttribute("logged-in").toString().trim().equals("true")) ? true : false;

                    if (!login) {%>
        <jsp:forward page="/index.jsp"></jsp:forward>
        <%                    } else {
        %>

        <div class="wrap">

            <s:include value="/menu.jsp"></s:include>

            <div id="content">

                <div id="sidebar">
                    <div class="box">
                        <s:include value="/submenu/user.jsp"></s:include>
                    </div>
                </div>

                <div id="main">

                    <div class="full_w">

                        <div class="h_title"><b>All User</b></div>

                        <form id="formId" action="#" method="post">

                            <table id="example"  style="width: 100%;">

                                <thead>
                                    <tr style="background-color: #336ea5; font-size: 12px; color: #ffffff" >
                                        <th title="User Group">User Group</th>
                                        <th title="User Name">User Name</th>
                                        <th title="Display Name">Display Name</th>
                                        <th title="User Email">Email</th>
                                        <th title="User Contact">Contact</th>
                                        <th title="Number of Store Assigned">Report Store</th>
                                        <th title="User Status">Status</th>                                        
                                        <th><div id="dialog"></div></th>
                                        <th><img id="userCodeImage" src="" alt="" /></th>
                                    </tr>
                                </thead>

                                <s:if test="rtlUserList != null">

                                    <s:if test="rtlUserList.size()!=0">

                                        <tbody>
                                            <s:iterator value="rtlUserList">
                                                <tr>
                                                    <td><s:property value="rtlUserLevel.levelName"/></td>
                                                    <td><s:property value="userName"/></td>
                                                    <td><s:property value="displayName"/></td>
                                                    <td><s:property value="userEmail"/></td>
                                                    <td><s:property value="userContct"/></td>
                                                    <td style="text-align: center;">
                                                        <s:if test="totalAssignStore > 0"><s:property value="totalAssignStore"/></s:if>
                                                    </td>                                                    
                                                    <td>
                                                        <s:if test="status == 'Y'">Active</s:if>
                                                        <s:else>Inactive</s:else>
                                                    </td>
                                                    <td align="center">
                                                        <a href="javascript:void(0)" onclick="userDetailsView('<s:property value="userId"/>')" style="color: #0000FF;text-decoration: none;" >
                                                            <img alt=""  src="<%= request.getContextPath()%>/images/ViewDetails.jpg" title="User View" style="width:25px; height:25px;">
                                                        </a>
                                                    </td>
                                                    <td align="center">
                                                        <a href="javascript:void(0)" onclick="userStoreView('<s:property value="userId"/>')" style="color: #0000FF;text-decoration: none;" >
                                                            <img alt=""  src="<%= request.getContextPath()%>/images/handshake.png" title="User Vs Store Mapping" style="width:25px; height:25px;">
                                                        </a>
                                                    </td>
                                                </tr>
                                            </s:iterator>
                                        </tbody>

                                    </s:if>

                                </s:if>

                            </table>

                        </form>



                    </div>

                </div>
                <div class="clear"></div>

            </div>
            <s:include value="/footer.jsp"/>
        </div>

        <% }%>
    </body>
</html>
