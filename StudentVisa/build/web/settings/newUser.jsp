<%--
    Document   : invoice
    Created on : Jun 20, 2013, 1:09:47 PM
    Author     : MIS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sx" uri="/struts-dojo-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/common.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/style.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/navi.css" />
        <script type="text/javascript"   src="<%= request.getContextPath()%>/resources/js/jquery-1.8.3.js"></script>
        <script type="text/javascript"  src="<%= request.getContextPath()%>/resources/js/jquery-ui.js"></script>
        <link rel="stylesheet" href="<%= request.getContextPath()%>/resources/css/jquery-ui.css" />
        <link rel="shortcut icon" href="<%= request.getContextPath()%>/resources/ico/favicon.png">

        <link type="text/css" href="<%= request.getContextPath()%>/facebox/facebox.css" media="screen" rel="stylesheet" />
        <script type="text/javascript" src="<%= request.getContextPath()%>/facebox/facebox.js"></script>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/jquery.dataTables.css">
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery.dataTables.js"></script>


        <%
                    String hasSuccessful = request.getSession().getAttribute("hasSuccessful") != null ? request.getSession().getAttribute("hasSuccessful").toString().trim() : null;
                    String message = request.getSession().getAttribute("message") != null ? request.getSession().getAttribute("message").toString().trim() : null;
                    if (hasSuccessful != null) {
                        request.getSession().removeAttribute("hasSuccessful");
                        request.getSession().removeAttribute("message");
                    }
        %>
        <script type="text/javascript">

            $(function() {

            <% if (hasSuccessful != null && (hasSuccessful.equalsIgnoreCase("true") || hasSuccessful.equalsIgnoreCase("false"))) {%>
                    $('#dialog').html('<p><%out.print(message);%></p>');
                    $('#dialog').dialog({
                        title:"Result",
                        bgiframe: true,
                        height: 'auto',
                        width: 'auto',
                        modal: true,autoOpen: true,
                        buttons: { "Ok": function() {
                                $(this).dialog("close");}}
                    });

            <% }%>

                });
        </script>
        <sx:head/>
        <s:head/>

        <script type="text/javascript">

            $(function () {
                $("#newUserPassword").bind("keyup", function () {
                    //TextBox left blank.
                    if ($(this).val().length == 0) {
                        $("#password_strength").html("");
                        return;
                    }

                    //Regular Expressions.
                    var regex = new Array();
                    regex.push("[A-Z]"); //Uppercase Alphabet.
                    regex.push("[a-z]"); //Lowercase Alphabet.
                    regex.push("[0-9]"); //Digit.
                    regex.push("[$@$!%*#?&]"); //Special Character.

                    var passed = 0;

                    //Validate for each Regular Expression.
                    for (var i = 0; i < regex.length; i++) {
                        if (new RegExp(regex[i]).test($(this).val())) {
                            passed++;
                        }
                    }


                    //Validate for length of Password.
                    if (passed > 2 && $(this).val().length > 8) {
                        passed++;
                    }

                    //Display status.
                    var color = "";
                    var strength = "";
                    switch (passed) {
                        case 0:
                        case 1:
                            strength = "Weak";
                            color = "red";
                            break;
                        case 2:
                            strength = "Good";
                            color = "darkorange";
                            break;
                        case 3:
                        case 4:
                            strength = "Strong";
                            color = "green";
                            break;
                        case 5:
                            strength = "Very Strong";
                            color = "darkgreen";
                            break;
                    }
                    $("#password_strength").html(strength);
                    $("#password_strength").css("color", color);
                });
            });

            function checkValidateEmail() {

                var email=document.getElementById("newUserEmail").value;

                if(email.trim().length > 0){
                    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                   
                    if(!expr.test(email)){
                        alert("Invalid email address.");
                        document.getElementById("newUserEmail").value='';
                    }
                
                }
            };

            function checkDuplicateUser(){

                var newUserName=document.getElementById("newUserName").value;

                if(newUserName.trim().length > 0){
                    // alert(newComCode);
                    document.getElementById('newUserImage').src='images/ajax-loader2.gif';

                    $.ajax({
                        url: "checkDuplicateUserNameUSESET.bata?userName="+newUserName,
                        cache: false,
                        type: "POST"
                    }).done(function( html ) {                        
                        if(html.trim()=='1'){
                            document.getElementById("duplicateUser").value='1';
                            alert("User already exist!");
                            document.getElementById('newUserImage').src='images/deleteImg.jpeg';
                        }else{
                            document.getElementById("duplicateUser").value='0';
                            document.getElementById('newUserImage').src='images/okImg.jpeg';
                        }                        
                    });
                }else{
                    document.getElementById("duplicateUser").value='-1';
                    document.getElementById('newUserImage').src='';
                }
            }
           
            function createNewUser(){

                var r = confirm("Do you want to create?");

                if (r == true) {

                    var duplicateUser=document.getElementById("duplicateUser").value;
                    //
                    if(duplicateUser=="1"){
                        alert("User already exist!");
                    }else if(duplicateUser=="-1"){
                        alert("User code not found.");
                    }else if(duplicateUser=="0"){

                        var newUserName=document.getElementById("newUserName").value;
                        var newUserPassword=document.getElementById("newUserPassword").value;
                        var newUserDisplayName=document.getElementById("newUserDisplayName").value;
                        var newUserEmail=document.getElementById("newUserEmail").value;
                        var newUserContact=document.getElementById("newUserContact").value;
                        var newUserDescription=document.getElementById("newUserDescription").value;
                        var newUserGroup= $("#userGroupList" ).val();
                        var newUserStatus= $("#statusList" ).val();

                        var flag=true;

                        if(newUserName.trim().length == 0){
                            flag=false;
                            alert('User name not found!');
                        }

                        if(flag){
                            if(newUserPassword.trim().length == 0){
                                flag=false;
                                alert('Password not found!');
                            }
                        }

                        if(flag){
                            if(newUserGroup.trim().length == 0){
                                flag=false;
                                alert('Group not found!');
                            }
                        }

                        if(flag){

                            var param='userName='+newUserName;
                            param+='&userPassword='+newUserPassword;
                            param+='&userDisplayName='+newUserDisplayName;
                            param+='&userEmail='+newUserEmail;
                            param+='&userContact='+newUserContact;
                            param+='&userDescription='+newUserDescription;
                            param+='&levelId='+newUserGroup;
                            param+='&userActiveStatus='+newUserStatus;

                            document.getElementById('createUserImage').src='images/ajax-loader2.gif';

                            $.ajax({
                                url: "createNewUserUSESET.bata?"+param,
                                cache: false,
                                type: "POST"
                            }).done(function( html ) {
                                alert(html);
                                document.getElementById('createUserImage').src='';
                                window.location.reload(true);
                            });
                        }

                    }

                }
            }
        
        </script>


    </head>
    <body>
        <%
                    boolean login = (request.getSession().getAttribute("logged-in") != null && request.getSession().getAttribute("logged-in").toString().trim().equals("true")) ? true : false;

                    if (!login) {%>
        <jsp:forward page="/index.jsp"></jsp:forward>
        <%                    } else {
        %>

        <div class="wrap">

            <s:include value="/menu.jsp"></s:include>

            <div id="content">

                <div id="sidebar">
                    <div class="box">
                        <s:include value="/submenu/user.jsp"></s:include>
                    </div>
                </div>

                <div id="main">

                    <div class="full_w">
                        <div class="h_title"><b>New User</b></div>
                        <form id="formId" action="#" method="post">

                            <table width="80%">

                                <tr>                                    
                                    <td style="width: 30%; text-align: right; white-space: nowrap ;"><label style="color: red;">*</label>User Name</td>
                                    <td style="width: 50%; text-align: left;">
                                        <input type="hidden" id="duplicateUser" name="duplicateUser" value="-1" >
                                        <input onchange="checkDuplicateUser();" type="text" id="newUserName" name="newUserName" size="35" maxlength="50" >
                                        <img id="newUserImage" src="" alt="" />
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: right;"><label style="color: red;">*</label>Password:</td>
                                    <td style="text-align: left;">
                                        <input type="password" id="newUserPassword" name="newUserPassword" size="35" maxlength="15" >
                                        <label id="password_strength">&nbsp;</label>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: right; white-space: nowrap;"><label style="color: red;">*</label>Display Name:</td>
                                    <td style="text-align: left;">
                                        <input type="text" id="newUserDisplayName" name="newUserDisplayName" size="60" maxlength="250" >
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: right">User Email:</td>
                                    <td style="text-align: left">
                                        <input onchange="checkValidateEmail();" type="text" id="newUserEmail" name="newUserEmail" size="60" maxlength="100" >
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: right">Contact Number:</td>
                                    <td style="text-align: left">
                                        <input type="text" id="newUserContact" name="newUserContact" size="60" maxlength="100" >
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: right;">Description:</td>
                                    <td style="text-align: left;">
                                        <input type="text" id="newUserDescription" name="newUserDescription" size="60" maxlength="250" >
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right"><label style="color: red;">*</label>Group:</td>
                                    <td style="text-align: left">
                                        <select id="userGroupList" name="userGroupList" >
                                            <option  value="-1">Select Group</option>
                                            <s:if test="rtlUserLevelList != null">
                                                <s:if test="rtlUserLevelList.size()!=0">
                                                    <s:iterator value="rtlUserLevelList">
                                                        <option value="<s:property value="levelId"/>"><s:property value="levelName"/></option>
                                                    </s:iterator>
                                                </s:if>
                                            </s:if>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: right">Status:</td>
                                    <td style="text-align: left">
                                        <select id="statusList" name="statusList" >
                                            <option value="Y">Active</option>
                                            <option value="N">Inactive</option>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="4" style="text-align: center">
                                        <input onclick="createNewUser();" type="button" id="saveButton" name="saveButton" value="Create" >
                                        <img id="createUserImage" src="" alt="" />
                                    </td>
                                </tr>

                            </table>
                        </form>

                    </div>

                </div>
                <div class="clear"></div>

            </div>
            <s:include value="/footer.jsp"/>
        </div>

        <% }%>
    </body>
</html>
