<%--
    Document   : invoice
    Created on : Jun 20, 2013, 1:09:47 PM
    Author     : MIS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sx" uri="/struts-dojo-tags" %>

<style type="text/css">
    <!--
    .style1 {color: #FF0000}

    .divTitle {
        background: #336EA5;
        height: 22px;
        text-align: center;
        padding-top: 5px;
        font-size: 12px;
        font-weight: bold;
    }

    .divTextField{
        height: 20px;
        padding: 2px;

    }

    .divLevel{
        color: #000000;
    }

    -->
</style>


<script type="text/javascript">
    
    function checkValidateEmail() {

        var email=document.getElementById("newUserEmailFb").value;

        if(email.trim().length > 0){
            var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                   
            if(!expr.test(email)){
                alert("Invalid email address.");
                document.getElementById("newUserEmail").value='';
            }
                
        }
    };
  
    function updateUser(){

        var r = confirm("Do you want to update?");

        if (r == true) {

            var newUserId=document.getElementById("newUserIdFb").value;
            var newUserPassword=document.getElementById("newUserPasswordFb").value;
            var newUserName=document.getElementById("newUserNameFb").value;
            var userEmail=document.getElementById("newUserEmailFb").value;
            var userDescription=document.getElementById("userDescriptionFb").value;
            // var comCode= $("#companyListFb" ).val();
            var comCode="";
            var groupCode= $("#groupListFb" ).val();
            var userStatus= $("#statusListFb" ).val();
            var fld = document.getElementById('companyListFb');

            var flag=true;

            if(newUserName.trim().length == 0){
                flag=false;
                alert('User name not found!');
            }

            if(flag){
                if(newUserPassword.trim().length == 0){
                    flag=false;
                    alert('User password not found!');
                }
            }

            if(flag){

                // var values = [];
                for (var i = 1; i < fld.options.length; i++) {
                    if (fld.options[i].selected) {
                        // alert(fld.options[i].value);
                        // values.push(fld.options[i].value);
                        comCode += fld.options[i].value+"@";
                    }
                }

                if(comCode == ""){
                    flag=false;
                    alert('Company not found!');
                }
            }

            if(flag){
                if(groupCode == '-1'){
                    flag=false;
                    alert('Group not found!');
                }
            }

            if(flag){
                
                var param='newUserId='+newUserId;
                param+='&newUserPassword='+newUserPassword;
                param+='&newUserName='+newUserName;
                param+='&userEmail='+userEmail;
                param+='&userDescription='+userDescription;
                param+='&comCode='+comCode;
                param+='&groupCode='+groupCode;
                param+='&userStatus='+userStatus;

                //alert(param);
                            
                document.getElementById('userDetailsViewImage2').src='images/ajax-loader2.gif';

                $.ajax({
                    url: "UpdateUserDetails.action?"+param,
                    cache: false,
                    type: "POST"
                }).done(function( html ) {
                    alert(html);
                    document.getElementById('userDetailsViewImage2').src='';
                    window.location.reload(true);
                });
            }
                        
           
                
        }
    }    

    $(function() {
        $('#companyListFb').multiselect({
            includeSelectAllOption: true
        });
    });

</script>


<div id="main">

    <div class="divTitle"><b>User Information</b></div>

    <s:if test="bsoUserInfoList != null">

        <s:if test="bsoUserInfoList.size()!=0">

            <s:iterator value="bsoUserInfoList">

                <table style="width: 500px;">
                    <tr class="divLevel">
                        <td style="width: 80px; text-align: right">User Id:</td>
                        <td style="width: 420px; text-align: left">
                            <input class="divTextField" type="text" id="newUserIdFb" name="newUserIdFb" size="25" maxlength="10" value="<s:property value="userId"/>" readonly >
                        </td>
                    </tr>
                    <tr>
                        <td class="divLevel" style="text-align: right">User Name:</td>
                        <td style="text-align: left">
                            <input class="divTextField" type="text" id="newUserNameFb" name="newUserNameFb" size="25" maxlength="20" value="<s:property value="userName"/>" >
                        </td>
                    </tr>

                    <tr>
                        <td class="divLevel" style="text-align: right">Company:</td>
                        <td style="text-align: left">

                            <select id="companyListFb" multiple="multiple">

                                <s:if test="bsoCompanyInfoList != null">

                                    <s:if test="bsoCompanyInfoList.size()!=0">

                                        <s:set var="tt" value="%{0}"/>

                                        <s:iterator value="bsoCompanyInfoList">
                                            <s:set var="tt" value="%{0}"/>

                                            <s:iterator value="bsoUserCompanyInfoList">
                                                <s:if test='companyCode==bsoCompanyInfo.companyCode'>
                                                    <option selected value="<s:property value="companyCode"/>"><s:property value="countryName"/>&nbsp; - &nbsp;(<s:property value="companyName"/>)</option>
                                                    <s:set var="tt" value="%{1}"/>
                                                </s:if>
                                            </s:iterator>
                                            <s:if test='#attr.tt==0'>
                                                <option value="<s:property value="companyCode"/>"><s:property value="countryName"/>&nbsp; - &nbsp;(<s:property value="companyName"/>)</option>
                                            </s:if>
                                        </s:iterator>
                                    </s:if>
                                </s:if>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td class="divLevel" style="text-align: right">User Group:</td>
                        <td style="text-align: left">
                            <select class="divTextField" id="groupListFb" name="groupListFb" >
                                <option value="-1">Group</option>
                                <s:if test="bsoUserGroupInfoList != null">
                                    <s:if test="bsoUserGroupInfoList.size()!=0">
                                        <s:iterator value="bsoUserGroupInfoList">
                                            <s:if test='groupCode==groupCodeInfo.groupCode'>
                                                <option selected value="<s:property value="groupCode"/>"><s:property value="groupName"/></option>
                                            </s:if>
                                            <s:else>
                                                <option value="<s:property value="groupCode"/>"><s:property value="groupName"/></option>
                                            </s:else>

                                        </s:iterator>
                                    </s:if>
                                </s:if>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td class="divLevel" style="text-align: right">Email:</td>
                        <td style="text-align: left">
                            <input class="divTextField" onchange="checkValidateEmail();" type="text" id="newUserEmailFb" name="newUserEmailFb" size="25" maxlength="100" value="<s:property value="emailAddress"/>" >
                        </td>
                    </tr>

                    <tr>
                        <td class="divLevel" style="text-align: right">Description:</td>
                        <td colspan="3" style="text-align: left">
                            <input class="divTextField" type="text" id="userDescriptionFb" name="userDescriptionFb" size="50" maxlength="100" value="<s:property value="description"/>" >
                        </td>
                    </tr>

                    <tr>
                        <td class="divLevel" style="text-align: right">Password:</td>
                        <td colspan="3" style="text-align: left">
                            <input class="divTextField" type="text" id="newUserPasswordFb" name="newUserPasswordFb" size="25" maxlength="20" value="<s:property value="userPassword"/>" >
                        </td>
                    </tr>

                    <tr>
                        <td class="divLevel" style="text-align: right">Status:</td>
                        <td style="text-align: left">
                            <select class="divTextField" id="statusListFb" name="statusListFb" >
                                <s:if test="userStatus=='Y'">
                                    <option value="Y" selected>Active</option>
                                    <option value="N">Inactive</option>
                                </s:if>
                                <s:else>
                                    <option value="Y">Active</option>
                                    <option value="N" selected>Inactive</option>
                                </s:else>

                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4" style="text-align: center">
                            <input onclick="updateUser();" type="button" id="updateButton" name="updateButton" value="Update" >
                            <img id="userDetailsViewImage2" src="" alt="" />
                        </td>
                    </tr>

                </table>

            </s:iterator>

        </s:if>

    </s:if>
</div>

