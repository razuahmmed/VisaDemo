
<%@page import="mis.drd.action.ParamUtil"%>
<!DOCTYPE html>
<%@taglib  prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html;" charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <title>FootIn</title>

        <link href="<%= request.getContextPath()%>/resources/css/style.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/bootstrap.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/yamm.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/style_menu.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/resources/css/jquery.dataTables.css" rel="stylesheet" />

    </head>

    <style type="text/css">
        <!--
        #example th {
            /* color: #E06C0A; 009999 */
            background: none repeat scroll 0 0 #E06C0A;
            color: #FFFFFF;
            font-size: 12px;
            height: 22px;
            vertical-align: middle;
            padding: 2px;
            text-align: center;
            border: 1px solid #FFFFFF;
        }

        #example td {
            vertical-align: middle;
        }

        -->
    </style>


    <body>

        <jsp:include page="/top_menu.jsp" flush="true"></jsp:include>

        <div id="content">

            <div id="sidebar">

                <div class="box">

                    <jsp:include page="/submenu/myaccount.jsp" flush="true"></jsp:include>

                </div>

            </div>

            <div id="middlecontent">

                <div class="div_title"><b>MY POINTS</b></div>

                <div id="my_point_div">

                    <table id="example" cellspacing="0" width="100%">

                        <thead>

                            <tr>
                                <th>Total Purchase</th>
                                <th>Total Point</th>
                                <th>Redeemed Point</th>
                                <th>Expired Point</th>
                                <th>Remain Point</th>
                            </tr>

                        </thead>

                        <tbody></tbody>

                    </table>

                </div>

            </div>

        </div>

        <jsp:include page="/footer.jsp" flush="true"></jsp:include>

        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery-1.8.3.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery-ui.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/bootstrap.min.js"></script>


        <script type="text/javascript">
            
        </script>

    </body>

</html>
