
<%@page import="mis.drd.action.ParamUtil"%>
<!DOCTYPE html>
<%@taglib  prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html;" charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <title>FootIn</title>

        <link href="<%= request.getContextPath()%>/resources/css/style.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/bootstrap.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/yamm.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/style_menu.css" rel="stylesheet" media="screen"/>

    </head>
    <body>

        <jsp:include page="top_menu.jsp" flush="true"></jsp:include>

        <div class="body">


            <!-- ==============================Carousel================================ -->

            <div id="myCarousel" class="carousel slide">

                <ol class="carousel-indicators">

                    <li data-target="#myCarousel" data-slide-to="0" class="Active"></li>

                    <li data-target="#myCarousel" data-slide-to="1" class=""></li>

                    <li data-target="#myCarousel" data-slide-to="2" class=""></li>

                    <li data-target="#myCarousel" data-slide-to="3" class=""></li>

                    <li data-target="#myCarousel" data-slide-to="4" class=""></li>

                </ol>

                <div class="carousel-inner img_res">

                    <div class="item active" >
                        <a href="#">
                            <img src="<%= request.getContextPath()%>/resources/images/banner1/12c7bcc5-b6d5-45d0-9f79-e500536bbe4c.jpg"  alt="banner2" />
                        </a>
                    </div>

                    <div class="item" >
                        <a href="#" >
                            <img src="<%= request.getContextPath()%>/resources/images/banner1/93fbdbd7-a232-4f4a-910e-7f787fa0276f.jpg"  alt="banner1"  />
                        </a>
                    </div>

                    <div class="item" >
                        <a href="#" >
                            <img src="<%= request.getContextPath()%>/resources/images/banner1/ffb00114-9d32-4d84-9750-e579b723e9d4.jpg"  alt="@footin_shoes"  />
                        </a>
                    </div>

                    <div class="item" >
                        <a href="#">
                            <img src="<%= request.getContextPath()%>/resources/images/banner1/4ea093dc-f2e6-44f6-93f0-871ead37cdce.jpg"  alt="Zalora"  />
                        </a>
                    </div>

                    <div class="item" >
                        <a href="#">
                            <img src="<%= request.getContextPath()%>/resources/images/banner1/fd5d8c9a-f8cc-4131-a96e-7c3297b5ad17.jpg"  alt="Lazada - Footin"  />
                        </a>
                    </div>

                </div>

                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="">
                        <img alt="" src="<%= request.getContextPath()%>/resources/images/bt_prev2.png" style="margin-top:140%;width:40%;height:auto;"/>
                    </span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="">
                        <img alt="" src="<%= request.getContextPath()%>/resources/images/bt_next2.png" style="margin-top:140%;width:40%;height:auto;"/>
                    </span>
                </a>

            </div>

            <!-- =============================End Carousel================================ -->


            <div class="row no-gutter img_res bottom_banner_ctrl">
                <div class="btb_center">
                    <div class="col-xs-6 bann01">
                        <a href="#" id="abanner2_1">
                            <img src="<%= request.getContextPath()%>/resources/images/banner2/304f7f6a-0194-431e-9516-95d05edd8198.jpg" id="imgbanner2_1" alt="Accessories" />
                        </a>
                    </div>
                    <div class="col-xs-6 bann02">
                        <a href="#" id="abanner2_2">
                            <img src="<%= request.getContextPath()%>/resources/images/banner2/3d4aea02-05eb-4e1d-a67c-fd58385d45e5.jpg" id="imgbanner2_2" alt="Ballerinas" />
                        </a>
                    </div>
                    <div class="col-xs-3 bann03">
                        <a href="#" id="abanner2_3">
                            <img src="<%= request.getContextPath()%>/resources/images/banner2/300a1a5b-ff38-44c2-beba-ac78e9310969.jpg" id="imgbanner2_3" alt="Heels" />
                        </a>
                    </div>
                    <div class="col-xs-3 bann04">
                        <a href="#" id="abanner2_4">
                            <img src="<%= request.getContextPath()%>/resources/images/banner2/f0f32f31-c31a-4159-a18a-65d08445a78f.jpg" id="imgbanner2_4" alt="Sneakers" />
                        </a>
                    </div>
                </div>
            </div>

            <div class="row no-gutter">
                <div class="col-xs-12">
                    <p class="just_in">JUST IN, Let?s take a look</p>
                </div>
            </div>

            <div class="row no-gutter img_res fix_product">
                <div class="hp_center">

                    <div class="col-sm-3 h_p_ctrl">
                        <a href="#">
                            <div class="imgz">
                                <img alt="" src="<%= request.getContextPath()%>/resources/images/product/thumb/f2b01644-d33b-4e36-a2a2-e010f0083f8b.jpg" />
                            </div>
                            <p class="product_name">Bonds</p>
                        </a>
                    </div>

                    <div class="col-sm-3 h_p_ctrl">
                        <a href="#">
                            <div class="imgz">
                                <img alt="" src="<%= request.getContextPath()%>/resources/images/product/thumb/802b8eeb-a927-428f-a091-f85e91862e8b.jpg" />
                            </div>
                            <p class="product_name">Military Handbag</p>
                        </a>
                    </div>

                    <div class="col-sm-3 h_p_ctrl">
                        <a href="#">
                            <div class="imgz">
                                <img alt="" src="<%= request.getContextPath()%>/resources/images/product/thumb/398f2e8d-7ff9-4b0a-a3ff-6ebe8f740717.jpg" />
                            </div>
                            <p class="product_name">Budda | Loafer</p>
                        </a>
                    </div>

                    <div class="col-sm-3 h_p_ctrl">
                        <a href="#">
                            <div class="imgz">
                                <img src="<%= request.getContextPath()%>/resources/images/product/thumb/3f6957d0-76af-46bd-8387-f124326ffe89.jpg" />
                            </div>
                            <p class="product_name">Ramone</p>
                        </a>
                    </div>

                    <div class="col-sm-3 h_p_ctrl">
                        <a href="#">
                            <div class="imgz">
                                <img alt="" src="<%= request.getContextPath()%>/resources/images/product/thumb/54e5336c-d388-460e-b3be-b6d85746903e.jpg" />
                            </div>
                            <p class="product_name">Spoiler Magnolia</p>
                        </a>
                    </div>

                    <div class="col-sm-3 h_p_ctrl">
                        <a href="productdetail4055.html?product_id=144">
                            <div class="imgz">
                                <img alt="" src="<%= request.getContextPath()%>/resources/images/product/thumb/cf0f5ad1-f22c-4f6c-9d55-02d434c16722.jpg" />
                            </div>
                            <p class="product_name">Spoiler Luxe</p>
                        </a>
                    </div>

                    <div class="col-sm-3 h_p_ctrl">
                        <a href="productdetail005a.html?product_id=141">
                            <div class="imgz">
                                <img alt="" src="<%= request.getContextPath()%>/resources/images/product/thumb/e21004b5-b06f-4ee8-83aa-ca3468fb89f8.jpg" />
                            </div>
                            <p class="product_name">Maison | Army</p>
                        </a>
                    </div>

                    <div class="col-sm-3 h_p_ctrl">
                        <a href="productdetail0263.html?product_id=140">
                            <div class="imgz">
                                <img alt="" src="<%= request.getContextPath()%>/resources/images/product/thumb/aaedfe38-e68c-4a5c-bbe8-fa1c30650803.jpg" />
                            </div>
                            <p class="product_name">The Legacy | Caesar</p>
                        </a>
                    </div>

                </div>
            </div>

            <!-- ====================== News & Promotions  Carousel ================================ -->
            <div class="g100">
                <div class="row no-gutter img_res fix_1160 bg-grey mg-top-bt">
                    <div class="col-xs-12">
                        <p class="h_NP">News & Promotions</p>
                    </div>

                    <div id="myCarousel2" class="carousel slide">

                        <ol class="carousel-indicators" style="bottom: -70px!important;">

                            <li data-target="#myCarousel2"  data-slide-to="0" class="active"></li>

                            <li data-target="#myCarousel2"  data-slide-to="1" class=""></li>

                            <li data-target="#myCarousel2"  data-slide-to="2" class=""></li>

                            <li data-target="#myCarousel2"  data-slide-to="3" class=""></li>

                            <li data-target="#myCarousel2"  data-slide-to="4" class=""></li>

                            <li data-target="#myCarousel2"  data-slide-to="5" class=""></li>

                        </ol>

                        <div class="carousel-inner img_res">

                            <div class="item active">
                                <div class="row no-gutter img_res fix_product">
                                    <div class="hnn_center">
                                        <div class="col-xs-4 h_n_ctrl">
                                            <a href="#">
                                                <div class="imgz_hn">
                                                    <img alt="" src="<%= request.getContextPath()%>/resources/images/news/thumb/fc0b2512-46a6-4bb6-9187-05815b6ff13a.jpg" />
                                                </div>
                                                <p class="news_head">The Best Fashion Trends at the Oscars</p>
                                                <p class="news_date">Feb 29, 2016</p>
                                            </a>
                                        </div>
                                        <div class="col-xs-4 h_n_ctrl"  >
                                            <a href="#">
                                                <div class="imgz_hn">
                                                    <img src="<%= request.getContextPath()%>/resources/images/news/thumb/fb0cdaea-295b-4c03-be52-a27f9763c3c3.jpg" />
                                                </div>
                                                <p class="news_head">Why Los Angeles Could Be the Next Fashion Capital</p>
                                                <p class="news_date">Feb 26, 2016</p>
                                            </a>
                                        </div>
                                        <div class="col-xs-4 h_n_ctrl"  >
                                            <a href="#">
                                                <div class="imgz_hn">
                                                    <img alt="" src="<%= request.getContextPath()%>/resources/images/news/thumb/537e52b8-a84e-4604-aad2-6103fa23142a.jpg" />
                                                </div>
                                                <p class="news_head">Hoodies Are Chic Now</p>
                                                <p class="news_date">Feb 16, 2016</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <div class="row no-gutter img_res fix_product">
                                    <div class="hnn_center">
                                        <div class="col-xs-4 h_n_ctrl">
                                            <a href="#">
                                                <div class="imgz_hn">
                                                    <img alt="" src="<%= request.getContextPath()%>/resources/images/news/thumb/f10488db-b948-468d-9733-1da9d3b5e0e9.jpg" />
                                                </div>
                                                <p class="news_head">Why Fall 2016 Will Be the Season of Bowie</p>
                                                <p class="news_date">Feb 11, 2016</p>
                                            </a>
                                        </div>
                                        <div class="col-xs-4 h_n_ctrl"  >
                                            <a href="#">
                                                <div class="imgz_hn">
                                                    <img alt="" src="<%= request.getContextPath()%>/resources/images/news/thumb/83e24305-289e-49e5-98cf-4a6b20186ab0.jpg" />
                                                </div>
                                                <p class="news_head">Zayn Malik - Pilowtalk</p>
                                                <p class="news_date">Feb 11, 2016</p>
                                            </a>
                                        </div>
                                        <div class="col-xs-4 h_n_ctrl"  >
                                            <a href="#">
                                                <div class="imgz_hn">
                                                    <img alt="" src="<%= request.getContextPath()%>/resources/images/news/thumb/d62fe6f6-e481-4241-bfdf-c92c21c98511.jpg" />
                                                </div>
                                                <p class="news_head">Japanese Souvenir Jackets Are Going to Be Huge This 2016</p>
                                                <p class="news_date">Feb 9, 2016</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <div class="row no-gutter img_res fix_product">
                                    <div class="hnn_center">
                                        <div class="col-xs-4 h_n_ctrl">
                                            <a href="#">
                                                <div class="imgz_hn">
                                                    <img alt="" src="<%= request.getContextPath()%>/resources/images/news/thumb/befa84cd-28c1-4f57-8ef9-74e459d933b5.jpg" />
                                                </div>
                                                <p class="news_head">Copenhagen Fashion Week FW16</p>
                                                <p class="news_date">Feb 9, 2016</p></a>
                                        </div>
                                        <div class="col-xs-4 h_n_ctrl"  >
                                            <a href="#">
                                                <div class="imgz_hn">
                                                    <img alt="" src="<%= request.getContextPath()%>/resources/images/news/thumb/6c688336-3b44-4841-81c2-58246634dc79.jpg" />
                                                </div>
                                                <p class="news_head">The Top Street Style Pics From Kiev Fashion Week</p>
                                                <p class="news_date">Feb 8, 2016</p>
                                            </a>
                                        </div>
                                        <div class="col-xs-4 h_n_ctrl"  >
                                            <a href="#">
                                                <div class="imgz_hn">
                                                    <img alt="" src="<%= request.getContextPath()%>/resources/images/news/thumb/8b27cc7c-ea97-4056-91c2-955494a7dd1c.jpg" />
                                                </div>
                                                <p class="news_head">Beyonce wears Christian Louboutin Boots During Super Bowl 50 Performance</p>
                                                <p class="news_date">Feb 8, 2016</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <div class="row no-gutter img_res fix_product">
                                    <div class="hnn_center">
                                        <div class="col-xs-4 h_n_ctrl">
                                            <a href="#">
                                                <div class="inmgz_hn">
                                                    <img alt="" src="<%= request.getContextPath()%>/resources/images/news/thumb/fc4a24dc-963f-499e-81b3-44fe6a48e2df.jpg" />
                                                </div>
                                                <p class="news_head">Adidas Debuts Tubular Doom Primeknit Collaboration With Kith</p>
                                                <p class="news_date">JAN 29, 2016</p>
                                            </a>
                                        </div>
                                        <div class="col-xs-4 h_n_ctrl"  >
                                            <a href="newsdetail0858.html?news_id=21">
                                                <div class="imgz_hn">
                                                    <img alt="" src="<%= request.getContextPath()%>/resources/images/news/thumb/8807ccc0-1cae-412a-9d24-48cbbc50c15f.jpg" />
                                                </div>
                                                <p class="news_head">6 RARE JAPANESE LABELS ARE NOW AVAILABLE THROUGH MR PORTER</p>
                                                <p class="news_date">JAN 29, 2016</p>
                                            </a>
                                        </div>
                                        <div class="col-xs-4 h_n_ctrl"  >
                                            <a href="#">
                                                <div class="imgz_hn">
                                                    <img alt="" src="<%= request.getContextPath()%>/resources/images/news/thumb/ab9930b6-9d58-4264-9feb-ec5bc5dd9986.jpg" />
                                                </div>
                                                <p class="news_head">Streetsnaps: Paris Fashion Week</p>
                                                <p class="news_date">Jan 26, 2016</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <div class="row no-gutter img_res fix_product">
                                    <div class="hnn_center">
                                        <div class="col-xs-4 h_n_ctrl">
                                            <a href="#">
                                                <div class="imgz_hn">
                                                    <img src="<%= request.getContextPath()%>/resources/images/news/thumb/537cea85-9acd-46d2-959d-6031226868ae.jpg" />
                                                </div>
                                                <p class="news_head">A Front Row Look at the Y-3 2016 Fall/Winter Collection</p>
                                                <p class="news_date">Jan 26, 2016</p>
                                            </a>
                                        </div>
                                        <div class="col-xs-4 h_n_ctrl"  >
                                            <a href="#">
                                                <div class="imgz_hn">
                                                    <img alt="" src="<%= request.getContextPath()%>/resources/images/news/thumb/f3f8b0e2-738c-4481-948d-2b6cc5b143f9.jpg" />
                                                </div>
                                                <p class="news_head">MAKING OFF FOOTIN PHOTOSHOOT 2016</p>
                                                <p class="news_date">Jan 26, 2016</p>
                                            </a>
                                        </div>
                                        <div class="col-xs-4 h_n_ctrl"  >
                                            <a href="#">
                                                <div class="imgz_hn">
                                                    <img alt="" src="<%= request.getContextPath()%>/resources/images/news/thumb/f3992013-4668-44a5-ae94-40a610a8ca68.jpg" />
                                                </div>
                                                <p class="news_head">WHEN A WHISPER IS LOUDER THAN A ROAR : BROOKLYN BECKHAM</p>
                                                <p class="news_date">Dec 13, 2015</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <div class="row no-gutter img_res fix_product">
                                    <div class="hnn_center">
                                        <div class="col-xs-4 h_n_ctrl">
                                            <a href="#">
                                                <div class="imgz_hn">
                                                    <img alt="" src="<%= request.getContextPath()%>/resources/images/news/thumb/a292da4c-ac64-4701-ad5f-de302f62c508.jpg" />
                                                </div>
                                                <p class="news_head">50 YEARS OF FASHION</p>
                                                <p class="news_date">Dec 12, 2015</p>
                                            </a>
                                        </div>
                                        <div class="col-xs-4 h_n_ctrl"  >
                                            <a href="#">
                                                <div class="imgz_hn">
                                                    <img alt="" src="<%= request.getContextPath()%>/resources/images/news/thumb/3ca66390-ce4a-4bbd-b7d0-6945c18fab5e.jpg" />
                                                </div>
                                                <p class="news_head">INSPIRING INSTAGRAMS: THASSIA NAVES</p>
                                                <p class="news_date">Dec 12, 2015</p>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
            <!-- ======================  End  News & Promotions  Carousel  ============================= -->

            <div class="row no-gutter img_res ig_ctrl">
                <div class="ig_cen">
                    <div class="col-xs-1 ig_ctrl00"></div>

                    <div class="col-xs-4 ig_ctrl01">

                        <div class="ig_01">
                            <a href="#" id="aig01" target="_blank">
                                <img alt="" src="<%= request.getContextPath()%>/resources/images/h_ig01.jpg" id="imgig01" /></a>
                        </div>

                        <div class="ig_in4">
                            <div class="ig_hash">
                                <p class="hashtag">#KEEPLOVINGFOOTIN</p>
                            </div>
                            <div class="ig_dt">
                                <p class="g_dt">For a chance to be featured.</p>
                            </div>
                            <div class="ig_social">
                                <div class="g_ig">
                                    <a href="#">
                                        <img alt="" src="<%= request.getContextPath()%>/resources/images/ig.png"/>
                                    </a>
                                </div>
                                <div class="g_tw">
                                    <a href="#">
                                        <img alt="" src="<%= request.getContextPath()%>/resources/images/tw.png"/>
                                    </a>
                                </div>
                                <div class="g_fb">
                                    <a href="#">
                                        <img alt="" src="<%= request.getContextPath()%>/resources/images/fb.png"/>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-2 ig_ctrl02">
                        <div class="ig_02">
                            <a href="#" id="aig02" target="_blank">
                                <img alt="" src="<%= request.getContextPath()%>/resources/images/h_ig02.jpg" id="imgig02" />
                            </a>
                        </div>
                        <div class="ig_03">
                            <a href="#" id="aig03" target="_blank">
                                <img alt="" src="<%= request.getContextPath()%>/resources/images/h_ig03.jpg" id="imgig03" />
                            </a>
                        </div>
                        <div class="ig_04">
                            <a href="#" id="aig04" target="_blank">
                                <img alt="" src="<%= request.getContextPath()%>/resources/images/h_ig04.jpg" id="imgig04" />
                            </a>
                        </div>
                    </div>

                    <div class="col-xs-2 ig_ctrl03">
                        <div class="ig_05">
                            <a href="#" id="aig05" target="_blank">
                                <img alt="" src="<%= request.getContextPath()%>/resources/images/h_ig05.jpg" id="imgig05" />
                            </a>
                        </div>
                        <div class="ig_06">
                            <a href="#" id="aig06" target="_blank">
                                <img alt="" src="<%= request.getContextPath()%>/resources/images/h_ig06.jpg" id="imgig06" />
                            </a>
                        </div>
                        <div class="ig_07">
                            <a href="#" id="aig07" target="_blank">
                                <img alt="" src="<%= request.getContextPath()%>/resources/images/h_ig07.jpg" id="imgig07" />
                            </a>
                        </div>
                    </div>

                    <div class="col-xs-1 ig_ctrl00"></div>
                </div>
            </div>


        </div>


        <jsp:include page="footer.jsp" flush="true"></jsp:include>


        <!-- Bootstrap Carousel-->
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery-1.4.2.min.js"/>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery.js"/>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/bootstrap.min.js"></script>

        <script type="text/javascript">
                !function ($) {
                $(function () {
                    $('#myCarousel').carousel();
                });
            } (window.jQuery);
        </script>

        <!-- HoverDrop Menu-->
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/script.js"></script>

        <!-- Bootstrap Yamm MegaMenu-->
        <script type="text/javascript">
            $(function () {
                // make code pretty
                window.prettyPrint && prettyPrint();

                $(document).on('click', '.yamm .dropdown-menu', function (e) {
                    e.stopPropagation();
                })

            })
        </script>


        <!-- jcarousel vertical thumb gallery-->
    <link href="<%= request.getContextPath()%>/resources/css/jquery.jcarousel.css" rel="stylesheet" media="screen"/>
    <link href="<%= request.getContextPath()%>/resources/css/skin.css" rel="stylesheet" media="screen"/>
    <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery.jcarousel.pack.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            //jCarousel Plugin
            $('#carousel').jcarousel({
                vertical: true,
                scroll: 1,
                auto: 0,
                wrap: 'last',
                initCallback: mycarousel_initCallback
            });

            //Front page Carousel - Initial Setup
            $('div#slideshow-carousel a img').css({ 'opacity': '0.5' });
            $('div#slideshow-carousel a img:first').css({ 'opacity': '1.0' });
            $('div#slideshow-carousel li a:first').append('<span class="arrow"></span>');


            //Combine jCarousel with Image Display
            $('div#slideshow-carousel li a').hover(
            function () {

                if (!$(this).has('span').length) {
                    $('div#slideshow-carousel li a img').stop(true, true).css({ 'opacity': '0.5' });
                    $(this).stop(true, true).children('img').css({ 'opacity': '1.0' });
                }
            },
            function () {

                $('div#slideshow-carousel li a img').stop(true, true).css({ 'opacity': '0.5' });
                $('div#slideshow-carousel li a').each(function () {

                    if ($(this).has('span').length) $(this).children('img').css({ 'opacity': '1.0' });

                });

            }
        ).click(function () {

                $('span.arrow').remove();
                $(this).append('<span class="arrow"></span>');
                $('div#slideshow-main li').removeClass('active');
                $('div#slideshow-main li.' + $(this).attr('rel')).addClass('active');

                return false;
            });


        });


        //Carousel Tweaking

        function mycarousel_initCallback(carousel) {

            // Pause autoscrolling if the user moves with the cursor over the clip.
            carousel.clip.hover(function () {
                carousel.stopAuto();
            }, function () {
                carousel.startAuto();
            });
        }

    </script>
    <script type="text/javascript">
        $('#myTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show');
        })
    </script>
</body>

</html>
