<%@page import="mis.drd.action.ParamUtil"%>
<!DOCTYPE html>
<%@taglib  prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<html lang="en" >

    <head>
        <meta content="" charset="UTF-8" />
        <title>FootIn</title>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/login.css" />       
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/common.css" />

        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js1/jquery-1.8.3.min.js" charset="UTF-8"></script>

    </head>

    <script  type="text/javascript">

        $(function() {

            $("#userFirstName").keydown(function (event) {
                switch (event.keyCode) {
                    case 8:  // Backspace
                    case 9:  // Tab
                    case 13: // Enter
                    case 37: // Left
                    case 38: // Up
                    case 39: // Right
                    case 40: // Down
                        break;
                    default:
                        var regex = new RegExp("^[a-zA-Z0-9]+$");
                        var key = event.key;
                        if (!regex.test(key)) {
                            event.preventDefault();
                            return false;
                        }
                        break;
                }
            });

            $("#userMiddleName").keydown(function (event) {
                switch (event.keyCode) {
                    case 8:  // Backspace
                    case 9:  // Tab
                    case 13: // Enter
                    case 37: // Left
                    case 38: // Up
                    case 39: // Right
                    case 40: // Down
                        break;
                    default:
                        var regex = new RegExp("^[a-zA-Z0-9]+$");
                        var key = event.key;
                        if (!regex.test(key)) {
                            event.preventDefault();
                            return false;
                        }
                        break;
                }
            });

            $("#userLastName").keydown(function (event) {
                switch (event.keyCode) {
                    case 8:  // Backspace
                    case 9:  // Tab
                    case 13: // Enter
                    case 37: // Left
                    case 38: // Up
                    case 39: // Right
                    case 40: // Down
                        break;
                    default:
                        var regex = new RegExp("^[a-zA-Z0-9]+$");
                        var key = event.key;
                        if (!regex.test(key)) {
                            event.preventDefault();
                            return false;
                        }
                        break;
                }
            });

            $("#userMobileNumber").keydown(function (e) {
                // Allow: backspace, tab, enter, delete
                if ($.inArray(e.keyCode, [8, 9, 13, 46]) !== -1 ||
                    // Allow: left, right
                (e.keyCode >= 37 && e.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57))
                    && (e.keyCode < 96 || e.keyCode > 105)){
                    e.preventDefault();
                }
            });

            $("#userMobileNumber").keyup(function (e) {

                var userMobileNumber = document.getElementById('userMobileNumber').value;

                if(userMobileNumber.trim().length == 11){

                    document.getElementById('duplicateMobileId').value='-1';
                
                    document.getElementById('mobileCheckImage').src='images/ajax-loader2.gif';

                    $.ajax({
                        type: "POST",
                        url: "duplicateUserCheckLog.footin?ftnContactNumber="+userMobileNumber,
                        success: function(data){
                            
                            document.getElementById('mobileCheckImage').src='';

                            if(parseInt(data.trim()) == 0){
                                document.getElementById('mobileCheckImage').src='images/okImg.jpeg';
                                document.getElementById('duplicateMobileId').value='0';
                            }else if(parseInt(data.trim()) > 0){
                                alert("Duplicate user found.");
                                document.getElementById('mobileCheckImage').src='images/deleteImg.jpeg';
                                document.getElementById('duplicateMobileId').value='1';
                            }

                        }
                    });
                }else{
                    document.getElementById('duplicateMobileId').value='-1';
                    document.getElementById('mobileCheckImage').src='';
                }

                e.preventDefault();

            });

        });

        function createNewUser() {

            var r = confirm("Are you want to create?");

            if (r == true) {

                var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                //
                var duplicateMobileId = document.getElementById('duplicateMobileId').value;
                //
                var userFirstName = document.getElementById('userFirstName').value;
                var userMiddleName = document.getElementById('userMiddleName').value;
                var userLastName = document.getElementById('userLastName').value;
                var userMobileNumber = document.getElementById('userMobileNumber').value;
                var userEmailAddress = document.getElementById('userEmailAddress').value;
                var createPassword = document.getElementById('createPassword').value;
                var confirmPassword = document.getElementById('confirmPassword').value;
                var cam_combo = document.getElementById("securityQuestion");
                var securityQuestion = cam_combo.options[cam_combo.selectedIndex].value;
                var securityAnswer = document.getElementById('securityAnswer').value;

               // alert(duplicateMobileId);
               
                if (duplicateMobileId.trim() == '0') {
                    
                   // alert("duplicateMobileId");

                    if (userMiddleName.trim() == '') {
                        alert('Name required.');
                    } else if (userMobileNumber.trim() == '') {
                        alert('Mobile number required.');
                    } else if (userEmailAddress.trim() == '') {
                        alert('Email address required.');
                    } else if (!filter.test(userEmailAddress)) {
                        alert('Please provide valid email address.');
                    }else if (createPassword.trim() == '') {
                        alert('Password required.');
                    } else if (confirmPassword.trim() == '') {
                        alert('Confirm password required.');
                    }  else if (createPassword != confirmPassword) {
                        alert('Password and Confirm password should be same.');
                    }  else if ((securityQuestion != '-1') && (securityAnswer.trim() == '')){
                        alert('Answer required.');
                    }else{
           
                        var dataString = "ftnUserFirstName="+userFirstName;
                        dataString += "&ftnUserMiddleName="+userMiddleName;
                        dataString += "&ftnUserLastName="+userLastName;
                        dataString += "&ftnContactNumber="+userMobileNumber;
                        dataString += "&ftnEmailAddress="+userEmailAddress;
                        dataString += "&ftnUserPassword="+confirmPassword;
                        dataString += "&ftnSecurityQuestion="+securityQuestion;
                        dataString += "&ftnSecurityAnswer="+securityAnswer;

                        //alert(dataString);

                        document.getElementById('userCreateImage').src='images/ajax-loader2.gif';

                        $.ajax({
                            type: "POST",
                            url: "createNewUserLog.footin?"+dataString,
                            success: function(data){

                                document.getElementById('userCreateImage').src='';

                                alert(data);


                            }
                        });

                    }
                }
            }
        }
        


    </script>


    <body>

        <div style="background-color: #E06C0A; height: 40px;">&nbsp;</div>

        <br/><br/>

        <table border="0" width="100%">

            <tr>
                <td style="width: 30px;">&nbsp;</td>
                <td width="90%">
                    <img src="resources/images_back/logo.png" width="100px" height="30px" alt="" align="left"/>
                </td>
                <td style="background-color: #E06C0A; width: 30px;">&nbsp;</td>
                <td style="width: 30px;">&nbsp;</td>
            </tr>

        </table>

        <div class="wrap1">

            <div id="main">

                <form action="" method="post" >

                    <table style="width: 100%;">
                        <tr>
                            <td colspan="3">
                                <label style="color: #E06C0A;">User Registration</label>
                                <input type="hidden" id="duplicateMobileId" name="duplicateMobileId" value="-1"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 33%;"><label>First Name</label></td>
                            <td style="width: 33%;"><label>Middle Name&nbsp;<span style="color: red;">*</span></label></td>
                            <td style="width: 33%;"><label>Last Name</label></td>
                        </tr>

                        <tr>
                            <td><input class="text" id="userFirstName" name="userFirstName" value=""/></td>
                            <td><input class="text" id="userMiddleName" name="userMiddleName" value=""/></td>
                            <td><input class="text" id="userLastName" name="userLastName" value=""/></td>
                        </tr>

                    </table>


                    <label>Mobile Number:&nbsp;<span style="color: red;">*</span>&nbsp;<img id="mobileCheckImage" src="" alt="" /></label>
                    <input id="userMobileNumber" name="userMobileNumber"  class="text" maxlength="11"/>                    
                    <label>Email Address:</label>
                    <input id="userEmailAddress" name="userEmailAddress"  class="text" maxlength="50"/>
                    <label>Password:&nbsp;<span style="color: red;">*</span></label>
                    <input id="createPassword" name="createPassword" type="password" class="text" maxlength="20" />
                    <label>Confirm Password:&nbsp;<span style="color: red;">*</span></label>
                    <input id="confirmPassword" name="confirmPassword" type="password" class="text" maxlength="20" />
                    <label>Question:</label>
                    <select id="securityQuestion" name="securityQuestion" >
                        <option value="-1"></option>
                        <option value="What was your favorite place to visit?">What was your favorite place to visit?</option>
                        <option value="Who is your favorite actor?">Who is your favorite actor?</option>
                        <option value="What is the name of your favorite pet?">What is the name of your favorite pet?</option>
                        <option value="In what city were you born?">In what city were you born?</option>
                        <option value="What is the name of your first school?">What is the name of your first school?</option>
                        <option value="What is your favorite movie?">What is your favorite movie?</option>
                        <option value="What is your mother's maiden name?">What is your mother's maiden name?</option>
                        <option value="What is your father's middle name?">What is your father's middle name?</option>
                        <option value="What is your favorite color?">What is your favorite color?</option>
                        <option value="What is the name of your first grade teacher?">What is the name of your first grade teacher?</option>
                    </select>
                    <label>Answer:</label>
                    <input id="securityAnswer" name="securityAnswer" class="text" />

                    <a href="javascript:void(0);" onclick="createNewUser();" class="button2">Create</a>
                    &nbsp;<img id="userCreateImage" src="" alt="" />

                </form>

            </div>



            <br/>

        </div>

        <jsp:include page="user_footer.jsp" flush="true"></jsp:include>

    </body>

</html>