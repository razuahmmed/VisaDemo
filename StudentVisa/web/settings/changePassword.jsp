<%--
    Document   : invoice
    Created on : Jun 20, 2013, 1:09:47 PM
    Author     : MIS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sx" uri="/struts-dojo-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/common.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/style.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/navi.css" />
        <script type="text/javascript"   src="<%= request.getContextPath()%>/resources/js/jquery-1.8.3.js"></script>
        <script type="text/javascript"  src="<%= request.getContextPath()%>/resources/js/jquery-ui.js"></script>
        <link rel="stylesheet" href="<%= request.getContextPath()%>/resources/css/jquery-ui.css" />
        <link rel="shortcut icon" href="<%= request.getContextPath()%>/resources/ico/favicon.png">

        <%
                    String hasSuccessful = request.getSession().getAttribute("hasSuccessful") != null ? request.getSession().getAttribute("hasSuccessful").toString().trim() : null;
                    String message = request.getSession().getAttribute("message") != null ? request.getSession().getAttribute("message").toString().trim() : null;
                    if (hasSuccessful != null) {
                        request.getSession().removeAttribute("hasSuccessful");
                        request.getSession().removeAttribute("message");
                    }
        %>
        <script type="text/javascript">

            $(function () {
                $("#userNewPassword").bind("keyup", function () {
                    //TextBox left blank.
                    if ($(this).val().length == 0) {
                        $("#password_strength").html("");
                        return;
                    }

                    //Regular Expressions.
                    var regex = new Array();
                    regex.push("[A-Z]"); //Uppercase Alphabet.
                    regex.push("[a-z]"); //Lowercase Alphabet.
                    regex.push("[0-9]"); //Digit.
                    regex.push("[$@$!%*#?&]"); //Special Character.

                    var passed = 0;

                    //Validate for each Regular Expression.
                    for (var i = 0; i < regex.length; i++) {
                        if (new RegExp(regex[i]).test($(this).val())) {
                            passed++;
                        }
                    }


                    //Validate for length of Password.
                    if (passed > 2 && $(this).val().length > 8) {
                        passed++;
                    }

                    //Display status.
                    var color = "";
                    var strength = "";
                    switch (passed) {
                        case 0:
                        case 1:
                            strength = "Weak";
                            color = "red";
                            break;
                        case 2:
                            strength = "Good";
                            color = "darkorange";
                            break;
                        case 3:
                        case 4:
                            strength = "Strong";
                            color = "green";
                            break;
                        case 5:
                            strength = "Very Strong";
                            color = "darkgreen";
                            break;
                    }
                    $("#password_strength").html(strength);
                    $("#password_strength").css("color", color);
                });
            });
        </script>
        <sx:head/>
        <s:head/>

        <script type="text/javascript">

            function updateChangePassword(){

                var r = confirm("Do you want to update?");

                if (r == true) {

                    var newUserId= document.getElementById("newUserId").value;
                    var userNewPassword= document.getElementById("userNewPassword").value;
                 
                    // alert(newUserId+ ' '+ userNewPassword);

                    if((newUserId.length != 0)&&(userNewPassword.length != 0)){               

                        document.getElementById('imageId').src='images/ajax-loader2.gif';

                        $.ajax({
                            url: "passwordChangeSaveUSESET.bata?userId="+newUserId+"&userPassword="+userNewPassword,
                            cache: false,
                            type: "POST"
                        }).done(function( html ) {
                            alert(html);
                            document.getElementById('imageId').src='';
                            window.location.reload(true);
                        });
                        
                    }

                }

            }

           
        </script>

        <style type="text/css">
            <!--
            .style1 {color: #FF0000}
            -->
        </style>

    </head>
    <body>
        <%
                    boolean login = (request.getSession().getAttribute("logged-in") != null && request.getSession().getAttribute("logged-in").toString().trim().equals("true")) ? true : false;

                    if (!login) {%>
        <jsp:forward page="/index.jsp"></jsp:forward>
        <%                    } else {
        %>

        <div class="wrap">

            <s:include value="/menu.jsp"></s:include>

            <div id="content">

                <div id="sidebar">
                    <div class="box">
                        <s:include value="/submenu/user.jsp"></s:include>
                    </div>
                </div>

                <div id="main">

                    <div class="full_w">
                        <div class="h_title"><b>Change Password</b></div>
                        <form id="formId1" action="#" method="post">
                            <table width="100%">

                                <s:if test="rtlUserList != null">

                                    <s:if test="rtlUserList.size()!=0">

                                        <s:iterator value="rtlUserList">
                                            <tr>
                                                <td style="width: 30%; text-align: right;">
                                                    <input type="hidden" id="newUserId" name="newUserId" value="<s:property value="userId"/>">
                                                    <b>User Name: &nbsp; </b>
                                                </td>
                                                <td style="width: 70%; text-align: left;">&nbsp;<b><s:property value="userName"/></b></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right;">
                                                    <input type="hidden" id="newUserId" name="newUserId" value="<s:property value="userId"/>">
                                                    <b>User Password: &nbsp;</b>
                                                </td>
                                                <td style="text-align: left;">&nbsp;<b><s:property value="userPassword"/></b></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right;">
                                                    <b>New Password:</b> &nbsp;
                                                </td>
                                                <td style="text-align: left;">&nbsp;
                                                    <input type="password" id="userNewPassword" name="userNewPassword" maxlength="20" size="30">
                                                    <label id="password_strength">&nbsp;</label>
                                                    &nbsp;
                                                    <input type="button" onclick="updateChangePassword();" id="passwordUpdateBtn" name="passwordUpdateBtn" value="Update">
                                                    &nbsp;
                                                    <img id="imageId" src="" alt="" />
                                                </td>
                                            </tr>
                                        </s:iterator>
                                    </s:if>
                                </s:if>
                            </table>
                        </form>


                        <div id="selectedGroupDivId"></div>

                    </div>

                </div>
                <div class="clear"></div>

            </div>
            <s:include value="/footer.jsp"/>
        </div>

        <% }%>
    </body>
</html>
