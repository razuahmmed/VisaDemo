<%--
    Document   : invoice
    Created on : Jun 20, 2013, 1:09:47 PM
    Author     : MIS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sx" uri="/struts-dojo-tags" %>

<style type="text/css">
    <!--
    .style1 {color: #FF0000}

    .divTitle {
        background: #336EA5;
        height: 22px;
        text-align: center;
        padding-top: 5px;
        font-size: 12px;
        font-weight: bold;
    }

    .divTextField{
        height: 20px;
        padding: 2px;
    }

    .divLevel{
        color: #000000;
    }

    -->
</style>


<script type="text/javascript">
    
    function checkValidateEmail() {

        var email=document.getElementById("newComEmailFb").value;

        if(email.trim().length > 0){
            var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                   
            if(!expr.test(email)){
                alert("Invalid email address.");
                document.getElementById("newComEmailFb").value='';
            }
                
        }
    };
  
    function updateCompany(){

        var r = confirm("Do you want to update?");

        if (r == true) {

            var newCompanyCode=document.getElementById("companyCodeFb").value;
            var newCompanyName=document.getElementById("companyNameFb").value;
            var newCompanyBanner=document.getElementById("companyBannerFb").value;
            var newCompanyManager=document.getElementById("companyManagerFb").value;
            var newCompanyAddress=document.getElementById("companyAddressFb").value;
            var newCompanyRegion=document.getElementById("companyRegionFb").value;
            var newCompanyCountry=document.getElementById("companyCountryFb").value;
            var newCompanyContact=document.getElementById("companyContactFb").value;
            var newCompanyEmail=document.getElementById("companyEmailFb").value;
            var newCompanyFax=document.getElementById("companyFaxFb").value;
            var newCompanyStatus= $("#statusListFb" ).val();

            var flag=true;

            if(newCompanyName.trim().length == 0){
                flag=false;
                alert('Company name not found!');
            }

            if(flag){
                if(newCompanyBanner.trim().length == 0){
                    flag=false;
                    alert('Banner not found!');
                }
            }

            if(flag){
                if(newCompanyManager.trim().length == 0){
                    flag=false;
                    alert('Company manager not found!');
                }
            }

            if(flag){
                if(newCompanyRegion.trim().length == 0){
                    flag=false;
                    alert('Region not found!');
                }
            }

            if(flag){
                if(newCompanyCountry.trim().length == 0){
                    flag=false;
                    alert('Country not found!');
                }
            }

            if(flag){

                var param='companyCode='+newCompanyCode;
                param+='&companyName='+newCompanyName;
                param+='&companyBanner='+newCompanyBanner;
                param+='&companyManager='+newCompanyManager;
                param+='&companyAddress='+newCompanyAddress;
                param+='&companyRegion='+newCompanyRegion;
                param+='&companyCountry='+newCompanyCountry;
                param+='&companyContact='+newCompanyContact;
                param+='&companyEmail='+newCompanyEmail;
                param+='&companyFax='+newCompanyFax;
                param+='&companyStatus='+newCompanyStatus;

                document.getElementById('userDetailsViewImage2').src='images/ajax-loader2.gif';

                $.ajax({
                    url: "updateCompanyDetailsBACSET.bata?"+param,
                    cache: false,
                    type: "POST"
                }).done(function( html ) {
                    alert(html);
                    document.getElementById('userDetailsViewImage2').src='';
                    window.location.reload(true);
                });
            }
        }
    }    

</script>


<div id="main">

    <div class="divTitle"><b>Company Information</b></div>

    <s:if test="rtlCompanyList != null">

        <s:if test="rtlCompanyList.size()!=0">

            <s:iterator value="rtlCompanyList">

                <table style="width: 700px;">

                    <tr>
                        <td class="divLevel" style="width: 100px; text-align: right; white-space: nowrap ;"><label style="color: red;">*</label>Company Code:</td>
                        <td style="width: 250px; text-align: left;">
                            <input class="divTextField" type="text" id="companyCodeFb" name="companyCodeFb" size="15" maxlength="10" value="<s:property value="comCode"/>" readonly >
                        </td>
                        <td style="width: 100px; text-align: right">&nbsp;</td>
                        <td style="width: 250px; text-align: left">&nbsp;</td>
                    </tr>

                    <tr>
                        <td class="divLevel" style="text-align: right; white-space: nowrap;"><label style="color: red;">*</label>Company Name:</td>
                        <td colspan="3" style="text-align: left;">
                            <input class="divTextField" type="text" id="companyNameFb" name="companyNameFb" size="100" maxlength="250" value="<s:property value="comName"/>" >
                        </td>
                    </tr>

                    <tr>
                        <td class="divLevel" style="text-align: right; white-space: nowrap ;"><label style="color: red;">*</label>Company Banner:</td>
                        <td colspan="3" style="text-align: left;">
                            <input class="divTextField" type="text" id="companyBannerFb" name="companyBannerFb" size="100" maxlength="250" value="<s:property value="comBanner"/>" >
                        </td>
                    </tr>

                    <tr>
                        <td class="divLevel" style="text-align: right; white-space: nowrap ;"><label style="color: red;">*</label>Company Manager:</td>
                        <td colspan="3" style="text-align: left;">
                            <input class="divTextField" type="text" id="companyManagerFb" name="companyManagerFb" size="100" maxlength="250" value="<s:property value="comManager"/>">
                        </td>
                    </tr>

                    <tr>
                        <td class="divLevel" style="text-align: right;">Company Address:</td>
                        <td colspan="3" style="text-align: left;">
                            <input class="divTextField" type="text" id="companyAddressFb" name="companyAddressFb" size="100" maxlength="250" value="<s:property value="comAddress"/>" >
                        </td>
                    </tr>

                    <tr>
                        <td class="divLevel" style="text-align: right"><label style="color: red;">*</label>Region:</td>
                        <td style="text-align: left">
                            <input class="divTextField" type="text" id="companyRegionFb" name="companyRegionFb" size="35" maxlength="50" value="<s:property value="comRegion"/>" >
                        </td>
                        <td class="divLevel" style="text-align: right"><label style="color: red;">*</label>Country:</td>
                        <td style="text-align: left">
                            <input class="divTextField" type="text" id="companyCountryFb" name="companyCountryFb" size="35" maxlength="50" value="<s:property value="comCountry"/>" >
                        </td>
                    </tr>

                    <tr>
                        <td class="divLevel" style="text-align: right">Contact Number:</td>
                        <td style="text-align: left">
                            <input class="divTextField" type="text" id="companyContactFb" name="companyContactFb" size="35" maxlength="100" value="<s:property value="comContact"/>" >
                        </td>
                        <td class="divLevel" style="text-align: right">Company Email:</td>
                        <td style="text-align: left">
                            <input class="divTextField" onchange="checkValidateEmail();" type="text" id="companyEmailFb" name="companyEmailFb" size="35" maxlength="100" value="<s:property value="comEmail"/>" >
                        </td>
                    </tr>

                    <tr>
                        <td class="divLevel" style="text-align: right">Company Fax:</td>
                        <td style="text-align: left">
                            <input class="divTextField" type="text" id="companyFaxFb" name="companyFaxFb" size="35" maxlength="100" value="<s:property value="comFax"/>">
                        </td>
                        <td class="divLevel" style="text-align: right">Status:</td>
                        <td style="text-align: left">
                            <select class="divTextField" id="statusListFb" name="statusListFb" >
                                <s:if test="comStatus=='Y'">
                                    <option value="Y" selected>Active</option>
                                    <option value="N">Inactive</option>
                                </s:if>
                                <s:else>
                                    <option value="Y">Active</option>
                                    <option value="N" selected>Inactive</option>
                                </s:else>
                            </select>
                        </td>
                    </tr>


                    <tr>
                        <td class="divLevel" colspan="4" style="text-align: center">
                            <input  onclick="updateCompany();" type="button" id="updateButton" name="updateButton" value="Update" >
                            <img id="userDetailsViewImage2" src="" alt="" />
                        </td>
                    </tr>

                </table>

            </s:iterator>

        </s:if>

    </s:if>
</div>

