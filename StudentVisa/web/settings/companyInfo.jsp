<%--
    Document   : invoice
    Created on : Jun 20, 2013, 1:09:47 PM
    Author     : MIS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sx" uri="/struts-dojo-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/common.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/style.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/navi.css" />
        <script type="text/javascript"   src="<%= request.getContextPath()%>/resources/js/jquery-1.8.3.js"></script>
        <script type="text/javascript"  src="<%= request.getContextPath()%>/resources/js/jquery-ui.js"></script>
        <link rel="stylesheet" href="<%= request.getContextPath()%>/resources/css/jquery-ui.css" />
        <link rel="shortcut icon" href="<%= request.getContextPath()%>/resources/ico/favicon.png">

        <link type="text/css" href="<%= request.getContextPath()%>/facebox/facebox.css" media="screen" rel="stylesheet" />
        <script type="text/javascript" src="<%= request.getContextPath()%>/facebox/facebox.js"></script>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/jquery.dataTables.css">
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery.dataTables.js"></script>


        <%
                    String hasSuccessful = request.getSession().getAttribute("hasSuccessful") != null ? request.getSession().getAttribute("hasSuccessful").toString().trim() : null;
                    String message = request.getSession().getAttribute("message") != null ? request.getSession().getAttribute("message").toString().trim() : null;
                    if (hasSuccessful != null) {
                        request.getSession().removeAttribute("hasSuccessful");
                        request.getSession().removeAttribute("message");
                    }
        %>
        <script type="text/javascript">

            $(function() {

            <% if (hasSuccessful != null && (hasSuccessful.equalsIgnoreCase("true") || hasSuccessful.equalsIgnoreCase("false"))) {%>
                    $('#dialog').html('<p><%out.print(message);%></p>');
                    $('#dialog').dialog({
                        title:"Result",
                        bgiframe: true,
                        height: 'auto',
                        width: 'auto',
                        modal: true,autoOpen: true,
                        buttons: { "Ok": function() {
                                $(this).dialog("close");}}
                    });

            <% }%>

                });
        </script>
        <sx:head/>
        <s:head/>

        <script type="text/javascript">

            function onloadFunction(){
                // DataTable
                $('#example').DataTable({
                    "aoColumnDefs": [{'bSortable': false, 'aTargets': [0]}],
                    "order": [[ 1, "asc" ]]
                });
            }


            $(document).ready(function($) {

                onloadFunction();

                $("#companyCode").keydown(function (e) {
                    // Allow: backspace, tab, enter, delete
                    if (($.inArray(e.keyCode, [8, 9, 13, 46]) !== -1 ||
                        // Allow: left, right
                    (e.keyCode >= 37 && e.keyCode <= 39))||
                        (e.keyCode >= 65 && e.keyCode <= 90))
                    {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if (((e.keyCode < 48 || e.keyCode > 57))
                        && (e.keyCode < 96 || e.keyCode > 105)
                        && (e.keyCode < 110 || e.keyCode > 112)
                        && (e.keyCode < 190 || e.keyCode > 192)
                ){
                        e.preventDefault();
                    }
                });

            });

            function checkValidateEmail() {

                var email=document.getElementById("companyEmail").value;

                if(email.trim().length > 0){
                    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                   
                    if(!expr.test(email)){
                        alert("Invalid email address.");
                        document.getElementById("companyEmail").value='';
                    }
                
                }
            };

            function checkDuplicateCompany(){

                var newComCode=document.getElementById("companyCode").value;

                if(newComCode.trim().length > 0){
                    // alert(newComCode);
                    document.getElementById('newComCodeImage').src='images/ajax-loader2.gif';

                    $.ajax({
                        url: "checkDuplicateCompanyCodeBACSET.bata?companyCode="+newComCode,
                        cache: false,
                        type: "POST"
                    }).done(function( html ) {                        
                        if(html.trim()=='1'){
                            document.getElementById("duplicateComCode").value='1';
                            alert("Company already exist!");
                            document.getElementById('newComCodeImage').src='images/deleteImg.jpeg';
                        }else{
                            document.getElementById("duplicateComCode").value='0';
                            document.getElementById('newComCodeImage').src='images/okImg.jpeg';
                        }                        
                    });
                }else{
                    document.getElementById("duplicateComCode").value='-1';
                    document.getElementById('newComCodeImage').src='';
                }
            }
           
            function createNewCompany(){

                var r = confirm("Do you want to create?");

                if (r == true) {

                    var duplicateComCode=document.getElementById("duplicateComCode").value;
                    //
                    if(duplicateComCode=="1"){
                        alert("Company already exist!");
                    }else if(duplicateComCode=="-1"){
                        alert("Company code not found.");
                    }else if(duplicateComCode=="0"){

                        var newCompanyCode=document.getElementById("companyCode").value;
                        var newCompanyName=document.getElementById("companyName").value;
                        var newCompanyBanner=document.getElementById("companyBanner").value;
                        var newCompanyManager=document.getElementById("companyManager").value;
                        var newCompanyAddress=document.getElementById("companyAddress").value;
                        var newCompanyRegion=document.getElementById("companyRegion").value;
                        var newCompanyCountry=document.getElementById("companyCountry").value;
                        var newCompanyContact=document.getElementById("companyContact").value;
                        var newCompanyEmail=document.getElementById("companyEmail").value;
                        var newCompanyFax=document.getElementById("companyFax").value;
                        var newCompanyStatus= $("#statusList" ).val();

                        var flag=true;

                        if(newCompanyName.trim().length == 0){
                            flag=false;
                            alert('Company name not found!');
                        }

                        if(flag){
                            if(newCompanyBanner.trim().length == 0){
                                flag=false;
                                alert('Banner not found!');
                            }
                        }

                        if(flag){
                            if(newCompanyManager.trim().length == 0){
                                flag=false;
                                alert('Company manager not found!');
                            }
                        }

                        if(flag){
                            if(newCompanyRegion.trim().length == 0){
                                flag=false;
                                alert('Region not found!');
                            }
                        }

                        if(flag){
                            if(newCompanyCountry.trim().length == 0){
                                flag=false;
                                alert('Country not found!');
                            }
                        }

                        if(flag){

                            var param='companyCode='+newCompanyCode;
                            param+='&companyName='+newCompanyName;
                            param+='&companyBanner='+newCompanyBanner;
                            param+='&companyManager='+newCompanyManager;
                            param+='&companyAddress='+newCompanyAddress;
                            param+='&companyRegion='+newCompanyRegion;
                            param+='&companyCountry='+newCompanyCountry;
                            param+='&companyContact='+newCompanyContact;
                            param+='&companyEmail='+newCompanyEmail;
                            param+='&companyFax='+newCompanyFax;
                            param+='&companyStatus='+newCompanyStatus;

                            document.getElementById('createUserImage').src='images/ajax-loader2.gif';

                            $.ajax({
                                url: "createNewCompanyBACSET.bata?"+param,
                                cache: false,
                                type: "POST"
                            }).done(function( html ) {
                                alert(html);
                                document.getElementById('createUserImage').src='';
                                window.location.reload(true);
                            });
                        }

                    }

                }
            }

            function companyDetailsView(comId){

                var dataString = 'companyCode='+comId;

                document.getElementById('userDetailsViewImage').src='images/ajax-loader2.gif';

                $.ajax({
                    type: "POST",
                    url:'companyDetailsViewBACSET.bata',
                    data: dataString,
                    success: function(data){
                        //alert(data);
                        document.getElementById('userDetailsViewImage').src='';
                        jQuery.facebox(data);
                        
                    }
                });

            }
           
            function companyDelete(companyrId){

                var r = confirm("Do you want to delete?");

                if (r == true) {

                    var param='newCompanyCode='+companyrId;

                    document.getElementById('userDetailsViewImage').src='images/ajax-loader2.gif';

                    $.ajax({
                        url: "deleteCompanyDetailsBACSET.bata?"+param,
                        cache: false,
                        type: "POST"
                    }).done(function( html ) {
                        alert(html);
                        document.getElementById('userDetailsViewImage').src='';
                        window.location.reload(true);
                    });                
                }
            }

        
        </script>


    </head>
    <body>
        <%
                    boolean login = (request.getSession().getAttribute("logged-in") != null && request.getSession().getAttribute("logged-in").toString().trim().equals("true")) ? true : false;

                    if (!login) {%>
        <jsp:forward page="/index.jsp"></jsp:forward>
        <%                    } else {
        %>

        <div class="wrap">

            <s:include value="/menu.jsp"></s:include>

            <div id="content">

                <div id="sidebar">
                    <div class="box">
                        <s:include value="/submenu/basicSetup.jsp"></s:include>
                    </div>
                </div>

                <div id="main">

                    <div class="full_w">
                        <div class="h_title"><b>Company Information</b></div>
                        <form id="formId" action="#" method="post">
                            <table width="100%">
                                <tr>                                    
                                    <td style="width: 20%; text-align: right; white-space: nowrap ;"><label style="color: red;">*</label>Company Code:</td>
                                    <td style="width: 30%; text-align: left;">
                                        <input type="hidden" id="duplicateComCode" name="duplicateComCode" value="-1" >
                                        <input onchange="checkDuplicateCompany();" type="text" id="companyCode" name="companyCode" size="15" maxlength="10" >
                                        <img id="newComCodeImage" src="" alt="" />
                                    </td>
                                    <td style="width: 20%; text-align: right">&nbsp;</td>
                                    <td style="width: 30%; text-align: left">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td style="text-align: right; white-space: nowrap;"><label style="color: red;">*</label>Company Name:</td>
                                    <td colspan="3" style="text-align: left;">
                                        <input type="text" id="companyName" name="companyName" size="100" maxlength="250" >
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: right; white-space: nowrap ;"><label style="color: red;">*</label>Company Banner:</td>
                                    <td colspan="3" style="text-align: left;">
                                        <input type="text" id="companyBanner" name="companyBanner" size="100" maxlength="250" >
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: right; white-space: nowrap ;"><label style="color: red;">*</label>Company Manager:</td>
                                    <td colspan="3" style="text-align: left;">
                                        <input type="text" id="companyManager" name="companyManager" size="100" maxlength="250" >
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: right;">Company Address:</td>
                                    <td colspan="3" style="text-align: left;">
                                        <input type="text" id="companyAddress" name="companyAddress" size="100" maxlength="250" >
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: right"><label style="color: red;">*</label>Region:</td>
                                    <td style="text-align: left">
                                        <input type="text" id="companyRegion" name="companyRegion" size="35" maxlength="50" >
                                    </td>
                                    <td style="text-align: right"><label style="color: red;">*</label>Country:</td>
                                    <td style="text-align: left">
                                        <input type="text" id="companyCountry" name="companyCountry" size="35" maxlength="50" >
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: right">Contact Number:</td>
                                    <td style="text-align: left">
                                        <input type="text" id="companyContact" name="companyContact" size="35" maxlength="100" >
                                    </td>
                                    <td style="text-align: right">Company Email:</td>
                                    <td style="text-align: left">
                                        <input onchange="checkValidateEmail();" type="text" id="companyEmail" name="companyEmail" size="35" maxlength="100" >
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: right">Company Fax:</td>
                                    <td style="text-align: left">
                                        <input type="text" id="companyFax" name="companyFax" size="35" maxlength="100" >
                                    </td>
                                    <td style="text-align: right">Status:</td>
                                    <td style="text-align: left">
                                        <select id="statusList" name="statusList" >
                                            <option value="Y">Active</option>
                                            <option value="N">Inactive</option>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="4" style="text-align: center">
                                        <input onclick="createNewCompany();" type="button" id="saveButton" name="saveButton" value="Create" >
                                        <img id="createUserImage" src="" alt="" />
                                    </td>
                                </tr>

                            </table>
                        </form>

                        <div id="bsoUserDivId">

                            <form id="formId" action="#" method="post">

                                <table id="example"  style="width: 100%;">

                                    <thead>
                                        <tr>                                          
                                            <th style="width: 30px;">Code</th>
                                            <th style="width: 300px;">Company Name</th>                                            
                                            <th style="width: 200px;">Company Manager</th>
                                            <th style="width: 70px;">Country</th>
                                            <th style="width: 50px;">Status</th>
                                            <th style="width: 30px;"><img id="userDetailsViewImage" src="" alt="" /></th>
                                        </tr>
                                    </thead>

                                    <s:if test="rtlCompanyList != null">

                                        <s:if test="rtlCompanyList.size()!=0">

                                            <tbody>

                                                <s:iterator value="rtlCompanyList">

                                                    <tr>
                                                        <td align="left"><s:property value="comCode"/></td>
                                                        <td align="left"><s:property value="comName"/></td>
                                                        <td align="left"><s:property value="comManager"/></td>
                                                        <td align="left"><s:property value="comCountry"/></td>
                                                        <td align="left">
                                                            <s:if test="comStatus=='Y'">Active</s:if>
                                                            <s:else>Inactive</s:else>
                                                        </td>
                                                        <td align="center">
                                                            <a href="javascript:void(0)" onclick="companyDetailsView('<s:property value="comCode"/>')" style="color: #0000FF;text-decoration: none;" >
                                                                Edit
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </s:iterator>
                                            </tbody>
                                        </s:if>
                                    </s:if>

                                </table>

                            </form>
                        </div>

                    </div>

                </div>
                <div class="clear"></div>

            </div>
            <s:include value="/footer.jsp"/>
        </div>

        <% }%>
    </body>
</html>
