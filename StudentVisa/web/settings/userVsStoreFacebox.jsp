<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>

<style type="text/css">
    <!--
    .style1 {color: #FF0000}

    .divTitle {
        background: #336EA5;
        height: 22px;
        text-align: center;
        padding-top: 5px;
        font-size: 12px;
        font-weight: bold;
    }

    .divTextField{
        height: 20px;
        padding: 2px;
    }

    .divLevel{
        color: #000000;
        white-space: nowrap ;
    }

    -->
</style>

<script type="text/javascript">


    $(document).ready(function() {

        userStoreViewByConcept();

        $("#conceptRdId").click(function () {
            userStoreViewByConcept();
        });

        $("#areaRdId").click(function () {
            userStoreViewByArea();
        });

    });

    function userStoreViewByConcept(){
        
        var userId=document.getElementById('userId').value;

        document.getElementById('userCodeImage2').src='images/ajax-loader2.gif';

        $.ajax({
            url: "userVsStoreViewByConceptUSESET.bata?userId="+userId,
            cache: false,
            type: "POST"
        }).done(function( html ) {
            document.getElementById('userCodeImage2').src='';
            $("#dataDivId").html(html);
        });

    }

    function userStoreViewByArea(){

        var userId=document.getElementById('userId').value;

        document.getElementById('userCodeImage2').src='images/ajax-loader2.gif';

        $.ajax({
            url: "userVsStoreViewByAreaUSESET.bata?userId="+userId,
            cache: false,
            type: "POST"
        }).done(function( html ) {
            document.getElementById('userCodeImage2').src='';
            $("#dataDivId").html(html);
        });

    }
  

</script>

<div >

    <div>
        <table>
            <tr class="divLevel">
                <td style="width: 40%;">
                    <input type="hidden" id="userId" name="userId" value="<s:property value="userId"/>" >
                </td>
                <td style="width: 5%; text-align: right;">
                    <input type="radio" id="conceptRdId" name="rd" value="Concept" checked>
                </td>
                <td style="width: 5%; text-align: left;">Concept</td>
                <td style="width: 5%; text-align: right;">
                    <input type="radio" id="areaRdId" name="rd" value="Area">
                </td>
                <td style="width: 5%; text-align: left;">Area/District</td>
                <td style="width: 40%;"><img id="userCodeImage2" src="" alt="" /></td>
            </tr>
        </table>
    </div>
    <br/>
    <div id="dataDivId" >

    </div>

</div>

