<%--
    Document   : invoice
    Created on : Jun 20, 2013, 1:09:47 PM
    Author     : MIS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sx" uri="/struts-dojo-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<style type="text/css">
    <!--
    .style1 {color: #FF0000}

    .divTitle {
        background: #336EA5;
        height: 22px;
        text-align: center;
        padding-top: 5px;
        font-size: 12px;
        font-weight: bold;
    }

    .divTextField{
        height: 20px;
        padding: 2px;
    }

    .divLevel{
        color: #000000;
    }

    -->
</style>

<script type="text/javascript">

    $(function () {
        $("#newUserPassword").bind("keyup", function () {
            //TextBox left blank.
            if ($(this).val().length == 0) {
                $("#password_strength").html("");
                return;
            }

            //Regular Expressions.
            var regex = new Array();
            regex.push("[A-Z]"); //Uppercase Alphabet.
            regex.push("[a-z]"); //Lowercase Alphabet.
            regex.push("[0-9]"); //Digit.
            regex.push("[$@$!%*#?&]"); //Special Character.

            var passed = 0;

            //Validate for each Regular Expression.
            for (var i = 0; i < regex.length; i++) {
                if (new RegExp(regex[i]).test($(this).val())) {
                    passed++;
                }
            }

            //Validate for length of Password.
            if (passed > 2 && $(this).val().length > 8) {
                passed++;
            }

            //Display status.
            var color = "";
            var strength = "";
            switch (passed) {
                case 0:
                case 1:
                    strength = "Weak";
                    color = "red";
                    break;
                case 2:
                    strength = "Good";
                    color = "darkorange";
                    break;
                case 3:
                case 4:
                    strength = "Strong";
                    color = "green";
                    break;
                case 5:
                    strength = "Very Strong";
                    color = "darkgreen";
                    break;
            }
            $("#password_strength").html(strength);
            $("#password_strength").css("color", color);
        });
    });
</script>


<script type="text/javascript">

    function checkValidateEmail() {

        var email=document.getElementById("newUserEmail").value;

        if(email.trim().length > 0){
            var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

            if(!expr.test(email)){
                alert("Invalid email address.");
                document.getElementById("newUserEmail").value='';
            }

        }
    };
    
    function updateNewUser(){

        var r = confirm("Do you want to update?");

        if (r == true) {

            var userId=document.getElementById("userId").value;
            var newUserName=document.getElementById("newUserName").value;
            var newUserPassword=document.getElementById("newUserPassword").value;
            var newUserDisplayName=document.getElementById("newUserDisplayName").value;
            var newUserEmail=document.getElementById("newUserEmail").value;
            var newUserContact=document.getElementById("newUserContact").value;
            var newUserDescription=document.getElementById("newUserDescription").value;
            var newUserGroup= $("#userGroupList" ).val();
            var newUserStatus= $("#statusList" ).val();

            var flag=true;

            if(newUserName.trim().length == 0){
                flag=false;
                alert('User name not found!');
            }

            if(flag){
                if(newUserPassword.trim().length == 0){
                    flag=false;
                    alert('Password not found!');
                }
            }

            if(flag){
                if(newUserGroup.trim().length == 0){
                    flag=false;
                    alert('Group not found!');
                }
            }

            if(flag){

                var param='userName='+newUserName;
                param+='&userPassword='+newUserPassword;
                param+='&userDisplayName='+newUserDisplayName;
                param+='&userEmail='+newUserEmail;
                param+='&userContact='+newUserContact;
                param+='&userDescription='+newUserDescription;
                param+='&levelId='+newUserGroup;
                param+='&userActiveStatus='+newUserStatus;
                param+='&userId='+userId;

                document.getElementById('createUserImage').src='images/ajax-loader2.gif';

                $.ajax({
                    url: "updateNewUserUSESET.bata?"+param,
                    cache: false,
                    type: "POST"
                }).done(function( html ) {
                    alert(html);
                    document.getElementById('createUserImage').src='';
                    window.location.reload(true);
                });
            }


        }
    }

</script>


<div id="main">

    <div class="divTitle"><b>User Information</b></div>

    <s:if test="rtlUserList != null">

        <s:if test="rtlUserList.size()!=0">

            <s:iterator value="rtlUserList">

                <form id="formId" action="#" method="post">

                    <table width="80%">

                        <tr>
                            <td class="divLevel" style="width: 30%; text-align: right; white-space: nowrap ;"><label style="color: red;">*</label>User Name</td>
                            <td style="width: 50%; text-align: left;">
                                <input type="hidden" id="userId" name="userId" value="<s:property value="userId"/>" >
                                <input type="text" id="newUserName" name="newUserName" value="<s:property value="userName"/>" size="35" maxlength="50" readonly >
                                <img id="newUserImage" src="" alt="" />
                            </td>
                        </tr>

                        <tr>
                            <td class="divLevel" style="text-align: right;"><label style="color: red;">*</label>Password:</td>
                            <td style="text-align: left;">
                                <input type="text" id="newUserPassword" name="newUserPassword" value="<s:property value="userPassword"/>" size="35" maxlength="15" >
                                <label id="password_strength">&nbsp;</label>
                            </td>
                        </tr>

                        <tr>
                            <td class="divLevel" style="text-align: right; white-space: nowrap;"><label style="color: red;">*</label>Display Name:</td>
                            <td style="text-align: left;">
                                <input type="text" id="newUserDisplayName" name="newUserDisplayName" value="<s:property value="displayName"/>" size="60" maxlength="250" >
                            </td>
                        </tr>

                        <tr>
                            <td class="divLevel" style="text-align: right; white-space: nowrap ;">User Email:</td>
                            <td style="text-align: left">
                                <input onchange="checkValidateEmail();" type="text" id="newUserEmail" name="newUserEmail" value="<s:property value="userEmail"/>" size="60" maxlength="100" >
                            </td>
                        </tr>

                        <tr>
                            <td class="divLevel" style="text-align: right; white-space: nowrap ;">Contact Number:</td>
                            <td style="text-align: left">
                                <input type="text" id="newUserContact" name="newUserContact" value="<s:property value="userContct"/>" size="60" maxlength="100" >
                            </td>
                        </tr>

                        <tr>
                            <td class="divLevel" style="text-align: right;">Description:</td>
                            <td style="text-align: left;">
                                <input type="text" id="newUserDescription" name="newUserDescription" value="<s:property value="description"/>" size="60" maxlength="250" >
                            </td>
                        </tr>
                        <tr>
                            <td class="divLevel" style="text-align: right"><label style="color: red;">*</label>Group:</td>
                            <td style="text-align: left">
                                <select id="userGroupList" name="userGroupList" >
                                    <option  value="-1">Select Group</option>
                                    <s:if test="rtlUserLevelList != null">
                                        <s:if test="rtlUserLevelList.size()!=0">
                                            <s:iterator value="rtlUserLevelList">
                                                <option <s:if test="rtlUserLevel.levelId==levelId">selected</s:if> value="<s:property value="levelId"/>">
                                                    <s:property value="levelName"/>
                                                </option>
                                            </s:iterator>
                                        </s:if>
                                    </s:if>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td class="divLevel" style="text-align: right">Status:</td>
                            <td style="text-align: left">
                                <select id="statusList" name="statusList" >
                                    <option <s:if test="status=='Y'">selected</s:if> value="Y">Active</option>
                                    <option <s:if test="status=='N'">selected</s:if> value="N">Inactive</option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="4" style="text-align: center">
                                <input onclick="updateNewUser();" type="button" id="saveButton" name="saveButton" value="Update" >
                                <img id="createUserImage" src="" alt="" />
                            </td>
                        </tr>

                    </table>
                </form>

            </s:iterator>

        </s:if>

    </s:if>
</div>



