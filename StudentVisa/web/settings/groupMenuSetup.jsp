<%--
    Document   : invoice
    Created on : Jun 20, 2013, 1:09:47 PM
    Author     : MIS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sx" uri="/struts-dojo-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/common.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/style.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/navi.css" />
        <script type="text/javascript"   src="<%= request.getContextPath()%>/resources/js/jquery-1.8.3.js"></script>
        <script type="text/javascript"  src="<%= request.getContextPath()%>/resources/js/jquery-ui.js"></script>
        <link rel="stylesheet" href="<%= request.getContextPath()%>/resources/css/jquery-ui.css" />
        <link rel="shortcut icon" href="<%= request.getContextPath()%>/resources/ico/favicon.png">

        <%
                    String hasSuccessful = request.getSession().getAttribute("hasSuccessful") != null ? request.getSession().getAttribute("hasSuccessful").toString().trim() : null;
                    String message = request.getSession().getAttribute("message") != null ? request.getSession().getAttribute("message").toString().trim() : null;
                    if (hasSuccessful != null) {
                        request.getSession().removeAttribute("hasSuccessful");
                        request.getSession().removeAttribute("message");
                    }
        %>
        <script type="text/javascript">

            $(function() {

            <% if (hasSuccessful != null && (hasSuccessful.equalsIgnoreCase("true") || hasSuccessful.equalsIgnoreCase("false"))) {%>
                    $('#dialog').html('<p><%out.print(message);%></p>');
                    $('#dialog').dialog({
                        title:"Result",
                        bgiframe: true,
                        height: 'auto',
                        width: 'auto',
                        modal: true,autoOpen: true,
                        buttons: { "Ok": function() {
                                $(this).dialog("close");}}
                    });

            <% }%>

                });
        </script>
        <sx:head/>
        <s:head/>

        <script type="text/javascript">

            function findGroupForm(){

                var grpcode=document.getElementById("userGroupList");
                var levelId=grpcode.options[grpcode.selectedIndex].value;

                // alert(levelId);

                document.getElementById('groupListImageId').src='images/ajax-loader2.gif';

                $.ajax({
                    url: "selectedLevelAssignedMenuBACSET.bata?levelId="+levelId,
                    cache: false,
                    type: "POST"
                }).done(function( html ) {
                    document.getElementById('groupListImageId').src='';
                    $("#selectedGroupDivId").html(html);

                });

            }

            function updateGroupForm(){

                var r = confirm("Do you want to update?");

                if (r == true) {

                    var selected = new Array();
                    $('input:checkbox').each(function() {
                        selected.push($(this).attr('id'));
                    });

                    var stng="";

                    for(var i=1;i<selected.length;i++){

                        if(document.getElementById(selected[i]).checked){
                            stng+=document.getElementById('T'+selected[i]).value+'/fd/Y/rd/'
                        }else{
                            stng+=document.getElementById('T'+selected[i]).value+'/fd/N/rd/'
                        }
                        
                    }

                    // alert(stng);
                
                    //alert($("#userGroupList" ).val());
                
                    if($("#userGroupList" ).val()!='-1'){

                        if(stng.length!=0){
                      
                            document.getElementById('groupListImageId').src='images/ajax-loader2.gif';

                            $.ajax({
                                url: "groupAssignedMenuSavedBACSET.bata?levelId="+$("#userGroupList" ).val()+"&levelMenuString="+stng,
                                cache: false,
                                type: "POST"
                            }).done(function( html ) {
                                document.getElementById('groupListImageId').src='';
                                $("#selectedGroupDivId").html(html);
                            });
                        }
                    }

                }

            }

        </script>

        <style type="text/css">
            <!--
            .style1 {color: #FF0000}
            -->
        </style>

    </head>
    <body>
        <%
                    boolean login = (request.getSession().getAttribute("logged-in") != null && request.getSession().getAttribute("logged-in").toString().trim().equals("true")) ? true : false;

                    if (!login) {%>
        <jsp:forward page="/index.jsp"></jsp:forward>
        <%                    } else {
        %>

        <div class="wrap">

            <s:include value="/menu.jsp"></s:include>

            <div id="content">

                <div id="sidebar">
                    <div class="box">
                        <s:include value="/submenu/userGroup.jsp"></s:include>
                    </div>
                </div>

                <div id="main">

                    <div class="full_w">
                        <div class="h_title"><b>Group From Setup</b></div>
                        <form id="formId" action="#" method="post">
                            <table width="100%">
                                <tr>                                    
                                    <td style="width: 25%; text-align: right">Group:</td>
                                    <td style="width: 25%; text-align: left">
                                        <select id="userGroupList" name="userGroupList" onchange="findGroupForm();" >
                                            <option  value="-1">Select Group</option>
                                            <s:if test="rtlUserLevelList != null">
                                                <s:if test="rtlUserLevelList.size()!=0">
                                                    <s:iterator value="rtlUserLevelList">
                                                        <option onclick="findGroupForm();" value="<s:property value="levelId"/>"><s:property value="levelName"/></option>
                                                    </s:iterator>
                                                </s:if>
                                            </s:if>
                                        </select>
                                        <img id="groupListImageId" src="" alt="" />
                                    </td>
                                    <td style="width: 25%;">&nbsp;</td>
                                    <td style="width: 25%;">&nbsp;</td>
                                </tr>
                            </table>
                        </form>

                        <div id="selectedGroupDivId"></div>

                    </div>

                </div>
                <div class="clear"></div>

            </div>
            <s:include value="/footer.jsp"/>
        </div>

        <% }%>
    </body>
</html>
