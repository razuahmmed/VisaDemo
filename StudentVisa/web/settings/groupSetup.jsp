<%--
    Document   : invoice
    Created on : Jun 20, 2013, 1:09:47 PM
    Author     : MIS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sx" uri="/struts-dojo-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/common.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/style.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/resources/css/navi.css" />
        <script type="text/javascript"   src="<%= request.getContextPath()%>/resources/js/jquery-1.8.3.js"></script>
        <script type="text/javascript"  src="<%= request.getContextPath()%>/resources/js/jquery-ui.js"></script>
        <link rel="stylesheet" href="<%= request.getContextPath()%>/resources/css/jquery-ui.css" />
        <link rel="shortcut icon" href="<%= request.getContextPath()%>/resources/ico/favicon.png">

        <%
                    String hasSuccessful = request.getSession().getAttribute("hasSuccessful") != null ? request.getSession().getAttribute("hasSuccessful").toString().trim() : null;
                    String message = request.getSession().getAttribute("message") != null ? request.getSession().getAttribute("message").toString().trim() : null;
                    if (hasSuccessful != null) {
                        request.getSession().removeAttribute("hasSuccessful");
                        request.getSession().removeAttribute("message");
                    }
        %>
        <script type="text/javascript">

            $(function() {

            <% if (hasSuccessful != null && (hasSuccessful.equalsIgnoreCase("true") || hasSuccessful.equalsIgnoreCase("false"))) {%>
                    $('#dialog').html('<p><%out.print(message);%></p>');
                    $('#dialog').dialog({
                        title:"Result",
                        bgiframe: true,
                        height: 'auto',
                        width: 'auto',
                        modal: true,autoOpen: true,
                        buttons: { "Ok": function() {
                                $(this).dialog("close");}}
                    });

            <% }%>

                });
        </script>
        <sx:head/>
        <s:head/>

        <script type="text/javascript">

            function checkGroupShortName(){

                var newGroupShortName=document.getElementById("groupShortName").value;

                if(newGroupShortName.trim().length > 0){
                    // alert(newComCode);
                    document.getElementById('newGroupShortNameImage').src='images/ajax-loader2.gif';

                    $.ajax({
                        url: "checkGroupShortNameBACSET.bata?groupShortName="+newGroupShortName,
                        cache: false,
                        type: "POST"
                    }).done(function( html ) {
                        if(html.trim()=='1'){
                            document.getElementById("duplicateShortName").value='1';
                            alert("Group short name already exist!");
                            document.getElementById('newGroupShortNameImage').src='images/deleteImg.jpeg';
                        }else{
                            document.getElementById("duplicateShortName").value='0';
                            document.getElementById('newGroupShortNameImage').src='images/okImg.jpeg';
                        }
                    });
                }else{
                    document.getElementById("duplicateShortName").value='-1';
                    document.getElementById('newGroupShortNameImage').src='';
                }
            }

            function checkGroupName(){

                var newGroupName=document.getElementById("groupName").value;

                if(newGroupName.trim().length > 0){
                    // alert(newComCode);
                    document.getElementById('newGroupNameImage').src='images/ajax-loader2.gif';

                    $.ajax({
                        url: "checkGroupNameBACSET.bata?groupName="+newGroupName,
                        cache: false,
                        type: "POST"
                    }).done(function( html ) {
                        if(html.trim()=='1'){
                            document.getElementById("duplicateGroupName").value='1';
                            alert("Group name already exist!");
                            document.getElementById('newGroupNameImage').src='images/deleteImg.jpeg';
                        }else{
                            document.getElementById("duplicateGroupName").value='0';
                            document.getElementById('newGroupNameImage').src='images/okImg.jpeg';
                        }
                    });
                }else{
                    document.getElementById("duplicateShortName").value='-1';
                    document.getElementById('newGroupShortNameImage').src='';
                }
            }

            function createNewGroup(){

                var r = confirm("Do you want to save?");

                if (r == true) {

                    var duplicateShortName=document.getElementById("duplicateShortName").value;
                    var duplicateGroupName=document.getElementById("duplicateGroupName").value;
                    //
                    if((duplicateShortName=="1") || (duplicateGroupName=="1")){
                        alert("Group already exist!");
                    }else if((duplicateShortName=="0") || (duplicateGroupName=="0")){

                        var newGroupShortName=document.getElementById("groupShortName").value;
                        var newGroupName=document.getElementById("groupName").value;
                        var newGroupDescription=document.getElementById("groupDescription").value;

                        var flag=true;

                        if(newGroupShortName.trim().length == 0){
                            flag=false;
                            alert('Group short name not found!');
                        }

                        if(flag){
                            if(newGroupName.trim().length == 0){
                                flag=false;
                                alert('Group name not found!');
                            }
                        }

                        if(flag){

                            var param='groupShortName='+newGroupShortName;
                            param+='&groupName='+newGroupName;
                            param+='&groupDescription='+newGroupDescription;

                            document.getElementById('createGroupImage').src='images/ajax-loader2.gif';

                            $.ajax({
                                url: "createNewGroupBACSET.bata?"+param,
                                cache: false,
                                type: "POST"
                            }).done(function( html ) {
                                alert(html);
                                document.getElementById('createGroupImage').src='';
                                window.location.reload(true);
                            });
                        }

                    }

                }
            }

            function updateGroup(groupId,str){

                var r = confirm("Do you want to update?");

                if (r == true) {
                    
                    if(groupId!=''){

                        document.getElementById('createGroupImage').src='images/ajax-loader2.gif';

                        $.ajax({
                            url: "updateNewGroupBACSET.bata?groupActiveStatus="+str+"&levelId="+groupId,
                            cache: false,
                            type: "POST"
                        }).done(function( html ) {
                            alert(html);
                            document.getElementById('createGroupImage').src='';
                            window.location.reload(true);
                        });
                    }
                }

            }

        </script>

        <style type="text/css">
            <!--
            .style1 {color: #FF0000}
            -->
        </style>

    </head>
    <body>
        <%
                    boolean login = (request.getSession().getAttribute("logged-in") != null && request.getSession().getAttribute("logged-in").toString().trim().equals("true")) ? true : false;

                    if (!login) {%>
        <jsp:forward page="/index.jsp"></jsp:forward>
        <%                    } else {
        %>

        <div class="wrap">

            <s:include value="/menu.jsp"></s:include>

            <div id="content">

                <div id="sidebar">
                    <div class="box">
                         <s:include value="/submenu/userGroup.jsp"></s:include>
                    </div>
                </div>

                <div id="main">

                    <div class="full_w">
                        <div class="h_title"><b>Group Setup</b></div>
                        <form id="formId" action="#" method="post">

                            <table style="width: 100%;">

                                <thead>
                                    <tr style="background-color: #336ea5; font-size: 12px; color: #ffffff" >                                       
                                        <th style="width: 100px;">Short Name</th>
                                        <th style="width: 200px;">Name</th>
                                        <th style="width: 300px;">Description</th>
                                        <th style="width: 50px;">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>                                        
                                        <td>                                            
                                            <input type="text" onchange="checkGroupShortName();" id="groupShortName" name="groupShortName" size="15" maxlength="10" >
                                        </td>
                                        <td>
                                            <input type="text" onchange="checkGroupName();" id="groupName" name="groupName" size="30" maxlength="10" >
                                        </td>
                                        <td>
                                            <input type="text" onclick="" id="groupDescription" name="groupDescription" size="52" maxlength="250" >
                                        </td>
                                        <td style="text-align: center;">
                                            <input  onclick="createNewGroup();" type="button" id="saveButton" name="saveButton" value="Save" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <input type="hidden" id="duplicateShortName" name="duplicateShortName" value="-1" >
                                            <img id="newGroupShortNameImage" src="" alt="" />
                                        </td>
                                        <td>
                                            <input type="hidden" id="duplicateGroupName" name="duplicateGroupName" value="-1" >
                                            <img id="newGroupNameImage" src="" alt="" />
                                        </td>
                                        <td>&nbsp;</td>
                                        <td>
                                            <img id="createGroupImage" src="" alt="" />
                                        </td>
                                    </tr>
                                </tbody>

                            </table>

                            <div id="groupDivId">

                                <table style="width: 100%;">

                                    <thead>
                                        <tr style="background-color: #336ea5; font-size: 12px; color: #ffffff" >                                           
                                            <th style="width: 100px;">Short Name</th>
                                            <th style="width: 200px;">Name</th>
                                            <th style="width: 20px;">Status</th>
                                            <th style="width: 300px;">Description</th>
                                            <th style="width: 50px;">&nbsp;</th>
                                        </tr>
                                    </thead>

                                    <s:if test="rtlUserLevelList != null">

                                        <s:if test="rtlUserLevelList.size()!=0">

                                            <tbody>

                                                <s:iterator value="rtlUserLevelList">
                                                    <tr>                                                       
                                                        <td style="padding: 5px;"><s:property value="levelShortName"/></td>
                                                        <td style="padding: 5px;"><s:property value="levelName"/></td>
                                                        <td style="padding: 5px; text-align: center;">
                                                            <s:if test="status=='Y'">Active</s:if>
                                                            <s:else>Inactive</s:else>
                                                        </td>
                                                        <td style="padding: 5px;"><s:property value="levelDescription"/></td>
                                                        <td style="padding: 5px; text-align: center;">
                                                            <s:if test="status=='Y'">
                                                                <a href="javascript:void(0)" onclick="updateGroup('<s:property value="levelId"/>','N');" >Inactive</a>
                                                            </s:if>
                                                            <s:else>
                                                                <a href="javascript:void(0)" onclick="updateGroup('<s:property value="levelId"/>','Y');" >Active</a>
                                                            </s:else>
                                                        </td>
                                                    </tr>

                                                </s:iterator>

                                            </tbody>

                                        </s:if>

                                    </s:if>
                                </table>

                            </div>

                        </form>

                    </div>

                </div>
                <div class="clear"></div>

            </div>
            <s:include value="/footer.jsp"/>
        </div>

        <% }%>
    </body>
</html>
