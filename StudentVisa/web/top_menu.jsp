
<%
            String userName = (request.getSession().getAttribute("userName") != null) ? request.getSession().getAttribute("userName").toString() : "";
            //
            boolean PM01 = (request.getSession().getAttribute("PM01") != null && request.getSession().getAttribute("PM01").toString().equalsIgnoreCase("T")) ? true : false;
            boolean PM01CM01 = (request.getSession().getAttribute("PM01CM01") != null && request.getSession().getAttribute("PM01CM01").toString().equalsIgnoreCase("T")) ? true : false;
            boolean PM01CM02 = (request.getSession().getAttribute("PM01CM02") != null && request.getSession().getAttribute("PM01CM02").toString().equalsIgnoreCase("T")) ? true : false;
            boolean PM01CM03 = (request.getSession().getAttribute("PM01CM03") != null && request.getSession().getAttribute("PM01CM03").toString().equalsIgnoreCase("T")) ? true : false;
            boolean PM01CM04 = (request.getSession().getAttribute("PM01CM04") != null && request.getSession().getAttribute("PM01CM04").toString().equalsIgnoreCase("T")) ? true : false;
            //
            boolean PM02 = (request.getSession().getAttribute("PM02") != null && request.getSession().getAttribute("PM02").toString().equalsIgnoreCase("T")) ? true : false;
            boolean PM02CM01 = (request.getSession().getAttribute("PM02CM01") != null && request.getSession().getAttribute("PM02CM01").toString().equalsIgnoreCase("T")) ? true : false;
            boolean PM02CM02 = (request.getSession().getAttribute("PM02CM02") != null && request.getSession().getAttribute("PM02CM02").toString().equalsIgnoreCase("T")) ? true : false;
            boolean PM02CM03 = (request.getSession().getAttribute("PM02CM03") != null && request.getSession().getAttribute("PM02CM03").toString().equalsIgnoreCase("T")) ? true : false;

%>

<div class="header">

    <div class="orange_tab">

        <div class="head_right">         

            <% if (userName.isEmpty()) {%>
            <div class="h_lgin">
                <a href="footinSignInLog.footin">
                    <img alt="Log In" src="<%= request.getContextPath()%>/resources/images/login.png"/>
                </a>
            </div>
            <% }%>
            <% if (!userName.isEmpty()) {%>
            <div class="h_lgout">
                <a href="footinSignOutLog.footin">
                    <img alt="Log In" src="<%= request.getContextPath()%>/resources/images/logout.png"/>
                </a>
            </div>
            <% }%>

        </div>

        <div class="h_un">
            <% out.println(userName);%>
        </div>

    </div>

    <!-- ==============================Mini  NavBar============================== -->
    <div class="mini_nav_ctrl">
        <button class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse" style="">
            <span class="icon-bar" style="background-color:#333;"></span>
            <span class="icon-bar" style="background-color:#333;"></span>
            <span class="icon-bar" style="background-color:#333;"></span>
        </button>
    </div>
    <!-- ==================================================================== -->
    <div class="head_tab">

        <div class="head_left">

            <div class="logo">
                <a class="img_res" href="#">
                    <img alt="" src="<%= request.getContextPath()%>/resources/images/logo.png"/>
                </a>
            </div>

            <div id="cssmenu" class="menu_ctrll">

                <ul>
                    <li><a href='#'>Home</a></li> 
                    <li><a href='#'>ABOUT</a></li>
                    <li class='active'><a href="javascript:void(0);">PRODUCT</a>
                        <ul>
                            <li><a href='#'>WOMEN</a></li>
                            <li><a href='#'>MEN</a></li>
                            <li><a href='#'>ACCESSORIES</a></li>
                        </ul>
                    </li>
                    <li><a href='#'>NEWS</a></li>
                    <% if (PM01) {%>
                    <li class='active'><a href="javascript:void(0);">ORDER MANAGEMENT</a>
                        <ul>
                            <% if (PM01CM01) {%>
                            <li><a href="orderDashboardOdr.footin">ORDER DASHBOARD</a></li>
                            <% }%>
                            <% if (PM01CM02) {%>
                            <li><a href="newOrderForAdminOdr.footin">NEW ORDER</a></li>
                            <% }%>
                            <% if (PM01CM03) {%>
                            <li><a href="promotionDashboardOdr.footin">PROMOTION</a></li>
                            <% }%>
                            <% if (PM01CM04) {%>
                            <li><a href="remittanceProcessOdr.footin">REMITTANCE PROCESS</a></li>
                            <% }%>
                        </ul>
                    </li>
                    <% }%>
                    <li><a href='#'>CONTACT</a></li>

                    <% if (PM02) {%>
                    <li class='active'><a href="javascript:void(0);">MY ACCOUNT</a>
                        <ul>
                            <% if (PM02CM01) {%>
                            <li><a href="customerMyProfileMya.footin">MY PROFILE</a></li>
                            <% }%>
                            <% if (PM02CM02) {%>
                            <li><a href="customerMyOrderMya.footin">MY ORDERS</a></li>
                            <% }%>
                            <% if (PM02CM03) {%>
                            <li><a href="customerLoyaltyPointsMya.footin">My POINTS</a></li>
                            <% }%>
                        </ul>
                    </li>
                    <% }%>
                </ul>

            </div>

        </div>

    </div>

</div>


