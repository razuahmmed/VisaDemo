<%-- 
    Document   : edituser
    Created on : Jan 6, 2013, 5:01:14 AM
    Author     : MIS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sx" uri="/struts-dojo-tags" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit User Information</title>
        <script type="text/javascript"  src="<%= request.getContextPath() %>/resources/js/jquery-1.8.3.js"></script>
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/resources/css/common.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/resources/css/style.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/resources/css/navi.css" />
        <script type="text/javascript"   src="<%= request.getContextPath() %>/resources/js/jquery-1.8.3.js"></script>
        <script type="text/javascript"  src="<%= request.getContextPath() %>/resources/js/jquery-ui.js"></script>
        <link rel="stylesheet" href="<%= request.getContextPath() %>/resources/css/jquery-ui.css" />
        <link rel="shortcut icon" href="<%= request.getContextPath() %>/resources/ico/favicon.png">
        <script type="text/javascript">
          $(window).load(function() { $("#spinner").fadeOut("slow"); })
          $(function() {
            $( "#datepicker" ).datepicker();
                $(".box .h_title").not(this).next("ul").hide("normal");
                $(".box .h_title").not(this).next("#home").show("normal");
                $(".box").children(".h_title").click( function() { $(this).next("ul").slideToggle(); });
          });
        </script>
        <sx:head/>
        <s:head/>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#passDivId").live("click",function(){
                    $('#passwordDivId').toggle();
                    
                });
            });
        </script>
    </head>
    <body>
    <%
        boolean login = (request.getSession().getAttribute("logged-in")!=null  && request.getSession().getAttribute("logged-in").toString().trim().equals("true")) ? true: false;
        if(!login){%>
        <jsp:forward page="/index.jsp"></jsp:forward>
        <%
        }else {
    %>
    <div class="wrap">
        <s:include value="/menu.jsp"></s:include>
        <div id="content">
            <div id="sidebar">
                <div class="box">
                    <div class="h_title"><b>&#8250;&nbsp;User Info</b></div>
                    <ul id="home">
                        <li class="b1"><a class="icon add_user" href="NewUserInfoStep1">New User</a></li>
                        <li class="b1"><a class="icon add_user" href="NewAdminUserInfo">New Admin User</a></li>
                        <li class="b1"><a class="icon add_user" href="NewOtherUserInfoStep1">New Other User</a></li>
                        <li class="b1"><a class="icon users" href="GetAllUsers">Show All Users</a></li>
                        <li class="b1"><a class="icon search" href="SearchUser">Search</a></li>
                    </ul>
                </div>
                <div class="box">
                    <div class="h_title"><b>Calendar</b></div>
                    <ul id="home">
                    <div id="datepicker"></div>
                    </ul>
                </div>
            </div>
            <div id="main">
                <div class="full_w">
                    <div class="h_title"><b>Edit User Information</b></div>
                    <div id="spinner"></div>
                    <br/>
                    <div style="color:red; width: 50%" align="left">
                        <s:actionerror/>
                    </div>
                    <form id="cancelFormId" action="ListUser" method="POST">
                    </form>
                    <s:form id="editUserFormId" action="UpdateUserInfo" method="POST">
                    <s:hidden name="user.userId" value="%{user.userId}"/>
                    <s:token/>
                    <table><tr><td>
                        <table style="padding-left:2%" >
                            <s:textfield theme="xhtml" name="user.userName" label="User Name" size="40"/>
                            <s:select theme="xhtml" list="rtlUserLevels" listKey="levelId" listValue="levelName" name="user.rtlUserLevel.levelId" label="User Role" cssStyle="width:225px;" emptyOption="true" required="true" requiredposition="left"/>
                            <s:textfield theme="xhtml" name="user.saoCode" label="%{user.rtlUserLevel.levelName} Code"  size="40" />
                            <s:textarea theme="xhtml" name="user.description" label="Description" rows="6" cols="40" cssStyle="resize:none;" />
                            <%--<s:select theme="xhtml" name="user.status" label="Active" list="#{'Y':'Yes','N':'No','':''}" />--%>
                            <tr><td></td><td align="right"><a id="passDivId" href="#">change password</a></td></tr>
                        </table>
                        </td></tr>
                        <tr><td>
                            <div id="passwordDivId" style="display: none">
                                <table>
                                <s:password theme="xhtml" name="OldPassword" label="Old Password" size="40"/>
                                <s:password theme="xhtml" name="newPassword" label="New Password" size="40"/>
                                </table>
                            </div>
                        </td></tr>
                        </table>
                        <div class="divcenter" style="margin-left: 49%;">
                            <a id="save" href="#" class="button" onclick="var answer=confirm('Do you want to save it?'); if(answer==true){$('#editUserFormId').submit();}" ><span>Save</span></a>
                            <a href="#" onclick="var answer=confirm('Do you want to cancel it?'); if(answer==true){$('#cancelFormId').submit();}" class="button"><span>Cancel</span></a>
                        </div>
                    </s:form>
                </div>
            </div>
            <div class="clear"></div>
	</div>

	<s:include value="/footer.jsp"/>
    </div>
    <% } %>
    </body>
</html>
