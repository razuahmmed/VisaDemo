<%--
    Document   : operation
    Created on : Dec 27, 2012, 3:28:32 PM
    Author     : MIS-Monjur
--%>

<%@page import="java.util.Map"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sx" uri="/struts-dojo-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE html>
<html>
<head>
<script type="text/javascript"  src="<%= request.getContextPath() %>/resources/js/jquery-1.8.3.js"></script>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/resources/css/common.css" />
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/resources/css/style.css" />
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/resources/css/navi.css" />
<script type="text/javascript"   src="<%= request.getContextPath() %>/resources/js/jquery-1.8.3.js"></script>
<script type="text/javascript"  src="<%= request.getContextPath() %>/resources/js/jquery-ui.js"></script>
<link rel="stylesheet" href="<%= request.getContextPath() %>/resources/css/jquery-ui.css" />
<link rel="shortcut icon" href="<%= request.getContextPath() %>/resources/ico/favicon.png">
<script type="text/javascript">
  $(window).load(function() { $("#spinner").fadeOut("slow"); })
  $(function() {
    $( "#datepicker" ).datepicker();
        $(".box .h_title").not(this).next("ul").hide("normal");
        $(".box .h_title").not(this).next("#home").show("normal");
        $(".box").children(".h_title").click( function() { $(this).next("ul").slideToggle(); });
  });
</script>
<sx:head/>
<s:head/>
<script type="text/javascript"  >
    dhtmlx.skin = "dhx_skyblue";
</script>
<script type="text/javascript">
    $(document).ready(function($){

        $("#search").live("click",function(){
            var isValidate = true;
            if($("#userNameId").val().length == 0){
                $("#errorMessageDiv").html("<div class=\"n_warning\"><p>Warning: Please Type a valid User Name</p></div>");
                isValidate = false;
            }
            if(isValidate){
                $('#userSearchFormId').submit();
            }
        });
    });
</script>
</head>
<body>
<%
      boolean login = (request.getSession().getAttribute("logged-in")!=null  && request.getSession().getAttribute("logged-in").toString().trim().equals("true")) ? true: false;
      boolean isAdmin = (request.getSession().getAttribute("isAdmin")!=null && request.getSession().getAttribute("isAdmin").toString().equalsIgnoreCase("true"))? true:false;
      boolean isStore = (request.getSession().getAttribute("isStore")!=null && request.getSession().getAttribute("isStore").toString().equalsIgnoreCase("true"))? true:false;
      boolean isArea = (request.getSession().getAttribute("isArea")!=null && request.getSession().getAttribute("isArea").toString().equalsIgnoreCase("true"))? true:false;
      boolean isOperation = (request.getSession().getAttribute("isOperation")!=null && request.getSession().getAttribute("isOperation").toString().equalsIgnoreCase("true"))? true:false;
      if(!login){%>
      <jsp:forward page="/index.jsp"></jsp:forward>
<%
      }else {
%>
 <div class="wrap">
        <s:include value="/menu.jsp"></s:include>
        <div id="content">
            <div id="sidebar">
                <div class="box">
                    <div class="h_title"><b>&#8250;&nbsp;User Info</b></div>
                    <ul id="home">
                        <li class="b1"><a class="icon add_user" href="NewUserInfoStep1">New User</a></li>
                        <li class="b1"><a class="icon add_user" href="NewAdminUserInfo">New Admin User</a></li>
                        <li class="b1"><a class="icon add_user" href="NewOtherUserInfoStep1">New Other User</a></li>
                        <li class="b1"><a class="icon users" href="GetAllUsers">Show All Users</a></li>
                        <li class="b1"><a class="icon search" href="SearchUser">Search</a></li>
                    </ul>
                </div>
                <div class="box">
                    <div class="h_title"><b>Calendar</b></div>
                    <ul id="home">
                    <div id="datepicker"></div>
                    </ul>
                </div>
            </div>
            <div id="main">
                <div class="full_w">
                    <div class="h_title"><b>Search</b></div>
                    <div id="spinner"></div>
                    <form id="cancelFormId" action="ListUser" method="POST">
                    </form>
                    <div id="searchDiv" style="width: 90% ">
                        <form id="userSearchFormId" action="SearchUserInfo" method="post">
                            <table >
                              <tr><td>
                                  <table>
                                        <s:textfield id="userNameId" theme="xhtml" name="user.userName" label="User Name" size="30"/>
                                  </table>
                             </tr>
                            </table>
                            <div class="divcenter" style="margin-left: 43%;">
                                <a id="search" href="#" class="button"><span>Search</span></a>
                                <a href="#" onclick="var answer=confirm('Do you want to cancel it?'); if(answer==true){$('#cancelFormId').submit();}" class="button"><span>Cancel</span></a>
                            </div>
                         </form>
                         <div id="errorMessageDiv"></div>
                    </div>
                    
                    <br/>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="clear"></div>
	</div>

	<s:include value="/footer.jsp"/>
    </div>
<% } %>

</body>
</html>

