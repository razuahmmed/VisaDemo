
<%@page import="mis.drd.action.ParamUtil"%>
<!DOCTYPE html>
<%@taglib  prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html;" charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <title>FootIn</title>

        <link href="<%= request.getContextPath()%>/resources/css/style.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/bootstrap.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/yamm.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/style_menu.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/resources/css/jquery.dataTables.css" rel="stylesheet" />

    </head>

    <style type="text/css">
        <!--
        #example th {
            /* color: #E06C0A; 009999 */
            background: none repeat scroll 0 0 #E06C0A;
            color: #FFFFFF;
            font-size: 12px;
            height: 22px;
            vertical-align: middle;
            padding: 2px;
            text-align: center;
            border: 1px solid #FFFFFF;
        }

        #example td {
            vertical-align: middle;
        }

        -->
    </style>


    <body>

        <jsp:include page="/top_menu.jsp" flush="true"></jsp:include>

        <div id="content">

            <div id="sidebar">

                <div class="box">
                    <jsp:include page="/submenu/myaccount.jsp" flush="true"></jsp:include>
                </div>

            </div>

            <div id="middlecontent">

                <div class="div_title"><b>MY PROFILE</b></div>

                <div id="my_point_div">

                    <table style="width: 80%;">

                        <tr>
                            <td width="10%"><label class="label_bh1">Customer ID</label></td>
                            <td width="1%">&nbsp;</td>
                            <td width="29%" colspan="3" class="txt2_cust_tb">
                                <input type="text" id="customerId" name="customerId" class="form-control" style="width: 250px;" readonly />
                                <img id="customerSearchImage" src="" alt="" />
                            </td>
                            <td width="10%"><label class="label_bh1">Contact</label></td>
                            <td width="1%"><label class="label_bh1" style="color: red;">*</label></td>
                            <td width="29%" colspan="3" class="txt2_cust_tb">
                                <input type="text" id="contactNumber" name="contactNumber" class="form-control" readonly/>
                            </td>
                        </tr>

                        <tr>
                            <td><label class="label_bh1">First Name</label></td>
                            <td>&nbsp;</td>
                            <td colspan="3" class="txt2_cust_tb">
                                <input type="text" id="firstName" name="firstName" class="form-control" maxlength="200"/>
                            </td>
                            <td><label class="label_bh1">Middle Name</label></td>
                            <td>&nbsp;</td>
                            <td colspan="3" class="txt2_cust_tb">
                                <input type="text" id="middleName" name="middleName" class="form-control" maxlength="200"/>
                            </td>
                        </tr>

                        <tr>
                            <td><label class="label_bh1">Last Name</label></td>
                            <td>&nbsp;</td>
                            <td colspan="3" class="txt2_cust_tb">
                                <input type="text" id="lastName" name="lastName" class="form-control" maxlength="200"/>
                            </td>
                            <td><label class="label_bh1">Surname</label></td>
                            <td>&nbsp;</td>
                            <td colspan="3" class="txt2_cust_tb">
                                <input type="text" id="sureName" name="sureName" class="form-control" maxlength="200"/>
                            </td>
                        </tr>

                        <tr>
                            <td><label class="label_bh1">Email Address</label></td>
                            <td>&nbsp;</td>
                            <td colspan="3" class="txt2_cust_tb">
                                <input type="text" id="emailAddress" name="emailAddress" class="form-control" maxlength="50"/>
                            </td>
                            <td><label class="label_bh1">Occupation</label></td>
                            <td>&nbsp;</td>
                            <td colspan="3" class="txt2_cust_tb">
                                <input id="customerOccupation" name="customerOccupation" class="form-control" maxlength="250" type="text"/>
                            </td>
                        </tr>

                        <tr>
                            <td><label class="label_bh1">Country</label></td>
                            <td><label class="label_bh1" style="color: red;">*</label></td>
                            <td colspan="3" class="txt2_cust_tb">
                                <select id="countryList" name="countryList" class="form-control" style="width: 200px;" >
                                    <option value="0">SELECT</option>
                                    <s:if test="ftnCountryList != null">
                                        <s:if test="ftnCountryList.size()!=0">
                                            <s:iterator value="ftnCountryList">
                                                <option value="<s:property value="countryCode"/>"><s:property value="countryName"/></option>
                                            </s:iterator>
                                        </s:if>
                                    </s:if>
                                </select>
                                <img id="countryListImage" src="" alt="" />
                            </td>

                            <td><label class="label_bh1">State</label></td>
                            <td>&nbsp;</td>
                            <td colspan="3" class="txt2_cust_tb">
                                <select id="stateList" name="stateList" class="form-control" style="width: 200px;" >
                                </select>
                                <img id="stateListImage" src="" alt="" />
                            </td>
                        </tr>

                        <tr>
                            <td><label class="label_bh1">City</label></td>
                            <td>&nbsp;</td>
                            <td colspan="3" class="txt2_cust_tb">
                                <select id="cityList" name="cityList" class="form-control" style="width: 200px;" >
                                </select>
                            </td>
                            <td><label class="label_bh1">Postal Code</label></td>
                            <td>&nbsp;</td>
                            <td colspan="3" class="txt2_cust_tb">
                                <input id="postalCode" name="postalCode" class="form-control" style="width: 100px;" maxlength="250" type="text"/>
                            </td>
                        </tr>

                        <tr>
                            <td valign="top"><label class="label_bh1">Address</label></td>
                            <td valign="top">&nbsp;</td>
                            <td colspan="3" class="txt2_cust_tb">
                                <textarea cols="20" rows="3"  id="customerAddress" name="customerAddress" class="form-control" style="resize:none;" ></textarea>
                            </td>
                            <td valign="top"><label class="label_bh1">Shipping</label></td>
                            <td valign="top"><label class="label_bh1" style="color: red;">*</label></td>
                            <td colspan="3" class="txt2_cust_tb">
                                <textarea cols="20" rows="3"  id="shippingAddress" name="shippingAddress" class="form-control" style="resize:none;" ></textarea>
                            </td>
                        </tr>

                        <tr>
                            <td><label class="label_bh1">DOB</label></td>
                            <td>&nbsp;</td>
                            <td class="txt2_cust_tb">
                                <select id="dateOfBirthDayList" name="dateOfBirthDayList" class="form-control" style="width: 90px;" >
                                    <option value="0">Day</option>
                                    <option value="1">01</option>
                                    <option value="2">02</option>
                                    <option value="3">03</option>
                                    <option value="4">04</option>
                                    <option value="5">05</option>
                                    <option value="6">06</option>
                                    <option value="7">07</option>
                                    <option value="8">08</option>
                                    <option value="9">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>
                            </td>
                            <td class="txt2_cust_tb">
                                <select id="dateOfBirthMonthList" name="dateOfBirthMonthList" class="form-control" style="width: 125px;" >
                                    <option value="0">Month</option>
                                    <option value="1">January</option>
                                    <option value="2">February</option>
                                    <option value="3">March</option>
                                    <option value="4">April</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8">August</option>
                                    <option value="9">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                            </td>
                            <td class="txt2_cust_tb">
                                <select id="dateOfBirthYearList" name="dateOfBirthYearList" class="form-control" style="width: 90px;" >
                                    <option value="0">Year</option>
                                    <s:if test="ftnDobYearList != null">
                                        <s:if test="ftnDobYearList.size()!=0">
                                            <s:iterator value="ftnDobYearList">
                                                <option value="<s:property value="calYear"/>"><s:property value="calYear"/></option>
                                            </s:iterator>
                                        </s:if>
                                    </s:if>
                                </select>
                            </td>

                            <td><label class="label_bh1">Type</label></td>
                            <td>&nbsp;</td>
                            <td colspan="3" class="txt2_cust_tb">
                                <select id="customerTypeList" name="customerTypeList" class="form-control" style="width: 150px;" >
                                    <option value="Standard">STANDARD</option>
                                    <option value="Silver">SILVER</option>
                                    <option value="Gold">GOLD</option>
                                    <option value="Platinum">PLATINUM</option>
                                </select>
                            </td>

                        </tr>

                        <tr>
                            <td><label class="label_bh1">Gender</label></td>
                            <td>&nbsp;</td>
                            <td colspan="3" class="txt2_cust_tb">
                                <select id="genderList" name="genderList" class="form-control" style="width: 150px;" >
                                    <option value="0">SELECT</option>
                                    <option value="M">MALE</option>
                                    <option value="F">FEMALE</option>
                                    <option value="O">OTHER</option>
                                </select>
                            </td>
                            <td><label class="label_bh1">Religion</label></td>
                            <td>&nbsp;</td>
                            <td colspan="3" class="txt2_cust_tb">
                                <select id="religionList" name="religionList" class="form-control" style="width: 150px;" >
                                    <option value="0">SELECT</option>
                                    <option value="I">ISLAM</option>
                                    <option value="H">HINDU</option>
                                    <option value="B">BUDDHA</option>
                                    <option value="C">CHRISTIAN</option>
                                    <option value="O">OTHER</option>
                                </select>
                            </td>


                        </tr>

                        <tr>

                            <td><label class="label_bh1">Marital Status</label></td>
                            <td>&nbsp;</td>
                            <td colspan="3" class="txt2_cust_tb">
                                <select id="marritalList" name="marritalList" class="form-control" style="width: 150px;" >
                                    <option value="0">SELECT</option>
                                    <option value="M">MARRIED</option>
                                    <option value="U">UNMARRIED</option>
                                    <option value="S">SINGLE</option>
                                    <option value="W">WIDOW</option>
                                </select>
                            </td>
                            <td><label class="label_bh1">Marriage Date</label></td>
                            <td>&nbsp;</td>
                            <td class="txt2_cust_tb">
                                <select id="marriageDayList" name="marriageDayList" class="form-control" style="width: 90px;" >
                                    <option value="0">Day</option>
                                    <option value="1">01</option>
                                    <option value="2">02</option>
                                    <option value="3">03</option>
                                    <option value="4">04</option>
                                    <option value="5">05</option>
                                    <option value="6">06</option>
                                    <option value="7">07</option>
                                    <option value="8">08</option>
                                    <option value="9">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>
                            </td>
                            <td class="txt2_cust_tb">
                                <select id="marriageMonthList" name="marriageMonthList" class="form-control" style="width: 125px;" >
                                    <option value="0">Month</option>
                                    <option value="1">January</option>
                                    <option value="2">February</option>
                                    <option value="3">March</option>
                                    <option value="4">April</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8">August</option>
                                    <option value="9">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                            </td>
                            <td class="txt2_cust_tb">
                                <select id="marriageYearList" name="marriageYearList" class="form-control" style="width: 90px;" >
                                    <option value="0">Year</option>
                                    <s:if test="ftnDobYearList != null">
                                        <s:if test="ftnDobYearList.size()!=0">
                                            <s:iterator value="ftnDobYearList">
                                                <option value="<s:property value="calYear"/>"><s:property value="calYear"/></option>
                                            </s:iterator>
                                        </s:if>
                                    </s:if>
                                </select>
                            </td>
                        </tr>


                    </table>

                </div>

            </div>

        </div>

        <jsp:include page="/footer.jsp" flush="true"></jsp:include>

        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery-1.8.3.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery-ui.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/bootstrap.min.js"></script>


        <script type="text/javascript">

            function ftnCustomerSearch(){

                document.getElementById('customerSearchImage').src='images/ajax-loader2.gif';

                $.ajax({
                    type: "POST",
                    url: "customerProfileDetailsMya.footin?",
                    success: function(data){

                        document.getElementById('customerSearchImage').src='';

                        var dataArray = data.trim().split("<FD>");

                        // generate state lob
                        if(dataArray[0].trim() != ""){
                            var selectState = $('#stateList');
                            selectState.find('option').remove();
                            var jsonState = $.parseJSON(dataArray[0].trim());
                            $('<option>').val("0").text("SELECT").appendTo(selectState);
                            for (var i=0; i< jsonState.length; i++){
                                $('<option>').val(jsonState[i].name).text(jsonState[i].name).appendTo(selectState);
                            }
                        }
                        // generate city lob
                        if(dataArray[1].trim() != ""){
                            var selectCity = $('#cityList');
                            selectCity.find('option').remove();
                            var jsonCity = $.parseJSON(dataArray[1].trim());
                            $('<option>').val("0").text("SELECT").appendTo(selectCity);
                            for (var i=0; i< jsonCity.length; i++){
                                $('<option>').val(jsonCity[i].name).text(jsonCity[i].name).appendTo(selectCity);
                            }
                        }

                        // generate customer details data

                        if(dataArray[2].trim() != ""){

                            if(dataArray[2].trim() != "[]"){

                                var json = $.parseJSON(dataArray[2].trim());

                                var d, m, y, dateArry;

                                for (var i=0; i< json.length; i++){

                                    document.getElementById("customerId").value = json[i].custid;
                                    document.getElementById("contactNumber").value = json[i].contactno;
                                    document.getElementById("emailAddress").value = json[i].email;
                                    document.getElementById("firstName").value = json[i].fname;
                                    document.getElementById("middleName").value = json[i].mname;
                                    //document.getElementById("dateOfBirth").value = json[i].dob;
                                    //alert(json[i].dob);

                                    if(json[i].dob !=""){

                                        dateArry = json[i].dob.trim().split("/");

                                        d = dateArry[0];
                                        m = dateArry[1];
                                        y = dateArry[2];

                                        if(d.trim() == ''){
                                            $("#dateOfBirthDayList").val('0');
                                        }else{
                                            //$("#countryList").val(json[i].country.trim());
                                            $("#dateOfBirthDayList option:contains(" + d.trim() + ")").attr('selected', 'selected');
                                        }

                                        if(m.trim() == ''){
                                            $("#dateOfBirthMonthList").val('0');
                                        }else{
                                            $("#dateOfBirthMonthList").val(parseInt(m.trim()));
                                            //$("#dateOfBirthMonthList option:contains(" + m.trim() + ")").attr('selected', 'selected');
                                        }

                                        if(y.trim() == ''){
                                            $("#dateOfBirthYearList").val('0');
                                        }else{
                                            //$("#countryList").val(json[i].country.trim());
                                            $("#dateOfBirthYearList option:contains(" + y.trim() + ")").attr('selected', 'selected');
                                        }
                                    }

                                    //
                                    document.getElementById("lastName").value = json[i].lname;
                                    document.getElementById("sureName").value = json[i].sname;
                                    document.getElementById("postalCode").value = json[i].pscode;
                                    document.getElementById("customerAddress").value = json[i].add;
                                    document.getElementById("shippingAddress").value = json[i].sadd;
                                    document.getElementById("customerOccupation").value = json[i].occupation;

                                    //alert(json[i].country.trim());
                                    
                                    //
                                    if(json[i].country.trim() == ''){
                                        $("#countryList").val('0');
                                    }else{
                                        //$("#countryList").val(json[i].country.trim());
                                        $("#countryList option:contains(" + json[i].country.trim() + ")").attr('selected', 'selected');
                                    }
                                    if(json[i].state.trim() == ''){
                                        $("#stateList").val('0');
                                    }else{
                                        $("#stateList").val(json[i].state.trim());
                                    }
                                    if(json[i].city.trim() == ''){
                                        $("#cityList").val('0');
                                    }else{
                                        $("#cityList").val(json[i].city.trim());
                                    }
                                    //
                                    if(json[i].state.trim() == ''){
                                        $("#customerTypeList").val('Standard');
                                    }else{
                                        $("#customerTypeList").val(json[i].customertype.trim());
                                    }
                                    //
                                    if(json[i].gender.trim() == ''){
                                        $("#genderList").val('0');
                                    }else{
                                        $("#genderList").val(json[i].gender.trim());
                                    }

                                    if(json[i].religion.trim() == ''){
                                        $("#religionList").val('0');
                                    }else{
                                        $("#religionList").val(json[i].religion.trim());
                                    }

                                    if(json[i].mrstatus.trim() == ''){
                                        $("#marritalList").val('0');
                                    }else{
                                        $("#marritalList").val(json[i].mrstatus.trim());
                                    }
                                }

                            }
                        }

                    }
                });
                
            }
       
        </script>

        <script type="text/javascript">
            
            $(document).ready(function($) {

                $("#countryList").change(function(event) {

                    var country = $("select#countryList").val();

                    if(country =="0"){
                        var select = $('#stateList');
                        select.find('option').remove();
                        //
                        var select2 = $('#cityList');
                        select2.find('option').remove();
                    }else{

                        document.getElementById('countryListImage').src='images/ajax-loader2.gif';

                        $.ajax({
                            type: "POST",
                            url: "countryStateOdr.footin?countryCode="+country,
                            success: function(data){
                                document.getElementById('countryListImage').src='';
                                var select = $('#stateList');
                                select.find('option').remove();
                                var json = $.parseJSON(data);
                                $('<option>').val("0").text("SELECT").appendTo(select);
                                for (var i=0; i< json.length; i++){
                                    $('<option>').val(json[i].name).text(json[i].name).appendTo(select);
                                }
                            }
                        });

                    }

                });

                // State List change lisenner

                $("#stateList").change(function(event) {

                    var country = $("select#countryList").val();
                    var state = $("select#stateList").val();

                    if(state =="0"){
                        var select = $('#cityList');
                        select.find('option').remove();
                    }else{

                        document.getElementById('stateListImage').src='images/ajax-loader2.gif';

                        $.ajax({
                            type: "POST",
                            url: "countryStateCityOdr.footin?countryCode="+country+"&stateName="+state,
                            success: function(data){
                                document.getElementById('stateListImage').src='';
                                var select = $('#cityList');
                                select.find('option').remove();
                                var json = $.parseJSON(data);
                                $('<option>').val("0").text("SELECT").appendTo(select);
                                for (var i=0; i< json.length; i++){
                                    $('<option>').val(json[i].name).text(json[i].name).appendTo(select);
                                }
                            }
                        });

                    }

                });
                
                ftnCustomerSearch();

            });

        </script>


    </body>

</html>
