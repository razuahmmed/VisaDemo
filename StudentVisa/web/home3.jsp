<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>
        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

        <!--FOR COMPANY ICON-->
        <link href="<%= request.getContextPath()%>/my-css/company.css" rel="stylesheet" type="text/css"/>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!-- PAGE HEADER -->
        <%@include file="header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="page-title">
                                Home<small>&nbsp;Home&nbsp;Dashboard</small>
                            </h3>
                        </div>
                    </div>

                    <!-- BEGIN PAGE CONTENT-->

                    <!-- PAGE MARQUEE-->
                    <%@include file="marquee.jsp" %>

                    <div id="ms<s:property value="mes"/>" class="alert alert-<s:property value="messageColor"/> fade in">
                        <button class="close" data-dismiss="alert">
                            ×
                        </button>
                        <i class="fa-fw fa fa-times"></i>
                        <s:property value="messageString"/>
                    </div>

                    <!--return message div-->
                    <div id="message">

                    </div>

                    <div class="row">
                        <div class=" col-md-12" style="margin-bottom: 5px;">
                            <% if (PM03CM01 || PM04CM01) {%>
                            <% if (PERMISSION_MR || PERMISSION_MM) {%>
                            <h4>Quick&nbsp;Access</h4>
                            <% if (PM03CM01) {%>
                            <% if (PERMISSION_MR) {%>
                            <a href="SendMobileRecharge" class="icon-btn">
                                <i class="icon-paper-plane"></i>
                                <div>Send&nbsp;MR</div>
                            </a>
                            <a href="MobileRechargeHistory" class="icon-btn">
                                <i class="icon-list"></i>
                                <div>MR&nbsp;History</div>
                            </a>
                            <% }%>
                            <% }%>

                            <% if (PM04CM01) {%>
                            <% if (PERMISSION_MM) {%>
                            <a href="SendMobileMoney" class="icon-btn">
                                <i class="icon-wallet"></i>
                                <div>Send&nbsp;MM</div>
                            </a>
                            <a href="MobileMoneyHistory" class="icon-btn">
                                <i class="icon-list"></i>
                                <div>MM&nbsp;History</div>
                            </a>
                            <% }%>
                            <% }%>
                            <% }%>
                            <% }%>

                            <% if (groupId < 5) {%>
                            <a href="Payments" class="icon-btn">
                                <i class="icon-credit-card"></i>
                                <div>Payments</div>
                            </a>
                            <a href="ShowAllResellers" class="icon-btn">
                                <i class="icon-users"></i>
                                <div>Resellers</div>
                            </a>

                            <% if (groupId == 0) {%>
                            <a href="AddResellerSubAdmin" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% if (groupId == 1) {%>
                            <a href="AddReseller4" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% if (groupId == 2) {%>
                            <a href="AddReseller3" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% if (groupId == 3) {%>
                            <a href="AddReseller2" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% if (groupId == 4) {%>
                            <a href="AddReseller1" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% }%>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12" style="margin-bottom: 5px;">
                            <hr/>
                            <% if (PM03CM01 || PM04CM01) {%>
                            <% if (PERMISSION_MR || PERMISSION_MM) {%>

                            <% if (groupId < 5) {%>
                            <% if (PM03CM01) {%>
                            <% if (PERMISSION_MR) {%>
                            <h4>Total&nbsp;Resellers&nbsp;Mobile&nbsp;Recharge&nbsp;Balance&nbsp;(&nbsp;All&nbsp;Resellers&nbsp;Created&nbsp;by&nbsp;<% out.println(userID);%>)&nbsp;:<strong>&nbsp;BDT&nbsp;<s:property value="totalResellerMrBal"/></strong></h4>
                            <% }%>
                            <% }%>
                            <% if (PM04CM01) {%>
                            <% if (PERMISSION_MM) {%>
                            <h4>Total&nbsp;Resellers&nbsp;Mobile&nbsp;Money&nbsp;Balance&nbsp;(&nbsp;All&nbsp;Resellers&nbsp;Created&nbsp;by&nbsp;<% out.println(userID);%>)&nbsp;:<strong>&nbsp;BDT&nbsp;<s:property value="totalResellerMmBal"/></strong></h4>
                            <% }%>
                            <% }%>
                            <hr/>
                            <% }%>
                            <% if (PM03CM01) {%>
                            <% if (PERMISSION_MR) {%>
                            <a class="btn blue" data-toggle="modal" href="#modal_flexiload">
                                <i class="icon-paper-plane"></i>
                                Mobile&nbsp;Recharge
                            </a>
                            <% }%>
                            <% }%>
                            <% if (PM04CM01) {%>
                            <% if (PERMISSION_MM) {%>
                            <a class="btn red" data-toggle="modal" href="#modal_bkash">
                                <!--<i class="bd-bkash"></i>-->
                                <i class="icon-wallet"></i>
                                Mobile&nbsp;Money
                            </a>
                            <% }%>
                            <% }%>
                        </div>
                    </div>
                    <% }%>
                    <% }%>

                    <div class="row">
                        <% if (PM03CM01) {%>
                        <% if (PERMISSION_MR) {%>
                        <div class=" col-md-6">
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption">Last&nbsp;20&nbsp;Mobile&nbsp;Recharge&nbsp;Requests</div>
                                    <div class="tools">
                                        <a href="" class="collapse"></a>
                                        <a href="" class="reload"></a>
                                        <a href="" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-scrollable">
                                        <table class="table table-condensed table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Order</th>
                                                    <th>Sent&nbsp;By</th>
                                                    <th>Number</th>
                                                    <th>Type</th>
                                                    <th>Amount</th>
                                                    <th>Status</th>
                                                    <th>TrID</th>
                                                </tr>
                                            </thead>
                                            <tbody id="lastMRHHomeTable">
                                                <s:if test="lastMRechargeHInfoList !=null">
                                                    <s:if test="lastMRechargeHInfoList.size() !=0">
                                                        <s:iterator value="lastMRechargeHInfoList" var="lastMRHInfo">
                                                            <tr>
                                                                <td>${lastMRHInfo.mobileRechargeId}</td>
                                                                <td>${lastMRHInfo.sender}</td>
                                                                <td>${lastMRHInfo.receiver}</td>
                                                                <td>
                                                                    <s:if test="type==0">
                                                                        Pepaid
                                                                    </s:if>
                                                                    <s:else>
                                                                        <span style="color: #009966;">
                                                                            Postpaid
                                                                        </span>
                                                                    </s:else>
                                                                </td>
                                                                <td>${lastMRHInfo.givenBalance}</td>
                                                                <td>
                                                                    <s:if test="activeStatus=='Y'">
                                                                        <span>
                                                                            Success
                                                                        </span>
                                                                    </s:if>
                                                                    <s:elseif test="activeStatus=='N'">
                                                                        <span>
                                                                            Pending
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='P'">
                                                                        <span>
                                                                            Processing
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='W'">
                                                                        <span>
                                                                            Waiting
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='F'">
                                                                        <span>
                                                                            Failed
                                                                        </span>
                                                                    </s:elseif>
                                                                </td>
                                                                <td>${lastMRHInfo.trid}</td>
                                                            </tr>
                                                        </s:iterator>
                                                    </s:if>
                                                </s:if>
                                                <s:if test="lastMRechargeHInfoList.size()==0">
                                                    <tr>
                                                        <td colspan="7" style="text-align: center; color: #ff66cc; font-size: 16px; font-weight: bold;">Haven't any mobile recharge requests</td>
                                                    </tr>
                                                </s:if>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <% }%>
                        <% }%>

                        <% if (PM04CM01) {%>
                        <% if (PERMISSION_MM) {%>
                        <div class="col-md-6">
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption">Last&nbsp;20&nbsp;Mobile&nbsp;Money&nbsp;Requests</div>
                                    <div class="tools">
                                        <a href="" class="collapse"></a>
                                        <a href="" class="reload"></a>
                                        <a href="" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-scrollable">
                                        <table class="table table-condensed table-hover">
                                            <thead>
                                                <tr>
                                                    <th>order</th>
                                                    <th>Sent&nbsp;By</th>
                                                    <th>Number</th>
                                                    <th>Type</th>
                                                    <th>Amount</th>
                                                    <th>Status</th>
                                                    <th>TrID</th>
                                                </tr>
                                            </thead>
                                            <tbody id="lastMMHInHomeTable">
                                                <s:if test="lastMMoneyHInfoList !=null">
                                                    <s:if test="lastMMoneyHInfoList.size() !=0">
                                                        <s:iterator value="lastMMoneyHInfoList" var="lastMMHInfo">
                                                            <tr>
                                                                <td>${lastMMHInfo.mobileMoneyId}</td>
                                                                <td>${lastMMHInfo.sender}</td>
                                                                <td>${lastMMHInfo.receiver}</td>
                                                                <td>${lastMMHInfo.type}</td>
                                                                <td>${lastMMHInfo.givenBalance}</td>
                                                                <td>
                                                                    <s:if test="activeStatus=='Y'">
                                                                        <span>
                                                                            Success
                                                                        </span>
                                                                    </s:if>
                                                                    <s:elseif test="activeStatus=='N'">
                                                                        <span>
                                                                            Pending
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='P'">
                                                                        <span>
                                                                            Processing
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='W'">
                                                                        <span>
                                                                            Waiting
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='F'">
                                                                        <span>
                                                                            Failed
                                                                        </span>
                                                                    </s:elseif>
                                                                </td>
                                                                <td>${lastMMHInfo.trid}</td>
                                                            </tr>
                                                        </s:iterator>
                                                    </s:if>
                                                </s:if>
                                                <s:if test="lastMMoneyHInfoList.size()==0">
                                                    <tr>
                                                        <td colspan="7" style="text-align: center; color: #ff66cc; font-size: 16px; font-weight: bold;">Haven't any mobile money requests</td>
                                                    </tr>
                                                </s:if>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <% }%>
                        <% }%>
                    </div>

                    <!-- MOBILE RECHARGE MODAL -->
                    <div class="modal fade" id="modal_flexiload" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog">

                            <form role="form">

                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Your&nbsp;Mobile&nbsp;Recharge&nbsp;Balance&nbsp;Tk.
                                            <s:if test="currentMrBalInfoList !=null">
                                                <s:if test="currentMrBalInfoList.size() !=0">
                                                    <s:iterator value="currentMrBalInfoList">
                                                        <s:property value="mrCurrentBalance"/>
                                                    </s:iterator>
                                                </s:if>
                                            </s:if>
                                        </h4>

                                        <a style="margin-left: 5%; margin-top: 10px;" class="btn green" href="#phone">
                                            Bangladeshi Mobile Recharge&nbsp;&nbsp;
                                            <i style="margin-top: 5px;" class="m-icon-swapdown m-icon-white"></i>
                                        </a>

                                        <a style="margin-top: 10px; margin-left: 10px;" class="btn yellow" href="MalaysiaMobileRecharge">
                                            Malaysian Mobile Recharge&nbsp;&nbsp;
                                            <i style="margin-top: 5px;" class="m-icon-swapright m-icon-white"></i>
                                        </a>
                                    </div>

                                    <div class="modal-body">
                                        <div class="form-body">

                                            <div class="form-group has-success">
                                                <label><strong>Phone&nbsp;Number</strong></label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="icon-arrow-right"></i>
                                                    </span>
                                                    <input type="text" id="phone" name="phone" class="form-control input-lg" style="font-size:20px; color:#C00">
                                                </div>
                                            </div>

                                            <div class="form-group has-success">
                                                <label><strong>Re-type&nbsp;Phone&nbsp;Number</strong></label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="icon-arrow-right"></i>
                                                    </span>
                                                    <input type="text" id="reTypePhone" name="reTypePhone" class="form-control input-lg" style="font-size:20px; color:#C00">
                                                </div>
                                            </div>

                                            <div class="form-group has-success">
                                                <label>
                                                    <strong>Type</strong>
                                                </label>
                                                <div class="radio-list">
                                                    <label class="radio-inline">
                                                        <input type="radio" id="prepaid" name="type" value="Prepaid" checked>
                                                        Prepaid
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" id="postpaid" name="type" value="Postpaid">
                                                        Postpaid
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group has-success">
                                                <label>
                                                    <strong>Amount</strong>
                                                </label>
                                                <div class="input-group input-small">
                                                    <span class="input-group-addon">
                                                        <i class="icon-credit-card"></i>
                                                    </span>
                                                    <input type="text" id="amount" name="amount" class="form-control input-lg" style="font-size:20px; color:#C00">
                                                </div>
                                            </div>

                                            <div class="form-group has-success">
                                                <label>
                                                    <strong>PIN</strong>
                                                </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="icon-key"></i>
                                                    </span>
                                                    <input type="password" id="pin" name="pin" class="form-control input-lg" autocomplete="off" style="font-size:20px; color:#C00">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <img id="mrSendLoadingImage" src="" alt="" />
                                        <button type="button" class="btn default" data-dismiss="modal">
                                            <i class="icon-close"></i>
                                            Close
                                        </button>
                                        <button type="reset" class="btn yellow">Cancel</button>
                                        <button type="button" onclick="validateMRInHome();" class="btn blue">
                                            <i class="icon-check"></i>
                                            Send
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <!-- MOBILE MONEY MODAL -->
                    <div class="modal fade" id="modal_bkash" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog">
                            <form role="form">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Your&nbsp;Mobile&nbsp;Money&nbsp;Balance&nbsp;Tk.
                                            <s:if test="currentMmBalInfoList !=null">
                                                <s:if test="currentMmBalInfoList.size() !=0">
                                                    <s:iterator value="currentMmBalInfoList">
                                                        <s:property value="mmCurrentBalance"/>
                                                    </s:iterator>
                                                </s:if>
                                            </s:if>
                                        </h4>
                                    </div>

                                    <div class="modal-body">
                                        <div class="form-group has-success">
                                            <label>
                                                <strong>Mobile&nbsp;Money&nbsp;Operator</strong>
                                            </label>
                                            <select id="mmOperatorList" name="mmOperatorList" class="form-control input-small">
                                                <option value="bKash">bKash</option>
                                                <option value="DBBL">DBBL</option>
                                                <option value="mCash">mCash</option>
                                            </select>
                                        </div>

                                        <div class="form-group has-success">
                                            <label>
                                                <strong>Type</strong>
                                            </label>
                                            <select id="mmType" name="mmType" class="form-control input-small">
                                                <option value="Personal" selected>Personal</option>
                                                <option value="Agent">Agent</option>
                                            </select>
                                        </div>

                                        <div class="form-group has-success">
                                            <label><strong><span style="color: #0033cc;" id="mmOperator">bKash</span>&nbsp;Phone &nbsp;Number</strong></label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="icon-arrow-right"></i>
                                                </span>
                                                <input type="text" id="mmPhone" name="mmPhone" style="font-size:20px; color:#C00" class="form-control input-lg">
                                            </div>
                                        </div>

                                        <div class="form-group has-success">
                                            <label><strong><span>Re-type</span>&nbsp;Phone &nbsp;Number</strong></label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-arrow-right"></i></span>
                                                <input type="text" id="mmReTypePhone" name="mmReTypePhone" style="font-size:20px; color:#C00" class="form-control input-lg">
                                            </div>
                                        </div>

                                        <div class="form-group has-success">
                                            <label>
                                                <strong>Amount</strong>
                                            </label>
                                            <div class="input-group input-medium">
                                                <span class="input-group-addon">
                                                    <i class="icon-credit-card"></i>
                                                </span>
                                                <input type="text" id="mmAmount" name="mmAmount" class="form-control input-lg" style="font-size:20px; color:#C00">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn default" data-dismiss="modal">
                                            <i class="icon-close"></i>
                                            Close
                                        </button>
                                        <button type="reset" class="btn yellow">Cancel</button>
                                        <button type="button" onclick="validateMMInHome();" class="btn blue">
                                            <i class="icon-check"></i>
                                            Send
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>
        <!-- END CONTAINER -->

        <!-- PAGE FOOTER -->
        <%@include file="footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-hover-dropdown.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/layout.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/quick-sidebar.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/jquery_002.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/jquery_004.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/sendMRFromHome.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/sendMMFromHome.js" type="text/javascript"></script>

        <script type="text/javascript">
                                            jQuery(document).ready(function () {
                                                // initiate layout and plugins
                                                Metronic.init(); // init metronic core components
                                                Layout.init(); // init current layout
                                                QuickSidebar.init() // init quick sidebar
                                                //        UIIdleTimeout.init();

                                                $('#mmOperatorList').on('change', function (e) {
                                                    var mmop = $("option:selected", this);
                                                    var valueSelected = this.value;
                                                    $('#mmOperator').text(valueSelected);
                                                });

                                                $('#ms').hide();
                                            });

        </script>

    </body>
</html>