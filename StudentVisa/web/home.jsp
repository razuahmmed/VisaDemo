
<%@page import="mis.drd.action.ParamUtil"%>
<!DOCTYPE html>
<%@taglib  prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html;" charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <title>Student Visa</title>

        <link href="<%= request.getContextPath()%>/resources/css/style.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/bootstrap.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/yamm.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/style_menu.css" rel="stylesheet" media="screen"/>

    </head>
    <body>

        <jsp:include page="top_menu.jsp" flush="true"></jsp:include>

        <div class="body">

            <!-- ==============================Carousel================================ -->

            <div >

              Well Come Student Visa Process System

            </div>

            <!-- =============================End Carousel================================ -->

        </div>


        <jsp:include page="footer.jsp" flush="true"></jsp:include>


        <!-- Bootstrap Carousel-->
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery-1.4.2.min.js"/>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery.js"/>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/bootstrap.min.js"></script>



    </body>

</html>
