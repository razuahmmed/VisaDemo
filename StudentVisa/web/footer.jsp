

<div class="footer">

    <div class="f_row1">
        <div class="f_tab">

            <div class="f_left">
                <div class="f_logo">
                    <a class="img_res" href="#">
                        <img alt="" src="<%= request.getContextPath()%>/resources/images/f_logo.png"/>
                    </a>
                </div>
            </div>

            <div class="f_tag_box">
                <p class="footer_head">PRODUCT TAG</p>
                <a href="#" id="frontFooter1_aTag1" class="f_tagg"><div id="frontFooter1_divTag1" class="f_tag">Pump</div></a>
                <a href="#" id="frontFooter1_aTag2" class="f_tagg"><div id="frontFooter1_divTag2" class="f_tag">Blue</div></a>
                <a href="#" id="frontFooter1_aTag3" class="f_tagg"><div id="frontFooter1_divTag3" class="f_tag">Fuchsia</div></a>
                <a href="#" id="frontFooter1_aTag4" class="f_tagg"><div id="frontFooter1_divTag4" class="f_tag">Yellow</div></a>
                <a href="#" id="frontFooter1_aTag5" class="f_tagg"><div id="frontFooter1_divTag5" class="f_tag">Black</div></a>
                <a href="#" id="frontFooter1_aTag6" class="f_tagg"><div id="frontFooter1_divTag6" class="f_tag">Nude</div></a>
                <a href="#" id="frontFooter1_aTag7" class="f_tagg"><div id="frontFooter1_divTag7" class="f_tag">Croco</div></a>
                <a href="#" id="frontFooter1_aTag8" class="f_tagg"><div id="frontFooter1_divTag8" class="f_tag">Patent</div></a>
                <a href="#" id="frontFooter1_aTag9" class="f_tagg"><div id="frontFooter1_divTag9" class="f_tag">Grey</div></a>
                <a href="#" id="frontFooter1_aTag10" class="f_tagg"><div id="frontFooter1_divTag10" class="f_tag">Sandal</div></a>
            </div>

            <div class="f_right">
                <div class="up2date">
                    <a href="#">
                        <img alt="" src="<%= request.getContextPath()%>/resources/images/f_up2date.jpg"/>
                    </a>
                </div>
                <div class="cate">
                    <p class="footer_head">PRODUCT CATEGORY</p>
                    <a href="#"><p class="footer_link">Women</p></a>
                    <a href="#"><p class="footer_link">Men</p></a>
                    <a href="#"><p class="footer_link">Accessories</p></a>
                </div>
            </div>

        </div>
    </div>

    <div class="f_row2">
        <div class="f_tab">
            <div class="f_copy"><p class="f_txt">� COPYRIGHT ALL RIGHT RESERVED 2016 FOOT-IN.COM</p></div>
        </div>
    </div>

</div>
