<%@page import="mis.drd.action.ParamUtil"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%
            boolean PM01CM01 = (request.getSession().getAttribute("PM01CM01") != null && request.getSession().getAttribute("PM01CM01").toString().equalsIgnoreCase("T")) ? true : false;
            boolean PM01CM02 = (request.getSession().getAttribute("PM01CM02") != null && request.getSession().getAttribute("PM01CM02").toString().equalsIgnoreCase("T")) ? true : false;
            boolean PM01CM03 = (request.getSession().getAttribute("PM01CM03") != null && request.getSession().getAttribute("PM01CM03").toString().equalsIgnoreCase("T")) ? true : false;
            boolean PM01CM04 = (request.getSession().getAttribute("PM01CM04") != null && request.getSession().getAttribute("PM01CM04").toString().equalsIgnoreCase("T")) ? true : false;
            boolean PM01CM05 = (request.getSession().getAttribute("PM01CM05") != null && request.getSession().getAttribute("PM01CM05").toString().equalsIgnoreCase("T")) ? true : false;
            boolean PM01CM06 = (request.getSession().getAttribute("PM01CM06") != null && request.getSession().getAttribute("PM01CM06").toString().equalsIgnoreCase("T")) ? true : false;
            boolean PM01CM07 = (request.getSession().getAttribute("PM01CM07") != null && request.getSession().getAttribute("PM01CM07").toString().equalsIgnoreCase("T")) ? true : false;
            boolean PM01CM08 = (request.getSession().getAttribute("PM01CM08") != null && request.getSession().getAttribute("PM01CM08").toString().equalsIgnoreCase("T")) ? true : false;
            boolean PM01CM09 = (request.getSession().getAttribute("PM01CM09") != null && request.getSession().getAttribute("PM01CM09").toString().equalsIgnoreCase("T")) ? true : false;
            //
%>

<div class="h_title"><b>&#8250;&nbsp;DashBoard</b></div>
<ul id="home">

    <% if (PM01CM01) {%>
    <li class="b1"><a class="icon view_page" href="Home">Home</a></li>
    <% }%>
    <% if (PM01CM02) {%>
    <li class="b1"><a class="icon view_page" href="StoreProfitabilityAnalysis">Store Profitability Analysis</a></li>
    <% }%>
    <% if (PM01CM03) {%>
    <li class="b1"><a class="icon view_page" href="BataRetailDailyMirror">Business Performance</a></li>
    <% }%>
    <% if (PM01CM04) {%>
    <li class="b1"><a class="icon view_page" href="BataRetailStockAnalysis">Stock Analysis</a></li>
    <% }%>
    <% if (PM01CM05) {%>
    <li class="b1"><a class="icon view_page" href="CampaignSalesUpdate">Campaign Sales</a></li>
    <% }%>
    <% if (PM01CM06) {%>
    <li class="b1"><a class="icon view_page" href="CampaignSalesReport?option=store">Campaign Sales Report</a></li>
    <% }%>
    <% if (PM01CM07) {%>
    <li class="b1"><a class="icon view_page" href="UserCircular">Circular</a></li>
    <% }%>
    <% if (PM01CM08) {%>
    <li class="b1"><a class="icon search" href="GetAllStoreForDashboard">Search</a></li>
    <% }%>
    <% if (PM01CM09) {%>
    <li class="b1"><a class="icon view_page" href="LogInUser">Log In User</a></li>
    <% }%>



</ul>