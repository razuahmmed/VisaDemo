
<%@page import="mis.drd.action.ParamUtil"%>
<!DOCTYPE html>
<%@taglib  prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html;" charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <title>FootIn</title>

        <link href="<%= request.getContextPath()%>/resources/css/style.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/bootstrap.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/yamm.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/style_menu.css" rel="stylesheet" media="screen"/>

    </head>

    <style type="text/css">
        <!--
        #example td {
            vertical-align: middle;
            padding: 5px;
        }
        -->
    </style>


    <body>

        <jsp:include page="/top_menu.jsp" flush="true"></jsp:include>

        <div class="body" style="padding-left: 15px; padding-right: 15px; height: 195px; vertical-align: middle;">

            <br/>
            <div id="successMessageDiv" style="padding-left: 12px; font-size: 12px; color: red; text-align: center;">
                <s:property value="jsonData"/>
            </div>
            <br/>

            <form action="promCircularFileUploadOdr.footin" id="popXlsFileUploadFrom" enctype="multipart/form-data" method="post" accept-charset="utf-8">

                <table id="example" style="width: 100%;">

                    <tr>
                        <td style="width: 40%; text-align: right; padding-right: 5px;"><label>Circular File</label></td>
                        <td><input type="file" id="circularFileId" name="myFile" /></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td align="left">
                            <button class="btn btn-primary btn-green" type="submit">Upload</button>
                            <a href="<%= request.getContextPath()%>/CIRCULAR_SAMPLE_FILE/Circular_Sample.xlsx" >Circular Sample File</a>
                        </td>
                    </tr>

                </table>

            </form>

        </div>


        <jsp:include page="/footer.jsp" flush="true"></jsp:include>



        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery-1.8.3.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/bootstrap.min.js"></script>

        <script type="text/javascript">

            $(function() {
                setTimeout(function() {$("#successMessageDiv").hide('blind', {}, 500)}, 1000);
            });


        </script>

    </body>

</html>
