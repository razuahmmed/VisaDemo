
<%@page import="mis.drd.action.ParamUtil"%>
<!DOCTYPE html>
<%@taglib  prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html;" charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <title>FootIn</title>

        <link href="<%= request.getContextPath()%>/resources/css/style.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/bootstrap.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/yamm.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/style_menu.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/resources/css/jquery.dataTables.css" rel="stylesheet" />

    </head>

    <style type="text/css">
        <!--
        #example th {
            /* color: #E06C0A; 009999 */
            background: none repeat scroll 0 0 #E06C0A;
            color: #FFFFFF;
            font-size: 12px;
            height: 22px;
            vertical-align: middle;
            padding: 2px;
            text-align: center;
            border: 1px solid #FFFFFF;
        }

        #example td {
            vertical-align: middle;
        }

        #example1 th {
            /* color: #E06C0A; 009999 */
            background: none repeat scroll 0 0 #E06C0A;
            color: #FFFFFF;
            font-size: 12px;
            height: 22px;
            vertical-align: middle;
            padding: 2px;
            text-align: center;
            border: 1px solid #FFFFFF;
        }

        #example1 td {
            vertical-align: middle;
        }

        #example1 td:nth-child(3) {
            text-align: right;
            padding-right: 5px;
        }
        #example1 td:nth-child(4) {
            text-align: right;
            padding-right: 5px;
        }
        #example1 td:nth-child(5) {
            text-align: right;
            padding-right: 5px;
        }

        -->
    </style>

    <body>

        <jsp:include page="/top_menu.jsp" flush="true"></jsp:include>

        <div class="body" style="padding-left: 5px; padding-right: 5px;">

            <table width="100%">
                <tr>
                    <td><a href="promotionUploadOdr.footin" class="btn btn-primary">Promotion Upload</a></td>
                </tr>
            </table>

            <br/>

            <table width="100%">
                <tr>
                    <td style="vertical-align: top;" width="55%">

                        <table id="example" class="table table-striped" cellspacing="0" width="100%">

                            <thead>

                                <tr>
                                    <th>Circular</th>
                                    <th>Promotion Name</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Start Time</th>
                                    <th>End Time</th>
                                    <th>Status</th>
                                    <th>&nbsp;</th>
                                </tr>

                            </thead>

                            <tbody>

                                <s:if test="ftnPromotionMstList != null">
                                    <s:if test="ftnPromotionMstList.size()!=0">
                                        <s:iterator value="ftnPromotionMstList">
                                            <tr>
                                                <td>
                                                    <a onclick="circularDetails('<s:property value="promoId"/>');" href="javascript:void(0);" style="text-decoration: none;" >
                                                        <s:property value="promoId"/>
                                                    </a>
                                                </td>
                                                <td><s:property value="promoName"/></td>
                                                <td align="center"><s:date name="startDate" format="dd/MM/yyyy"/></td>
                                                <td align="center"><s:date name="endDate" format="dd/MM/yyyy"/></td>
                                                <td align="center"><s:property value="startTime"/></td>
                                                <td align="center"><s:property value="endTime"/></td>
                                                <td><s:property value="promoStatus"/></td>
                                                <td align="center">&nbsp;</td>

                                            </tr>

                                        </s:iterator>
                                    </s:if>
                                </s:if>
                            </tbody>

                        </table>

                    </td>

                    <td width="1%"></td>

                    <td style="vertical-align: top;" width="44%">

                        <table id="example1" class="table table-striped" cellspacing="0" width="100%">

                            <thead>

                                <tr>
                                    <th style="width: 20%;">Circular</th>
                                    <th style="width: 20%;">Article</th>
                                    <th style="width: 20%;">Old Price</th>
                                    <th style="width: 20%;">New Price</th>
                                    <th style="width: 20%;">Difference</th>
                                </tr>

                            </thead>

                            <tbody>
                            </tbody>

                        </table>

                    </td>
                </tr>
            </table>

        </div>


        <jsp:include page="/footer.jsp" flush="true"></jsp:include>


        <!-- Bootstrap Carousel-->
        <!--
        <script type="text/javascript" src="<//%= request.getContextPath()%>/resources/js/jquery-1.4.2.min.js"/>
        <script type="text/javascript" src="<//%= request.getContextPath()%>/resources/js/jquery.js"/>
        -->
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery-1.8.3.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery-ui.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/bootstrap.min.js"></script>

        <script type="text/javascript">

            $(document).ready(function() {
                onloadFunction();
                onloadFunction1();
            });

            function onloadFunction(){
                // DataTable
                var table =   $('#example').DataTable({
                    "aoColumnDefs": [{'bSortable': false, 'aTargets': [0]}],
                    "order": [[ 2, "desc" ]],
                    "pageLength": 25
                });

            }

            function onloadFunction1(){
                // DataTable
                $('#example1').DataTable({
                    "aoColumnDefs": [{'bSortable': false, 'aTargets': [0]}],
                    "order": [[ 2, "desc" ]],
                    "pageLength": 25
                });

            }

            function circularDetails(circular) {

                if(circular.trim() != ''){

                    // document.getElementById('circularArtImage').src='images/ajax-loader2.gif';

                    $.ajax({
                        type: "POST",
                        url: "promCircularArticleOdr.footin?ftnCircularNumber="+circular,
                        success: function(data){

                            // document.getElementById('circularArtImage').src='';

                            if(data.trim() != ""){

                                var $myTable = $('#example1');
                                var t = $myTable.DataTable();


                                $myTable.find('tr').each(function() {
                                    $(this).addClass('remove');
                                });
                                t.clear().draw(false);
                          
                                var jsonCircularArticle = $.parseJSON(data.trim());



                                for (var i=0; i< jsonCircularArticle.length; i++){

                                    t.row.add( [
                                        '<td style="text-align: left; padding: 2px;">'+ jsonCircularArticle[i].circular.trim() + '</td>',
                                        '<td style="text-align: left; padding: 2px;">'+ jsonCircularArticle[i].article.trim() + '</td>',
                                        '<td style="text-align: center; padding: 2px;">' + jsonCircularArticle[i].oldprice.trim() + '</td>',
                                        '<td style="text-align: center; padding: 2px;">' + jsonCircularArticle[i].newprice.trim() + '</td>',
                                        '<td style="text-align: right; padding: 2px;">' + jsonCircularArticle[i].discount.trim() + '</td>'                                      
                                    ] ).draw(false);
                                }


                            }

                        }
                    });


                }

            }



        </script>

    </body>

</html>
