
<%@page import="mis.drd.action.ParamUtil"%>
<!DOCTYPE html>
<%@taglib  prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html;" charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <title>FootIn</title>

        <link href="<%= request.getContextPath()%>/resources/css/style.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/bootstrap.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/yamm.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/style_menu.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css1/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen"/>


    </head>

    <style type="text/css">
        <!--
        #custInvHistoryTbl th {
            /* color: #E06C0A; 009999 */
            background: none repeat scroll 0 0 #E06C0A;
            color: #FFFFFF;
            font-size: 12px;
            height: 22px;
            vertical-align: middle;
            padding: 2px;
            text-align: center;
            border: 1px solid #FFFFFF;
        }
        -->
    </style>

    <body>

        <jsp:include page="/top_menu.jsp" flush="true"></jsp:include>

        <div class="body" style="padding-left: 10px; padding-right: 10px;">

            <table width="100%" >

                <tr>

                    <td valign="top" width="30%">

                        <table width="100%">

                            <thead>
                                <tr>
                                    <th class="cust_search_th" colspan="5">Customer Search</th>
                                </tr>
                            </thead>

                            <tbody>

                                <tr>
                                    <td style="width: 10px;">&nbsp;</td>
                                    <td style="width: 150px; padding-top: 5px;" class="txt1_cust_tb">
                                        <input type="text" id="customerSearchId" class="form-control" maxlength="10" placeholder="Customer ID" />
                                    </td>
                                    <td style="width: 200px; padding-top: 5px;" class="txt1_cust_tb">
                                        <input type="text" id="customerSearchContact" class="form-control" maxlength="11" placeholder="Contact No"/>
                                    </td>                                    
                                    <td style="width: 45px; padding-top: 5px;"class="txt1_cust_tb">
                                        <a id="customerSearchButton" onclick="ftnCustomerSearch();" class="sc-button" href="javascript:void(0);" rel="nofollow">
                                            <span style="color: white;">Search</span>
                                        </a>
                                    </td>
                                    <td><img id="customerSearchImage" src="" alt="" /></td>
                                </tr>

                            </tbody>

                        </table>

                        <br/>

                        <div id="customerIdDiv">

                            <table>

                                <tr>
                                    <td width="25%"><label class="label_bh1">Customer ID</label></td>
                                    <td width="1%">&nbsp;</td>
                                    <td width="64%" class="txt2_cust_tb">
                                        <input type="text" id="customerId" name="customerId" class="form-control" style="width: 150px;" readonly />
                                    </td>
                                </tr>

                                <tr>
                                    <td><label class="label_bh1">Contact Number</label></td>
                                    <td><label class="label_bh1" style="color: red;">*</label></td>
                                    <td class="txt2_cust_tb">
                                        <input type="text" id="contactNumber" name="contactNumber" class="form-control" maxlength="20"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label class="label_bh1">Email Address</label></td>
                                    <td>&nbsp;</td>
                                    <td class="txt2_cust_tb">
                                        <input type="text" id="emailAddress" name="emailAddress" class="form-control" maxlength="50"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label class="label_bh1">First Name</label></td>
                                    <td>&nbsp;</td>
                                    <td class="txt2_cust_tb">
                                        <input type="text" id="firstName" name="firstName" class="form-control" maxlength="200"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label class="label_bh1">Middle Name</label></td>
                                    <td>&nbsp;</td>
                                    <td class="txt2_cust_tb">
                                        <input type="text" id="middleName" name="middleName" class="form-control" maxlength="200"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label class="label_bh1">Last Name</label></td>
                                    <td>&nbsp;</td>
                                    <td class="txt2_cust_tb">
                                        <input type="text" id="lastName" name="lastName" class="form-control" maxlength="200"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label class="label_bh1">Surname</label></td>
                                    <td>&nbsp;</td>
                                    <td class="txt2_cust_tb">
                                        <input type="text" id="sureName" name="sureName" class="form-control" maxlength="200"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label class="label_bh1">Country</label></td>
                                    <td><label class="label_bh1" style="color: red;">*</label></td>
                                    <td class="txt2_cust_tb">

                                        <select id="countryList" name="countryList" class="form-control" style="width: 200px;" >
                                            <option value="0">SELECT</option>
                                            <s:if test="ftnCountryList != null">
                                                <s:if test="ftnCountryList.size()!=0">
                                                    <s:iterator value="ftnCountryList">
                                                        <option value="<s:property value="countryCode"/>"><s:property value="countryName"/></option>
                                                    </s:iterator>
                                                </s:if>
                                            </s:if>
                                        </select>
                                        <img id="countryListImage" src="" alt="" />
                                    </td>

                                </tr>

                                <tr>
                                    <td><label class="label_bh1">State</label></td>
                                    <td>&nbsp;</td>
                                    <td class="txt2_cust_tb">
                                        <select id="stateList" name="stateList" class="form-control" style="width: 200px;" >
                                        </select>
                                        <img id="stateListImage" src="" alt="" />
                                    </td>
                                </tr>

                                <tr>
                                    <td><label class="label_bh1">City</label></td>
                                    <td>&nbsp;</td>
                                    <td class="txt2_cust_tb">                               
                                        <select id="cityList" name="cityList" class="form-control" style="width: 200px;" >
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label class="label_bh1">Postal Code</label></td>
                                    <td>&nbsp;</td>
                                    <td class="txt2_cust_tb">
                                        <input id="postalCode" name="postalCode" class="form-control" style="width: 100px;" maxlength="250" type="text"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label class="label_bh1">Address</label></td>
                                    <td>&nbsp;</td>
                                    <td class="txt2_cust_tb">
                                        <input id="customerAddress" name="customerAddress" class="form-control" maxlength="250" type="text"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label class="label_bh1">Shipping</label></td>
                                    <td><label class="label_bh1" style="color: red;">*</label></td>
                                    <td class="txt2_cust_tb">
                                        <input id="shippingAddress" name="shippingAddress" class="form-control" maxlength="250" type="text"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label class="label_bh1">DOB</label></td>
                                    <td>&nbsp;</td>
                                    <td class="txt2_cust_tb">
                                        <div style="float: left;">
                                            <input type="text" id="dateOfBirth" name="dateOfBirth" class="form-control" style="width: 100px;" maxlength="10" placeholder="DD/MM/YYYY" />
                                        </div><div style="float: left; padding: 8px;"><span>DD/MM/YYYY</span></div>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label class="label_bh1">Customer Type</label></td>
                                    <td>&nbsp;</td>
                                    <td class="txt2_cust_tb">
                                        <select id="customerTypeList" name="customerTypeList" class="form-control" style="width: 150px;" >
                                            <option value="Standard">STANDARD</option>
                                            <option value="Silver">SILVER</option>
                                            <option value="Gold">GOLD</option>
                                            <option value="Platinum">PLATINUM</option>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label class="label_bh1">Gender</label></td>
                                    <td>&nbsp;</td>
                                    <td class="txt2_cust_tb">
                                        <select id="genderList" name="genderList" class="form-control" style="width: 150px;" >
                                            <option value="0">SELECT</option>
                                            <option value="M">MALE</option>
                                            <option value="F">FEMALE</option>
                                            <option value="O">OTHER</option>         
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label class="label_bh1">Religion</label></td>
                                    <td>&nbsp;</td>
                                    <td class="txt2_cust_tb">
                                        <select id="religionList" name="religionList" class="form-control" style="width: 150px;" >
                                            <option value="0">SELECT</option>
                                            <option value="I">ISLAM</option>
                                            <option value="H">HINDU</option>
                                            <option value="B">BUDDHA</option>
                                            <option value="C">CHRISTIAN</option>
                                            <option value="O">OTHER</option>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label class="label_bh1">Marital Status</label></td>
                                    <td>&nbsp;</td>
                                    <td class="txt2_cust_tb">
                                        <select id="marritalList" name="marritalList" class="form-control" style="width: 150px;" >
                                            <option value="0">SELECT</option>
                                            <option value="M">MARRIED</option>
                                            <option value="U">UNMARRIED</option>
                                            <option value="S">SINGLE</option>
                                            <option value="W">Widow</option>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label class="label_bh1">Occupation</label></td>
                                    <td>&nbsp;</td>
                                    <td class="txt2_cust_tb">
                                        <input id="customerOccupation" name="customerOccupation" class="form-control" maxlength="250" type="text"/>
                                    </td>
                                </tr>

                            </table>

                        </div>

                    </td>

                    <td style="padding-left: 2px; padding-right: 1px;" valign="top" width="40%">

                        <table width="100%">

                            <thead>
                                <tr>
                                    <th class="cust_search_th" colspan="6">Article Search</th>

                                </tr>
                            </thead>

                            <tbody>

                                <tr>
                                    <td style="width: 100px;">&nbsp;</td>
                                    <td style="width: 150px; padding-top: 5px; text-align: center;" class="txt1_cust_tb">
                                        <input type="text" id="searchArticleCode" class="form-control" maxlength="10" placeholder="Article Code"  />
                                    </td>
                                    <td style="width: 100px; padding-top: 5px; text-align: center;" class="txt1_cust_tb">
                                        <a id="articleSearchButton" onclick="ftnCustomerArticleSearch();" class="sc-button" href="javascript:void(0);" rel="nofollow">
                                            <span style="color: white;">Search</span>
                                        </a>
                                    </td>
                                    <td>
                                        <img id="articleSearchImage" src="" alt="" />
                                    </td>

                                    <td>
                                        Payment
                                    </td>

                                    <td>
                                        <select id="paymentModeList" name="paymentModeList" class="form-control" style="width: 80px;" >
                                            <s:if test="ftnPaymentModeList != null">
                                                <s:if test="ftnPaymentModeList.size()!=0">
                                                    <s:iterator value="ftnPaymentModeList">
                                                        <option value="<s:property value="paymentType"/>"><s:property value="paymentType"/></option>
                                                    </s:iterator>
                                                </s:if>
                                            </s:if>
                                        </select>
                                    </td>

                                </tr>

                            </tbody>

                        </table>

                        <table width="100%">

                            <tr>
                                <td style="width: 30%; height: 150px;">
                                    <img id="articleImageId" src="<%= request.getContextPath()%>/ARTICLE_IMAGE/test.jpg" width="150px;" height="150px;" alt="" />
                                </td>
                                <td valign="top" style="width: 70%;">

                                    <table style="width: 100%;">

                                        <tr>
                                            <td align="right" colspan="3" style="padding-right: 2px;">
                                                <label class="control-label">Order Date</label>
                                                <a onclick="clearDate();" href="javascript:void(0);" >
                                                    <img id="clearDateImage" src="<%= request.getContextPath()%>/images/details_close.png" alt="" />
                                                </a>
                                            </td>
                                            <td style="padding: 2px;">
                                                <div class="controls input-append date form_date" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="dd-mm-yyyy">
                                                    <input id="dtp_input4" class="form-control" size="14" type="text" value="" readonly />
                                                    <span class="add-on"><i class="icon-remove"></i></span>
                                                    <span class="add-on"><i class="icon-th"></i></span>
                                                </div>
                                                <input type="hidden" id="dtp_input2" value="" />
                                            </td>


                                        </tr>

                                        <tr>                                           
                                            <td class="article_txt" id="orderArticleCodelbl" colspan="2"></td>
                                            <td style="text-align: right;">
                                                <input type="hidden" id="orderArticleCode" value="" />
                                                <input type="hidden" id="orderArticleSizeCode" value="" />
                                                <input type="hidden" id="orderArticleMrp" value="" />
                                                <input type="hidden" id="orderArticlePromotionId" value="" />
                                                <label class="control-label">Discount</label>&nbsp;
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" style="width: 100px;" id="orderArticleDiscount" value="" readonly />
                                            </td>
                                        </tr>

                                        <tr>
                                            <td width="25%" class="price_txt"><label id="artMrp">MRP</label></td>
                                            <td width="25%" class="price_txt"><label id="artNet">Net</label></td>
                                            <td width="25%" class="price_txt"><label id="artVat">Vat</label></td>
                                            <td width="25%" class="price_txt"><label id="artCost">Cost</label></td>
                                        </tr>

                                        <tr>
                                            <td class="cat_txt">Brand</td>
                                            <td id="artBrand" class="cat_txt" colspan="3"></td>
                                        </tr>

                                        <tr>
                                            <td class="cat_txt">Category</td>
                                            <td id="artCat" class="cat_txt" colspan="3"></td>
                                        </tr>

                                        <tr>
                                            <td class="cat_txt">Sub Cat</td>
                                            <td id="artSubCat" class="cat_txt" colspan="3"></td>
                                        </tr>

                                    </table>

                                </td>

                            </tr>

                            <tr>
                                <td colspan="2">
                                    <table width="100%">
                                        <thead>

                                            <tr>
                                                <th class="price_txt">&nbsp;</th>
                                                <th id="sizeS1" class="price_txt"></th>
                                                <th id="sizeS2" class="price_txt"></th>
                                                <th id="sizeS3" class="price_txt"></th>
                                                <th id="sizeS4" class="price_txt"></th>
                                                <th id="sizeS5" class="price_txt"></th>
                                                <th id="sizeS6" class="price_txt"></th>
                                                <th id="sizeS7" class="price_txt"></th>
                                                <th id="sizeS8" class="price_txt"></th>
                                                <th id="sizeS9" class="price_txt"></th>
                                                <th id="sizeS10" class="price_txt"></th>
                                                <th id="sizeS11" class="price_txt"></th>
                                                <th id="sizeS12" class="price_txt"></th>
                                                <th id="sizeS13" class="price_txt"></th>
                                                <th class="price_txt">Tot</th>
                                                <th class="price_txt"><img id="orderArtImage" src="" alt="" /></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><label>S</label></td>
                                                <td id="stkPairS1" style="text-align: center;">&nbsp;</td>
                                                <td id="stkPairS2" style="text-align: center;">&nbsp;</td>
                                                <td id="stkPairS3" style="text-align: center;">&nbsp;</td>
                                                <td id="stkPairS4" style="text-align: center;">&nbsp;</td>
                                                <td id="stkPairS5" style="text-align: center;">&nbsp;</td>
                                                <td id="stkPairS6" style="text-align: center;">&nbsp;</td>
                                                <td id="stkPairS7" style="text-align: center;">&nbsp;</td>
                                                <td id="stkPairS8" style="text-align: center;">&nbsp;</td>
                                                <td id="stkPairS9" style="text-align: center;">&nbsp;</td>
                                                <td id="stkPairS10" style="text-align: center;">&nbsp;</td>
                                                <td id="stkPairS11" style="text-align: center;">&nbsp;</td>
                                                <td id="stkPairS12" style="text-align: center;">&nbsp;</td>
                                                <td id="stkPairS13" style="text-align: center;">&nbsp;</td>
                                                <td id="stkPairSTotal" style="text-align: center;">&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td><label>O</label></td>
                                                <td><input onchange="totalOrderPairCal();" type="text" id="ordPairS1" class="form-control" readonly /></td>
                                                <td><input onchange="totalOrderPairCal();" type="text" id="ordPairS2" class="form-control" readonly /></td>
                                                <td><input onchange="totalOrderPairCal();" type="text" id="ordPairS3" class="form-control" readonly /></td>
                                                <td><input onchange="totalOrderPairCal();" type="text" id="ordPairS4" class="form-control" readonly /></td>
                                                <td><input onchange="totalOrderPairCal();" type="text" id="ordPairS5" class="form-control" readonly /></td>
                                                <td><input onchange="totalOrderPairCal();" type="text" id="ordPairS6" class="form-control" readonly /></td>
                                                <td><input onchange="totalOrderPairCal();" type="text" id="ordPairS7" class="form-control" readonly /></td>
                                                <td><input onchange="totalOrderPairCal();" type="text" id="ordPairS8" class="form-control" readonly /></td>
                                                <td><input onchange="totalOrderPairCal();" type="text" id="ordPairS9" class="form-control" readonly /></td>
                                                <td><input onchange="totalOrderPairCal();" type="text" id="ordPairS10" class="form-control" readonly /></td>
                                                <td><input onchange="totalOrderPairCal();" type="text" id="ordPairS11" class="form-control" readonly /></td>
                                                <td><input onchange="totalOrderPairCal();" type="text" id="ordPairS12" class="form-control" readonly /></td>
                                                <td><input onchange="totalOrderPairCal();" type="text" id="ordPairS13" class="form-control" readonly /></td>
                                                <td><input type="text" id="ordPairTotal" class="form-control" readonly /></td>
                                                <td>
                                                    <a onclick="ftnArticleAdd();" href="javascript:void(0);" style="text-decoration: none;" >Add</a>
                                                </td>
                                            </tr>

                                        </tbody>

                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2" >&nbsp;</td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <table id="orderTbl" width="100%">
                                        <thead>
                                            <tr>
                                                <th width="10%" class="price_txt">Article</th>
                                                <th width="35%" class="price_txt">Description</th>
                                                <th width="10%" class="price_txt">Size</th>
                                                <th width="8%" class="price_txt">Qty</th>
                                                <th width="10%" class="price_txt">Price</th>
                                                <th width="10%" class="price_txt">Discount</th>
                                                <th width="15%" class="price_txt">Subtotal</th>
                                                <th width="2%" class="price_txt">&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td class="grand_total_txt" colspan="6">SUBTOTAL</td>
                                                <td class="grand_total_txt" id="orderArtSubTotal" >&nbsp;</td>
                                                <td class="grand_total_txt">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="grand_total_txt" colspan="6">-DISCOUNT</td>
                                                <td>                                         
                                                    <input style="text-align: right;" class="form-control" type="text"  id="additionalDiscount" value=""  />
                                                </td>
                                                <td class="grand_total_txt">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="grand_total_txt" colspan="6">TOTAL</td>
                                                <td class="grand_total_txt" id="orderArtGrandTotal" >&nbsp;</td>
                                                <td class="grand_total_txt">&nbsp;</td>
                                            </tr>
                                        </tfoot>

                                    </table>
                                </td>
                            </tr>

                            <tr>                               
                                <td colspan="2" style="padding-top: 5px; text-align: center;" class="txt1_cust_tb">
                                    <a id="orderClearButton" onclick="resetAll('1');" class="sc-button" href="javascript:void(0);" rel="nofollow">
                                        <span style="color: white;">Clear</span>
                                    </a>
                                    &nbsp;
                                    <a id="orderSaveButton" onclick="ftnOrderSave();" class="sc-button" href="javascript:void(0);" rel="nofollow">
                                        <span style="color: white;">Save</span>
                                    </a>
                                    &nbsp;
                                    <input type="checkbox" id="cdcConfirmChkbox" checked />
                                    &nbsp;
                                    Order verified
                                </td>

                            </tr>

                        </table>

                    </td>

                    <td style="padding-left: 1px;" valign="top" width="30%">

                        <table width="100%">

                            <thead>
                                <tr>
                                    <th class="cust_search_th" colspan="2">Promotional Dashboard<img id="promotionDashboardImage" src="" alt="" /></th>
                                </tr>
                            </thead>

                        </table>

                        <table width="100%">

                            <thead>

                                <tr>
                                    <th id="promotionTh" class="btn_tab">
                                        <a onclick="dispayPromotion();" style="color: #ffffff;" href="javascript:void(0);" >Promotion
                                        </a>
                                    </th>
                                    <th id="newArticleTh" class="btn_tab">
                                        <a onclick="dispayNewArticle();" style="color: #ffffff;" href="javascript:void(0);" >New
                                        </a>
                                    </th>
                                    <th id="invoiceHistoryTh" class="btn_tab">
                                        <a onclick="dispayInvoiceHistory();" style="color: #ffffff;" href="javascript:void(0);" >History
                                        </a>
                                    </th>
                                    <th id="ageArticleTh" class="btn_tab">
                                        <a onclick="dispayAgeArticle();" style="color: #ffffff;" href="javascript:void(0);" >Age
                                        </a>
                                    </th>
                                    <th id="seassionArticle" class="btn_tab">
                                        <a onclick="dispaySessionArticle();" style="color: #ffffff;" href="javascript:void(0);" >Session</a>
                                    </th>
                                </tr>
                            </thead>

                        </table>

                        <div class="tab-content">

                            <div id="promotionDiv">

                                <table id="artPromotionTbl" width="100%">

                                    <thead>
                                        <tr>
                                            <th class="promo_th">&nbsp;</th>
                                            <th class="promo_th">Title</th>
                                            <th class="promo_th">From</th>
                                            <th class="promo_th">To</th>
                                            <th class="promo_th">Discount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>

                                </table>

                            </div>

                            <div id="newArrivalDiv">

                                <table id="newArticleTbl" width="100%">

                                    <tbody>
                                    </tbody>

                                </table>

                            </div>

                            <div id="invoiceHistoryDiv">

                                <table id="custInvHistoryTbl" width="100%">
                                    <thead>

                                        <tr>
                                            <th style="width: 20%;">Article</th>
                                            <th style="width: 20%;">Order</th>
                                            <th style="width: 20%;">Date</th>                                            
                                            <th style="width: 20%;">Price</th>
                                            <th style="width: 20%;">Discount</th>
                                        </tr>

                                    </thead>

                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                            <div id="ageArticleDiv">
                                Age Wise Article
                            </div>

                            <div id="sessionArticleDiv">
                                Session Wise Article
                            </div>

                        </div>



                    </td>

                </tr>

            </table>

        </div>

        <jsp:include page="/footer.jsp" flush="true"></jsp:include>

        <!-- Bootstrap Carousel-->
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery-1.4.2.min.js"/>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery.js"/>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquerysession.js"></script>
        <!-- Bootstrap Calendar-->
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js1/jquery-1.8.3.min.js" charset="UTF-8"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js1/bootstrap-datetimepicker.js" charset="UTF-8"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js1/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>


        <script  type="text/javascript">

            function dispayPromotion(){
                document.getElementById('promotionDiv').style.display = 'block';
                document.getElementById('newArrivalDiv').style.display = 'none';
                document.getElementById('invoiceHistoryDiv').style.display = 'none';
                document.getElementById('ageArticleDiv').style.display = 'none';
                document.getElementById('sessionArticleDiv').style.display = 'none';

                $('#promotionTh').css("background-color", "#E06C0A");
                $('#newArticleTh').css("background-color", "#009999");
                $('#invoiceHistoryTh').css("background-color", "#009999");
                $('#ageArticleTh').css("background-color", "#009999");
                $('#seassionArticle').css("background-color", "#009999");
            }

            function dispayNewArticle(){
                
                document.getElementById('promotionDiv').style.display = 'none';
                document.getElementById('newArrivalDiv').style.display = 'block';
                document.getElementById('invoiceHistoryDiv').style.display = 'none';
                document.getElementById('ageArticleDiv').style.display = 'none';
                document.getElementById('sessionArticleDiv').style.display = 'none';

                $('#promotionTh').css("background-color", "#009999");
                $('#newArticleTh').css("background-color", "#E06C0A");
                $('#invoiceHistoryTh').css("background-color", "#009999");
                $('#ageArticleTh').css("background-color", "#009999");
                $('#seassionArticle').css("background-color", "#009999");

                $("#newArticleTbl > tbody").html("");

                document.getElementById('promotionDashboardImage').src='images/ajax-loader2.gif';

                $.ajax({
                    type: "POST",
                    url: "newArrivalArticleOdr.footin?ftnCustomerCode="+customerId,
                    success: function(data){

                        document.getElementById('promotionDashboardImage').src='';

                        if(data.trim() != ""){

                            var jsnData = $.parseJSON(data.trim());

                            var col = 0;
                            var fg = false;

                            var tmpTd = "";

                            for (var i=0; i< jsnData.length; i++){
                                fg = true;
                                tmpTd += "<td style=\"text-align: center; padding: 2px; font-size: 10px; color: #E06C0A;\">"+jsnData[i].catname.trim()+"<img src=\"<%= request.getContextPath()%>/ARTICLE_IMAGE/"+jsnData[i].artcode.trim()+".jpg\" style=\"width: 110px; height: 100px;\"/><label style=\"font-size: 12px;\">"+jsnData[i].artcode.trim()+"(Tk.-"+jsnData[i].mrp.trim()+")</label></td>"
                                if(col == 2){
                                    $("#newArticleTbl tbody").append("<tr>"+tmpTd+"<tr>");
                                    col = 0;
                                    tmpTd = "";
                                    fg = false;
                                }

                                col++;
                            }

                            if(fg == true){
                                $("#newArticleTbl tbody").append("<tr>"+tmpTd+"<tr>");
                            }


                        }

                    }
                });

                

            }

            function dispayInvoiceHistory(){

                document.getElementById('promotionDiv').style.display = 'none';
                document.getElementById('newArrivalDiv').style.display = 'none';
                document.getElementById('invoiceHistoryDiv').style.display = 'block';
                document.getElementById('ageArticleDiv').style.display = 'none';
                document.getElementById('sessionArticleDiv').style.display = 'none';

                $('#promotionTh').css("background-color", "#009999");
                $('#newArticleTh').css("background-color", "#009999");
                $('#invoiceHistoryTh').css("background-color", "#E06C0A");
                $('#ageArticleTh').css("background-color", "#009999");
                $('#seassionArticle').css("background-color", "#009999");

                var customerId = document.getElementById("customerId").value

                $("#custInvHistoryTbl > tbody").html("");

                if(customerId.trim()!=''){

                    document.getElementById('promotionDashboardImage').src='images/ajax-loader2.gif';

                    $.ajax({
                        type: "POST",
                        url: "customerInvoiceHistoryOdr.footin?ftnCustomerCode="+customerId,
                        success: function(data){

                            document.getElementById('promotionDashboardImage').src='';

                            if(data.trim() != ""){                                

                                var jsnData = $.parseJSON(data.trim());

                                for (var i=0; i< jsnData.length; i++){

                                    var markup = "<tr>"
                                    markup += "<td><img src=\"<%= request.getContextPath()%>/ARTICLE_IMAGE/"+jsnData[i].artcode.trim()+".jpg\" style=\"width: 70px; height: 60px;\"/>"+jsnData[i].artcode.trim()+"</td>"
                                    markup += "<td style=\"text-align: left; padding: 2px;\">"+ jsnData[i].orderno.trim() + "</td>"
                                    markup += "<td style=\"text-align: left; padding: 2px;\">"+ jsnData[i].orderdate.trim() + "</td>"
                                    markup += "<td style=\"text-align: center; padding: 2px;\">" + jsnData[i].unitprice.trim() + "</td>"
                                    markup += "<td style=\"text-align: right; padding: 2px;\">"+ jsnData[i].discount.trim() + "</td>"
                                    markup += "<tr>";

                                    $("#custInvHistoryTbl tbody").append(markup);
                                }


                            }

                        }
                    });
                
                }
            }

            function dispayAgeArticle(){
                document.getElementById('promotionDiv').style.display = 'none';
                document.getElementById('newArrivalDiv').style.display = 'none';
                document.getElementById('invoiceHistoryDiv').style.display = 'none';
                document.getElementById('ageArticleDiv').style.display = 'block';
                document.getElementById('sessionArticleDiv').style.display = 'none';

                $('#promotionTh').css("background-color", "#009999");
                $('#newArticleTh').css("background-color", "#009999");
                $('#invoiceHistoryTh').css("background-color", "#009999");
                $('#ageArticleTh').css("background-color", "#E06C0A");
                $('#seassionArticle').css("background-color", "#009999");
            }

            function dispaySessionArticle(){
                document.getElementById('promotionDiv').style.display = 'none';
                document.getElementById('newArrivalDiv').style.display = 'none';
                document.getElementById('invoiceHistoryDiv').style.display = 'none';
                document.getElementById('ageArticleDiv').style.display = 'none';
                document.getElementById('sessionArticleDiv').style.display = 'block';

                $('#promotionTh').css("background-color", "#009999");
                $('#newArticleTh').css("background-color", "#009999");
                $('#invoiceHistoryTh').css("background-color", "#009999");
                $('#ageArticleTh').css("background-color", "#009999");
                $('#seassionArticle').css("background-color", "#E06C0A");
            }

            function ftnOrderSave(){

                var r = confirm("Do you wannt to save?");

                if (r == true) {

                    var customerId = document.getElementById("customerId").value;
                    var contactNumber = document.getElementById("contactNumber").value;
                    var emailAddress = document.getElementById("emailAddress").value ;
                    var firstName = document.getElementById("firstName").value ;
                    var middleName = document.getElementById("middleName").value;
                    var dateOfBirth = document.getElementById("dateOfBirth").value;
                    var lastName = document.getElementById("lastName").value;
                    var sureName = document.getElementById("sureName").value ;
                    var postalCode = document.getElementById("postalCode").value;
                    var customerAddress = document.getElementById("customerAddress").value;
                    var shippingAddress = document.getElementById("shippingAddress").value ;
                    var customerOccupation = document.getElementById("customerOccupation").value;
                    //
                    var countryName = $('#countryList').find('option:selected').text();
                    var stateName = $("select#stateList").val();
                    var cityName = $("select#cityList").val();
                    var customerType = $("select#customerTypeList").val();
                    var gender = $("select#genderList").val();
                    var religion = $("select#religionList").val();
                    var marritalStatus = $("select#marritalList").val();
                    //
                    var additionalDiscount = document.getElementById("additionalDiscount").value;

                    var orderDate =  document.getElementById("dtp_input2").value;
                    var payType = $("select#paymentModeList").val();

                    // get checkbox status cdcConfirmChkbox

                    var cdcConfirmFlag ="N";

                    if($("#cdcConfirmChkbox").is(':checked')){
                        cdcConfirmFlag = "Y";
                    }else{
                        cdcConfirmFlag ="N";
                    }

                    var rowCount = $('#orderTbl >tbody >tr').length;

                    if(contactNumber.trim() == ''){
                        alert('Contact number not found.');
                    }else if(countryName.trim() == '0'){
                        alert('Country not found.');
                    }else if(shippingAddress.trim() == ''){
                        alert('Shipping address not found.');
                    }else if(rowCount == 0){
                        alert('Article not found.');
                    }else if(orderDate.trim() == ''){
                        alert('Order date not found.');
                    }else{
                    
                        var dataString = "ftnCustomerCode="+customerId;
                        dataString += "&ftnContactNumber="+contactNumber;
                        dataString += "&ftnEmailAddress="+emailAddress;
                        dataString += "&ftnFirstName="+firstName;
                        dataString += "&ftnMiddleName="+middleName;
                        dataString += "&ftnLastName="+lastName;
                        dataString += "&ftnSurName="+sureName;
                        dataString += "&ftnCountryName="+countryName;
                        dataString += "&ftnStateName="+stateName;
                        dataString += "&ftnCityName="+cityName;
                        dataString += "&ftnPostalCode="+postalCode;
                        dataString += "&ftnCustomerAddress="+customerAddress;
                        dataString += "&ftnShippingAddress="+shippingAddress;
                        dataString += "&ftnDateOfBirth="+dateOfBirth;
                        dataString += "&ftnCustomerType="+customerType;
                        dataString += "&ftnGender="+gender;
                        dataString += "&ftnReligion="+religion;
                        dataString += "&ftnMaritalStatus="+marritalStatus;
                        dataString += "&ftnOccupation="+customerOccupation;
                        dataString += "&additionalDiscount="+additionalDiscount;
                        dataString += "&cdcConfirmFlag="+cdcConfirmFlag;
                        dataString += "&ftnOrderDate="+orderDate;
                        dataString += "&ftnPaymentType="+payType;

                        //alert(dataString);

                        document.getElementById('orderArtImage').src='images/ajax-loader2.gif';

                        $.ajax({
                            type: "POST",
                            url: "saveCustomerOrderOdr.footin?"+dataString,
                            success: function(data){

                                document.getElementById('orderArtImage').src='';

                                alert(data);

                                resetAll('2');

                            }
                        });

                        

                    }
                }

            }
            
            function ftnArticleAdd(){

                var r = confirm("Do you wannt to save?");

                if (r == true) {

                    var contactNo = document.getElementById("contactNumber").value;

                    if(contactNo.trim() == ''){
                        alert('Please put customer contact number.');
                    }else{

                        var pairFlag = false;

                        var tempOrderData = "<CONTACT_NO>" + contactNo+"</CONTACT_NO>";

                        var art = $("#orderArticleCode").val();
                        tempOrderData += "<ART_CODE>" +art+"</ART_CODE>";
                        //
                        var size = $("#orderArticleSizeCode").val();
                        tempOrderData += "<SIZE_CODE>" +size+"</SIZE_CODE>";
                        //
                        var description = "";
                        tempOrderData += "<DESCRIPTION>" +description+"</DESCRIPTION>";
                        //
                        tempOrderData += "<SIZE_PAIR>";
                        //
                        var sizeS1 = $("#sizeS1").html();
                        var ordPairS1 = $("#ordPairS1").val();

                        if(ordPairS1.trim() !=''){
                            if(parseInt(ordPairS1.trim()) > 0){
                                tempOrderData += sizeS1+"<FD>"+ ordPairS1+"<FD><RD>";
                                pairFlag = true;
                            }
                        }

                        //
                        var sizeS2 = $("#sizeS2").html();
                        var ordPairS2 = $("#ordPairS2").val();

                        if(ordPairS2.trim() !=''){
                            if(parseInt(ordPairS2.trim()) > 0){
                                tempOrderData += sizeS2+"<FD>"+ ordPairS2+"<FD><RD>";
                                pairFlag = true;
                            }
                        }
                        //
                        var sizeS3 = $("#sizeS3").html();
                        var ordPairS3 = $("#ordPairS3").val();

                        if(ordPairS3.trim() !=''){
                            if(parseInt(ordPairS3.trim()) > 0){
                                tempOrderData += sizeS3+"<FD>"+ ordPairS3+"<FD><RD>";
                                pairFlag = true;
                            }
                        }
                        //
                        var sizeS4 = $("#sizeS4").html();
                        var ordPairS4 = $("#ordPairS4").val();

                        if(ordPairS4.trim() !=''){
                            if(parseInt(ordPairS4.trim()) > 0){
                                tempOrderData += sizeS4+"<FD>"+ ordPairS4+"<FD><RD>";
                                pairFlag = true;
                            }
                        }
                        //
                        var sizeS5 = $("#sizeS5").html();
                        var ordPairS5 = $("#ordPairS5").val();

                        if(ordPairS5.trim() !=''){
                            if(parseInt(ordPairS5.trim()) > 0){
                                tempOrderData += sizeS5+"<FD>"+ ordPairS5+"<FD><RD>";
                                pairFlag = true;
                            }
                        }
                        //
                        var sizeS6 = $("#sizeS6").html();
                        var ordPairS6 = $("#ordPairS6").val();

                        if(ordPairS6.trim() !=''){
                            if(parseInt(ordPairS6.trim()) > 0){
                                tempOrderData += sizeS6+"<FD>"+ ordPairS6+"<FD><RD>";
                                pairFlag = true;
                            }
                        }
                        //
                        var sizeS7 = $("#sizeS7").html();
                        var ordPairS7 = $("#ordPairS7").val();

                        if(ordPairS7.trim() !=''){
                            if(parseInt(ordPairS7.trim()) > 0){
                                tempOrderData += sizeS7+"<FD>"+ ordPairS7+"<FD><RD>";
                                pairFlag = true;
                            }
                        }
                        //
                        var sizeS8 = $("#sizeS8").html();
                        var ordPairS8 = $("#ordPairS8").val();

                        if(ordPairS8.trim() !=''){
                            if(parseInt(ordPairS8.trim()) > 0){
                                tempOrderData += sizeS8+"<FD>"+ ordPairS8+"<FD><RD>";
                                pairFlag = true;
                            }
                        }
                        //
                        var sizeS9 = $("#sizeS9").html();
                        var ordPairS9 = $("#ordPairS9").val();

                        if(ordPairS9.trim() !=''){
                            if(parseInt(ordPairS9.trim()) > 0){
                                tempOrderData += sizeS9+"<FD>"+ ordPairS9+"<FD><RD>";
                                pairFlag = true;
                            }
                        }
                        //
                        var sizeS10 = $("#sizeS10").html();
                        var ordPairS10 = $("#ordPairS10").val();

                        if(ordPairS10.trim() !=''){
                            if(parseInt(ordPairS10.trim()) > 0){
                                tempOrderData += sizeS10+"<FD>"+ ordPairS10+"<FD><RD>";
                                pairFlag = true;
                            }
                        }
                        //
                        var sizeS11 = $("#sizeS11").html();
                        var ordPairS11 = $("#ordPairS11").val();

                        if(ordPairS11.trim() !=''){
                            if(parseInt(ordPairS11.trim()) > 0){
                                tempOrderData += sizeS11+"<FD>"+ ordPairS11+"<FD><RD>";
                                pairFlag = true;
                            }
                        }
                        //
                        var sizeS12 = $("#sizeS12").html();
                        var ordPairS12 = $("#ordPairS12").val();

                        if(ordPairS12.trim() !=''){
                            if(parseInt(ordPairS12.trim()) > 0){
                                tempOrderData += sizeS12+"<FD>"+ ordPairS12+"<FD><RD>";
                                pairFlag = true;
                            }
                        }
                        //
                        var sizeS13 = $("#sizeS13").html();
                        var ordPairS13 = $("#ordPairS13").val();

                        if(ordPairS13.trim() !=''){
                            if(parseInt(ordPairS13.trim()) > 0){
                                tempOrderData += sizeS13+"<FD>"+ ordPairS13+"<FD><RD>";
                                pairFlag = true;
                            }
                        }
                        //
                        tempOrderData += "</SIZE_PAIR>";
                        //
                        var mrp = $("#orderArticleMrp").val();
                        tempOrderData +="<MRP>"+ mrp+"</MRP>";
                        //
                        var discount = document.getElementById('orderArticleDiscount').value;
                        tempOrderData += "<DISCOUNT>"+ (discount.trim() == '' ? "0" : discount.trim())+"</DISCOUNT>";
                        //
                        var promotionid = document.getElementById('orderArticlePromotionId').value;
                        tempOrderData += "<PROMOTION>" + promotionid + "</PROMOTION>";



                        if (pairFlag == false){
                            alert("Please put order pair.");
                        }else{

                            //alert(tempOrderData);

                            document.getElementById('orderArtImage').src='images/ajax-loader2.gif';

                            $.ajax({
                                type: "POST",
                                url: "tempOrderArticleOdr.footin?tempOrderArticleData="+tempOrderData,
                                success: function(data){

                                    document.getElementById('orderArtImage').src='';

                                    if(data.trim() != ""){

                                        $("#orderTbl > tbody").html("");

                                        var jsonOrderArticle = $.parseJSON(data.trim());

                                        var totalSubTot = '';

                                        for (var i=0; i< jsonOrderArticle.length; i++){

                                            var markup = "<tr>"
                                            markup += "<td style=\"text-align: left; padding: 2px;\">"+ jsonOrderArticle[i].artcode.trim() + "</td>"
                                            markup += "<td style=\"text-align: left; padding: 2px;\">"+ jsonOrderArticle[i].description.trim() + "</td>"
                                            markup += "<td style=\"text-align: center; padding: 2px;\">" + jsonOrderArticle[i].size.trim() + "</td>"
                                            markup += "<td style=\"text-align: center; padding: 2px;\">" + jsonOrderArticle[i].qty.trim() + "</td>"
                                            markup += "<td style=\"text-align: right; padding: 2px;\">" + jsonOrderArticle[i].mrp.trim() + "</td>"
                                            markup += "<td style=\"text-align: right; padding: 2px;\">"+ jsonOrderArticle[i].discount.trim() + "</td>"
                                            markup += "<td style=\"text-align: right; padding: 2px;\">" + jsonOrderArticle[i].subtotal.trim() + "</td>"
                                            markup += "<td style=\"text-align: center; padding: 2px;\"><a onClick=\"deleteRow('"+jsonOrderArticle[i].artcode.trim()+"','"+jsonOrderArticle[i].size.trim()+"');\" href=\"javascript:void(0);\" style=\"text-decoration: none;\" >Del</a></td>"
                                            markup += "</tr>";

                                            totalSubTot = jsonOrderArticle[i].totalsubtot.trim();

                                            $("#orderTbl tbody").append(markup);
                                        }

                                        document.getElementById("orderArtSubTotal").innerHTML = totalSubTot;
                                        document.getElementById("orderArtGrandTotal").innerHTML = totalSubTot;

                                    }

                                }
                            });

                        }

                    }
                    
                }
                
            }

            function deleteRow(artCode, sizeCode) {
            
                var r = confirm("Do you wannt to delete?");

                if (r == true) {

                    var contactNo = document.getElementById("contactNumber").value;

                    if(contactNo.trim() == ''){
                        alert('Please put customer contact number.');
                    }else{

                        document.getElementById('orderArtImage').src='images/ajax-loader2.gif';

                        $.ajax({
                            type: "POST",
                            url: "tempOrderArticleDeleteOdr.footin?searchContactNumber="+contactNo+"&searchArticleCode="+artCode+"&searchArticleSizeCode="+sizeCode,
                            success: function(data){

                                document.getElementById('orderArtImage').src='';

                                if(data.trim() != ""){

                                    $("#orderTbl > tbody").html("");

                                    var jsonOrderArticle = $.parseJSON(data.trim());

                                    var totalSubTot = '';


                                    for (var i=0; i< jsonOrderArticle.length; i++){

                                        var markup = "<tr>"
                                        markup += "<td style=\"text-align: left; padding: 2px;\">"+ jsonOrderArticle[i].artcode.trim() + "</td>"
                                        markup += "<td style=\"text-align: left; padding: 2px;\">"+ jsonOrderArticle[i].description.trim() + "</td>"
                                        markup += "<td style=\"text-align: center; padding: 2px;\">" + jsonOrderArticle[i].size.trim() + "</td>"
                                        markup += "<td style=\"text-align: center; padding: 2px;\">" + jsonOrderArticle[i].qty.trim() + "</td>"
                                        markup += "<td style=\"text-align: right; padding: 2px;\">" + jsonOrderArticle[i].mrp.trim() + "</td>"
                                        markup += "<td style=\"text-align: right; padding: 2px;\">"+ jsonOrderArticle[i].discount.trim() + "</td>"
                                        markup += "<td style=\"text-align: right; padding: 2px;\">" + jsonOrderArticle[i].subtotal.trim() + "</td>"
                                        markup += "<td style=\"text-align: center; padding: 2px;\"><a onClick=\"deleteRow('"+jsonOrderArticle[i].artcode.trim()+"','"+jsonOrderArticle[i].size.trim()+"');\" href=\"javascript:void(0);\" style=\"text-decoration: none;\" >Del</a></td>"
                                        markup += "<tr>";

                                        totalSubTot = jsonOrderArticle[i].totalsubtot.trim();


                                        $("#orderTbl tbody").append(markup);
                                    }

                                    document.getElementById("orderArtSubTotal").innerHTML = totalSubTot;

                                }

                            }
                        });


                    }
                }
            }

            function ftnCustomerSearch(){

                var custId = document.getElementById("customerSearchId").value;
                var contactNo = document.getElementById("customerSearchContact").value;

                if((custId == '') && (contactNo == '')){
                    alert("Please put search data.");
                }else{
                    
                    document.getElementById("customerId").value = '';
                    document.getElementById("contactNumber").value = '';
                    document.getElementById("emailAddress").value = '';
                    document.getElementById("firstName").value = '';
                    document.getElementById("middleName").value = '';
                    document.getElementById("dateOfBirth").value = '';
                    document.getElementById("lastName").value = '';
                    document.getElementById("sureName").value = '';
                    document.getElementById("postalCode").value = '';
                    document.getElementById("customerAddress").value = '';
                    document.getElementById("shippingAddress").value = '';
                    document.getElementById("customerOccupation").value = '';
                    
                    $("#countryList").val('0');
                    var selectState1 = $('#stateList');
                    selectState1.find('option').remove();
                    //$("#stateList").val('0');
                    var selectCity1 = $('#cityList');
                    selectCity1.find('option').remove();
                    // $("#cityList").val('0');
                    $("#customerTypeList").val('Standard');
                    $("#genderList").val('0');
                    $("#religionList").val('0');
                    $("#marritalList").val('0');
                    //
                    document.getElementById('customerSearchImage').src='images/ajax-loader2.gif';

                    $.ajax({
                        type: "POST",
                        url: "searchFtnCustomerOdr.footin?searchCustomerCode="+custId+"&searchContactNumber="+contactNo,
                        success: function(data){

                            document.getElementById('customerSearchImage').src='';

                            document.getElementById("contactNumber").value = contactNo;

                            //alert(data);

                            var dataArray = data.trim().split("<FD>");
                            
                            // generate state lob
                            if(dataArray[0].trim() != ""){
                                var selectState = $('#stateList');
                                selectState.find('option').remove();
                                var jsonState = $.parseJSON(dataArray[0].trim());
                                $('<option>').val("0").text("SELECT").appendTo(selectState);
                                for (var i=0; i< jsonState.length; i++){
                                    $('<option>').val(jsonState[i].name).text(jsonState[i].name).appendTo(selectState);
                                }
                            }
                            // generate city lob
                            if(dataArray[1].trim() != ""){
                                var selectCity = $('#cityList');
                                selectCity.find('option').remove();
                                var jsonCity = $.parseJSON(dataArray[1].trim());
                                $('<option>').val("0").text("SELECT").appendTo(selectCity);
                                for (var i=0; i< jsonCity.length; i++){
                                    $('<option>').val(jsonCity[i].name).text(jsonCity[i].name).appendTo(selectCity);
                                }
                            }

                            // generate customer details data

                            if(dataArray[2].trim() != ""){
                                if(dataArray[2].trim() != "[]"){

                                    var json = $.parseJSON(dataArray[2].trim());

                                    for (var i=0; i< json.length; i++){

                                        document.getElementById("customerId").value = json[i].custid;
                                        document.getElementById("contactNumber").value = json[i].contactno;
                                        document.getElementById("emailAddress").value = json[i].email;
                                        document.getElementById("firstName").value = json[i].fname;
                                        document.getElementById("middleName").value = json[i].mname;
                                        document.getElementById("dateOfBirth").value = json[i].dob;
                                        //
                                        document.getElementById("lastName").value = json[i].lname;
                                        document.getElementById("sureName").value = json[i].sname;
                                        document.getElementById("postalCode").value = json[i].pscode;
                                        document.getElementById("customerAddress").value = json[i].add;
                                        document.getElementById("shippingAddress").value = json[i].sadd;
                                        document.getElementById("customerOccupation").value = json[i].occupation;

                                        //
                                        if(json[i].country.trim() == ''){
                                            $("#countryList").val('0');
                                        }else{
                                            //$("#countryList").val(json[i].country.trim());
                                            $("#countryList option:contains(" + json[i].country.trim() + ")").attr('selected', 'selected');
                                        }
                                        if(json[i].state.trim() == ''){
                                            $("#stateList").val('0');
                                        }else{
                                            $("#stateList").val(json[i].state.trim());
                                        }
                                        if(json[i].city.trim() == ''){
                                            $("#cityList").val('0');
                                        }else{
                                            $("#cityList").val(json[i].city.trim());
                                        }
                                        //
                                        if(json[i].state.trim() == ''){
                                            $("#customerTypeList").val('Standard');
                                        }else{
                                            $("#customerTypeList").val(json[i].customertype.trim());
                                        }
                                        //
                                        if(json[i].gender.trim() == ''){
                                            $("#genderList").val('0');
                                        }else{
                                            $("#genderList").val(json[i].gender.trim());
                                        }

                                        if(json[i].religion.trim() == ''){
                                            $("#religionList").val('0');
                                        }else{
                                            $("#religionList").val(json[i].religion.trim());
                                        }

                                        if(json[i].mrstatus.trim() == ''){
                                            $("#marritalList").val('0');
                                        }else{
                                            $("#marritalList").val(json[i].mrstatus.trim());
                                        }
                                    }

                                }
                            }

                            // Temporary order article

                            if(dataArray[3].trim() != ""){

                                $("#orderTbl > tbody").html("");

                                //alert(dataArray[3].trim());

                                var jsonOrderArticle = $.parseJSON(dataArray[3].trim());

                                //alert(jsonOrderArticle.length);


                                var totalSubTot = '';


                                for (var i=0; i< jsonOrderArticle.length; i++){

                                    var markup = "<tr>"
                                    markup += "<td style=\"text-align: left; padding: 2px;\">"+ jsonOrderArticle[i].artcode.trim() + "</td>"
                                    markup += "<td style=\"text-align: left; padding: 2px;\">"+ jsonOrderArticle[i].description.trim() + "</td>"
                                    markup += "<td style=\"text-align: center; padding: 2px;\">" + jsonOrderArticle[i].size.trim() + "</td>"
                                    markup += "<td style=\"text-align: center; padding: 2px;\">" + jsonOrderArticle[i].qty.trim() + "</td>"
                                    markup += "<td style=\"text-align: right; padding: 2px;\">" + jsonOrderArticle[i].mrp.trim() + "</td>"
                                    markup += "<td style=\"text-align: right; padding: 2px;\">"+ jsonOrderArticle[i].discount.trim() + "</td>"
                                    markup += "<td style=\"text-align: right; padding: 2px;\">" + jsonOrderArticle[i].subtotal.trim() + "</td>"
                                    markup += "<td style=\"text-align: center; padding: 2px;\"><a onClick=\"deleteRow('"+jsonOrderArticle[i].artcode.trim()+"','"+jsonOrderArticle[i].size.trim()+"');\" href=\"javascript:void(0);\" style=\"text-decoration: none;\" >Del</a></td>"
                                    markup += "<tr>";

                                    totalSubTot = jsonOrderArticle[i].totalsubtot.trim();

                                    //alert(totalSubTot);

                                    $("#orderTbl tbody").append(markup);
                                }

                                document.getElementById("orderArtSubTotal").innerHTML = totalSubTot;
                                document.getElementById("orderArtGrandTotal").innerHTML = totalSubTot;

                            }


                        }
                    });
                }
            }
            
            function ftnCustomerArticleSearch(){

                var custId = document.getElementById("customerSearchId").value;
                var searchArtCode = document.getElementById("searchArticleCode").value;

                if((custId == '') && (searchArtCode == '')){
                    alert("Please put article code.");
                }else{

                    document.getElementById("orderArticleCodelbl").innerHTML  = '';
                    document.getElementById("orderArticleCode").value  = '';
                    document.getElementById("orderArticleSizeCode").value  = '';
                    document.getElementById("orderArticleMrp").value  = '';
                    document.getElementById("orderArticleDiscount").value  = '';
                    //document.getElementById("orderArtSubTotal").innerHTML  = '';
                    //document.getElementById("orderArtGrandTotal").innerHTML  = '';
                    document.getElementById("orderArticlePromotionId").value  = '';
                    //
                    document.getElementById("artBrand").innerHTML  = '';
                    document.getElementById("artCat").innerHTML  = '';
                    document.getElementById("artSubCat").innerHTML  = '';
                    //
                    document.getElementById("artMrp").innerHTML  = "MRP";
                    document.getElementById("artNet").innerHTML  = "Net";
                    document.getElementById("artVat").innerHTML  = "Vat";
                    document.getElementById("artCost").innerHTML  = "Cost";
                    //
                    document.getElementById("sizeS1").innerHTML  = '';
                    document.getElementById("sizeS2").innerHTML  = '';
                    document.getElementById("sizeS3").innerHTML  = '';
                    document.getElementById("sizeS4").innerHTML  = '';
                    document.getElementById("sizeS5").innerHTML  = '';
                    document.getElementById("sizeS6").innerHTML  = '';
                    document.getElementById("sizeS7").innerHTML  = '';
                    document.getElementById("sizeS8").innerHTML  = '';
                    document.getElementById("sizeS9").innerHTML  = '';
                    document.getElementById("sizeS10").innerHTML = '';
                    document.getElementById("sizeS11").innerHTML  = '';
                    document.getElementById("sizeS12").innerHTML  = '';
                    document.getElementById("sizeS13").innerHTML  = '';
                    //
                    document.getElementById("stkPairS1").innerHTML  = '';
                    document.getElementById("stkPairS2").innerHTML  = '';
                    document.getElementById("stkPairS3").innerHTML  = '';
                    document.getElementById("stkPairS4").innerHTML  = '';
                    document.getElementById("stkPairS5").innerHTML  = '';
                    document.getElementById("stkPairS6").innerHTML  = '';
                    document.getElementById("stkPairS7").innerHTML  = '';
                    document.getElementById("stkPairS8").innerHTML  = '';
                    document.getElementById("stkPairS9").innerHTML  = '';
                    document.getElementById("stkPairS10").innerHTML  = '';
                    document.getElementById("stkPairS11").innerHTML  = '';
                    document.getElementById("stkPairS12").innerHTML  = '';
                    document.getElementById("stkPairS13").innerHTML  = '';
                    document.getElementById("stkPairSTotal").innerHTML  = '';
                    //
                    document.getElementById("ordPairS1").value  = '';
                    document.getElementById("ordPairS2").value  = '';
                    document.getElementById("ordPairS3").value  = '';
                    document.getElementById("ordPairS4").value  = '';
                    document.getElementById("ordPairS5").value  = '';
                    document.getElementById("ordPairS6").value  = '';
                    document.getElementById("ordPairS7").value  = '';
                    document.getElementById("ordPairS8").value  = '';
                    document.getElementById("ordPairS9").value  = '';
                    document.getElementById("ordPairS10").value  = '';
                    document.getElementById("ordPairS11").value  = '';
                    document.getElementById("ordPairS12").value  = '';
                    document.getElementById("ordPairS13").value  = '';
                    document.getElementById("ordPairTotal").value  = '';
                    //
                    $("#ordPairS1").attr('readonly', true);
                    $("#ordPairS2").attr('readonly', true);
                    $("#ordPairS3").attr('readonly', true);
                    $("#ordPairS4").attr('readonly', true);
                    $("#ordPairS5").attr('readonly', true);
                    $("#ordPairS6").attr('readonly', true);
                    $("#ordPairS7").attr('readonly', true);
                    $("#ordPairS8").attr('readonly', true);
                    $("#ordPairS9").attr('readonly', true);
                    $("#ordPairS10").attr('readonly', true);
                    $("#ordPairS11").attr('readonly', true);
                    $("#ordPairS12").attr('readonly', true);
                    $("#ordPairS13").attr('readonly', true);
                    
                    //
                    document.getElementById('articleSearchImage').src='images/ajax-loader2.gif';

                    $.ajax({
                        type: "POST",
                        url: "searchFtnArticleOdr.footin?searchCustomerCode="+custId+"&searchArticleCode="+searchArtCode,
                        success: function(data){

                            document.getElementById('articleSearchImage').src='';

                            document.getElementById('articleImageId').src='ARTICLE_IMAGE/'+searchArtCode+'.jpg';

                            // <//%= request.getContextPath()%>/ARTICLE_IMAGE/test.jpg


                            var dataArray = data.trim().split("<FD>");

                            // generate customer details data

                            var json = $.parseJSON(dataArray[0].trim());

                            for (var i=0; i< json.length; i++){


                                document.getElementById("orderArticleCodelbl").innerHTML  = json[i].artcode;
                                document.getElementById("orderArticleCode").value  = json[i].artcode;
                                document.getElementById("orderArticleSizeCode").value  = json[i].sizecode;
                                document.getElementById("orderArticleMrp").value  = json[i].artmrp;


                                document.getElementById("artBrand").innerHTML  = json[i].brandcode+"&nbsp;-&nbsp;"+json[i].brandname;
                                document.getElementById("artCat").innerHTML  = json[i].catcode+"&nbsp;-&nbsp;"+json[i].catname;
                                document.getElementById("artSubCat").innerHTML  = json[i].sbcatcode+"&nbsp;-&nbsp;"+json[i].sbcatname;
                                //
                                document.getElementById("artMrp").innerHTML  = "MRP<br/>" + json[i].artmrp+"&nbsp;BDT";
                                document.getElementById("artNet").innerHTML  = "Net<br/>" + json[i].artnet+"&nbsp;BDT";
                                document.getElementById("artVat").innerHTML  = "Vat<br/>" + json[i].artvat+"&nbsp;BDT";
                                document.getElementById("artCost").innerHTML  = "Cost<br/>" + json[i].artcost+"&nbsp;BDT";


                                document.getElementById("sizeS1").innerHTML  = json[i].sizes1;
                                document.getElementById("sizeS2").innerHTML  = json[i].sizes2;
                                document.getElementById("sizeS3").innerHTML  = json[i].sizes3;
                                document.getElementById("sizeS4").innerHTML  = json[i].sizes4;
                                document.getElementById("sizeS5").innerHTML  = json[i].sizes5;
                                document.getElementById("sizeS6").innerHTML  = json[i].sizes6;
                                document.getElementById("sizeS7").innerHTML  = json[i].sizes7;
                                document.getElementById("sizeS8").innerHTML  = json[i].sizes8;
                                document.getElementById("sizeS9").innerHTML  = json[i].sizes9;
                                document.getElementById("sizeS10").innerHTML  = json[i].sizes10;
                                document.getElementById("sizeS11").innerHTML  = json[i].sizes11;
                                document.getElementById("sizeS12").innerHTML  = json[i].sizes12;
                                document.getElementById("sizeS13").innerHTML  = json[i].sizes13;
                                //
                                document.getElementById("stkPairS1").innerHTML  = json[i].stkr1;
                                document.getElementById("stkPairS2").innerHTML  = json[i].stkr2;
                                document.getElementById("stkPairS3").innerHTML  = json[i].stkr3;
                                document.getElementById("stkPairS4").innerHTML  = json[i].stkr4;
                                document.getElementById("stkPairS5").innerHTML  = json[i].stkr5;
                                document.getElementById("stkPairS6").innerHTML  = json[i].stkr6;
                                document.getElementById("stkPairS7").innerHTML  = json[i].stkr7;
                                document.getElementById("stkPairS8").innerHTML  = json[i].stkr8;
                                document.getElementById("stkPairS9").innerHTML  = json[i].stkr9;
                                document.getElementById("stkPairS10").innerHTML  = json[i].stkr10;
                                document.getElementById("stkPairS11").innerHTML  = json[i].stkr11;
                                document.getElementById("stkPairS12").innerHTML  = json[i].stkr12;
                                document.getElementById("stkPairS13").innerHTML  = json[i].stkr13;
                                document.getElementById("stkPairSTotal").innerHTML  = json[i].stktotal;

                                if(json[i].sizes1.trim()!=''){
                                    $("#ordPairS1").attr('readonly', false);
                                }
                                if(json[i].sizes2.trim()!=''){
                                    $("#ordPairS2").attr('readonly', false);
                                }
                                if(json[i].sizes3.trim()!=''){
                                    $("#ordPairS3").attr('readonly', false);
                                }
                                if(json[i].sizes4.trim()!=''){
                                    $("#ordPairS4").attr('readonly', false);
                                }
                                if(json[i].sizes5.trim()!=''){
                                    $("#ordPairS5").attr('readonly', false);
                                }
                                if(json[i].sizes6.trim()!=''){
                                    $("#ordPairS6").attr('readonly', false);
                                }
                                if(json[i].sizes7.trim()!=''){
                                    $("#ordPairS7").attr('readonly', false);
                                }
                                if(json[i].sizes8.trim()!=''){
                                    $("#ordPairS8").attr('readonly', false);
                                }
                                if(json[i].sizes9.trim()!=''){
                                    $("#ordPairS9").attr('readonly', false);
                                }
                                if(json[i].sizes10.trim()!=''){
                                    $("#ordPairS10").attr('readonly', false);
                                }
                                if(json[i].sizes11.trim()!=''){
                                    $("#ordPairS11").attr('readonly', false);
                                }
                                if(json[i].sizes12.trim()!=''){
                                    $("#ordPairS12").attr('readonly', false);
                                }
                                if(json[i].sizes13.trim()!=''){
                                    $("#ordPairS13").attr('readonly', false);
                                }

                                break;

                            }

                            // generate promotion
                            if(dataArray[1].trim() != ""){

                                $("#artPromotionTbl > tbody").html("");

                                var jsonPromotion= $.parseJSON(dataArray[1].trim());

                                for (var i=0; i< jsonPromotion.length; i++){

                                    var markup = "<tr>"
                                    markup += "<td style=\"text-align: center; padding: 2px;\"><input onClick=\"putArtDiscount('"+jsonPromotion[i].promotionid.trim()+"','"+jsonPromotion[i].discount.trim()+"');\" type=\"radio\" name=\"promotion\" /></td>"
                                    markup += "<td style=\"text-align: left; padding: 2px;\">"+ jsonPromotion[i].promotionname.trim() + "</td>"
                                    markup += "<td style=\"text-align: center; padding: 2px;\">"+ jsonPromotion[i].starttime.trim() + "</td>"
                                    markup += "<td style=\"text-align: center; padding: 2px;\">" + jsonPromotion[i].endtime.trim() + "</td>"
                                    markup += "<td style=\"text-align: right; padding: 2px;\">"+ jsonPromotion[i].discount.trim() + "</td>"
                                    markup += "<tr>";

                                    // alert(markup);

                                    $("#artPromotionTbl tbody").append(markup);
                                }


                            }

                        }
                    });
                }
            }

            function putArtDiscount(promotionid, discount) {

                //alert(promotionid);
                document.getElementById("orderArticleDiscount").value = discount;
                document.getElementById("orderArticlePromotionId").value = promotionid;

            }

            function clearDate() {
                document.getElementById("dtp_input2").value = '';
                document.getElementById("dtp_input4").value = '';
            }

            function resetAll(fn){

                var r = false;

                if(fn == '1'){
                    r = confirm("Do you wannt to clear?");
                }else{
                    r = true;
                }

                if (r == true) {

                    document.getElementById('articleImageId').src = '';
                    
                    document.getElementById("customerSearchId").value = '';
                    document.getElementById("customerSearchContact").value = '';
                    document.getElementById("searchArticleCode").value = '';

                    document.getElementById("customerId").value = '';
                    document.getElementById("contactNumber").value = '';
                    document.getElementById("emailAddress").value = '';
                    document.getElementById("firstName").value = '';
                    document.getElementById("middleName").value = '';
                    document.getElementById("dateOfBirth").value = '';
                    document.getElementById("lastName").value = '';
                    document.getElementById("sureName").value = '';
                    document.getElementById("postalCode").value = '';
                    document.getElementById("customerAddress").value = '';
                    document.getElementById("shippingAddress").value = '';
                    document.getElementById("customerOccupation").value = '';
                    document.getElementById("additionalDiscount").value = '';

                    $("#countryList").val('0');
                    var selectState1 = $('#stateList');
                    selectState1.find('option').remove();
                    var selectCity1 = $('#cityList');
                    selectCity1.find('option').remove();
                    $("#customerTypeList").val('Standard');
                    $("#paymentModeList").val('COD');
                    $("#genderList").val('0');
                    $("#religionList").val('0');
                    $("#marritalList").val('0');
                    //
                    document.getElementById("orderArticleCodelbl").innerHTML  = '';
                    document.getElementById("orderArticleCode").value  = '';
                    document.getElementById("orderArticleSizeCode").value  = '';
                    document.getElementById("orderArticleMrp").value  = '';
                    document.getElementById("orderArticleDiscount").value  = '';
                    document.getElementById("orderArtSubTotal").innerHTML  = '';
                    document.getElementById("orderArtGrandTotal").innerHTML  = '';
                    document.getElementById("orderArticlePromotionId").value  = '';
                    //
                    document.getElementById("artBrand").innerHTML  = '';
                    document.getElementById("artCat").innerHTML  = '';
                    document.getElementById("artSubCat").innerHTML  = '';
                    //
                    document.getElementById("artMrp").innerHTML  = "MRP";
                    document.getElementById("artNet").innerHTML  = "Net";
                    document.getElementById("artVat").innerHTML  = "Vat";
                    document.getElementById("artCost").innerHTML  = "Cost";
                    //
                    document.getElementById("sizeS1").innerHTML  = '';
                    document.getElementById("sizeS2").innerHTML  = '';
                    document.getElementById("sizeS3").innerHTML  = '';
                    document.getElementById("sizeS4").innerHTML  = '';
                    document.getElementById("sizeS5").innerHTML  = '';
                    document.getElementById("sizeS6").innerHTML  = '';
                    document.getElementById("sizeS7").innerHTML  = '';
                    document.getElementById("sizeS8").innerHTML  = '';
                    document.getElementById("sizeS9").innerHTML  = '';
                    document.getElementById("sizeS10").innerHTML = '';
                    document.getElementById("sizeS11").innerHTML  = '';
                    document.getElementById("sizeS12").innerHTML  = '';
                    document.getElementById("sizeS13").innerHTML  = '';
                    //
                    document.getElementById("stkPairS1").innerHTML  = '';
                    document.getElementById("stkPairS2").innerHTML  = '';
                    document.getElementById("stkPairS3").innerHTML  = '';
                    document.getElementById("stkPairS4").innerHTML  = '';
                    document.getElementById("stkPairS5").innerHTML  = '';
                    document.getElementById("stkPairS6").innerHTML  = '';
                    document.getElementById("stkPairS7").innerHTML  = '';
                    document.getElementById("stkPairS8").innerHTML  = '';
                    document.getElementById("stkPairS9").innerHTML  = '';
                    document.getElementById("stkPairS10").innerHTML  = '';
                    document.getElementById("stkPairS11").innerHTML  = '';
                    document.getElementById("stkPairS12").innerHTML  = '';
                    document.getElementById("stkPairS13").innerHTML  = '';
                    document.getElementById("stkPairSTotal").innerHTML  = '';
                    //
                    document.getElementById("ordPairS1").value  = '';
                    document.getElementById("ordPairS2").value  = '';
                    document.getElementById("ordPairS3").value  = '';
                    document.getElementById("ordPairS4").value  = '';
                    document.getElementById("ordPairS5").value  = '';
                    document.getElementById("ordPairS6").value  = '';
                    document.getElementById("ordPairS7").value  = '';
                    document.getElementById("ordPairS8").value  = '';
                    document.getElementById("ordPairS9").value  = '';
                    document.getElementById("ordPairS10").value  = '';
                    document.getElementById("ordPairS11").value  = '';
                    document.getElementById("ordPairS12").value  = '';
                    document.getElementById("ordPairS13").value  = '';
                    document.getElementById("ordPairTotal").value  = '';
                    //
                    $("#ordPairS1").attr('readonly', true);
                    $("#ordPairS2").attr('readonly', true);
                    $("#ordPairS3").attr('readonly', true);
                    $("#ordPairS4").attr('readonly', true);
                    $("#ordPairS5").attr('readonly', true);
                    $("#ordPairS6").attr('readonly', true);
                    $("#ordPairS7").attr('readonly', true);
                    $("#ordPairS8").attr('readonly', true);
                    $("#ordPairS9").attr('readonly', true);
                    $("#ordPairS10").attr('readonly', true);
                    $("#ordPairS11").attr('readonly', true);
                    $("#ordPairS12").attr('readonly', true);
                    $("#ordPairS13").attr('readonly', true);
                    //                    
                    $("#orderTbl > tbody").html("");
                    $("#artPromotionTbl > tbody").html("");
                }

            }

            function totalOrderPairCal(){

                var odrPrS1 = document.getElementById("ordPairS1").value;
                var odrPrS2 = document.getElementById("ordPairS2").value;
                var odrPrS3 = document.getElementById("ordPairS3").value;
                var odrPrS4 = document.getElementById("ordPairS4").value;
                var odrPrS5 = document.getElementById("ordPairS5").value;
                var odrPrS6 = document.getElementById("ordPairS6").value;
                var odrPrS7 = document.getElementById("ordPairS7").value;
                var odrPrS8 = document.getElementById("ordPairS8").value;
                var odrPrS9 = document.getElementById("ordPairS9").value;
                var odrPrS10 = document.getElementById("ordPairS10").value;
                var odrPrS11 = document.getElementById("ordPairS11").value;
                var odrPrS12 = document.getElementById("ordPairS12").value;
                var odrPrS13 = document.getElementById("ordPairS13").value;

                var totalPair = (parseInt(odrPrS1 == '' ? '0' : odrPrS1)
                    + parseInt(odrPrS2 == '' ? '0' : odrPrS2)
                    + parseInt(odrPrS3 == '' ? '0' : odrPrS3)
                    + parseInt(odrPrS4 == '' ? '0' : odrPrS4)
                    + parseInt(odrPrS5 == '' ? '0' : odrPrS5)
                    + parseInt(odrPrS6 == '' ? '0' : odrPrS6)
                    + parseInt(odrPrS7 == '' ? '0' : odrPrS7)
                    + parseInt(odrPrS8 == '' ? '0' : odrPrS8)
                    + parseInt(odrPrS9 == '' ? '0' : odrPrS9)
                    + parseInt(odrPrS10 == '' ? '0' : odrPrS10)
                    + parseInt(odrPrS11 == '' ? '0' : odrPrS11)
                    + parseInt(odrPrS12 == '' ? '0' : odrPrS12)
                    + parseInt(odrPrS13 == '' ? '0' : odrPrS13));


                document.getElementById("ordPairTotal").value = totalPair;




            }

        </script>

        <script  type="text/javascript">

            function today(){
                var d = new Date();
                var curr_date = d.getDate();
                var curr_month = d.getMonth() + 1;
                var curr_year = d.getFullYear();
                //alert(curr_date.length);
                var dt = (curr_date < 9 ? ("0" + curr_date) : curr_date) + "-" + (curr_month < 9 ? "0" + curr_month : curr_month) + "-" + curr_year;
                //alert(dt);

                document.getElementById("dtp_input2").value = dt;
                document.getElementById("dtp_input4").value = dt;
            }

            $(document).ready(function($) {

                document.getElementById('promotionDiv').style.display = 'block';
                document.getElementById('newArrivalDiv').style.display = 'none';
                document.getElementById('invoiceHistoryDiv').style.display = 'none';
                document.getElementById('ageArticleDiv').style.display = 'none';
                document.getElementById('sessionArticleDiv').style.display = 'none';
                $('#promotionTh').css("background-color", "#E06C0A");

                today();

                $("#additionalDiscount").change(function() {

                    //alert('discount');

                    var additionaldiscount = document.getElementById("additionalDiscount").value;
                    var subtotal = document.getElementById("orderArtSubTotal").innerHTML;

                    if((additionaldiscount !='') && (subtotal !='')){
                        if((additionaldiscount !=undefined) && (subtotal != undefined)){

                            var disc = parseFloat(additionaldiscount);
                            var sbt = parseFloat(subtotal);

                            if ((disc > 0) && (sbt > 0)){
                                if(disc >= sbt){
                                    alert("Discount should be less than subtotal amount.");
                                }else{
                                    document.getElementById("orderArtGrandTotal").innerHTML = Math.round(sbt - disc);
                                }
                            }

                            parseFloat(subtotal) > parseFloat(additionaldiscount)

                        }
                    }

                    document.getElementById("orderArtGrandTotal").innerHTML = totalSubTot;

                });

                $("#countryList").change(function(event) {

                    var country = $("select#countryList").val();

                    if(country =="0"){
                        var select = $('#stateList');
                        select.find('option').remove();
                        //
                        var select2 = $('#cityList');
                        select2.find('option').remove();
                    }else{

                        document.getElementById('countryListImage').src='images/ajax-loader2.gif';

                        $.ajax({
                            type: "POST",
                            url: "countryStateOdr.footin?countryCode="+country,
                            success: function(data){
                                document.getElementById('countryListImage').src='';
                                var select = $('#stateList');
                                select.find('option').remove();
                                var json = $.parseJSON(data);
                                $('<option>').val("0").text("SELECT").appendTo(select);
                                for (var i=0; i< json.length; i++){
                                    $('<option>').val(json[i].name).text(json[i].name).appendTo(select);
                                }
                            }
                        });

                    }

                });

                // State List change lisenner

                $("#stateList").change(function(event) {

                    var country = $("select#countryList").val();
                    var state = $("select#stateList").val();

                    if(state =="0"){
                        var select = $('#cityList');
                        select.find('option').remove();
                    }else{

                        document.getElementById('stateListImage').src='images/ajax-loader2.gif';

                        $.ajax({
                            type: "POST",
                            url: "countryStateCityOdr.footin?countryCode="+country+"&stateName="+state,
                            success: function(data){
                                document.getElementById('stateListImage').src='';
                                var select = $('#cityList');
                                select.find('option').remove();
                                var json = $.parseJSON(data);
                                $('<option>').val("0").text("SELECT").appendTo(select);
                                for (var i=0; i< json.length; i++){
                                    $('<option>').val(json[i].name).text(json[i].name).appendTo(select);
                                }
                            }
                        });

                    }

                });

                $("#customerSearchId").keydown(function (e) {
                    // Allow: backspace, tab, enter, delete
                    if ($.inArray(e.keyCode, [8, 9, 13, 46]) !== -1 ||
                        // Allow: left, right
                    (e.keyCode >= 37 && e.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57))
                        && (e.keyCode < 96 || e.keyCode > 105)){
                        e.preventDefault();
                    }
                });

                $("#customerSearchContact").keydown(function (e) {
                    // Allow: backspace, tab, enter, delete
                    if ($.inArray(e.keyCode, [8, 9, 13, 46]) !== -1 ||
                        // Allow: left, right
                    (e.keyCode >= 37 && e.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57))
                        && (e.keyCode < 96 || e.keyCode > 105)){
                        e.preventDefault();
                    }
                });


                $("#dateOfBirth").keydown(function (e) {
                    // Allow: backspace, tab, enter, delete
                    if ($.inArray(e.keyCode, [8, 9, 13, 46]) !== -1 ||
                        // Allow: left, right
                    (e.keyCode >= 37 && e.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57))
                        && (e.keyCode < 96 || e.keyCode > 105)
                        && (e.keyCode < 110 || e.keyCode > 112)
                        && (e.keyCode < 190 || e.keyCode > 192)
                ){
                        e.preventDefault();
                    }
                });


                $("#additionalDiscount").keydown(function (e) {

                    //alert(e.keyCode);

                    // Allow: backspace, tab, enter, delete
                    if ($.inArray(e.keyCode, [8, 9, 13, 46, 190]) !== -1 ||
                        // Allow: left, right
                    (e.keyCode >= 37 && e.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57))
                        && (e.keyCode < 96 || e.keyCode > 105)){
                        e.preventDefault();
                    }
                });

                $("#ordPairS1").keydown(function (e) {
                    // Allow: backspace, tab, enter, delete
                    if ($.inArray(e.keyCode, [8, 9, 13, 46]) !== -1 ||
                        // Allow: left, right
                    (e.keyCode >= 37 && e.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57))
                        && (e.keyCode < 96 || e.keyCode > 105)){
                        e.preventDefault();
                    }
                });
                $("#ordPairS2").keydown(function (e) {
                    // Allow: backspace, tab, enter, delete
                    if ($.inArray(e.keyCode, [8, 9, 13, 46]) !== -1 ||
                        // Allow: left, right
                    (e.keyCode >= 37 && e.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57))
                        && (e.keyCode < 96 || e.keyCode > 105)){
                        e.preventDefault();
                    }
                });
                $("#ordPairS3").keydown(function (e) {
                    // Allow: backspace, tab, enter, delete
                    if ($.inArray(e.keyCode, [8, 9, 13, 46]) !== -1 ||
                        // Allow: left, right
                    (e.keyCode >= 37 && e.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57))
                        && (e.keyCode < 96 || e.keyCode > 105)){
                        e.preventDefault();
                    }
                });
                $("#ordPairS4").keydown(function (e) {
                    // Allow: backspace, tab, enter, delete
                    if ($.inArray(e.keyCode, [8, 9, 13, 46]) !== -1 ||
                        // Allow: left, right
                    (e.keyCode >= 37 && e.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57))
                        && (e.keyCode < 96 || e.keyCode > 105)){
                        e.preventDefault();
                    }
                });
                $("#ordPairS5").keydown(function (e) {
                    // Allow: backspace, tab, enter, delete
                    if ($.inArray(e.keyCode, [8, 9, 13, 46]) !== -1 ||
                        // Allow: left, right
                    (e.keyCode >= 37 && e.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57))
                        && (e.keyCode < 96 || e.keyCode > 105)){
                        e.preventDefault();
                    }
                });
                $("#ordPairS6").keydown(function (e) {
                    // Allow: backspace, tab, enter, delete
                    if ($.inArray(e.keyCode, [8, 9, 13, 46]) !== -1 ||
                        // Allow: left, right
                    (e.keyCode >= 37 && e.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57))
                        && (e.keyCode < 96 || e.keyCode > 105)){
                        e.preventDefault();
                    }
                });
                $("#ordPairS7").keydown(function (e) {
                    // Allow: backspace, tab, enter, delete
                    if ($.inArray(e.keyCode, [8, 9, 13, 46]) !== -1 ||
                        // Allow: left, right
                    (e.keyCode >= 37 && e.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57))
                        && (e.keyCode < 96 || e.keyCode > 105)){
                        e.preventDefault();
                    }
                });
                $("#ordPairS8").keydown(function (e) {
                    // Allow: backspace, tab, enter, delete
                    if ($.inArray(e.keyCode, [8, 9, 13, 46]) !== -1 ||
                        // Allow: left, right
                    (e.keyCode >= 37 && e.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57))
                        && (e.keyCode < 96 || e.keyCode > 105)){
                        e.preventDefault();
                    }
                });
                $("#ordPairS9").keydown(function (e) {
                    // Allow: backspace, tab, enter, delete
                    if ($.inArray(e.keyCode, [8, 9, 13, 46]) !== -1 ||
                        // Allow: left, right
                    (e.keyCode >= 37 && e.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57))
                        && (e.keyCode < 96 || e.keyCode > 105)){
                        e.preventDefault();
                    }
                });
                $("#ordPairS10").keydown(function (e) {
                    // Allow: backspace, tab, enter, delete
                    if ($.inArray(e.keyCode, [8, 9, 13, 46]) !== -1 ||
                        // Allow: left, right
                    (e.keyCode >= 37 && e.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57))
                        && (e.keyCode < 96 || e.keyCode > 105)){
                        e.preventDefault();
                    }
                });
                $("#ordPairS11").keydown(function (e) {
                    // Allow: backspace, tab, enter, delete
                    if ($.inArray(e.keyCode, [8, 9, 13, 46]) !== -1 ||
                        // Allow: left, right
                    (e.keyCode >= 37 && e.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57))
                        && (e.keyCode < 96 || e.keyCode > 105)){
                        e.preventDefault();
                    }
                });
                $("#ordPairS12").keydown(function (e) {
                    // Allow: backspace, tab, enter, delete
                    if ($.inArray(e.keyCode, [8, 9, 13, 46]) !== -1 ||
                        // Allow: left, right
                    (e.keyCode >= 37 && e.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57))
                        && (e.keyCode < 96 || e.keyCode > 105)){
                        e.preventDefault();
                    }
                });
                $("#ordPairS13").keydown(function (e) {
                    // Allow: backspace, tab, enter, delete
                    if ($.inArray(e.keyCode, [8, 9, 13, 46]) !== -1 ||
                        // Allow: left, right
                    (e.keyCode >= 37 && e.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57))
                        && (e.keyCode < 96 || e.keyCode > 105)){
                        e.preventDefault();
                    }
                });

                //$("#datetimepicker9").datetimepicker();

              

            });


        </script>

        <script type="text/javascript">
          
            $('.form_date').datetimepicker({
                language:  'fr',
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                format:'dd-mm-yyyy'
            });
          
        </script>

    </body>

</html>
