
<%@page import="mis.drd.action.ParamUtil"%>
<!DOCTYPE html>
<%@taglib  prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html;" charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <title>FootIn</title>

        <link href="<%= request.getContextPath()%>/resources/css/style.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/bootstrap.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/yamm.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/style_menu.css" rel="stylesheet" media="screen"/>
        <link href="<%= request.getContextPath()%>/resources/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/resources/css/jquery.dataTables.css" rel="stylesheet" />

    </head>

    <style type="text/css">
        <!--
        #example th {
            /* color: #E06C0A; 009999 */
            background: none repeat scroll 0 0 #E06C0A;
            color: #FFFFFF;
            font-size: 12px;
            height: 22px;
            vertical-align: middle;
            padding: 2px;
            text-align: center;
            border: 1px solid #FFFFFF;
        }

        #example td {
            vertical-align: middle;
        }

        #example td.details-control {
            background: url('./images/details_open.png') no-repeat center center;
            cursor: pointer;
        }
        #example tr.shown td.details-control {
            background: url('./images/details_close.png') no-repeat center center;
        }

        #myDialog { display: none; }

        .ui-dialog-titlebar-close {
            visibility: hidden;
        }

        -->
    </style>

    <body>

        <jsp:include page="/top_menu.jsp" flush="true"></jsp:include>

        <div class="body" style="padding-left: 5px; padding-right: 5px;">

            <div style="text-align: left;" id="myDialog" title="Vat & AWB">
                <span style="color: red;">*</span>Date:<input type="text" id="vatDate"/>
                <span style="color: red;">*</span>Inv &nbsp;:<input type="text" id="vatInvoiceNo"/>
                <span style="color: red;">*</span>AWB:<input type="text" id="orderAwbNumber"/>
            </div>

            <table id="example" class="table table-striped" cellspacing="0" width="100%">

                <thead>

                    <tr>
                        <th rowspan="2">Customer</th>
                        <th rowspan="2">Contact</th>
                        <th colspan="5">Order Details<img id="cdcStatusImage" src="" alt="" /></th>
                        <th colspan="2">CDC</th>
                        <th colspan="2">Vat</th>
                        <th colspan="4">Invoice</th>
                        <th colspan="2">logistic</th>
                        <th colspan="2">Customer Receive</th>
                        <th colspan="2">Payment</th>
                        <th colspan="2">Cancel<img id="cancelImage" src="" alt="" /></th>
                    </tr>

                    <tr>
                        <th style="width: 10px;"></th>
                        <th>Number</th>
                        <th style="width: 10px;" title="Order place to CDC" >Go</th>
                        <th title="Order CDC place date" >Date</th>
                        <th>Print</th>
                        <th>Status</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Date</th>
                        <th style="width: 10px;">Go</th>
                        <th>Number</th>
                        <th>Date</th>
                        <th>Print</th>
                        <th>Status</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Del</th>
                    </tr>

                </thead>

                <tbody>

                    <s:if test="ftnOrderMasterList != null">
                        <s:if test="ftnOrderMasterList.size()!=0">
                            <s:iterator value="ftnOrderMasterList">
                                <tr>
                                    <td><s:property value="customer_id"/></td>
                                    <td><s:property value="contact_no"/></td>
                                    <td class="details-control"></td>
                                    <td><s:property value="order_no"/></td>
                                    <td align="center"><input type="checkbox" id="CHKCDCS<s:property value="order_no"/>" onclick="sendToCdc('<s:property value="order_no"/>');" <s:if test='order_to_cdc_status == "Y"'> checked disabled </s:if> /></td>
                                    <td align="left"><s:date name="order_to_cdc_date" format="dd/MM/yyyy"/></td>
                                    <td>
                                        <s:if test='order_to_cdc_status == "Y"'>
                                            <a onclick="orderPrint('<s:property value="order_no"/>');" href="javascript:void(0);" >
                                                <img id="cdcStatusImage" src="<%= request.getContextPath()%>/images/printIcon.png" alt="" style="width: 22px; height: 22px;" />
                                            </a>
                                        </s:if>

                                    </td>
                                    <td align="center">
                                        <s:if test='cdc_packing_status == "Y"'>
                                            <img id="cdcStatusImage" src="<%= request.getContextPath()%>/images/okIcon1.png" alt="" style="width: 30px; height: 30px;" />
                                        </s:if>
                                    </td>
                                    <td align="left"><s:date name="cdc_packing_date" format="dd/MM/yyyy"/></td>
                                    <td align="center">
                                        <s:if test='vat_status == "Y"'>
                                            <img id="cdcStatusImage" src="<%= request.getContextPath()%>/images/okIcon1.png" alt="" style="width: 30px; height: 30px;" />
                                        </s:if>
                                    </td>
                                    <td align="left"><s:date name="vat_date" format="dd/MM/yyyy"/></td>
                                    <td align="center">
                                        <input type="checkbox" id="CHKINV<s:property value="order_no"/>"
                                               <s:if test='cdc_packing_status == "N"'>disabled</s:if>
                                               <s:if test='inv_status == "Y"'>disabled</s:if>
                                               onclick="makeOrderInvoice('<s:property value="order_no"/>');" />
                                    </td>
                                    <td><s:property value="inv_no"/></td>
                                    <td align="left"><s:date name="inv_date" format="dd/MM/yyyy"/></td>
                                    <td>
                                        <s:if test='inv_status == "Y"'>
                                            <a onclick="invoicePrint('<s:property value="order_no"/>');" href="javascript:void(0);" >
                                                <img id="cdcStatusImage" src="<%= request.getContextPath()%>/images/printIcon.png" alt="" style="width: 22px; height: 22px;" />
                                            </a>
                                        </s:if>

                                    </td>
                                    <td>
                                        <s:if test='dlv_to_carry_con_status == "Y"'>
                                            <img id="cdcStatusImage" src="<%= request.getContextPath()%>/images/okIcon1.png" alt="" style="width: 30px; height: 30px;" />
                                        </s:if>
                                    </td>
                                    <td align="left"><s:date name="dlv_to_carry_con_date" format="dd/MM/yyyy"/></td>
                                    <td>
                                        <s:if test='cust_rcv_status == "Y"'>
                                            <img id="cdcStatusImage" src="<%= request.getContextPath()%>/images/okIcon1.png" alt="" style="width: 30px; height: 30px;" />
                                        </s:if>
                                    </td>
                                    <td align="left"><s:date name="cust_rcv_ret_date" format="dd/MM/yyyy"/></td>
                                    <td>
                                        <s:if test='payment_status == "Y"'>
                                            <img id="cdcStatusImage" src="<%= request.getContextPath()%>/images/okIcon1.png" alt="" style="width: 30px; height: 30px;" />
                                        </s:if>
                                    </td>
                                    <td align="left"><s:date name="payment_date" format="dd/MM/yyyy"/></td>
                                    <td>
                                        <s:if test='cancel_status == "Y"'>Cancel</s:if>
                                    </td>
                                    <td>
                                        <s:if test='cancel_status != "Y"'>
                                            <a onclick="orderCancel('<s:property value="order_no"/>');" href="javascript:void(0);" >
                                                <img id="cancelImage" src="<%= request.getContextPath()%>/images/cancelIcon.png" alt="" style="width: 30px; height: 30px;" />
                                            </a>
                                        </s:if>
                                    </td>
                                </tr>

                            </s:iterator>
                        </s:if>
                    </s:if>
                </tbody>

            </table>

        </div>


        <jsp:include page="/footer.jsp" flush="true"></jsp:include>


        <!-- Bootstrap Carousel-->
        <!--
        <script type="text/javascript" src="<//%= request.getContextPath()%>/resources/js/jquery-1.4.2.min.js"/>
        <script type="text/javascript" src="<//%= request.getContextPath()%>/resources/js/jquery.js"/>
        -->
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery-1.8.3.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery-ui.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/bootstrap.min.js"></script>

        <script type="text/javascript">

            $(document).ready(function() {
                onloadFunction();
            });

            function onloadFunction(){
                // DataTable
                var table =   $('#example').DataTable({
                    "aoColumnDefs": [{'bSortable': false, 'aTargets': [0]}],
                    "order": [[ 3, "desc" ]],
                    "pageLength": 25
                });

                // Add event listener for opening and closing details
                $('#example tbody').on('click', 'td.details-control', function (e) {

                    e.preventDefault();
                   
                    var tr = $(this).closest('tr');                    

                    var ordernumber =  tr.find('td:eq(3)').text();
                    
                    if(ordernumber.trim() !=''){
                        
                        var row = table.row(tr);
 
                        if ( row.child.isShown() ) {
                            // This row is already open - close it
                            row.child.hide();
                            tr.removeClass('shown');
                        }
                        else {

                            $.ajax({
                                type: "POST",
                                url: "customerOrderDetailsOdr.footin?ftnOrderNo="+ordernumber,
                                success: function(data){

                                    var dataArray = data.trim().split("<SEP>");

                                    var json = $.parseJSON(dataArray[0].trim());

                                    var custDetails = '<table width="80%" border="1" style="padding-left:30px;">'
                                        +'<tr>'
                                        +'<td style = "vertical-align: top" width="50%">';

                                    custDetails += '<table cellpadding="5" width="100%" cellspacing="0" border="0">';

                                    for (var i=0; i< json.length; i++){
                                        custDetails += '<tr><td width="30%">Name:</td><td width="70%">' + json[i].custname + '</td></tr>';
                                        custDetails += '<tr><td>Email Address:</td><td>' + json[i].emailadd + '</td></tr>';
                                        custDetails += '<tr><td>Shipping Date:</td><td>' + json[i].shipmentdate + '</td></tr>';
                                        custDetails += '<tr><td>Shipping Address:</td><td>' + json[i].shipping + '</td></tr>';
                                        custDetails += '<tr><td>AWB:</td><td>' + json[i].awb + '</td></tr>';
                                    }
                                    custDetails += '</table>';

                                    custDetails +='</td>'
                                        +'<td style = "vertical-align: top" width="50%">';

                                    // generate promotion
                                    if(dataArray[1].trim() != ""){

                                        var jsonPromotion= $.parseJSON(dataArray[1].trim());

                                        custDetails += '<table cellpadding="5" width="100%" cellspacing="0" border="0">';

                                        custDetails += '<thead><tr><td>Article</td><td>Description</td><td>Size</td><td>Qty</td><td>Mrp</td><td>Discount</td><td>Sub Total</td></tr></thead>';

                                        custDetails += '<tbody>';
                                        var total_sp_discount = 0;
                                        var total_gross = 0;
                                        var total_sp_gross = 0;
                                        for (var i=0; i< jsonPromotion.length; i++){
                                            custDetails += '<tr><td align="left">'+jsonPromotion[i].artcode+'</td><td align="left">'+jsonPromotion[i].description+'</td><td align="center">'+jsonPromotion[i].size+'</td><td align="center">'+jsonPromotion[i].qty+'</td><td align="center">'+jsonPromotion[i].mrp+'</td><td align="right">'+jsonPromotion[i].discount+'</td><td align="right">'+jsonPromotion[i].subtotal+'</td></tr>';
                                        
                                            total_sp_discount += parseFloat(jsonPromotion[i].spdiscount);
                                            total_gross += parseFloat(jsonPromotion[i].subtotal);
                                            total_sp_gross += parseFloat(jsonPromotion[i].subtotalaftersp);
                                            
                                        }
                                        custDetails += '</tbody>';

                                        custDetails += '<tfoot>';
                                        custDetails += '<tr><td colspan="6" align="right">Sub Total</td><td align="right">'+total_gross.toFixed(2);+'</td></tr>';
                                        custDetails += '<tr><td colspan="6" align="right">-Discount</td><td align="right">'+total_sp_discount.toFixed(2);+'</td></tr>';
                                        custDetails += '<tr><td colspan="6" align="right">Total</td><td align="right">'+total_sp_gross.toFixed(2);+'</td></tr>';
                                        custDetails += '</tfoot>';

                                        custDetails += '</table>';

                                    }

                                    custDetails += '</td>'
                                        +'</tr>'
                                        +'</table>';

                                    // Open this row
                                    row.child(custDetails).show();
                                    tr.addClass('shown');

                                }
                            });

                            
                        }
                        
                    }

                } );
            }
          
            function sendToCdc(orderNo){

                var r = confirm("Do you want to place order into CDC?");

                if (r == true) {

                    var dataString = "ftnOrderNo="+orderNo;
                    dataString += "&cdcConfirmFlag=Y";

                    document.getElementById('cdcStatusImage').src='images/ajax-loader2.gif';

                    $.ajax({
                        type: "POST",
                        url: "orderPlaceToCdcOdr.footin?"+dataString,
                        success: function(data){

                            document.getElementById('cdcStatusImage').src='';

                            if(data.trim() == 1){
                                alert("Successfuly order place to CDC.");
                                window.location.reload();
                            }else{
                                alert("order does not place to CDC.");
                            }

                        }
                    });


                }

            }

            function makeOrderInvoice(orderNo){

                var r = confirm("Do you want to make invoice?");

                if (r == true) {

                    $('#myDialog').dialog({
                        open: function() {
                            $('#vatDate').datepicker({ dateFormat: 'dd/mm/yy' }).blur();
                        },
                        buttons: {
                            Update: function () {

                                var vdate = document.getElementById('vatDate').value;
                                var vinv = document.getElementById('vatInvoiceNo').value;
                                var awb = document.getElementById('orderAwbNumber').value;

                                if((vdate.trim()!='') && (vinv.trim()!='') && (awb.trim()!='')){

                                    var dataString = "ftnOrderNo="+orderNo;
                                    dataString += "&ftnVatDate="+vdate;
                                    dataString += "&ftnVatInvoiceNo="+vinv;
                                    dataString += "&ftnOrderAwbNo="+awb;

                                    $.ajax({
                                        type: "POST",
                                        url: "makeOrderInvoiceOdr.footin?"+dataString,
                                        success: function(data){

                                            if(data.trim() == 1){
                                                alert("Successfuly invoice number created.");
                                                window.location.reload();
                                            }else{
                                                alert("Invoice number does not created.");
                                            }                                      

                                        }
                                    });
                                    
                                    $(this).dialog("close");

                                }
                            },
                            No: function () {
                                $(this).dialog("close");
                            }
                        },
                        close: function() {
                            $('#vatDate').datepicker('destroy');
                        }
                    });

                }

            }

            function orderPrint(orderNo){

                var r = confirm("Do you want to print?");

                if (r == true) {
                    // print start
                    window.open('printCustomerOrderRpt.footin?orderNumber='+orderNo+'&format=pdf','_blank');
                    // print end
                }
            }

            function invoicePrint(orderNo){

                var r = confirm("Do you want to print?");

                if (r == true) {
                    // print start
                    window.open('printCustomerInvoiceRpt.footin?orderNumber='+orderNo+'&format=pdf','_blank');
                    // print end
                }
            }

            function orderCancel(orderNo){

                var reason = window.prompt("Order No: "+orderNo+"\nDo you want to cancel order?\nIf cancel then put cancel reason.", "");

                if(reason.trim() ==""){
                    alert('Please put cancel reason.');
                }else{

                    var dataString = "ftnOrderNo="+orderNo;
                    dataString += "&ftnCancelReason="+reason;

                    document.getElementById('cancelImage').src='images/ajax-loader2.gif';

                    $.ajax({
                        type: "POST",
                        url: "orderCancelOdr.footin?"+dataString,
                        success: function(data){
                            document.getElementById('cancelImage').src='';
                            alert(data);
                            window.location.reload();
                        }
                    });

                }
            }
        </script>

    </body>

</html>
