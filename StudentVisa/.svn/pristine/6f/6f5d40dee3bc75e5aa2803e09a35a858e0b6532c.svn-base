/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.drd.action;

import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import mis.drd.hibernateutil.HibernateUtil;
import mis.drd.utility.DailySalesMirror;
import mis.drd.utility.DatabaseConnection;
import mis.retail.dto.InstitutionalSalesInfoDTO;
import mis.retail.dto.ItemSalesDetailsDTO;
import mis.retail.dto.OperationApprovalDTO;
import mis.retail.dto.StoreSalesDetailsDTO;
import mis.retail.dto.StoreTransactionDetailsDTO;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Settings;
import org.hibernate.criterion.Restrictions;
import org.hibernate.impl.SessionFactoryImpl;
import org.hibernate.transform.Transformers;

/**
 *
 * @author MIS
 */
public class ReportAction extends ActionSupport {

    private Date fromDate;
    private Date toDate;
    private String salesDate;
    private String openingDate;
    private String receiveDate;
    private String format;
    private String comCode;

    public String dailySalesProcessDataView() {

        System.out.println("comCode: " + comCode);
        System.out.println("salesDate: " + salesDate);
        System.out.println("format: " + format);

        if (ParamUtil.isSet(salesDate) && ParamUtil.isSet(comCode) && ParamUtil.isSet(format)) {

            Map sessionMap = ActionContext.getContext().getSession();
            String userName = (String) sessionMap.get("userName");
            Map parameters = new HashMap();

            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            try {
                parameters.put("P_FROM_DATE", formatter.parse(salesDate));
                parameters.put("P_TO_DATE", formatter.parse(salesDate));
            } catch (ParseException ex) {
                ex.printStackTrace();
                addActionError("Date format error.");
                return ERROR;
            }

            parameters.put("P_COM_CODE", comCode);
            parameters.put("P_USER_NAME", userName);

            sessionMap.put("report", "dailySalesView/salesDataView.jrxml");
            sessionMap.put("format", format);
            sessionMap.put("parameters", parameters);
        } else {
            addActionError("Missing Report Parameters");
            return ERROR;
        }
        return SUCCESS;
    }

    public String openingStockDataView() {

        System.out.println("comCode: " + comCode);
        System.out.println("openingDate: " + openingDate);
        System.out.println("format: " + format);

        if (ParamUtil.isSet(openingDate) && ParamUtil.isSet(comCode) && ParamUtil.isSet(format)) {

            Map sessionMap = ActionContext.getContext().getSession();
            String userName = (String) sessionMap.get("userName");
            Map parameters = new HashMap();

            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            try {
                parameters.put("P_FROM_DATE", formatter.parse(openingDate));
                parameters.put("P_TO_DATE", formatter.parse(openingDate));
            } catch (ParseException ex) {
                ex.printStackTrace();
                addActionError("Date format error.");
                return ERROR;
            }

            parameters.put("P_COM_CODE", comCode);
            parameters.put("P_USER_NAME", userName);

            sessionMap.put("report", "openingStockView/openingStockView.jrxml");
            sessionMap.put("format", format);
            sessionMap.put("parameters", parameters);
        } else {
            addActionError("Missing Report Parameters");
            return ERROR;
        }
        return SUCCESS;
    }

    public String receiveStockDataView() {

        System.out.println("comCode: " + comCode);
        System.out.println("receiveDate: " + receiveDate);
        System.out.println("format: " + format);

        if (ParamUtil.isSet(receiveDate) && ParamUtil.isSet(comCode) && ParamUtil.isSet(format)) {

            Map sessionMap = ActionContext.getContext().getSession();
            String userName = (String) sessionMap.get("userName");
            Map parameters = new HashMap();

            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            try {
                parameters.put("P_FROM_DATE", formatter.parse(receiveDate));
                parameters.put("P_TO_DATE", formatter.parse(receiveDate));
            } catch (ParseException ex) {
                ex.printStackTrace();
                addActionError("Date format error.");
                return ERROR;
            }

            parameters.put("P_COM_CODE", comCode);
            parameters.put("P_USER_NAME", userName);

            sessionMap.put("report", "receiveStockView/receiveStockView.jrxml");
            sessionMap.put("format", format);
            sessionMap.put("parameters", parameters);
        } else {
            addActionError("Missing Report Parameters");
            return ERROR;
        }
        return SUCCESS;
    }

    /**
     * @return the fromDate
     */
    public Date getFromDate() {
        return fromDate;
    }

    /**
     * @param fromDate the fromDate to set
     */
    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    /**
     * @return the toDate
     */
    public Date getToDate() {
        return toDate;
    }

    /**
     * @param toDate the toDate to set
     */
    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    /**
     * @return the format
     */
    public String getFormat() {
        return format;
    }

    /**
     * @param format the format to set
     */
    public void setFormat(String format) {
        this.format = format;
    }

    /**
     * @return the comCode
     */
    public String getComCode() {
        return comCode;
    }

    /**
     * @param comCode the comCode to set
     */
    public void setComCode(String comCode) {
        this.comCode = comCode;
    }

    /**
     * @return the salesDate
     */
    public String getSalesDate() {
        return salesDate;
    }

    /**
     * @param salesDate the salesDate to set
     */
    public void setSalesDate(String salesDate) {
        this.salesDate = salesDate;
    }

    /**
     * @return the openingDate
     */
    public String getOpeningDate() {
        return openingDate;
    }

    /**
     * @param openingDate the openingDate to set
     */
    public void setOpeningDate(String openingDate) {
        this.openingDate = openingDate;
    }

    /**
     * @return the receiveDate
     */
    public String getReceiveDate() {
        return receiveDate;
    }

    /**
     * @param receiveDate the receiveDate to set
     */
    public void setReceiveDate(String receiveDate) {
        this.receiveDate = receiveDate;
    }
}
