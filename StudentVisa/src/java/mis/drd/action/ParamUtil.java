/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.drd.action;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;
import mis.drd.persistance.FtnCalendar;
import mis.drd.utility.DatabaseConnection;

/**
 *
 * @author MIS
 */
public class ParamUtil {

    public static boolean isSet(Object obj) {
        if (obj == null) {
            return false;
        }
        return true;
    }

    public static boolean isSet(String obj) {
        if (obj == null || obj.length() == 0) {
            return false;
        }
        return true;
    }

    public static boolean isSet(Collection<Object> object) {
        if (object == null || object.isEmpty()) {
            return false;
        }
        return true;
    }

    public static boolean isSet(Set<Object> obj) {
        if (obj == null || obj.isEmpty()) {
            return false;
        }
        return true;
    }

    public static BigDecimal getId(String tableName, String tableId) throws Exception {

        BigDecimal id = null;
        Connection connection = DatabaseConnection.connectDB();
        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String idQuery = "select nvl(max(" + tableId + "),0)+1 as num from " + tableName;

                System.out.println("Query:" + idQuery);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(idQuery);

                while (resultSet.next()) {
                    id = resultSet.getBigDecimal("num");
                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }


        return id;
    }

    public static List<FtnCalendar> ftnDateOfBirthYearList() {

        List<FtnCalendar> ftnInfoList = new ArrayList<FtnCalendar>();

        FtnCalendar ftnInfo = null;

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());
        int year = calendar.get(Calendar.YEAR);

        for (int i = year; i >= year - 100; i--) {
            ftnInfo = new FtnCalendar();
            ftnInfo.setCalYear(String.valueOf(i));
            ftnInfoList.add(ftnInfo);
        }

        return ftnInfoList;

    }
}
