/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.drd.action;

/**
 *
 * @author MIS
 */
public interface ActionResult {

    public static final String ADMINSIGNIN = "signin";
    public static final String SIGNIN = "signin";
    public static final String WELCOME = "welcome";
    //
    public static final String SIGNUP = "signup";
    public static final String SIGNOUT = "signout";
    public static final String USERLOGIN = "userlogin";
    public static final String SIGNUPERROR = "signuperror";
    public static final String MESSAGE = "confirmationmessage";
    //
    public static final String ORDERDASHBOARD = "orderdashboard";
    public static final String PROMOTIONDASHBOARD = "promotiondashboard";
    public static final String PROMOTIONUPLOAD = "promotionupload";
    public static final String REMITTANCEPROCESS = "remittancprocess";
    public static final String NEWREMITTANCEPROCESS = "newremittancprocess";
    //
    public static final String NEWORDER = "neworder";
    public static final String JSONDATA = "datajson";
    //
    public static final String BASICSETUP = "basicsetup";
    public static final String COMPANYINFOVIEW = "companyinfoview";
    public static final String DELETECOMPANYDETAILS = "deletecompanydetails";
    public static final String COMPANYDETAILSVIEW = "companydetailsview";
    public static final String GROUPMENUSETUP = "groupmenusetup";
    public static final String SELECTEDLEVELASSIGNEDMENU = "selectedlevelassignedmenu";
    public static final String GROUPASSIGNEDMENUSAVED = "groupassignedmenusaved";
    //
    public static final String NEWUSER = "newuser";
    public static final String ALLUSER = "alluser";
    public static final String USERVIEW = "userview";
    public static final String CHANGEPASSWORD = "changepassword";
    //
    public static final String MYPROFILE = "myprofile";
    public static final String MYORDER = "myorder";
    public static final String MYPOINT = "mypoint";

    public String execute() throws Exception;
}
