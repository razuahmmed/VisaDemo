/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.drd.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import mis.drd.model.FtnMyAccountModel;
import mis.drd.model.FtnOrderModel;
import mis.drd.persistance.FtnCalendar;
import mis.drd.persistance.FtnCountry;

/**
 *
 * @author MIS
 */
public class FtnMyAccountAction extends ActionSupport implements ActionResult {

    private FtnMyAccountModel ftnMyAccountModel = null;
    private FtnOrderModel ftnOrderModel = null;
    private List<FtnCalendar> ftnDobYearList;
    private List<FtnCountry> ftnCountryList;
    //
    //
    private String jsonData;
    private String message;

    /*
     ***************************************************************************
     ************************** My Points Start ********************************
     ***************************************************************************
     */
    public String customerLoyaltyPoints() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

//        if (ftnMyAccountModel == null) {
//            ftnMyAccountModel = new FtnMyAccountModel();
//        }

        return MYPOINT;
    }

    public String customerMyProfile() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

//        if (ftnMyAccountModel == null) {
//            ftnMyAccountModel = new FtnMyAccountModel();
//        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        ftnCountryList = ftnOrderModel.ftnCountryList();

        ftnDobYearList = ParamUtil.ftnDateOfBirthYearList();

        return MYPROFILE;
    }

    public String customerProfileDetails() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");
        String custId = (String) sessionMap.get("CUST_ID");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

        if (ftnMyAccountModel == null) {
            ftnMyAccountModel = new FtnMyAccountModel();
        }

        HashMap<Object, Object> paramMap = new HashMap<Object, Object>();
        paramMap.put("CUSTOMER_ID", (custId == null ? "" : custId.trim()));

        jsonData = ftnMyAccountModel.ftnCustomerDetails(paramMap);

        System.out.println("customerJsonData:" + getJsonData());

        return JSONDATA;
    }

    public String customerMyOrder() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

//        if (ftnMyAccountModel == null) {
//            ftnMyAccountModel = new FtnMyAccountModel();
//        }

        return MYORDER;
    }

    /*
     ***************************************************************************
     ************************** My Points End **********************************
     ***************************************************************************
     */
    /**
     * @return the ftnDobYearList
     */
    public List<FtnCalendar> getFtnDobYearList() {
        return ftnDobYearList;
    }

    /**
     * @param ftnDobYearList the ftnDobYearList to set
     */
    public void setFtnDobYearList(List<FtnCalendar> ftnDobYearList) {
        this.ftnDobYearList = ftnDobYearList;
    }

    /**
     * @return the ftnCountryList
     */
    public List<FtnCountry> getFtnCountryList() {
        return ftnCountryList;
    }

    /**
     * @param ftnCountryList the ftnCountryList to set
     */
    public void setFtnCountryList(List<FtnCountry> ftnCountryList) {
        this.ftnCountryList = ftnCountryList;
    }

    /**
     * @return the jsonData
     */
    public String getJsonData() {
        return jsonData;
    }

    /**
     * @param jsonData the jsonData to set
     */
    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
