/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.drd.action;

import java.util.HashMap;
import mis.drd.model.*;
import mis.drd.persistance.FtnCountry;
import mis.drd.persistance.FtnOrderMaster;
import mis.drd.persistance.FtnPromotion;
//
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import mis.drd.persistance.FtnPaymentMode;
import mis.drd.persistance.FtnRemittance;
import mis.drd.utility.XlsxFileReader;
import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author MIS
 */
public class FtnOrderAction extends ActionSupport implements ActionResult {

    //
    private FtnOrderModel ftnOrderModel = null;
    //
    private List<FtnCountry> ftnCountryList;
    private List<FtnOrderMaster> ftnOrderMasterList;
    private List<FtnPromotion> ftnPromotionMstList;
    private List<FtnPaymentMode> ftnPaymentModeList;
    private List<FtnRemittance> ftnRemittanceList;
    //
    private String jsonData;
    private String message;
    //
    private String searchCustomerCode;
    private String searchContactNumber;
    private String searchArticleCode;
    private String searchArticleSizeCode;
    private String tempOrderArticleData;
    //
    private String countryCode;
    private String countryName;
    private String stateName;
    private String cityName;
    //
    private String ftnCustomerCode;
    private String ftnContactNumber;
    private String ftnEmailAddress;
    private String ftnFirstName;
    private String ftnMiddleName;
    private String ftnLastName;
    private String ftnSurName;
    private String ftnCountryName;
    private String ftnStateName;
    private String ftnCityName;
    private String ftnPostalCode;
    private String ftnCustomerAddress;
    private String ftnShippingAddress;
    private String ftnDateOfBirth;
    private String ftnCustomerType;
    private String ftnPaymentType;
    private String ftnGender;
    private String ftnReligion;
    private String ftnMaritalStatus;
    private String ftnOccupation;
    private String additionalDiscount;
    private String cdcConfirmFlag;
    //
    private String ftnOrderNo;
    private String ftnOrderDate;
    private String ftnCancelReason;
    //
    private String destPath;
    private File myFile;
    private String myFileContentType;
    private String myFileFileName;
    private String filePath;
    //
    private String ftnCircularNumber;
    //
    private String ftnVatDate;
    private String ftnVatInvoiceNo;
    private String ftnOrderAwbNo;
    //
    private String ftnYear;
    private String ftnWeek;
    //
    private String yearList;
    private String weekList;
    private String dateList;

    public String orderDashboard() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        ftnOrderMasterList = ftnOrderModel.ftnOrderMasterList();

        return ORDERDASHBOARD;
    }

    public String newOrderForAdmin() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        ftnCountryList = ftnOrderModel.ftnCountryList();

        ftnPaymentModeList = ftnOrderModel.ftnPaymentModeList();

        return NEWORDER;
    }

    public String countryState() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        HashMap<Object, Object> paramMap = new HashMap<Object, Object>();
        paramMap.put("COUNTRY_CODE", countryCode);

        jsonData = ftnOrderModel.ftnCountryStateJsonData(paramMap);

        System.out.println("stateJsonData:" + jsonData);

        return JSONDATA;
    }

    public String countryStateCity() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        HashMap<Object, Object> paramMap = new HashMap<Object, Object>();
        paramMap.put("COUNTRY_CODE", countryCode);
        paramMap.put("SATE_REGION", stateName);

        jsonData = ftnOrderModel.ftnCountryStateCityJsonData(paramMap);

        System.out.println("stateJsonData:" + jsonData);

        return JSONDATA;
    }

    public String searchFtnCustomer() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        HashMap<Object, Object> paramMap = new HashMap<Object, Object>();
        paramMap.put("CUSTOMER_ID", (searchCustomerCode == null ? "" : searchCustomerCode));
        paramMap.put("CONTACT_NO", (searchContactNumber == null ? "" : searchContactNumber));

        jsonData = ftnOrderModel.ftnSearchCustomerDetails(paramMap);

        System.out.println("customerJsonData:" + jsonData);

        return JSONDATA;
    }

    public String searchFtnArticle() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        HashMap<Object, Object> paramMap = new HashMap<Object, Object>();
        paramMap.put("CUSTOMER_ID", (searchCustomerCode == null ? "" : searchCustomerCode));
        paramMap.put("ART_CODE", (searchArticleCode == null ? "" : searchArticleCode));

        jsonData = ftnOrderModel.ftnSearchArticleDetails(paramMap);

        System.out.println("articleJsonData:" + jsonData);

        return JSONDATA;
    }

    public String tempOrderArticle() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        HashMap<Object, Object> paramMap = new HashMap<Object, Object>();
        paramMap.put("ORDER_ARTICLE", (tempOrderArticleData == null ? "" : tempOrderArticleData));
        paramMap.put("INSERT_BY", (name == null ? "" : name));

        jsonData = ftnOrderModel.ftnOrderTempArticleDetails(paramMap);

        System.out.println("orderArticleJsonData:" + jsonData);

        return JSONDATA;
    }

    public String tempOrderArticleDelete() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        HashMap<Object, Object> paramMap = new HashMap<Object, Object>();
        paramMap.put("CONTACT_NO", (searchContactNumber == null ? "" : searchContactNumber));
        paramMap.put("ART_CODE", (searchArticleCode == null ? "" : searchArticleCode));
        paramMap.put("ART_SIZE", (searchArticleSizeCode == null ? "" : searchArticleSizeCode));

        jsonData = ftnOrderModel.ftnOrderTempArticleDelete(paramMap);

        System.out.println("orderArticleJsonData:" + jsonData);

        return JSONDATA;
    }

    public String saveCustomerOrder() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");
        String storeCode = (String) sessionMap.get("storeCode");
        String comCode = (String) sessionMap.get("COM_CODE");


        if (name == null || name.length() == 0) {
            return LOGIN;
        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        System.out.println("ftnCustomerCode:" + ftnCustomerCode);
        System.out.println("ftnContactNumber:" + ftnContactNumber);
        System.out.println("ftnEmailAddress:" + ftnEmailAddress);
        System.out.println("ftnFirstName:" + ftnFirstName);
        System.out.println("ftnMiddleName:" + ftnMiddleName);
        System.out.println("ftnLastName:" + ftnLastName);
        System.out.println("ftnSurName:" + ftnSurName);
        System.out.println("ftnCountryName:" + ftnCountryName);
        System.out.println("ftnStateName:" + ftnStateName);
        System.out.println("ftnCityName:" + ftnCityName);
        System.out.println("ftnPostalCode:" + ftnPostalCode);
        System.out.println("ftnCustomerAddress:" + ftnCustomerAddress);
        System.out.println("ftnShippingAddress:" + ftnShippingAddress);
        System.out.println("ftnDateOfBirth:" + ftnDateOfBirth);
        System.out.println("ftnGender:" + ftnCustomerType);
        System.out.println("ftnGender:" + ftnGender);
        System.out.println("ftnReligion:" + ftnReligion);
        System.out.println("ftnMaritalStatus:" + ftnMaritalStatus);
        System.out.println("ftnOccupation:" + ftnOccupation);
        System.out.println("additionalDiscount:" + additionalDiscount);
        System.out.println("cdcConfirmFlag:" + cdcConfirmFlag);
        System.out.println("ftnOrderDate:" + ftnOrderDate);
        System.out.println("ftnPaymentType:" + ftnPaymentType);

        HashMap<Object, Object> paramMap = new HashMap<Object, Object>();

        paramMap.put("STORE_CODE", (storeCode == null ? "" : storeCode));
        paramMap.put("CUSTOMER_ID", (ftnCustomerCode == null ? "" : ftnCustomerCode));
        paramMap.put("CONTACT_NO", (ftnContactNumber == null ? "" : ftnContactNumber));
        paramMap.put("EMAIL_ADD", (ftnEmailAddress == null ? "" : ftnEmailAddress));
        paramMap.put("FIRST_NAME", (ftnFirstName == null ? "" : ftnFirstName));
        paramMap.put("MIDDLE_NAME", (ftnMiddleName == null ? "" : ftnMiddleName));
        paramMap.put("LAST_NAME", (ftnLastName == null ? "" : ftnLastName));
        paramMap.put("SURE_NAME", (ftnSurName == null ? "" : ftnSurName));
        paramMap.put("COUNTRY_NAME", (ftnCountryName == null ? "" : ftnCountryName));
        paramMap.put("STATE_REGION", (ftnStateName.equalsIgnoreCase("0") ? "" : ftnStateName));
        paramMap.put("CITY", (ftnCityName.equalsIgnoreCase("0") ? "" : (ftnCityName.equalsIgnoreCase("null") ? "" : ftnCityName)));
        paramMap.put("POSTAL_CODE", (ftnPostalCode == null ? "" : ftnPostalCode));
        paramMap.put("ADD1", (ftnCustomerAddress == null ? "" : ftnCustomerAddress));
        paramMap.put("SHIPPING_ADDRESS", (ftnShippingAddress == null ? "" : ftnShippingAddress));
        paramMap.put("DOB", (ftnDateOfBirth == null ? "" : ftnDateOfBirth));
        paramMap.put("CUSTOMER_TYPE", (ftnCustomerType.equalsIgnoreCase("Standard") ? "" : ftnCustomerType));
        paramMap.put("GENDER", (ftnGender.equalsIgnoreCase("0") ? "" : ftnGender));
        paramMap.put("RELIGION", (ftnReligion.equalsIgnoreCase("0") ? "" : ftnReligion));
        paramMap.put("MARITAL_STATUS", (ftnMaritalStatus.equalsIgnoreCase("0") ? "" : ftnMaritalStatus));
        paramMap.put("OCCUPATION", (ftnOccupation == null ? "" : ftnOccupation));
        paramMap.put("SP_DISCOUNT", (additionalDiscount == null ? "0" : (additionalDiscount.isEmpty() ? "0" : additionalDiscount)));
        paramMap.put("INSERT_BY", (name == null ? "" : name));
        paramMap.put("UPDATE_BY", (name == null ? "" : name));
        paramMap.put("USER_ID", (name == null ? "" : name));
        paramMap.put("SALES_PCODE", "01");
        paramMap.put("ORDER_TO_CDC_STATUS", (cdcConfirmFlag == null ? "N" : cdcConfirmFlag));
        paramMap.put("ORDER_DATE", (ftnOrderDate == null ? "" : ftnOrderDate));
        paramMap.put("COM_CODE", "518");
        paramMap.put("PAYMENT_MODE", (ftnPaymentType == null ? "COD" : ftnPaymentType));

        jsonData = ftnOrderModel.ftnOrderSave(paramMap);

        System.out.println("orderSaveJsonData:" + jsonData);

        return JSONDATA;
    }

    public String orderPlaceToCdc() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        HashMap<Object, Object> paramMap = new HashMap<Object, Object>();
        paramMap.put("ORDER_NO", (ftnOrderNo == null ? "" : ftnOrderNo));
        paramMap.put("ORDER_TO_CDC_STATUS", (cdcConfirmFlag == null ? "N" : cdcConfirmFlag));
        paramMap.put("USER_ID", (name == null ? "" : name));

        jsonData = ftnOrderModel.ftnOrderPlaceToCdc(paramMap);

        System.out.println("orderArticleJsonData:" + jsonData);

        return JSONDATA;
    }

    public String makeOrderInvoice() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");
        String storeCode = (String) sessionMap.get("storeCode");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        HashMap<Object, Object> paramMap = new HashMap<Object, Object>();
        paramMap.put("STORE_CODE", (storeCode == null ? "" : storeCode));
        paramMap.put("ORDER_NO", (ftnOrderNo == null ? "" : ftnOrderNo));

        paramMap.put("VAT_DATE", (ftnVatDate == null ? "" : ftnVatDate));
        paramMap.put("VAT_INV_NO", (ftnVatInvoiceNo == null ? "" : ftnVatInvoiceNo));
        paramMap.put("AWB", (ftnOrderAwbNo == null ? "" : ftnOrderAwbNo));

        paramMap.put("USER_ID", (name == null ? "" : name));

        jsonData = ftnOrderModel.ftnOrderInvoiceMake(paramMap);

        System.out.println("orderArticleJsonData:" + jsonData);

        return JSONDATA;
    }

    public String customerOrderDetails() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        HashMap<Object, Object> paramMap = new HashMap<Object, Object>();
        paramMap.put("ORDER_NO", (ftnOrderNo == null ? "" : ftnOrderNo));

        jsonData = ftnOrderModel.ftnOrderDetailsJsonData(paramMap);

        System.out.println("CustomerOrderArticleJsonData:" + jsonData);

        return JSONDATA;
    }

    public String orderCancel() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        HashMap<Object, Object> paramMap = new HashMap<Object, Object>();
        paramMap.put("ORDER_NO", (ftnOrderNo == null ? "" : ftnOrderNo));
        paramMap.put("CANCEL_REASON", (ftnCancelReason == null ? "" : ftnCancelReason));
        paramMap.put("USER_ID", (name == null ? "" : name));

        jsonData = ftnOrderModel.ftnOrderCancel(paramMap);

        System.out.println("orderCancelJsonData:" + jsonData);

        return JSONDATA;
    }

    public String promotionDashboard() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        ftnPromotionMstList = ftnOrderModel.ftnPromotionList();

        return PROMOTIONDASHBOARD;
    }

    public String promotionUpload() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

        jsonData = "";


        return PROMOTIONUPLOAD;
    }

    public String promCircularFileUpload() {

        Map sessionMap = ActionContext.getContext().getSession();

        String uName = (String) sessionMap.get("userName");
        String storeCode = (String) sessionMap.get("storeCode");
        String comCode = (String) sessionMap.get("COM_CODE");

        if (uName == null || uName.length() == 0) {
            return LOGIN;
        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        SimpleDateFormat formatter = new SimpleDateFormat("dd_MMM_yyyy_hh_mm_ss");
        destPath = ServletActionContext.getRequest().getSession().getServletContext().getRealPath("/") + "CIRCULAR_DATA_FILE/";

        try {

            System.out.println("Src File name: " + myFile);
            System.out.println("Dst File name: " + myFileFileName);

            setFilePath(destPath.replace("\\", "/") + storeCode + "_" + getMyFileFileName());

            File file = new File(destPath + storeCode + "_" + getMyFileFileName());

            if (file.exists()) {
                file.renameTo(new File(destPath + formatter.format(new Date()) + "_" + storeCode + "_" + file.getName()));
            }

            File destFile = new File(destPath, storeCode + "_" + getMyFileFileName());
            FileUtils.copyFile(myFile, destFile);

            System.out.println("getFilePath():" + getFilePath());

            XlsxFileReader xlsxFileReader = new XlsxFileReader();

            List<FtnPromotion> xmlSmtpInfoList = xlsxFileReader.xlsxCircularFileRead(getFilePath());

            if (xmlSmtpInfoList != null) {
                if (xmlSmtpInfoList.size() > 0) {
                    jsonData = ftnOrderModel.promCircularFileProcess(xmlSmtpInfoList, uName);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

//        ftnPromotionMstList = ftnOrderModel.ftnPromotionList();

        return PROMOTIONUPLOAD;

    }

    public String promCircularArticle() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        HashMap<Object, Object> paramMap = new HashMap<Object, Object>();
        paramMap.put("PROMO_ID", (ftnCircularNumber == null ? "" : ftnCircularNumber));

        jsonData = ftnOrderModel.ftnCircularArticleJsonData(paramMap);

        System.out.println("CirculaArticleJsonData:" + jsonData);

        return JSONDATA;
    }

    public String customerInvoiceHistory() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        HashMap<Object, Object> paramMap = new HashMap<Object, Object>();
        paramMap.put("CUSTOMER_ID", (ftnCustomerCode == null ? "" : ftnCustomerCode));

        jsonData = ftnOrderModel.ftnCustomerInvoiceHistoryJsonData(paramMap);

        System.out.println("CustomerInvoiceHistoryJsonData:" + jsonData);

        return JSONDATA;
    }

    public String newArrivalArticle() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        jsonData = ftnOrderModel.ftnNewArrivalArticleJsonData();

        System.out.println("NewArrivalArticleJsonData:" + jsonData);

        return JSONDATA;
    }

    public String remittanceProcess() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        jsonData = "";
        message = "";

        ftnRemittanceList = ftnOrderModel.ftnRemittanceList();

        return REMITTANCEPROCESS;
    }

    public String newRemittanceProcess() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        jsonData = "";
        message = "";
        ftnRemittanceList = null;

        return NEWREMITTANCEPROCESS;
    }

    public String remittanceYear() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        jsonData = ftnOrderModel.ftnRemittanceYearJsonData();

        System.out.println("JsonData:" + jsonData);

        return JSONDATA;
    }

    public String remittanceWeek() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        HashMap<Object, Object> paramMap = new HashMap<Object, Object>();
        paramMap.put("F_YEAR", (ftnYear == null ? "" : ftnYear));

        jsonData = ftnOrderModel.ftnRemittanceWeekJsonData(paramMap);

        System.out.println("JsonData:" + jsonData);

        return JSONDATA;
    }

    public String remittanceDate() {

        Map sessionMap = ActionContext.getContext().getSession();

        String name = (String) sessionMap.get("userName");

        if (name == null || name.length() == 0) {
            return LOGIN;
        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        HashMap<Object, Object> paramMap = new HashMap<Object, Object>();
        paramMap.put("F_YEAR", (ftnYear == null ? "" : ftnYear));
        paramMap.put("WEEK_NO", (ftnWeek == null ? "" : ftnWeek));

        jsonData = ftnOrderModel.ftnRemittanceDateJsonData(paramMap);

        System.out.println("JsonData:" + jsonData);

        return JSONDATA;
    }

    public String remittanceFileUpload() {

        Map sessionMap = ActionContext.getContext().getSession();

        String uName = (String) sessionMap.get("userName");
        String storeCode = (String) sessionMap.get("storeCode");

        message = "";
        jsonData = "";

        if (uName == null || uName.length() == 0) {
            return LOGIN;
        }

        if (ftnOrderModel == null) {
            ftnOrderModel = new FtnOrderModel();
        }

        if (dateList != null) {

            if (!dateList.equalsIgnoreCase("0")) {

                if (myFile != null) {

                    SimpleDateFormat formatter = new SimpleDateFormat("dd_MMM_yyyy_hh_mm_ss");
                    destPath = ServletActionContext.getRequest().getSession().getServletContext().getRealPath("/") + "REMITTANCE_DATA_FILE/";

                    try {

                        System.out.println("Src File name: " + myFile);
                        System.out.println("Dst File name: " + myFileFileName);

                        setFilePath(destPath.replace("\\", "/") + storeCode + "_" + getMyFileFileName());

                        File file = new File(destPath + storeCode + "_" + getMyFileFileName());

                        if (file.exists()) {
                            file.renameTo(new File(destPath + formatter.format(new Date()) + "_" + storeCode + "_" + file.getName()));
                        }

                        File destFile = new File(destPath, storeCode + "_" + getMyFileFileName());
                        FileUtils.copyFile(myFile, destFile);

                        System.out.println("getFilePath():" + getFilePath());

                        XlsxFileReader xlsxFileReader = new XlsxFileReader();

                        List<FtnRemittance> xmlSmtpInfoList = xlsxFileReader.xlsxRemittanceFileRead(getFilePath());

                        System.out.println("xmlSmtpInfoList:" + xmlSmtpInfoList.size());

                        if (xmlSmtpInfoList != null) {
                            if (xmlSmtpInfoList.size() > 0) {
                                message = ftnOrderModel.remittanceFileProcess(xmlSmtpInfoList, yearList, weekList, dateList, uName);
                            }
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    message = "Process file required.";
                }

                HashMap<Object, Object> paramMap = new HashMap<Object, Object>();
                paramMap.put("PROCESS_DATE", (dateList == null ? "" : dateList));

                ftnRemittanceList = ftnOrderModel.ftnRemittanceList(paramMap);

            } else {
                message = "Process date required.";
            }
        } else {
            message = "Process date required.";
        }

        return REMITTANCEPROCESS;

    }

    /**
     * @return the ftnCountryList
     */
    public List<FtnCountry> getFtnCountryList() {
        return ftnCountryList;


    }

    /**
     * @param ftnCountryList the ftnCountryList to set
     */
    public void setFtnCountryList(List<FtnCountry> ftnCountryList) {
        this.ftnCountryList = ftnCountryList;


    }

    /**
     * @return the countryName
     */
    public String getCountryName() {
        return countryName;


    }

    /**
     * @param countryName the countryName to set
     */
    public void setCountryName(String countryName) {
        this.countryName = countryName;


    }

    /**
     * @return the stateName
     */
    public String getStateName() {
        return stateName;


    }

    /**
     * @param stateName the stateName to set
     */
    public void setStateName(String stateName) {
        this.stateName = stateName;


    }

    /**
     * @return the cityName
     */
    public String getCityName() {
        return cityName;


    }

    /**
     * @param cityName the cityName to set
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;


    }

    /**
     * @return the countryCode
     */
    public String getCountryCode() {
        return countryCode;


    }

    /**
     * @param countryCode the countryCode to set
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;


    }

    /**
     * @return the searchCustomerCode
     */
    public String getSearchCustomerCode() {
        return searchCustomerCode;


    }

    /**
     * @param searchCustomerCode the searchCustomerCode to set
     */
    public void setSearchCustomerCode(String searchCustomerCode) {
        this.searchCustomerCode = searchCustomerCode;


    }

    /**
     * @return the searchContactNumber
     */
    public String getSearchContactNumber() {
        return searchContactNumber;


    }

    /**
     * @param searchContactNumber the searchContactNumber to set
     */
    public void setSearchContactNumber(String searchContactNumber) {
        this.searchContactNumber = searchContactNumber;


    }

    /**
     * @return the searchArticleCode
     */
    public String getSearchArticleCode() {
        return searchArticleCode;


    }

    /**
     * @param searchArticleCode the searchArticleCode to set
     */
    public void setSearchArticleCode(String searchArticleCode) {
        this.searchArticleCode = searchArticleCode;


    }

    /**
     * @return the jsonData
     */
    public String getJsonData() {
        return jsonData;


    }

    /**
     * @param jsonData the jsonData to set
     */
    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;


    }

    /**
     * @return the tempOrderArticleData
     */
    public String getTempOrderArticleData() {
        return tempOrderArticleData;


    }

    /**
     * @param tempOrderArticleData the tempOrderArticleData to set
     */
    public void setTempOrderArticleData(String tempOrderArticleData) {
        this.tempOrderArticleData = tempOrderArticleData;


    }

    /**
     * @return the searchArticleSizeCode
     */
    public String getSearchArticleSizeCode() {
        return searchArticleSizeCode;


    }

    /**
     * @param searchArticleSizeCode the searchArticleSizeCode to set
     */
    public void setSearchArticleSizeCode(String searchArticleSizeCode) {
        this.searchArticleSizeCode = searchArticleSizeCode;


    }

    /**
     * @return the ftnCustomerCode
     */
    public String getFtnCustomerCode() {
        return ftnCustomerCode;


    }

    /**
     * @param ftnCustomerCode the ftnCustomerCode to set
     */
    public void setFtnCustomerCode(String ftnCustomerCode) {
        this.ftnCustomerCode = ftnCustomerCode;


    }

    /**
     * @return the ftnContactNumber
     */
    public String getFtnContactNumber() {
        return ftnContactNumber;


    }

    /**
     * @param ftnContactNumber the ftnContactNumber to set
     */
    public void setFtnContactNumber(String ftnContactNumber) {
        this.ftnContactNumber = ftnContactNumber;


    }

    /**
     * @return the ftnEmailAddress
     */
    public String getFtnEmailAddress() {
        return ftnEmailAddress;


    }

    /**
     * @param ftnEmailAddress the ftnEmailAddress to set
     */
    public void setFtnEmailAddress(String ftnEmailAddress) {
        this.ftnEmailAddress = ftnEmailAddress;


    }

    /**
     * @return the ftnFirstName
     */
    public String getFtnFirstName() {
        return ftnFirstName;


    }

    /**
     * @param ftnFirstName the ftnFirstName to set
     */
    public void setFtnFirstName(String ftnFirstName) {
        this.ftnFirstName = ftnFirstName;


    }

    /**
     * @return the ftnMiddleName
     */
    public String getFtnMiddleName() {
        return ftnMiddleName;


    }

    /**
     * @param ftnMiddleName the ftnMiddleName to set
     */
    public void setFtnMiddleName(String ftnMiddleName) {
        this.ftnMiddleName = ftnMiddleName;


    }

    /**
     * @return the ftnLastName
     */
    public String getFtnLastName() {
        return ftnLastName;


    }

    /**
     * @param ftnLastName the ftnLastName to set
     */
    public void setFtnLastName(String ftnLastName) {
        this.ftnLastName = ftnLastName;


    }

    /**
     * @return the ftnSurName
     */
    public String getFtnSurName() {
        return ftnSurName;


    }

    /**
     * @param ftnSurName the ftnSurName to set
     */
    public void setFtnSurName(String ftnSurName) {
        this.ftnSurName = ftnSurName;


    }

    /**
     * @return the ftnCountryName
     */
    public String getFtnCountryName() {
        return ftnCountryName;


    }

    /**
     * @param ftnCountryName the ftnCountryName to set
     */
    public void setFtnCountryName(String ftnCountryName) {
        this.ftnCountryName = ftnCountryName;


    }

    /**
     * @return the ftnStateName
     */
    public String getFtnStateName() {
        return ftnStateName;


    }

    /**
     * @param ftnStateName the ftnStateName to set
     */
    public void setFtnStateName(String ftnStateName) {
        this.ftnStateName = ftnStateName;


    }

    /**
     * @return the ftnCityName
     */
    public String getFtnCityName() {
        return ftnCityName;


    }

    /**
     * @param ftnCityName the ftnCityName to set
     */
    public void setFtnCityName(String ftnCityName) {
        this.ftnCityName = ftnCityName;


    }

    /**
     * @return the ftnPostalCode
     */
    public String getFtnPostalCode() {
        return ftnPostalCode;


    }

    /**
     * @param ftnPostalCode the ftnPostalCode to set
     */
    public void setFtnPostalCode(String ftnPostalCode) {
        this.ftnPostalCode = ftnPostalCode;


    }

    /**
     * @return the ftnCustomerAddress
     */
    public String getFtnCustomerAddress() {
        return ftnCustomerAddress;


    }

    /**
     * @param ftnCustomerAddress the ftnCustomerAddress to set
     */
    public void setFtnCustomerAddress(String ftnCustomerAddress) {
        this.ftnCustomerAddress = ftnCustomerAddress;


    }

    /**
     * @return the ftnShippingAddress
     */
    public String getFtnShippingAddress() {
        return ftnShippingAddress;


    }

    /**
     * @param ftnShippingAddress the ftnShippingAddress to set
     */
    public void setFtnShippingAddress(String ftnShippingAddress) {
        this.ftnShippingAddress = ftnShippingAddress;


    }

    /**
     * @return the ftnDateOfBirth
     */
    public String getFtnDateOfBirth() {
        return ftnDateOfBirth;


    }

    /**
     * @param ftnDateOfBirth the ftnDateOfBirth to set
     */
    public void setFtnDateOfBirth(String ftnDateOfBirth) {
        this.ftnDateOfBirth = ftnDateOfBirth;


    }

    /**
     * @return the ftnGender
     */
    public String getFtnGender() {
        return ftnGender;


    }

    /**
     * @param ftnGender the ftnGender to set
     */
    public void setFtnGender(String ftnGender) {
        this.ftnGender = ftnGender;


    }

    /**
     * @return the ftnReligion
     */
    public String getFtnReligion() {
        return ftnReligion;


    }

    /**
     * @param ftnReligion the ftnReligion to set
     */
    public void setFtnReligion(String ftnReligion) {
        this.ftnReligion = ftnReligion;


    }

    /**
     * @return the ftnMaritalStatus
     */
    public String getFtnMaritalStatus() {
        return ftnMaritalStatus;


    }

    /**
     * @param ftnMaritalStatus the ftnMaritalStatus to set
     */
    public void setFtnMaritalStatus(String ftnMaritalStatus) {
        this.ftnMaritalStatus = ftnMaritalStatus;


    }

    /**
     * @return the ftnOccupation
     */
    public String getFtnOccupation() {
        return ftnOccupation;


    }

    /**
     * @param ftnOccupation the ftnOccupation to set
     */
    public void setFtnOccupation(String ftnOccupation) {
        this.ftnOccupation = ftnOccupation;


    }

    /**
     * @return the additionalDiscount
     */
    public String getAdditionalDiscount() {
        return additionalDiscount;


    }

    /**
     * @param additionalDiscount the additionalDiscount to set
     */
    public void setAdditionalDiscount(String additionalDiscount) {
        this.additionalDiscount = additionalDiscount;

    }

    /**
     * @return the ftnOrderMasterList
     */
    public List<FtnOrderMaster> getFtnOrderMasterList() {
        return ftnOrderMasterList;
    }

    /**
     * @param ftnOrderMasterList the ftnOrderMasterList to set
     */
    public void setFtnOrderMasterList(List<FtnOrderMaster> ftnOrderMasterList) {
        this.ftnOrderMasterList = ftnOrderMasterList;
    }

    /**
     * @return the cdcConfirmFlag
     */
    public String getCdcConfirmFlag() {
        return cdcConfirmFlag;
    }

    /**
     * @param cdcConfirmFlag the cdcConfirmFlag to set
     */
    public void setCdcConfirmFlag(String cdcConfirmFlag) {
        this.cdcConfirmFlag = cdcConfirmFlag;
    }

    /**
     * @return the ftnOrderNo
     */
    public String getFtnOrderNo() {
        return ftnOrderNo;
    }

    /**
     * @param ftnOrderNo the ftnOrderNo to set
     */
    public void setFtnOrderNo(String ftnOrderNo) {
        this.ftnOrderNo = ftnOrderNo;
    }

    /**
     * @return the ftnOrderDate
     */
    public String getFtnOrderDate() {
        return ftnOrderDate;
    }

    /**
     * @param ftnOrderDate the ftnOrderDate to set
     */
    public void setFtnOrderDate(String ftnOrderDate) {
        this.ftnOrderDate = ftnOrderDate;
    }

    /**
     * @return the ftnPromotionList
     */
    public List<FtnPromotion> getFtnPromotionMstList() {
        return ftnPromotionMstList;
    }

    /**
     * @param ftnPromotionList the ftnPromotionList to set
     */
    public void setFtnPromotionMstList(List<FtnPromotion> ftnPromotionMstList) {
        this.ftnPromotionMstList = ftnPromotionMstList;
    }

    /**
     * @return the ftnCancelReason
     */
    public String getFtnCancelReason() {
        return ftnCancelReason;
    }

    /**
     * @param ftnCancelReason the ftnCancelReason to set
     */
    public void setFtnCancelReason(String ftnCancelReason) {
        this.ftnCancelReason = ftnCancelReason;
    }

    /**
     * @return the myFile
     */
    public File getMyFile() {
        return myFile;
    }

    /**
     * @param myFile the myFile to set
     */
    public void setMyFile(File myFile) {
        this.myFile = myFile;
    }

    /**
     * @return the myFileContentType
     */
    public String getMyFileContentType() {
        return myFileContentType;
    }

    /**
     * @param myFileContentType the myFileContentType to set
     */
    public void setMyFileContentType(String myFileContentType) {
        this.myFileContentType = myFileContentType;
    }

    /**
     * @return the myFileFileName
     */
    public String getMyFileFileName() {
        return myFileFileName;
    }

    /**
     * @param myFileFileName the myFileFileName to set
     */
    public void setMyFileFileName(String myFileFileName) {
        this.myFileFileName = myFileFileName;
    }

    /**
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * @param filePath the filePath to set
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * @return the ftnCircularNumber
     */
    public String getFtnCircularNumber() {
        return ftnCircularNumber;
    }

    /**
     * @param ftnCircularNumber the ftnCircularNumber to set
     */
    public void setFtnCircularNumber(String ftnCircularNumber) {
        this.ftnCircularNumber = ftnCircularNumber;
    }

    /**
     * @return the ftnCustomerType
     */
    public String getFtnCustomerType() {
        return ftnCustomerType;
    }

    /**
     * @param ftnCustomerType the ftnCustomerType to set
     */
    public void setFtnCustomerType(String ftnCustomerType) {
        this.ftnCustomerType = ftnCustomerType;
    }

    /**
     * @return the ftnPaymentModeList
     */
    public List<FtnPaymentMode> getFtnPaymentModeList() {
        return ftnPaymentModeList;
    }

    /**
     * @param ftnPaymentModeList the ftnPaymentModeList to set
     */
    public void setFtnPaymentModeList(List<FtnPaymentMode> ftnPaymentModeList) {
        this.ftnPaymentModeList = ftnPaymentModeList;
    }

    /**
     * @return the ftnPaymentType
     */
    public String getFtnPaymentType() {
        return ftnPaymentType;
    }

    /**
     * @param ftnPaymentType the ftnPaymentType to set
     */
    public void setFtnPaymentType(String ftnPaymentType) {
        this.ftnPaymentType = ftnPaymentType;
    }

    /**
     * @return the ftnVatDate
     */
    public String getFtnVatDate() {
        return ftnVatDate;
    }

    /**
     * @param ftnVatDate the ftnVatDate to set
     */
    public void setFtnVatDate(String ftnVatDate) {
        this.ftnVatDate = ftnVatDate;
    }

    /**
     * @return the ftnVatInvoiceNo
     */
    public String getFtnVatInvoiceNo() {
        return ftnVatInvoiceNo;
    }

    /**
     * @param ftnVatInvoiceNo the ftnVatInvoiceNo to set
     */
    public void setFtnVatInvoiceNo(String ftnVatInvoiceNo) {
        this.ftnVatInvoiceNo = ftnVatInvoiceNo;
    }

    /**
     * @return the ftnOrderAwbNo
     */
    public String getFtnOrderAwbNo() {
        return ftnOrderAwbNo;
    }

    /**
     * @param ftnOrderAwbNo the ftnOrderAwbNo to set
     */
    public void setFtnOrderAwbNo(String ftnOrderAwbNo) {
        this.ftnOrderAwbNo = ftnOrderAwbNo;
    }

    /**
     * @return the ftnYear
     */
    public String getFtnYear() {
        return ftnYear;
    }

    /**
     * @param ftnYear the ftnYear to set
     */
    public void setFtnYear(String ftnYear) {
        this.ftnYear = ftnYear;
    }

    /**
     * @return the ftnWeek
     */
    public String getFtnWeek() {
        return ftnWeek;
    }

    /**
     * @param ftnWeek the ftnWeek to set
     */
    public void setFtnWeek(String ftnWeek) {
        this.ftnWeek = ftnWeek;
    }

    /**
     * @return the yearList
     */
    public String getYearList() {
        return yearList;
    }

    /**
     * @param yearList the yearList to set
     */
    public void setYearList(String yearList) {
        this.yearList = yearList;
    }

    /**
     * @return the weekList
     */
    public String getWeekList() {
        return weekList;
    }

    /**
     * @param weekList the weekList to set
     */
    public void setWeekList(String weekList) {
        this.weekList = weekList;
    }

    /**
     * @return the dateList
     */
    public String getDateList() {
        return dateList;
    }

    /**
     * @param dateList the dateList to set
     */
    public void setDateList(String dateList) {
        this.dateList = dateList;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the ftnRemittanceList
     */
    public List<FtnRemittance> getFtnRemittanceList() {
        return ftnRemittanceList;
    }

    /**
     * @param ftnRemittanceList the ftnRemittanceList to set
     */
    public void setFtnRemittanceList(List<FtnRemittance> ftnRemittanceList) {
        this.ftnRemittanceList = ftnRemittanceList;
    }
}
