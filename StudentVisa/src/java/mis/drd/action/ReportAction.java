/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.drd.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author MIS-Monjur
 */
public class ReportAction extends ActionSupport {

    private String format;
    private String reportType;
    public String option;
    //
    private String orderNumber;

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public String printCustomerInvoice() {

        if (!ParamUtil.isSet(orderNumber) || !ParamUtil.isSet(format)) {
            addActionError("Missing Report Parameters");
            return ERROR;
        }

        Map sessionMap = ActionContext.getContext().getSession();
        Map parameters = new HashMap();

        System.out.println("orderNumber:" + orderNumber);

        parameters.put("ORDER_NUMBER", orderNumber);
        sessionMap.put("report", "order/customerInvoice.jrxml");
        sessionMap.put("format", format);
        sessionMap.put("parameters", parameters);

        return SUCCESS;
    }

    public String printCustomerOrder() {

        if (!ParamUtil.isSet(orderNumber) || !ParamUtil.isSet(format)) {
            addActionError("Missing Report Parameters");
            return ERROR;
        }

        Map sessionMap = ActionContext.getContext().getSession();
        String name = (String) sessionMap.get("userName");
        Map parameters = new HashMap();

        System.out.println("orderNumber:" + orderNumber);

        parameters.put("REPORT_USER", name);
        parameters.put("ORDER_NUMBER", orderNumber);
        sessionMap.put("report", "order/customerOrder.jrxml");
        sessionMap.put("format", format);
        sessionMap.put("parameters", parameters);

        return SUCCESS;
    }

    /**
     * @return the format
     */
    public String getFormat() {
        return format;
    }

    /**
     * @param format the format to set
     */
    public void setFormat(String format) {
        this.format = format;
    }

    /**
     * @return the reportType
     */
    public String getReportType() {
        return reportType;
    }

    /**
     * @param reportType the reportType to set
     */
    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    /**
     * @return the orderNumber
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * @param orderNumber the orderNumber to set
     */
    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }
}
