/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.drd.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSessionListener;
import mis.drd.model.*;
import mis.drd.persistance.*;
import mis.drd.utility.Encryption;
import org.apache.struts2.interceptor.ServletRequestAware;

/**
 *
 * @author MIS
 */
public class LoginAction extends ActionSupport implements ActionResult, ServletRequestAware, HttpSessionListener {

    /**
     * @return the activeSessions
     */
    public static int getActiveSessions() {
        return activeSessions;
    }

    /**
     * @param aActiveSessions the activeSessions to set
     */
    public static void setActiveSessions(int aActiveSessions) {
        activeSessions = aActiveSessions;
    }
    private String userName;
    private String password;
    private String userRole;
    //
    private HttpServletRequest request;
    private static int activeSessions = 0;
    //
    private LoginModel loginModel = null;
    //
    private String jsonData;
    //
    private String ftnUserFirstName;
    private String ftnUserMiddleName;
    private String ftnUserLastName;
    private String ftnContactNumber;
    private String ftnEmailAddress;
    private String ftnUserPassword;
    private String ftnSecurityQuestion;
    private String ftnSecurityAnswer;
    //
    private String ftnActivationLink;

    public String signIn() {

        if (!ParamUtil.isSet(userName) || !ParamUtil.isSet(password)) {
            addActionError("User Name or Password is not set properly");
            return INPUT;
        }

        Map sessionMap = ActionContext.getContext().getSession();

        boolean hasUserFound = false;

        HashMap<Object, Object> paramMap = new HashMap<Object, Object>();
        paramMap.put("USER_NAME", userName);

        if (loginModel == null) {
            loginModel = new LoginModel();
        }

        List<SvUser> users = loginModel.ftnUserList(paramMap);


        if (ParamUtil.isSet(users)) {

            for (SvUser user : users) {

                if (user.getUserPassword().equals(password)) {

                    sessionMap.put("userName", user.getDisplayName());
                    sessionMap.put("userId", user.getUserId());
                    sessionMap.put("groupId", user.getSvUserGroupInfo().getGroupId());

                    sessionMap.put("comCode", user.getSvCompanyInfo().getComCode());
                    sessionMap.put("comName", user.getSvCompanyInfo().getComName());

                    sessionMap.put("logged-in", true);

                    hasUserFound = true;
                    break;
                }
            }
        }

        if (!hasUserFound) {
            addActionError("User Name or Password is incorrect");
            return INPUT;
        }

        return SIGNIN;
    }

    public String welcome() {
        return WELCOME;
    }

    public String footinSignIn() {
        return USERLOGIN;
    }

    public String signUp() {
        return SIGNUP;
    }

    public String footinSignOut() {

        Map sessionMap = ActionContext.getContext().getSession();
        sessionMap.remove("userName");
        sessionMap.remove("userId");
        sessionMap.remove("levelId");
        sessionMap.remove("levelShortName");
        sessionMap.remove("comCode");
        sessionMap.remove("comName");
        sessionMap.remove("storeCode");
        sessionMap.remove("USER_GROUP_ID");
        sessionMap.remove("COM_CODE");
        sessionMap.remove("logged-in");

        sessionMap.clear();

        return USERLOGIN;
    }

    public String duplicateUserCheck() {

        if (loginModel == null) {
            loginModel = new LoginModel();
        }

        HashMap<Object, Object> paramMap = new HashMap<Object, Object>();
        paramMap.put("CONTACT_NO", (ftnContactNumber == null ? "" : ftnContactNumber));

        jsonData = loginModel.ftnDuplicateUserCheck(paramMap);

        System.out.println("JsonData:" + jsonData);

        return JSONDATA;
    }

    public String createNewUser() {

        if (loginModel == null) {
            loginModel = new LoginModel();
        }

        System.out.println("ftnUserFirstName:" + ftnUserFirstName);
        System.out.println("ftnUserMiddleName:" + ftnUserMiddleName);
        System.out.println("ftnUserLastName:" + ftnUserLastName);
        System.out.println("ftnContactNumber:" + ftnContactNumber);
        System.out.println("ftnEmailAddress:" + ftnEmailAddress);
        System.out.println("ftnUserPassword:" + ftnUserPassword);
        System.out.println("ftnSecurityQuestion:" + ftnSecurityQuestion);
        System.out.println("ftnSecurityAnswer:" + ftnSecurityAnswer);

        HashMap<Object, Object> paramMap = new HashMap<Object, Object>();

        // This one  for ftn_customer_info table
        paramMap.put("FIRST_NAME", (ftnUserFirstName == null ? "" : ftnUserFirstName));
        paramMap.put("MIDDLE_NAME", (ftnUserMiddleName == null ? "" : ftnUserMiddleName));
        paramMap.put("LAST_NAME", (ftnUserLastName == null ? "" : ftnUserLastName));
        paramMap.put("CONTACT_NO", (ftnContactNumber == null ? "" : ftnContactNumber));
        paramMap.put("EMAIL_ADD", (ftnEmailAddress == null ? "" : ftnEmailAddress));
        // This one  for ftn_user table
        paramMap.put("USER_NAME", (ftnEmailAddress == null ? ftnContactNumber : (ftnEmailAddress.isEmpty() ? ftnContactNumber : ftnEmailAddress)));
        paramMap.put("USER_GROUP_ID", "2");
        paramMap.put("COM_CODE", "518");
        paramMap.put("DISPLAY_NAME", ((ftnUserFirstName == null ? "" : ftnUserFirstName) + " " + (ftnUserMiddleName == null ? "" : ftnUserMiddleName) + " " + (ftnUserLastName == null ? "" : ftnUserLastName)).trim());
        paramMap.put("USER_EMAIL", (ftnEmailAddress == null ? "" : ftnEmailAddress));
        paramMap.put("USER_CONTACT", (ftnContactNumber == null ? "" : ftnContactNumber));
        paramMap.put("USER_PASSWORD", (ftnUserPassword == null ? "" : ftnUserPassword));
        paramMap.put("SECURITY_QUESTION", (ftnSecurityQuestion == null ? "" : ftnSecurityQuestion));
        paramMap.put("SECURITY_ANSWER", (ftnSecurityAnswer == null ? "" : ftnSecurityAnswer));

        jsonData = loginModel.ftnNewUserSave(paramMap);

        System.out.println("JsonData:" + jsonData);

        return JSONDATA;
    }

    public String ftnUserActivation() {

        if (loginModel == null) {
            loginModel = new LoginModel();
        }

        int feed = 0;

        HashMap<Object, Object> paramMap = new HashMap<Object, Object>();

        System.out.println("ftnActivationLink:" + ftnActivationLink);

        if (ftnActivationLink != null) {
            if (!ftnActivationLink.isEmpty()) {

                String decryptString = "";

                try {
                    decryptString = Encryption.decryption(ftnActivationLink);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    feed = 1;
                }

                if (feed == 0) {

                    int str_index = decryptString.indexOf("<START>");
                    int end_index = decryptString.indexOf("<END>");

                    if ((str_index == -1) || (end_index == -1)) {
                        feed = 1;
                    } else {

                        str_index = decryptString.indexOf("<USER>");
                        end_index = decryptString.indexOf("</USER>");

                        if ((str_index == -1) || (end_index == -1)) {
                            feed = 1;
                        } else {

                            paramMap.put("USER_ID", decryptString.substring(str_index + 6, end_index).trim());

                            str_index = decryptString.indexOf("<CUSTOMER>");
                            end_index = decryptString.indexOf("</CUSTOMER>");

                            if ((str_index == -1) || (end_index == -1)) {
                                feed = 1;
                            } else {

                                paramMap.put("CUSTOMER_ID", decryptString.substring(str_index + 10, end_index).trim());

                                str_index = decryptString.indexOf("<CODE>");
                                end_index = decryptString.indexOf("</CODE>");

                                if ((str_index == -1) || (end_index == -1)) {
                                    feed = 1;
                                } else {
                                    paramMap.put("ACTIVATION_CODE", decryptString.substring(str_index + 6, end_index).trim());
                                }

                            }

                        }

                    }
                }
            }
        }

        if (feed == 0) {

            jsonData = loginModel.userActivation(paramMap);

            System.out.println("JsonData:" + jsonData);

            if (jsonData.equalsIgnoreCase("1")) {
                return USERLOGIN;
            } else {
                return SIGNUPERROR;
            }

        } else {
            return SIGNUPERROR;
        }

    }

    /**
     * Start get and set method
     */
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public HttpServletRequest getServletRequest() {
        return request;
    }

    @Override
    public void setServletRequest(HttpServletRequest req) {
        this.request = req;
    }

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        activeSessions++;
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        activeSessions--;
    }

    /**
     * @return the ftnContactNumber
     */
    public String getFtnContactNumber() {
        return ftnContactNumber;
    }

    /**
     * @param ftnContactNumber the ftnContactNumber to set
     */
    public void setFtnContactNumber(String ftnContactNumber) {
        this.ftnContactNumber = ftnContactNumber;
    }

    /**
     * @return the jsonData
     */
    public String getJsonData() {
        return jsonData;
    }

    /**
     * @param jsonData the jsonData to set
     */
    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }

    /**
     * @return the ftnUserFirstName
     */
    public String getFtnUserFirstName() {
        return ftnUserFirstName;
    }

    /**
     * @param ftnUserFirstName the ftnUserFirstName to set
     */
    public void setFtnUserFirstName(String ftnUserFirstName) {
        this.ftnUserFirstName = ftnUserFirstName;
    }

    /**
     * @return the ftnUserMiddleName
     */
    public String getFtnUserMiddleName() {
        return ftnUserMiddleName;
    }

    /**
     * @param ftnUserMiddleName the ftnUserMiddleName to set
     */
    public void setFtnUserMiddleName(String ftnUserMiddleName) {
        this.ftnUserMiddleName = ftnUserMiddleName;
    }

    /**
     * @return the ftnUserLastName
     */
    public String getFtnUserLastName() {
        return ftnUserLastName;
    }

    /**
     * @param ftnUserLastName the ftnUserLastName to set
     */
    public void setFtnUserLastName(String ftnUserLastName) {
        this.ftnUserLastName = ftnUserLastName;
    }

    /**
     * @return the ftnEmailAddress
     */
    public String getFtnEmailAddress() {
        return ftnEmailAddress;
    }

    /**
     * @param ftnEmailAddress the ftnEmailAddress to set
     */
    public void setFtnEmailAddress(String ftnEmailAddress) {
        this.ftnEmailAddress = ftnEmailAddress;
    }

    /**
     * @return the ftnUserPassword
     */
    public String getFtnUserPassword() {
        return ftnUserPassword;
    }

    /**
     * @param ftnUserPassword the ftnUserPassword to set
     */
    public void setFtnUserPassword(String ftnUserPassword) {
        this.ftnUserPassword = ftnUserPassword;
    }

    /**
     * @return the ftnSecurityQuestion
     */
    public String getFtnSecurityQuestion() {
        return ftnSecurityQuestion;
    }

    /**
     * @param ftnSecurityQuestion the ftnSecurityQuestion to set
     */
    public void setFtnSecurityQuestion(String ftnSecurityQuestion) {
        this.ftnSecurityQuestion = ftnSecurityQuestion;
    }

    /**
     * @return the ftnSecurityAnswer
     */
    public String getFtnSecurityAnswer() {
        return ftnSecurityAnswer;
    }

    /**
     * @param ftnSecurityAnswer the ftnSecurityAnswer to set
     */
    public void setFtnSecurityAnswer(String ftnSecurityAnswer) {
        this.ftnSecurityAnswer = ftnSecurityAnswer;
    }

    /**
     * @return the ftnActivationLink
     */
    public String getFtnActivationLink() {
        return ftnActivationLink;
    }

    /**
     * @param ftnActivationLink the ftnActivationLink to set
     */
    public void setFtnActivationLink(String ftnActivationLink) {
        this.ftnActivationLink = ftnActivationLink;
    }
}
