package mis.drd.utility;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author MIS
 */
public class DatabaseConnection {

    //static member holds only one instance of the DatabaseConnection class.
    private static DatabaseConnection databaseConnection = null;

    //DatabaseConnection prevents the instantiation from any other class.
    private DatabaseConnection() {
    }

    //Now we are providing gloabal point of access.
    public static synchronized DatabaseConnection getInstance() {

        if (databaseConnection == null) {
            databaseConnection = new DatabaseConnection();
        }

        return databaseConnection;

    }

    public static Connection connectDB() {

        Connection connect = null;

        try {

            Properties properties = new Properties();
            InputStream inputStream = DatabaseConnection.class.getClassLoader().getResourceAsStream("connectionString.properties");
            properties.load(inputStream);

            Class.forName("com.mysql.jdbc.Driver").newInstance();

            System.out.println(properties.getProperty("dbURL"));
            System.out.println(properties.getProperty("dbUserName"));
            System.out.println(properties.getProperty("dbUesrPass"));

            connect = DriverManager.getConnection(properties.getProperty("dbURL"),
                    properties.getProperty("dbUserName"),
                    properties.getProperty("dbUesrPass"));

            connect.setAutoCommit(true);

            if (inputStream != null) {
                inputStream.close();
                inputStream = null;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            connect = null;
        }

        return connect;
    }

    public static void main(String[] args) throws ClassNotFoundException {

        Connection con = DatabaseConnection.connectDB();

        if (con != null) {
            System.out.println("success");
        } else {
            System.out.println("fail");
        }

    }
}
