/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.drd.utility;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import mis.drd.persistance.FtnPromotion;
import mis.drd.persistance.FtnRemittance;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author MIS
 */
public class XlsxFileReader {

    public static void main(String[] args) {
        XlsxFileReader salesXlsxFileReader = new XlsxFileReader();
    }

    public List<FtnPromotion> xlsxCircularFileRead(String filePath) {


        FtnPromotion xmlInfo;
        List<FtnPromotion> xmlInfoList = null;

//        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");



        try {
            // Get the workbook instance for XLSX file
            XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(new File(filePath)));

            // Get first sheet from the workbook
            XSSFSheet sheet = wb.getSheetAt(0);

            Row row;
            Cell cell;

            // Iterate through each rows from first sheet
            Iterator<Row> rowIterator = sheet.iterator();

            xmlInfoList = new ArrayList<FtnPromotion>();

            int firstRow = 0;
            boolean fg = false;

            while (rowIterator.hasNext()) {
                if (firstRow == 0) {
                    firstRow++;
                    row = rowIterator.next();
                }
                row = rowIterator.next();

                fg = true;

                // For each row, iterate through each columns
                Iterator<Cell> cellIterator = row.cellIterator();

                xmlInfo = new FtnPromotion();
                //

                String cellValue = "";

                while (cellIterator.hasNext()) {

                    cell = cellIterator.next();
                    cellValue = getXlsxCellValue(cell);

                    // Index 0 for CIRCULAR_NO
                    if (cell.getColumnIndex() == 0) {
                        xmlInfo.setPromoId(cellValue == null ? "" : cellValue);
                    } // Index 1 for PROMOTION_NAME
                    else if (cell.getColumnIndex() == 1) {
                        xmlInfo.setPromoName(cellValue == null ? "" : cellValue);
                    } // Index 2 for CIRCULAR_DATE
                    else if (cell.getColumnIndex() == 2) {
                        xmlInfo.setCircularDate(cellValue == null ? "" : cellValue);
                    } // Index 3 for EFFECT_FROM_DATE
                    else if (cell.getColumnIndex() == 3) {
                        xmlInfo.setEffectStartDate(cellValue == null ? "" : cellValue);
                    } // Index 4 for EFFECT_TO_DATE
                    else if (cell.getColumnIndex() == 4) {
                        xmlInfo.setEffectEndDate(cellValue == null ? "" : cellValue);
                    } // Index 5 for EFFECT_FROM_TIME                 
                    else if (cell.getColumnIndex() == 5) {
                        xmlInfo.setStartTime(Integer.parseInt(cellValue == null ? "0" : cellValue.isEmpty() ? "0" : cellValue));
                    } // Index 6 for EFFECT_TO_TIME
                    else if (cell.getColumnIndex() == 6) {
                        xmlInfo.setEndTime(Integer.parseInt(cellValue == null ? "0" : cellValue.isEmpty() ? "0" : cellValue));
                    } // Index 7 for ARTICLE_CODE
                    else if (cell.getColumnIndex() == 7) {
                        xmlInfo.setArtCode(cellValue == null ? "" : cellValue);
                    } // Index 8 for OLD_PRICE
                    else if (cell.getColumnIndex() == 8) {
                        xmlInfo.setMrp(cellValue == null ? "" : cellValue);
                    } // Index 9 for NEW_PRICE
                    else if (cell.getColumnIndex() == 9) {
                        xmlInfo.setNetSalePriceAfterDis(cellValue == null ? "" : cellValue);
                    }// Index 10 for DIFF
                    else if (cell.getColumnIndex() == 10) {
                        xmlInfo.setDiscountAmount(cellValue == null ? "" : cellValue);
                    }

                }

                xmlInfoList.add(xmlInfo);


            }

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Exception :" + e.getMessage());

        }

        return xmlInfoList;

    }

    private String getXlsxCellValue(Cell cell) {

        String cellValue = "";
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                cellValue = cell.getRichStringCellValue().getString();
                break;
            case Cell.CELL_TYPE_NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    cellValue = (cell.getDateCellValue() == null ? "" : formatter.format(cell.getDateCellValue()));
                } else {
                    cellValue = String.valueOf(cell.getNumericCellValue());
                    if (cellValue.endsWith(".0")) {
                        cellValue = cellValue.substring(0, cellValue.indexOf(".0"));
                    }
                }
                break;
            case Cell.CELL_TYPE_BOOLEAN:
                cellValue = String.valueOf(cell.getBooleanCellValue());
                break;
            case Cell.CELL_TYPE_FORMULA:
                cellValue = cell.getCellFormula();
                break;
            default:
                cellValue = cell.toString();
        }
        return cellValue.replaceAll("'", "");
    }

    public List<FtnRemittance> xlsxRemittanceFileRead(String filePath) {


        FtnRemittance xmlInfo;
        List<FtnRemittance> xmlInfoList = null;

        try {
            // Get the workbook instance for XLSX file
            XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(new File(filePath)));

            // Get first sheet from the workbook
            XSSFSheet sheet = wb.getSheetAt(0);

            Row row;
            Cell cell;

            // Iterate through each rows from first sheet
            Iterator<Row> rowIterator = sheet.iterator();

            xmlInfoList = new ArrayList<FtnRemittance>();

            int firstRow = 0;
            boolean fg = false;

            while (rowIterator.hasNext()) {
                if (firstRow == 0) {
                    firstRow++;
                    row = rowIterator.next();
                }
                row = rowIterator.next();

                fg = true;

                // For each row, iterate through each columns
                Iterator<Cell> cellIterator = row.cellIterator();

                xmlInfo = new FtnRemittance();
                //

                String cellValue = "";

                while (cellIterator.hasNext()) {

                    cell = cellIterator.next();
                    cellValue = getXlsxCellValue(cell);

                    // Index 0 for AWB
                    if (cell.getColumnIndex() == 0) {
                        xmlInfo.setAwb(cellValue == null ? "" : cellValue);
                    } // Index 1 for AMOUNT
                    else if (cell.getColumnIndex() == 1) {
                        xmlInfo.setAmount(cellValue == null ? "0" : cellValue);
                    } // Index 2 for MR_NO
                    else if (cell.getColumnIndex() == 2) {
                        xmlInfo.setMrno(cellValue == null ? "" : cellValue);
                    } // Index 3 for MR_DATE
                    else if (cell.getColumnIndex() == 3) {
                        xmlInfo.setMrdate(cellValue == null ? "" : cellValue);
                    } // Index 4 for COLLECTED_AMOUNT
                    else if (cell.getColumnIndex() == 4) {
                        xmlInfo.setCollectedAmount(cellValue == null ? "0" : cellValue);
                    } // Index 5 for PICKUP_DATE
                    else if (cell.getColumnIndex() == 5) {
                        xmlInfo.setPickupDate(cellValue == null ? "0" : cellValue);
                    } // Index 6 for SHIPPER_NAME
                    else if (cell.getColumnIndex() == 6) {
                        xmlInfo.setShipperName(cellValue == null ? "0" : cellValue);
                    } // Index 7 for REMARKS
                    else if (cell.getColumnIndex() == 7) {
                        xmlInfo.setRemarks(cellValue == null ? "" : cellValue);
                    }

                }

                xmlInfoList.add(xmlInfo);


            }

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Exception :" + e.getMessage());

        }

        return xmlInfoList;

    }
}
