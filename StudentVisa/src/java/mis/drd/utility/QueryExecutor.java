package mis.drd.utility;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author MIS
 */
public class QueryExecutor {

    //static member holds only one instance of the QueryExecutor class.
    private static QueryExecutor queryExecutor = null;

    //QueryExecutor prevents the instantiation from any other class.
    private QueryExecutor() {
    }

    //Now we are providing gloabal point of access.
    public static synchronized QueryExecutor getInstance() {

        if (queryExecutor == null) {
            queryExecutor = new QueryExecutor();
        }

        return queryExecutor;

    }

    public static int insert(Connection connection, String query) {

        int insertRecord = 0;
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement(query);
            insertRecord = ps.executeUpdate();
        } catch (SQLException ex) {
            insertRecord = 0;
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                insertRecord = 0;
                ex.printStackTrace();
            }
        }

        return insertRecord;
    }

    public static int update(Connection connection, String query) {

        int updateRecord = 0;
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement(query);
            updateRecord = ps.executeUpdate();
        } catch (SQLException ex) {
            updateRecord = 0;
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                updateRecord = 0;
                ex.printStackTrace();
            }
        }

        return updateRecord;
    }

    public static int delete(Connection connection, String query) {

        int deleteRecord = 0;
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement(query);
            deleteRecord = ps.executeUpdate();
        } catch (SQLException ex) {
            deleteRecord = 0;
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                deleteRecord = 0;
                ex.printStackTrace();
            }
        }

        return deleteRecord;
    }

    public static BigDecimal getId(Connection connection, String tableName, String tableId) {

        PreparedStatement ps = null;
        ResultSet resultSet = null;
        BigDecimal id = null;

        try {

            String idQuery = "select nvl(max(" + tableId + "),0)+1 as num from " + tableName;
            ps = connection.prepareStatement(idQuery);
            resultSet = ps.executeQuery();

            while (resultSet.next()) {
                id = (BigDecimal) resultSet.getBigDecimal("num");
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
        return id;
    }

    public static String getRandomalphanumericNumber(int numOfCharacters) {

        StringBuilder buffer = new StringBuilder();

        Random random = new Random();

        char[] chars = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
            'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
            'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F',
            'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
            'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4',
            '5', '6', '7', '8', '9', '0'};

        for (int i = 0; i < numOfCharacters; i++) {
            buffer.append(chars[random.nextInt(chars.length)]);
        }

        return buffer.toString();

    }

    public static void main(String[] args) throws ClassNotFoundException {



//        Format formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//        String s = formatter.format(new Date());
//        System.out.println(s);

//        newCustomerCode = cont.substring(cont.length() - 6) + "0001";

//        String cont = "01716414405";

//        System.out.println(cont.substring(cont.length() - 6) + cont.substring(1, 3) + "01");

//         System.out.println(getRandomalphanumericNumber(4));

         System.out.println(24/6*4);


    }
}
