/**
 *
 */
package mis.drd.utility;

/**
 * @author MIS
 *
 */
public class Encryption {

    private static String encryptIdString = "34523434";
    private static int addValue = 333;
    //static member holds only one instance of the LoginStatement class.
    private static Encryption encryption = null;

    //LoginStatement prevents the instantiation from any other class.
    private Encryption() {
    }

    //Now we are providing gloabal point of access.
    public static synchronized Encryption getInstance() {

        if (encryption == null) {
            encryption = new Encryption();
        }

        return encryption;

    }

    // ========================= Encryption ====================================
    public static String encryption(String urlString) throws Exception {
        String encryptedUrl = "", asciiString = "";
        char ch;
        int position = 0;
        int len = urlString.length();
        boolean flag = true;
        while (flag) {
            if (len <= position) {
                flag = false;
            } else {
                ch = urlString.charAt(0);
                asciiString += String.valueOf((int) ch + addValue);
                urlString = urlString.substring(1);
            }
            position++;
        }
        encryptedUrl = getEncryptedAsciiString(asciiString);
        return encryptedUrl;
    }

    private static String getEncryptedAsciiString(String ascii_String) throws Exception {

        int addPosition = 0, randomLimit = 0;
        int stringLength = ascii_String.length();
        if (stringLength >= 100) {
            randomLimit = 99;
        } else {
            randomLimit = stringLength - 1;
        }
        addPosition = getRandomNumber(randomLimit);
        String firstPartString = ascii_String.substring(0, addPosition);
        String endPartString = ascii_String.substring(addPosition);
        String addPositionString = String.valueOf(addPosition);
        if (addPositionString.length() == 1) {
            addPositionString = "0" + addPositionString;
        }
        return addPositionString + firstPartString + encryptIdString + endPartString;

    }

    private static int getRandomNumber(int limit) {
        java.util.Random rand = new java.util.Random();
        return rand.nextInt(limit);
    }

    // ========================= Decryption ====================================
    public static String decryption(String encryptedString) throws Exception {
        String positionIdentityString = encryptedString.substring(0, 2);
        String encrypt_String;
        encryptedString = encryptedString.substring(2);
        float numberValue = Float.valueOf(positionIdentityString).floatValue();
        if (numberValue > 0) {
            encrypt_String = encryptedString.substring(0, (int) numberValue) + encryptedString.substring((int) numberValue + 8);
        } else {
            encrypt_String = encryptedString.substring(8);
        }
        return getString(encrypt_String);
    }

    private static String getString(String string) throws Exception {
        String decryptString = "";
        boolean flag = true;
        int lenght = string.length(), loopCount = 0;
        while (flag) {
            if (lenght <= loopCount) {
                flag = false;
            } else {
                String tmpString = string.substring(0, 3);
                float numberValue = Float.valueOf(tmpString).floatValue();
                int number = (int) (numberValue - addValue);
                char ch = (char) number;
                decryptString += ch;
                string = string.substring(3);
            }
            loopCount += 3;
        }

        return decryptString;
    }

    public static void main(String[] args) {
        Encryption encryption = new Encryption();
        //encryption.urlEncryption("jahurul");
        String val = "SYS123";

        //  String val = "1641642241638238333452343484";
        try {
            val = encryption.encryption(val);
            System.out.println("Encryption " + val);
            String st = encryption.decryption(val);
            System.out.println("Decryption " + st);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
