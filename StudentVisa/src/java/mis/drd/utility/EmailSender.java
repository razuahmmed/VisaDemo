/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.drd.utility;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class EmailSender {

    //static member holds only one instance of the LoginStatement class.
    private static EmailSender emailSender = null;

    //LoginStatement prevents the instantiation from any other class.
    private EmailSender() {
    }

    //Now we are providing gloabal point of access.
    public static synchronized EmailSender getInstance() {

        if (emailSender == null) {
            emailSender = new EmailSender();
        }

        return emailSender;

    }

    public static String sendEmail(String host, String port,
            final String userName, final String password, String toAddress,
            String subject, String message, String bounceAddr) {

        try {
            Properties properties = new Properties();
            properties.put("mail.smtp.host", host);
            properties.put("mail.smtp.port", port);
            properties.put("mail.smtp.auth", "true");
            properties.put("mail.smtp.starttls.enable", "true");
            properties.put("mail.user", userName);
            properties.put("mail.password", password);
            properties.put("mail.smtp.from", bounceAddr);
            properties.put("mail.transport.protocol", "smtp");

            Authenticator auth = new Authenticator() {

                @Override
                public PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(userName, password);
                }
            };
            Session session = Session.getInstance(properties, auth);

            // creates a new e-mail message
            Message msg = new MimeMessage(session);

            msg.setFrom(new InternetAddress(userName));
            InternetAddress[] toAddresses = {new InternetAddress(toAddress)};
            msg.setRecipients(Message.RecipientType.TO, toAddresses);
            msg.setSubject(subject);
            msg.setSentDate(new Date());

            // creates message part
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(message, "text/html");

            // creates multi-part
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);

            // sets the multi-part as e-mail's content
            msg.setContent(multipart);

            // sends the e-mail
            Transport.send(msg);

            return "success";

        } catch (Exception ex) {
            System.out.println("Could not send email.");
            ex.printStackTrace();
            return "error";
        }

    }

    /**
     * Test sending e-mail with attachments
     */
    public static void main(String[] args) {
        // SMTP info
        String host = "lx.alpha.net.bd";
        String port = "25";
        String bounceAddr = "info@footin.com.bd";
        String mailFrom = "info@footin.com.bd";
        String password = "FootinMail518";

        // message info
        String mailTo = "badal688@gmail.com";
        String subject = "New email with attachments";
        String message = "I have some attachments for you.";


        try {
            sendEmail(host, port, mailFrom, password, mailTo, subject, message, bounceAddr);
            System.out.println("Email sent.");
        } catch (Exception ex) {
            System.out.println("Could not send email.");
            ex.printStackTrace();
        }
    }
}
