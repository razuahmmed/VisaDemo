/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.drd.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import mis.drd.persistance.*;
import mis.drd.statement.FtnMyAccountStatement;
import mis.drd.statement.LoginStatement;
import mis.drd.utility.DatabaseConnection;

/**
 *
 * @author MIS
 */
public class FtnMyAccountModel {

    public String ftnCountryStateJsonData(Connection connection, HashMap<Object, Object> paramMap) {

        StringBuilder dataString = new StringBuilder();


        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnMyAccountStatement.countryStateQuery(paramMap);

                System.out.println("CountryStateQuery:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                //var j ='[{"name":"test1"},{"id":"2","name":"test2"},{"id":"3","name":"test3"}]';

                dataString.append("[");

                boolean fg = true;

                while (resultSet.next()) {

                    if (fg) {
                        dataString.append("{\"name\":\"");
                        dataString.append(resultSet.getString("SATE_REGION"));
                        dataString.append("\"}");
                        fg = false;
                    } else {
                        dataString.append(",{\"name\":\"");
                        dataString.append(resultSet.getString("SATE_REGION"));
                        dataString.append("\"}");
                    }

                }

                dataString.append("]");

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return dataString.toString().equalsIgnoreCase("[]") ? "\"\"" : dataString.toString();

    }

    public String ftnCountryStateCityJsonData(Connection connection, HashMap<Object, Object> paramMap) {

        StringBuilder dataString = new StringBuilder();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnMyAccountStatement.countryStateCityQuery(paramMap);

                System.out.println("CountryStateCityQuery:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                //var j ='[{"name":"test1"},{"id":"2","name":"test2"},{"id":"3","name":"test3"}]';

                dataString.append("[");

                boolean fg = true;

                while (resultSet.next()) {

                    if (fg) {
                        dataString.append("{\"name\":\"");
                        dataString.append(resultSet.getString("CITY_NAME"));
                        dataString.append("\"}");
                        fg = false;
                    } else {
                        dataString.append(",{\"name\":\"");
                        dataString.append(resultSet.getString("CITY_NAME"));
                        dataString.append("\"}");
                    }

                }

                dataString.append("]");

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return dataString.toString().equalsIgnoreCase("[]") ? "\"\"" : dataString.toString();

    }

    public String ftnCustomerDetails(HashMap<Object, Object> paramMap) {

        StringBuilder dataString = new StringBuilder();
        StringBuilder stateString = new StringBuilder();
        StringBuilder cityString = new StringBuilder();

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnMyAccountStatement.searchCustomerQuery(paramMap);

                System.out.println("CustomerQuery:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                while (resultSet.next()) {

                    // get all state for a country
                    HashMap<Object, Object> countryMap = new HashMap<Object, Object>();
                    countryMap.put("COUNTRY_NAME", resultSet.getString("COUNTRY_NAME") == null ? "" : resultSet.getString("COUNTRY_NAME"));
                    stateString.append(ftnCountryStateJsonData(connection, countryMap));
                    // get all city for a state
                    HashMap<Object, Object> stateMap = new HashMap<Object, Object>();
                    stateMap.put("COUNTRY_NAME", resultSet.getString("COUNTRY_NAME") == null ? "" : resultSet.getString("COUNTRY_NAME"));
                    stateMap.put("SATE_REGION", resultSet.getString("STATE_REGION") == null ? "" : resultSet.getString("STATE_REGION"));
                    cityString.append(ftnCountryStateCityJsonData(connection, stateMap));
                    // get customer details
                    dataString.append("[{");
                    ////
                    dataString.append("\"custid\":\"");
                    dataString.append(resultSet.getString("CUSTOMER_ID") == null ? "" : resultSet.getString("CUSTOMER_ID"));
                    dataString.append("\",");
                    //
                    dataString.append("\"contactno\":\"");
                    dataString.append(resultSet.getString("CONTACT_NO") == null ? "" : resultSet.getString("CONTACT_NO"));
                    dataString.append("\",");
                    //
                    dataString.append("\"email\":\"");
                    dataString.append(resultSet.getString("EMAIL_ADD") == null ? "" : resultSet.getString("EMAIL_ADD"));
                    dataString.append("\",");
                    //
                    dataString.append("\"fname\":\"");
                    dataString.append(resultSet.getString("FIRST_NAME") == null ? "" : resultSet.getString("FIRST_NAME"));
                    dataString.append("\",");
                    //
                    dataString.append("\"mname\":\"");
                    dataString.append(resultSet.getString("MIDDLE_NAME") == null ? "" : resultSet.getString("MIDDLE_NAME"));
                    dataString.append("\",");
                    //
                    dataString.append("\"lname\":\"");
                    dataString.append(resultSet.getString("LAST_NAME") == null ? "" : resultSet.getString("LAST_NAME"));
                    dataString.append("\",");
                    //
                    dataString.append("\"sname\":\"");
                    dataString.append(resultSet.getString("SURE_NAME") == null ? "" : resultSet.getString("SURE_NAME"));
                    dataString.append("\",");
                    //
                    dataString.append("\"country\":\"");
                    dataString.append(resultSet.getString("COUNTRY_NAME") == null ? "" : resultSet.getString("COUNTRY_NAME"));
                    dataString.append("\",");
                    //
                    dataString.append("\"state\":\"");
                    dataString.append(resultSet.getString("STATE_REGION") == null ? "" : resultSet.getString("STATE_REGION"));
                    dataString.append("\",");
                    //
                    dataString.append("\"city\":\"");
                    dataString.append(resultSet.getString("CITY") == null ? "" : resultSet.getString("CITY"));
                    dataString.append("\",");
                    //
                    dataString.append("\"pscode\":\"");
                    dataString.append(resultSet.getString("POSTAL_CODE") == null ? "" : resultSet.getString("POSTAL_CODE"));
                    dataString.append("\",");
                    //
                    dataString.append("\"add\":\"");
                    dataString.append(resultSet.getString("ADD1") == null ? "" : resultSet.getString("ADD1"));
                    dataString.append("\",");
                    //
                    dataString.append("\"sadd\":\"");
                    dataString.append(resultSet.getString("SHIPPING_ADDRESS") == null ? "" : resultSet.getString("SHIPPING_ADDRESS"));
                    dataString.append("\",");
                    //
                    dataString.append("\"dob\":\"");
                    dataString.append(resultSet.getString("DOB") == null ? "" : resultSet.getString("DOB"));
                    dataString.append("\",");
                    //
                    dataString.append("\"customertype\":\"");
                    dataString.append(resultSet.getString("CUSTOMER_TYPE") == null ? "Standard" : resultSet.getString("CUSTOMER_TYPE"));
                    dataString.append("\",");
                    //
                    dataString.append("\"gender\":\"");
                    dataString.append(resultSet.getString("GENDER") == null ? "" : resultSet.getString("GENDER"));
                    dataString.append("\",");
                    //
                    dataString.append("\"religion\":\"");
                    dataString.append(resultSet.getString("RELIGION") == null ? "" : resultSet.getString("RELIGION"));
                    dataString.append("\",");
                    //
                    dataString.append("\"mrstatus\":\"");
                    dataString.append(resultSet.getString("MARITAL_STATUS") == null ? "" : resultSet.getString("MARITAL_STATUS"));
                    dataString.append("\",");
                    //
                    dataString.append("\"marriagedate\":\"");
                    dataString.append(resultSet.getString("MARRIAGE_DATE") == null ? "" : resultSet.getString("MARRIAGE_DATE"));
                    dataString.append("\",");
                    //
                    dataString.append("\"occupation\":\"");
                    dataString.append(resultSet.getString("OCCUPATION") == null ? "" : resultSet.getString("OCCUPATION"));
                    dataString.append("\"");
                    ////
                    dataString.append("}]");

                    break;

                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return stateString.toString() + "<FD>" // 0
                + cityString.toString() + "<FD>" // 1
                + dataString.toString() + "<FD>"; // 2

    }
}
