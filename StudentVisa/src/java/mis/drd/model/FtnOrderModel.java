/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.drd.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import mis.drd.persistance.*;
import mis.drd.statement.FtnOrderStatement;
import mis.drd.utility.DatabaseConnection;

/**
 *
 * @author MIS
 */
public class FtnOrderModel {

    public List<FtnCountry> ftnCountryList() {

        List<FtnCountry> tmpInfoList = null;

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.countryQuery();

                System.out.println("CountryQuery:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                tmpInfoList = new ArrayList<FtnCountry>();
                //
                FtnCountry ftnCountryInfo = null;

                while (resultSet.next()) {

                    ftnCountryInfo = new FtnCountry();
                    //
                    ftnCountryInfo.setCountryCode(resultSet.getString("COUNTRY_CODE"));
                    ftnCountryInfo.setCountryName(resultSet.getString("COUNTRY_NAME"));
                    //
                    tmpInfoList.add(ftnCountryInfo);

                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return tmpInfoList;

    }

    public List<FtnPaymentMode> ftnPaymentModeList() {

        List<FtnPaymentMode> tmpInfoList = null;

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.paymentModeQuery();

                System.out.println("Payment Mode Query:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                tmpInfoList = new ArrayList<FtnPaymentMode>();
                //
                FtnPaymentMode ftnInfo = null;

                while (resultSet.next()) {

                    ftnInfo = new FtnPaymentMode();
                    //
                    ftnInfo.setPaymentType(resultSet.getString("PAYMENT_TYPE"));
                    ftnInfo.setPaymentDescription(resultSet.getString("PAYMENT_DESC"));
                    //
                    tmpInfoList.add(ftnInfo);

                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return tmpInfoList;

    }

    public String ftnCountryJsonData(Connection connection) {

        StringBuilder dataString = new StringBuilder();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.countryQuery();

                System.out.println("CountryQuery:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                //var j ='[{"name":"test1"},{"id":"2","name":"test2"},{"id":"3","name":"test3"}]';

                dataString.append("[");

                boolean fg = true;

                while (resultSet.next()) {

                    if (fg) {
                        dataString.append("{");
                        ////
                        dataString.append("\"countrycode\":\"");
                        dataString.append(resultSet.getString("COUNTRY_CODE") == null ? "" : resultSet.getString("COUNTRY_CODE"));
                        dataString.append("\",");
                        //
                        dataString.append("\"countryname\":\"");
                        dataString.append(resultSet.getString("COUNTRY_NAME") == null ? "" : resultSet.getString("COUNTRY_NAME"));
                        dataString.append("\"");
                        ////
                        dataString.append("}");
                        fg = false;
                    } else {
                        dataString.append(",{");
                        ////
                        dataString.append("\"countrycode\":\"");
                        dataString.append(resultSet.getString("COUNTRY_CODE") == null ? "" : resultSet.getString("COUNTRY_CODE"));
                        dataString.append("\",");
                        //
                        dataString.append("\"countryname\":\"");
                        dataString.append(resultSet.getString("COUNTRY_NAME") == null ? "" : resultSet.getString("COUNTRY_NAME"));
                        dataString.append("\"");
                        ////
                        dataString.append("}");
                    }

                }

                dataString.append("]");

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return dataString.toString().equalsIgnoreCase("[]") ? "\"\"" : dataString.toString();

    }

    public String ftnCountryStateJsonData(HashMap<Object, Object> paramMap) {

        StringBuilder dataString = new StringBuilder();

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.countryStateQuery(paramMap);

                System.out.println("CountryStateQuery:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                //var j ='[{"name":"test1"},{"id":"2","name":"test2"},{"id":"3","name":"test3"}]';

                dataString.append("[");

                boolean fg = true;

                while (resultSet.next()) {

                    if (fg) {
                        dataString.append("{\"name\":\"");
                        dataString.append(resultSet.getString("SATE_REGION"));
                        dataString.append("\"}");
                        fg = false;
                    } else {
                        dataString.append(",{\"name\":\"");
                        dataString.append(resultSet.getString("SATE_REGION"));
                        dataString.append("\"}");
                    }

                }

                dataString.append("]");

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return dataString.toString().equalsIgnoreCase("[]") ? "\"\"" : dataString.toString();

    }

    public String ftnCountryStateJsonData(Connection connection, HashMap<Object, Object> paramMap) {

        StringBuilder dataString = new StringBuilder();


        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.countryStateQuery2(paramMap);

                System.out.println("CountryStateQuery:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                //var j ='[{"name":"test1"},{"id":"2","name":"test2"},{"id":"3","name":"test3"}]';

                dataString.append("[");

                boolean fg = true;

                while (resultSet.next()) {

                    if (fg) {
                        dataString.append("{\"name\":\"");
                        dataString.append(resultSet.getString("SATE_REGION"));
                        dataString.append("\"}");
                        fg = false;
                    } else {
                        dataString.append(",{\"name\":\"");
                        dataString.append(resultSet.getString("SATE_REGION"));
                        dataString.append("\"}");
                    }

                }

                dataString.append("]");

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return dataString.toString().equalsIgnoreCase("[]") ? "\"\"" : dataString.toString();

    }

    public String ftnCountryStateCityJsonData(HashMap<Object, Object> paramMap) {

        StringBuilder dataString = new StringBuilder();

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.countryStateCityQuery(paramMap);

                System.out.println("CountryStateCityQuery:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                //var j ='[{"name":"test1"},{"id":"2","name":"test2"},{"id":"3","name":"test3"}]';

                dataString.append("[");

                boolean fg = true;

                while (resultSet.next()) {

                    if (fg) {
                        dataString.append("{\"name\":\"");
                        dataString.append(resultSet.getString("CITY_NAME"));
                        dataString.append("\"}");
                        fg = false;
                    } else {
                        dataString.append(",{\"name\":\"");
                        dataString.append(resultSet.getString("CITY_NAME"));
                        dataString.append("\"}");
                    }

                }

                dataString.append("]");

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return dataString.toString().equalsIgnoreCase("[]") ? "\"\"" : dataString.toString();

    }

    public String ftnCountryStateCityJsonData(Connection connection, HashMap<Object, Object> paramMap) {

        StringBuilder dataString = new StringBuilder();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.countryStateCityQuery2(paramMap);

                System.out.println("CountryStateCityQuery2:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                //var j ='[{"name":"test1"},{"id":"2","name":"test2"},{"id":"3","name":"test3"}]';

                dataString.append("[");

                boolean fg = true;

                while (resultSet.next()) {

                    if (fg) {
                        dataString.append("{\"name\":\"");
                        dataString.append(resultSet.getString("CITY_NAME"));
                        dataString.append("\"}");
                        fg = false;
                    } else {
                        dataString.append(",{\"name\":\"");
                        dataString.append(resultSet.getString("CITY_NAME"));
                        dataString.append("\"}");
                    }

                }

                dataString.append("]");

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return dataString.toString().equalsIgnoreCase("[]") ? "\"\"" : dataString.toString();

    }

    public String ftnCustomerTempOrderArticleJsonData(Connection connection, HashMap<Object, Object> paramMap) {

        StringBuilder dataString = new StringBuilder();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.customerTempOrderArticleQuery(paramMap);

                System.out.println("CustomerTempOrderArticleQuery:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                //var j ='[{"name":"test1"},{"id":"2","name":"test2"},{"id":"3","name":"test3"}]';

                dataString.append("[");

                boolean fg = true;

                while (resultSet.next()) {

                    if (fg) {
                        dataString.append("{");
                        ////
                        dataString.append("\"artcode\":\"");
                        dataString.append((resultSet.getString("ART_CODE") == null ? "" : resultSet.getString("ART_CODE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"description\":\"");
                        dataString.append((resultSet.getString("ART_DESCRIPTION") == null ? "" : resultSet.getString("ART_DESCRIPTION")));
                        dataString.append("\",");
                        //
                        dataString.append("\"size\":\"");
                        dataString.append((resultSet.getString("ART_SIZE") == null ? "" : resultSet.getString("ART_SIZE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"qty\":\"");
                        dataString.append((resultSet.getString("QTY") == null ? "" : resultSet.getString("QTY")));
                        dataString.append("\",");
                        //
                        dataString.append("\"mrp\":\"");
                        dataString.append((resultSet.getString("MRP") == null ? "" : resultSet.getString("MRP")));
                        dataString.append("\",");
                        //
                        dataString.append("\"discount\":\"");
                        dataString.append((resultSet.getString("DISCOUNT") == null ? "" : resultSet.getString("DISCOUNT")));
                        dataString.append("\",");
                        //
                        dataString.append("\"subtotal\":\"");
                        dataString.append((resultSet.getString("SUB_TOTAL") == null ? "" : resultSet.getString("SUB_TOTAL")));
                        dataString.append("\",");
                        //
                        dataString.append("\"totaldiscount\":\"");
                        dataString.append((resultSet.getString("TOTAL_DISCOUNT") == null ? "" : resultSet.getString("TOTAL_DISCOUNT")));
                        dataString.append("\",");
                        //
                        dataString.append("\"totalsubtot\":\"");
                        dataString.append((resultSet.getString("TOTAL_SUB_TOT") == null ? "" : resultSet.getString("TOTAL_SUB_TOT")));
                        dataString.append("\"");
                        ////
                        dataString.append("}");
                        fg = false;
                    } else {
                        dataString.append(",{");
                        ////
                        dataString.append("\"artcode\":\"");
                        dataString.append((resultSet.getString("ART_CODE") == null ? "" : resultSet.getString("ART_CODE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"description\":\"");
                        dataString.append((resultSet.getString("ART_DESCRIPTION") == null ? "" : resultSet.getString("ART_DESCRIPTION")));
                        dataString.append("\",");
                        //
                        dataString.append("\"size\":\"");
                        dataString.append((resultSet.getString("ART_SIZE") == null ? "" : resultSet.getString("ART_SIZE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"qty\":\"");
                        dataString.append((resultSet.getString("QTY") == null ? "" : resultSet.getString("QTY")));
                        dataString.append("\",");
                        //
                        dataString.append("\"mrp\":\"");
                        dataString.append((resultSet.getString("MRP") == null ? "" : resultSet.getString("MRP")));
                        dataString.append("\",");
                        //
                        dataString.append("\"discount\":\"");
                        dataString.append((resultSet.getString("DISCOUNT") == null ? "" : resultSet.getString("DISCOUNT")));
                        dataString.append("\",");
                        //
                        dataString.append("\"subtotal\":\"");
                        dataString.append((resultSet.getString("SUB_TOTAL") == null ? "" : resultSet.getString("SUB_TOTAL")));
                        dataString.append("\",");
                        //
                        dataString.append("\"totaldiscount\":\"");
                        dataString.append((resultSet.getString("TOTAL_DISCOUNT") == null ? "" : resultSet.getString("TOTAL_DISCOUNT")));
                        dataString.append("\",");
                        //
                        dataString.append("\"totalsubtot\":\"");
                        dataString.append((resultSet.getString("TOTAL_SUB_TOT") == null ? "" : resultSet.getString("TOTAL_SUB_TOT")));
                        dataString.append("\"");
                        ////
                        dataString.append("}");
                    }

                }

                dataString.append("]");

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return dataString.toString().equalsIgnoreCase("[]") ? "\"\"" : dataString.toString();

    }

    public String ftnSearchCustomerDetails(HashMap<Object, Object> paramMap) {

        StringBuilder dataString = new StringBuilder();
        StringBuilder stateString = new StringBuilder();
        StringBuilder cityString = new StringBuilder();
        StringBuilder tempOrderString = new StringBuilder();

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.searchCustomerQuery(paramMap);

                System.out.println("SearchCustomerQuery:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                //var j ='[{
                // "country":[{"countrycode":"BD","countryname":"BANGLAESH"}],
                // "state":[{"name":"DHAKA"}],
                // "city":[{"name":"COMILLA"}],
                // "coustomer":[{"id":"2","name":"test2"},{"id":"3","name":"test3"}]
                // }]';


                while (resultSet.next()) {

                    // get all state for a country
                    HashMap<Object, Object> countryMap = new HashMap<Object, Object>();
                    countryMap.put("COUNTRY_NAME", resultSet.getString("COUNTRY_NAME") == null ? "" : resultSet.getString("COUNTRY_NAME"));
                    stateString.append(ftnCountryStateJsonData(connection, countryMap));
                    // get all city for a state
                    HashMap<Object, Object> stateMap = new HashMap<Object, Object>();
                    stateMap.put("COUNTRY_NAME", resultSet.getString("COUNTRY_NAME") == null ? "" : resultSet.getString("COUNTRY_NAME"));
                    stateMap.put("SATE_REGION", resultSet.getString("STATE_REGION") == null ? "" : resultSet.getString("STATE_REGION"));
                    cityString.append(ftnCountryStateCityJsonData(connection, stateMap));
                    // get customer details
                    dataString.append("[{");
                    ////
                    dataString.append("\"custid\":\"");
                    dataString.append(resultSet.getString("CUSTOMER_ID") == null ? "" : resultSet.getString("CUSTOMER_ID"));
                    dataString.append("\",");
                    //
                    dataString.append("\"contactno\":\"");
                    dataString.append(resultSet.getString("CONTACT_NO") == null ? "" : resultSet.getString("CONTACT_NO"));
                    dataString.append("\",");
                    //
                    dataString.append("\"email\":\"");
                    dataString.append(resultSet.getString("EMAIL_ADD") == null ? "" : resultSet.getString("EMAIL_ADD"));
                    dataString.append("\",");
                    //
                    dataString.append("\"fname\":\"");
                    dataString.append(resultSet.getString("FIRST_NAME") == null ? "" : resultSet.getString("FIRST_NAME"));
                    dataString.append("\",");
                    //
                    dataString.append("\"mname\":\"");
                    dataString.append(resultSet.getString("MIDDLE_NAME") == null ? "" : resultSet.getString("MIDDLE_NAME"));
                    dataString.append("\",");
                    //
                    dataString.append("\"lname\":\"");
                    dataString.append(resultSet.getString("LAST_NAME") == null ? "" : resultSet.getString("LAST_NAME"));
                    dataString.append("\",");
                    //
                    dataString.append("\"sname\":\"");
                    dataString.append(resultSet.getString("SURE_NAME") == null ? "" : resultSet.getString("SURE_NAME"));
                    dataString.append("\",");
                    //
                    dataString.append("\"country\":\"");
                    dataString.append(resultSet.getString("COUNTRY_NAME") == null ? "" : resultSet.getString("COUNTRY_NAME"));
                    dataString.append("\",");
                    //
                    dataString.append("\"state\":\"");
                    dataString.append(resultSet.getString("STATE_REGION") == null ? "" : resultSet.getString("STATE_REGION"));
                    dataString.append("\",");
                    //
                    dataString.append("\"city\":\"");
                    dataString.append(resultSet.getString("CITY") == null ? "" : resultSet.getString("CITY"));
                    dataString.append("\",");
                    //
                    dataString.append("\"pscode\":\"");
                    dataString.append(resultSet.getString("POSTAL_CODE") == null ? "" : resultSet.getString("POSTAL_CODE"));
                    dataString.append("\",");
                    //
                    dataString.append("\"add\":\"");
                    dataString.append(resultSet.getString("ADD1") == null ? "" : resultSet.getString("ADD1"));
                    dataString.append("\",");
                    //
                    dataString.append("\"sadd\":\"");
                    dataString.append(resultSet.getString("SHIPPING_ADDRESS") == null ? "" : resultSet.getString("SHIPPING_ADDRESS"));
                    dataString.append("\",");
                    //
                    dataString.append("\"dob\":\"");
                    dataString.append(resultSet.getString("DOB") == null ? "" : resultSet.getString("DOB"));
                    dataString.append("\",");
                    //
                    dataString.append("\"customertype\":\"");
                    dataString.append(resultSet.getString("CUSTOMER_TYPE") == null ? "Standard" : resultSet.getString("CUSTOMER_TYPE"));
                    dataString.append("\",");
                    //
                    dataString.append("\"gender\":\"");
                    dataString.append(resultSet.getString("GENDER") == null ? "" : resultSet.getString("GENDER"));
                    dataString.append("\",");
                    //
                    dataString.append("\"religion\":\"");
                    dataString.append(resultSet.getString("RELIGION") == null ? "" : resultSet.getString("RELIGION"));
                    dataString.append("\",");
                    //
                    dataString.append("\"mrstatus\":\"");
                    dataString.append(resultSet.getString("MARITAL_STATUS") == null ? "" : resultSet.getString("MARITAL_STATUS"));
                    dataString.append("\",");
                    //
                    dataString.append("\"occupation\":\"");
                    dataString.append(resultSet.getString("OCCUPATION") == null ? "" : resultSet.getString("OCCUPATION"));
                    dataString.append("\"");
                    ////
                    dataString.append("}]");

                    break;

                }

                tempOrderString.append(ftnCustomerTempOrderArticleJsonData(connection, paramMap));

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return stateString.toString() + "<FD>" // 0
                + cityString.toString() + "<FD>" // 1
                + dataString.toString() + "<FD>" // 2
                + tempOrderString.toString() + "<FD>"; // 3

    }

    public String ftnArticlePromotionJsonData(Connection connection, HashMap<Object, Object> paramMap) {

        StringBuilder dataString = new StringBuilder();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.articlePromotionQuery(paramMap);

                System.out.println("ArticlePromotionQuery:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                //var j ='[{"name":"test1"},{"id":"2","name":"test2"},{"id":"3","name":"test3"}]';

                dataString.append("[");

                boolean fg = true;

                while (resultSet.next()) {

                    if (fg) {
                        dataString.append("{");
                        ////
                        dataString.append("\"promotionid\":\"");
                        dataString.append((resultSet.getString("PROMO_ID") == null ? "" : resultSet.getString("PROMO_ID")));
                        dataString.append("\",");
                        //
                        dataString.append("\"promotionname\":\"");
                        dataString.append((resultSet.getString("PROMO_NAME") == null ? "" : resultSet.getString("PROMO_NAME")));
                        dataString.append("\",");
                        //
                        dataString.append("\"artcode\":\"");
                        dataString.append((resultSet.getString("ART_CODE") == null ? "" : resultSet.getString("ART_CODE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"mrp\":\"");
                        dataString.append((resultSet.getString("MRP") == null ? "" : resultSet.getString("MRP")));
                        dataString.append("\",");
                        //
                        dataString.append("\"discount\":\"");
                        dataString.append((resultSet.getString("DISCOUNT_AMOUNT") == null ? "" : resultSet.getString("DISCOUNT_AMOUNT")));
                        dataString.append("\",");
                        //
                        dataString.append("\"startdate\":\"");
                        dataString.append((resultSet.getString("START_DATE") == null ? "" : resultSet.getString("START_DATE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"starttime\":\"");
                        dataString.append((resultSet.getString("START_TIME") == null ? "" : resultSet.getString("START_TIME")));
                        dataString.append("\",");
                        //
                        dataString.append("\"enddate\":\"");
                        dataString.append((resultSet.getString("END_DATE") == null ? "" : resultSet.getString("END_DATE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"endtime\":\"");
                        dataString.append((resultSet.getString("END_TIME") == null ? "" : resultSet.getString("END_TIME")));
                        dataString.append("\"");
                        ////
                        dataString.append("}");
                        fg = false;
                    } else {
                        dataString.append(",{");
                        ////
                        dataString.append("\"promotionid\":\"");
                        dataString.append((resultSet.getString("PROMO_ID") == null ? "" : resultSet.getString("PROMO_ID")));
                        dataString.append("\",");
                        //
                        dataString.append("\"promotionname\":\"");
                        dataString.append((resultSet.getString("PROMO_NAME") == null ? "" : resultSet.getString("PROMO_NAME")));
                        dataString.append("\",");
                        //
                        dataString.append("\"artcode\":\"");
                        dataString.append((resultSet.getString("ART_CODE") == null ? "" : resultSet.getString("ART_CODE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"mrp\":\"");
                        dataString.append((resultSet.getString("MRP") == null ? "" : resultSet.getString("MRP")));
                        dataString.append("\",");
                        //
                        dataString.append("\"discount\":\"");
                        dataString.append((resultSet.getString("DISCOUNT_AMOUNT") == null ? "" : resultSet.getString("DISCOUNT_AMOUNT")));
                        dataString.append("\",");
                        //
                        dataString.append("\"startdate\":\"");
                        dataString.append((resultSet.getString("START_DATE") == null ? "" : resultSet.getString("START_DATE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"starttime\":\"");
                        dataString.append((resultSet.getString("START_TIME") == null ? "" : resultSet.getString("START_TIME")));
                        dataString.append("\",");
                        //
                        dataString.append("\"enddate\":\"");
                        dataString.append((resultSet.getString("END_DATE") == null ? "" : resultSet.getString("END_DATE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"endtime\":\"");
                        dataString.append((resultSet.getString("END_TIME") == null ? "" : resultSet.getString("END_TIME")));
                        dataString.append("\"");
                        ////
                        dataString.append("}");
                    }

                }

                dataString.append("]");

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return dataString.toString().equalsIgnoreCase("[]") ? "\"\"" : dataString.toString();

    }

    public String ftnSearchArticleDetails(HashMap<Object, Object> paramMap) {

        StringBuilder dataString = new StringBuilder();

        StringBuilder promotionDataString = new StringBuilder();

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.searchArticleQuery(paramMap);

                System.out.println("SearchArticleQuery:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                //var j ='[{
                // "country":[{"countrycode":"BD","countryname":"BANGLAESH"}],
                // "state":[{"name":"DHAKA"}],
                // "city":[{"name":"COMILLA"}],
                // "coustomer":[{"id":"2","name":"test2"},{"id":"3","name":"test3"}]
                // }]';

                while (resultSet.next()) {
                    // get article details
                    dataString.append("[{");
                    ////
                    dataString.append("\"artcode\":\"");
                    dataString.append(resultSet.getString("ART_CODE") == null ? "" : resultSet.getString("ART_CODE"));
                    dataString.append("\",");
                    //
                    dataString.append("\"sizecode\":\"");
                    dataString.append(resultSet.getString("SIZE_CODE") == null ? "" : resultSet.getString("SIZE_CODE"));
                    dataString.append("\",");
                    //
                    dataString.append("\"catcode\":\"");
                    dataString.append(resultSet.getString("CAT_CODE") == null ? "" : resultSet.getString("CAT_CODE"));
                    dataString.append("\",");
                    //
                    dataString.append("\"catname\":\"");
                    dataString.append(resultSet.getString("CAT_NAME") == null ? "" : resultSet.getString("CAT_NAME"));
                    dataString.append("\",");
                    //
                    dataString.append("\"sbcatcode\":\"");
                    dataString.append(resultSet.getString("SUB_CAT_CODE") == null ? "" : resultSet.getString("SUB_CAT_CODE"));
                    dataString.append("\",");
                    //
                    dataString.append("\"sbcatname\":\"");
                    dataString.append(resultSet.getString("SUB_CAT_NAME") == null ? "" : resultSet.getString("SUB_CAT_NAME"));
                    dataString.append("\",");
                    //
                    dataString.append("\"brandcode\":\"");
                    dataString.append(resultSet.getString("BRAND_CODE") == null ? "" : resultSet.getString("BRAND_CODE"));
                    dataString.append("\",");
                    //
                    dataString.append("\"brandname\":\"");
                    dataString.append(resultSet.getString("BRAND_NAME") == null ? "" : resultSet.getString("BRAND_NAME"));
                    dataString.append("\",");
                    //
                    dataString.append("\"artmrp\":\"");
                    dataString.append(resultSet.getString("MRP") == null ? "" : resultSet.getString("MRP"));
                    dataString.append("\",");
                    //
                    dataString.append("\"artcost\":\"");
                    dataString.append(resultSet.getString("COST_PRICE") == null ? "" : resultSet.getString("COST_PRICE"));
                    dataString.append("\",");
                    //
                    dataString.append("\"artvat\":\"");
                    dataString.append(resultSet.getString("VAT") == null ? "" : resultSet.getString("VAT"));
                    dataString.append("\",");
                    //
                    dataString.append("\"artnet\":\"");
                    dataString.append(resultSet.getString("NET_PRICE") == null ? "" : resultSet.getString("NET_PRICE"));
                    dataString.append("\",");
                    //
                    dataString.append("\"artimage\":\"");
                    dataString.append(resultSet.getString("IMGE_URL_1") == null ? "" : resultSet.getString("IMGE_URL_1"));
                    dataString.append("\",");
                    ///////
                    dataString.append("\"stkr1\":\"");
                    dataString.append(resultSet.getString("R1") == null ? "" : resultSet.getString("R1"));
                    dataString.append("\",");
                    //
                    dataString.append("\"stkr2\":\"");
                    dataString.append(resultSet.getString("R2") == null ? "" : resultSet.getString("R2"));
                    dataString.append("\",");
                    //
                    dataString.append("\"stkr3\":\"");
                    dataString.append(resultSet.getString("R3") == null ? "" : resultSet.getString("R3"));
                    dataString.append("\",");
                    //
                    dataString.append("\"stkr4\":\"");
                    dataString.append(resultSet.getString("R4") == null ? "" : resultSet.getString("R4"));
                    dataString.append("\",");
                    //
                    dataString.append("\"stkr5\":\"");
                    dataString.append(resultSet.getString("R5") == null ? "" : resultSet.getString("R5"));
                    dataString.append("\",");
                    //
                    dataString.append("\"stkr6\":\"");
                    dataString.append(resultSet.getString("R6") == null ? "" : resultSet.getString("R6"));
                    dataString.append("\",");
                    //
                    dataString.append("\"stkr7\":\"");
                    dataString.append(resultSet.getString("R7") == null ? "" : resultSet.getString("R7"));
                    dataString.append("\",");
                    //
                    dataString.append("\"stkr8\":\"");
                    dataString.append(resultSet.getString("R8") == null ? "" : resultSet.getString("R8"));
                    dataString.append("\",");
                    //
                    dataString.append("\"stkr9\":\"");
                    dataString.append(resultSet.getString("R9") == null ? "" : resultSet.getString("R9"));
                    dataString.append("\",");
                    //
                    dataString.append("\"stkr10\":\"");
                    dataString.append(resultSet.getString("R10") == null ? "" : resultSet.getString("R10"));
                    dataString.append("\",");
                    //
                    dataString.append("\"stkr11\":\"");
                    dataString.append(resultSet.getString("R11") == null ? "" : resultSet.getString("R11"));
                    dataString.append("\",");
                    //
                    dataString.append("\"stkr12\":\"");
                    dataString.append(resultSet.getString("R12") == null ? "" : resultSet.getString("R12"));
                    dataString.append("\",");
                    //
                    dataString.append("\"stkr13\":\"");
                    dataString.append(resultSet.getString("R13") == null ? "" : resultSet.getString("R13"));
                    dataString.append("\",");
                    //
                    dataString.append("\"stktotal\":\"");
                    dataString.append(resultSet.getString("PAIRS") == null ? "" : resultSet.getString("PAIRS"));
                    dataString.append("\",");
                    //////
                    /////// Size range
                    dataString.append("\"sizes1\":\"");
                    dataString.append(resultSet.getString("S1") == null ? "" : resultSet.getString("S1"));
                    dataString.append("\",");
                    //
                    dataString.append("\"sizes2\":\"");
                    dataString.append(resultSet.getString("S2") == null ? "" : resultSet.getString("S2"));
                    dataString.append("\",");
                    //
                    dataString.append("\"sizes3\":\"");
                    dataString.append(resultSet.getString("S3") == null ? "" : resultSet.getString("S3"));
                    dataString.append("\",");
                    //
                    dataString.append("\"sizes4\":\"");
                    dataString.append(resultSet.getString("S4") == null ? "" : resultSet.getString("S4"));
                    dataString.append("\",");
                    //
                    dataString.append("\"sizes5\":\"");
                    dataString.append(resultSet.getString("S5") == null ? "" : resultSet.getString("S5"));
                    dataString.append("\",");
                    //
                    dataString.append("\"sizes6\":\"");
                    dataString.append(resultSet.getString("S6") == null ? "" : resultSet.getString("S6"));
                    dataString.append("\",");
                    //
                    dataString.append("\"sizes7\":\"");
                    dataString.append(resultSet.getString("S7") == null ? "" : resultSet.getString("S7"));
                    dataString.append("\",");
                    //
                    dataString.append("\"sizes8\":\"");
                    dataString.append(resultSet.getString("S8") == null ? "" : resultSet.getString("S8"));
                    dataString.append("\",");
                    //
                    dataString.append("\"sizes9\":\"");
                    dataString.append(resultSet.getString("S9") == null ? "" : resultSet.getString("S9"));
                    dataString.append("\",");
                    //
                    dataString.append("\"sizes10\":\"");
                    dataString.append(resultSet.getString("S10") == null ? "" : resultSet.getString("S10"));
                    dataString.append("\",");
                    //
                    dataString.append("\"sizes11\":\"");
                    dataString.append(resultSet.getString("S11") == null ? "" : resultSet.getString("S11"));
                    dataString.append("\",");
                    //
                    dataString.append("\"sizes12\":\"");
                    dataString.append(resultSet.getString("S12") == null ? "" : resultSet.getString("S12"));
                    dataString.append("\",");
                    //
                    dataString.append("\"sizes13\":\"");
                    dataString.append(resultSet.getString("S13") == null ? "" : resultSet.getString("S13"));
                    dataString.append("\"");
                    //////
                    ////
                    dataString.append("}]");

                    break;

                }

                promotionDataString.append(ftnArticlePromotionJsonData(connection, paramMap));


            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return dataString.toString() + "<FD>" + promotionDataString.toString();

    }

    public String ftnOrderTempArticleDetails(HashMap<Object, Object> paramMap) {

        HashMap<Object, Object> contactMap = new HashMap<Object, Object>();

        StringBuilder tempOrderString = new StringBuilder();

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;

        if (connection != null) {

            try {

                String mainString = paramMap.get("ORDER_ARTICLE").toString();

                int startIndex = 0;
                int endIndex = 0;
                //
                String contactNumber = "";
                String articleCode = "";
                String sizeCode = "";
                String description = "";
                String saizeAndPair = "";
                String mrp = "";
                String discount = "";
                String promotionid = "";

                if (!mainString.isEmpty()) {

                    // Contact Number
                    startIndex = mainString.indexOf("<CONTACT_NO>");
                    endIndex = mainString.indexOf("</CONTACT_NO>");

                    if ((startIndex != -1) && (endIndex != -1)) {
                        contactNumber = mainString.substring(startIndex + 12, endIndex);
                        contactMap.put("CONTACT_NO", (contactNumber == null ? "" : contactNumber));
                    }
                    // Article Code
                    startIndex = mainString.indexOf("<ART_CODE>");
                    endIndex = mainString.indexOf("</ART_CODE>");

                    if ((startIndex != -1) && (endIndex != -1)) {
                        articleCode = mainString.substring(startIndex + 10, endIndex);
                    }

                    // Article Size Code
                    startIndex = mainString.indexOf("<SIZE_CODE>");
                    endIndex = mainString.indexOf("</SIZE_CODE>");

                    if ((startIndex != -1) && (endIndex != -1)) {
                        sizeCode = mainString.substring(startIndex + 11, endIndex);
                    }

                    // Article Description
                    startIndex = mainString.indexOf("<DESCRIPTION>");
                    endIndex = mainString.indexOf("</DESCRIPTION>");

                    if ((startIndex != -1) && (endIndex != -1)) {
                        description = mainString.substring(startIndex + 13, endIndex);
                    }
                    // Article MRP
                    startIndex = mainString.indexOf("<MRP>");
                    endIndex = mainString.indexOf("</MRP>");

                    if ((startIndex != -1) && (endIndex != -1)) {
                        mrp = mainString.substring(startIndex + 5, endIndex);
                    }
                    // Article Discount
                    startIndex = mainString.indexOf("<DISCOUNT>");
                    endIndex = mainString.indexOf("</DISCOUNT>");

                    if ((startIndex != -1) && (endIndex != -1)) {
                        discount = mainString.substring(startIndex + 10, endIndex);
                    }
                    // Article Size and pair
                    startIndex = mainString.indexOf("<SIZE_PAIR>");
                    endIndex = mainString.indexOf("</SIZE_PAIR>");

                    if ((startIndex != -1) && (endIndex != -1)) {
                        saizeAndPair = mainString.substring(startIndex + 11, endIndex);
                    }

                    // Article promotion
                    startIndex = mainString.indexOf("<PROMOTION>");
                    endIndex = mainString.indexOf("</PROMOTION>");

                    if ((startIndex != -1) && (endIndex != -1)) {
                        promotionid = mainString.substring(startIndex + 11, endIndex);
                    }



                    if (!saizeAndPair.isEmpty()) {

                        String[] masterArray = saizeAndPair.split("<RD>");
                        String[] chdArray = null;
                        String query = "";

                        DecimalFormat df = new DecimalFormat("#.##");

                        int updateRecord = 0;

                        double subtotal = 0.0d;
                        double discounttotal = 0.0d;

                        for (int i = 0; i < masterArray.length; i++) {

                            updateRecord = 0;

                            chdArray = masterArray[i].split("<FD>");

                            subtotal = (Integer.parseInt((chdArray[1] == null ? "0" : (chdArray[1].trim().isEmpty() ? "0" : chdArray[1].trim())))
                                    * (Double.parseDouble((mrp == null ? "0.0" : (mrp.trim().isEmpty() ? "0.0" : mrp.trim())))
                                    - Double.parseDouble((discount == null ? "0.0" : (discount.trim().isEmpty() ? "0.0" : discount.trim())))));

                            discounttotal = (Integer.parseInt((chdArray[1] == null ? "0" : (chdArray[1].trim().isEmpty() ? "0" : chdArray[1].trim())))
                                    * Double.parseDouble((discount == null ? "0.0" : (discount.trim().isEmpty() ? "0.0" : discount.trim()))));

                            HashMap<Object, Object> articleMap = new HashMap<Object, Object>();
                            articleMap.put("CONTACT_NO", (contactNumber == null ? "" : contactNumber));
                            articleMap.put("PROMO_ID", promotionid);
                            articleMap.put("ART_CODE", (articleCode == null ? "" : articleCode));
                            articleMap.put("SIZE_CODE", (sizeCode == null ? "" : sizeCode));
                            articleMap.put("ART_DESCRIPTION", (description == null ? "" : description));
                            articleMap.put("ART_SIZE", (chdArray[0] == null ? "" : chdArray[0]));
                            articleMap.put("QTY", (chdArray[1] == null ? "0" : chdArray[1]));
                            articleMap.put("MRP", (mrp == null ? "" : mrp));
                            articleMap.put("DISCOUNT", df.format(discounttotal));
                            articleMap.put("SUB_TOTAL", df.format(subtotal));
                            articleMap.put("INSERT_BY", paramMap.get("INSERT_BY"));

                            query = FtnOrderStatement.customerTempOrderArticleUpdateQuery(articleMap);

                            System.out.println("CustomerTempOrderArticleUpdateQuery:" + query);

                            statement = connection.createStatement();
                            updateRecord = statement.executeUpdate(query);

                            if (updateRecord == 0) {

                                query = FtnOrderStatement.customerTempOrderArticleInsertQuery(articleMap);

                                System.out.println("CustomerTempOrderArticleInsertQuery:" + query);

                                statement = connection.createStatement();
                                statement.executeUpdate(query);
                            }

                        }

                    }


                }


                tempOrderString.append(ftnCustomerTempOrderArticleJsonData(connection, contactMap));

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return tempOrderString.toString(); // 3

    }

    public String ftnOrderTempArticleDelete(HashMap<Object, Object> paramMap) {

        StringBuilder tempOrderString = new StringBuilder();

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.customerTempOrderArticleDeleteQuery(paramMap);

                System.out.println("CustomerTempOrderArticleDeleteQuery:" + query);

                statement = connection.createStatement();
                statement.executeUpdate(query);

                tempOrderString.append(ftnCustomerTempOrderArticleJsonData(connection, paramMap));

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return tempOrderString.toString(); // 3

    }

    public String ftnOrderSave(HashMap<Object, Object> paramMap) {

        StringBuilder tempOrderString = new StringBuilder();

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;
        ResultSet resultSet = null;

        String query = "";

        if (connection != null) {

            String customerCode = paramMap.get("CUSTOMER_ID") == null ? "" : paramMap.get("CUSTOMER_ID").toString();

            String newCustomerCode = "";

            try {

                if (customerCode.isEmpty()) {
                    // Insert customer information
                    // Get customer pk id
                    String pkQuery = FtnOrderStatement.customerPkQuery(paramMap);

                    System.out.println("pkQuery:" + pkQuery);

                    statement = connection.createStatement();
                    resultSet = statement.executeQuery(pkQuery);

                    while (resultSet.next()) {
                        newCustomerCode = resultSet.getString("CUSTOMER_ID") == null ? "" : resultSet.getString("CUSTOMER_ID");
                        break;
                    }
                    // If customer not available
                    if (newCustomerCode.isEmpty()) {
                        String cont = paramMap.get("CONTACT_NO") == null ? "" : paramMap.get("CONTACT_NO").toString();
                        newCustomerCode = cont.substring(cont.length() - 6) + cont.substring(1, 3) + "01";
                    } else {
                        String cont = paramMap.get("CONTACT_NO") == null ? "" : paramMap.get("CONTACT_NO").toString();
                        newCustomerCode = cont.substring(cont.length() - 6) + newCustomerCode;
                    }

                    paramMap.put("CUSTOMER_ID", newCustomerCode);

                    query = FtnOrderStatement.customerInsertQuery(paramMap);

                    System.out.println("CustomerInsertQuery:" + query);

                } else {
                    // Update customer information
                    query = FtnOrderStatement.customerUpdateQuery(paramMap);

                    System.out.println("CustomerUpdateQuery:" + query);
                }

                statement = connection.createStatement();
                statement.executeUpdate(query);

                // Now insert order article information

                // Get order pk id

                String orderPkQuery = FtnOrderStatement.orderPkQuery(paramMap);

                System.out.println("OrderPkQuery:" + orderPkQuery);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(orderPkQuery);

                String newOrderNo = "";

                while (resultSet.next()) {
                    newOrderNo = resultSet.getString("ORDER_NO") == null ? "" : resultSet.getString("ORDER_NO");
                    break;
                }

                if (newOrderNo.isEmpty()) {
                    Format formatter = new SimpleDateFormat("yyyyMMdd");
                    SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MM-yyyy");

                    if (paramMap.get("ORDER_DATE").toString().isEmpty()) {
                        newOrderNo = formatter.format(new Date()) + newOrderNo;
                    } else {
                        newOrderNo = formatter.format(formatter1.parse(paramMap.get("ORDER_DATE").toString())) + "0001";
                    }

                } else {
                    Format formatter = new SimpleDateFormat("yyyyMMdd");
                    SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MM-yyyy");

                    if (paramMap.get("ORDER_DATE").toString().isEmpty()) {
                        newOrderNo = formatter.format(new Date()) + newOrderNo;
                    } else {
                        newOrderNo = formatter.format(formatter1.parse(paramMap.get("ORDER_DATE").toString())) + newOrderNo;
                    }


                }

                // insert into order master table

                String year = "";
                String week = "";

                query = FtnOrderStatement.yearWeekQuery(paramMap.get("ORDER_DATE").toString());

                System.out.println("YearWeekQuery:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                while (resultSet.next()) {
                    year = resultSet.getString("F_YEAR");
                    week = resultSet.getString("WEEK_NO");
                    break;
                }


                paramMap.put("ORDER_NO", newOrderNo);
                paramMap.put("F_YEAR", year);
                paramMap.put("WEEK_NO", week);

                query = FtnOrderStatement.orderMasterQuery(paramMap);

                System.out.println("OrderMasterQuery:" + query);

                statement = connection.createStatement();
                statement.executeUpdate(query);


                // insert into order child table

                query = FtnOrderStatement.orderChildInsertQuery(paramMap);

                System.out.println("OrderChildInsertQuery:" + query);

                statement = connection.createStatement();
                statement.executeUpdate(query);

                // Delete data from order temp table

                query = FtnOrderStatement.customerTempOrderArticleDelete2Query(paramMap);

                System.out.println("CustomerTempOrderArticleDeleteQuery:" + query);

                statement = connection.createStatement();
                statement.executeUpdate(query);


                tempOrderString.append("Successfully order saved.");
                tempOrderString.append("\nCustomer Id = ");
                tempOrderString.append(paramMap.get("CUSTOMER_ID"));
                tempOrderString.append(".\nOrder Number = ");
                tempOrderString.append(newOrderNo);


            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return tempOrderString.toString(); // 3

    }

    public List<FtnOrderMaster> ftnOrderMasterList() {

        List<FtnOrderMaster> tmpInfoList = null;

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.ftnOrderQuery();

                System.out.println("Query:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                tmpInfoList = new ArrayList<FtnOrderMaster>();
                //
                FtnOrderMaster ftnInfo = null;

                while (resultSet.next()) {

                    ftnInfo = new FtnOrderMaster();
                    //
                    ftnInfo.setStore_code(resultSet.getString("STORE_CODE"));
                    ftnInfo.setOrder_date(resultSet.getDate("ORDER_DATE"));
                    ftnInfo.setF_year(resultSet.getInt("F_YEAR"));
                    ftnInfo.setWeek_no(resultSet.getString("WEEK_NO"));
                    ftnInfo.setOrder_no(resultSet.getString("ORDER_NO"));
                    ftnInfo.setCustomer_id(resultSet.getString("CUSTOMER_ID"));
                    ftnInfo.setContact_no(resultSet.getString("CONTACT_NO"));
                    ftnInfo.setSales_pcode(resultSet.getString("SALES_PCODE"));
                    ftnInfo.setDelivery_date(resultSet.getDate("DELIVERY_DATE"));
                    ftnInfo.setOrder_time(resultSet.getInt("ORDER_TIME"));
                    ftnInfo.setShipment_date(resultSet.getDate("SHIPMENT_DATE"));
                    ftnInfo.setOrder_source(resultSet.getString("ORDER_SOURCE"));
                    ftnInfo.setOrder_status(resultSet.getString("ORDER_STATUS"));
                    ftnInfo.setEntry_date(resultSet.getDate("ENTRY_DATE"));
//                    ftnInfo.setUser_id(resultSet.getInt("USER_ID"));
                    ftnInfo.setIp_address(resultSet.getString("IP_ADDRESS"));
                    ftnInfo.setOrder_to_cdc_status(resultSet.getString("ORDER_TO_CDC_STATUS"));
                    ftnInfo.setOrder_to_cdc_date(resultSet.getDate("ORDER_TO_CDC_DATE"));
                    ftnInfo.setCdc_packing_status(resultSet.getString("CDC_PACKING_STATUS"));
                    ftnInfo.setCdc_packing_date(resultSet.getDate("CDC_PACKING_DATE"));
                    ftnInfo.setDlv_to_carry_con_status(resultSet.getString("DLV_TO_CARRY_CON_STATUS"));
                    ftnInfo.setDlv_to_carry_con_date(resultSet.getDate("DLV_TO_CARRY_CON_DATE"));
                    ftnInfo.setCust_rcv_status(resultSet.getString("CUST_RCV_STATUS"));
                    ftnInfo.setCust_rcv_ret_date(resultSet.getDate("CUST_RCV_RET_DATE"));
                    ftnInfo.setPayment_status(resultSet.getString("PAYMENT_STATUS"));
                    ftnInfo.setPayment_amount(resultSet.getString("PAYMENT_AMOUNT"));
                    ftnInfo.setPayment_date(resultSet.getDate("PAYMENT_DATE"));
                    ftnInfo.setVat_status(resultSet.getString("VAT_STATUS"));
                    ftnInfo.setVat_date(resultSet.getDate("VAT_DATE"));
                    ftnInfo.setInv_no(resultSet.getString("INV_NO"));
                    ftnInfo.setInv_date(resultSet.getDate("INV_DATE"));
                    ftnInfo.setVat_inv_no(resultSet.getString("VAT_INV_NO"));
                    ftnInfo.setInv_status(resultSet.getString("INV_STATUS"));
                    ftnInfo.setCancel_status(resultSet.getString("CANCEL_STATUS"));

                    //
                    tmpInfoList.add(ftnInfo);

                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return tmpInfoList;

    }

    public String ftnOrderPlaceToCdc(HashMap<Object, Object> paramMap) {

        StringBuilder tempOrderString = new StringBuilder();

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.cdcStatusUpdateQuery(paramMap);

                System.out.println("CdcStatusUpdateQuery:" + query);

                statement = connection.createStatement();
                int rcd = statement.executeUpdate(query);

                if (rcd > 0) {
                    tempOrderString.append("1");
                } else {
                    tempOrderString.append("0");
                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return tempOrderString.toString(); // 3

    }

    public String ftnOrderCancel(HashMap<Object, Object> paramMap) {

        StringBuilder tempOrderString = new StringBuilder();

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.orderCancelQuery(paramMap);

                System.out.println("Order cancele Query:" + query);

                statement = connection.createStatement();
                int rcd = statement.executeUpdate(query);

                if (rcd > 0) {
                    tempOrderString.append("Successfully order cancel.");
                } else {
                    tempOrderString.append("Order does not cancel.");
                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return tempOrderString.toString(); // 3

    }

    public String ftnOrderInvoiceMake(HashMap<Object, Object> paramMap) {

        StringBuilder tempOrderString = new StringBuilder();

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                // Get order pk id

                String invoicePkQuery = FtnOrderStatement.invoicePkQuery(paramMap);

                System.out.println("InvoicePkQuery:" + invoicePkQuery);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(invoicePkQuery);

                String newInvoiceNo = "";

                while (resultSet.next()) {
                    newInvoiceNo = resultSet.getString("INVOICE_NO") == null ? "" : resultSet.getString("INVOICE_NO");
                    break;
                }

                if (newInvoiceNo.isEmpty()) {
                    Format formatter = new SimpleDateFormat("yyyyMMdd");
                    newInvoiceNo = formatter.format(new Date()) + "0001";
                } else {
                    Format formatter = new SimpleDateFormat("yyyyMMdd");
                    newInvoiceNo = formatter.format(new Date()) + newInvoiceNo;
                }

                paramMap.put("INV_NO", (newInvoiceNo == null ? "" : newInvoiceNo));

                // Check vat invoice number

                String query = FtnOrderStatement.invoiceUpdateQuery(paramMap);

                System.out.println("InvoiceUpdateQuery:" + query);

                statement = connection.createStatement();
                int rcd = statement.executeUpdate(query);

                if (rcd > 0) {
                    tempOrderString.append("1");
                } else {
                    tempOrderString.append("0");
                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return tempOrderString.toString(); // 3

    }

    public String ftnCustomerOrderArticleJsonData(Connection connection, HashMap<Object, Object> paramMap) {

        StringBuilder dataString = new StringBuilder();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.customerOrderArtQuery(paramMap);

                System.out.println("CustomerOrderArticleQuery:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                //var j ='[{"name":"test1"},{"id":"2","name":"test2"},{"id":"3","name":"test3"}]';

                dataString.append("[");

                boolean fg = true;

                while (resultSet.next()) {

                    if (fg) {
                        dataString.append("{");
                        ////
                        dataString.append("\"artcode\":\"");
                        dataString.append((resultSet.getString("ART_CODE") == null ? "" : resultSet.getString("ART_CODE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"description\":\"");
                        dataString.append((resultSet.getString("ART_DESCRIPTION") == null ? "" : resultSet.getString("ART_DESCRIPTION")));
                        dataString.append("\",");
                        //
                        dataString.append("\"size\":\"");
                        dataString.append((resultSet.getString("ART_SIZE") == null ? "" : resultSet.getString("ART_SIZE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"qty\":\"");
                        dataString.append((resultSet.getString("ART_PAIR") == null ? "" : resultSet.getString("ART_PAIR")));
                        dataString.append("\",");
                        //
                        dataString.append("\"mrp\":\"");
                        dataString.append((resultSet.getString("UNIT_PRICE") == null ? "" : resultSet.getString("UNIT_PRICE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"discount\":\"");
                        dataString.append((resultSet.getString("TOTAL_DISCOUNT") == null ? "" : resultSet.getString("TOTAL_DISCOUNT")));
                        dataString.append("\",");
                        //
                        dataString.append("\"subtotal\":\"");
                        dataString.append((resultSet.getString("GROSS_VALUE") == null ? "" : resultSet.getString("GROSS_VALUE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"spdiscount\":\"");
                        dataString.append((resultSet.getString("SP_DISCOUNT") == null ? "" : resultSet.getString("SP_DISCOUNT")));
                        dataString.append("\",");
                        //
                        dataString.append("\"subtotalaftersp\":\"");
                        dataString.append((resultSet.getString("GROSS_VALUE_AFTER_SP") == null ? "" : resultSet.getString("GROSS_VALUE_AFTER_SP")));
                        dataString.append("\"");
                        ////
                        dataString.append("}");
                        fg = false;
                    } else {
                        dataString.append(",{");
                        ////
                        dataString.append("\"artcode\":\"");
                        dataString.append((resultSet.getString("ART_CODE") == null ? "" : resultSet.getString("ART_CODE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"description\":\"");
                        dataString.append((resultSet.getString("ART_DESCRIPTION") == null ? "" : resultSet.getString("ART_DESCRIPTION")));
                        dataString.append("\",");
                        //
                        dataString.append("\"size\":\"");
                        dataString.append((resultSet.getString("ART_SIZE") == null ? "" : resultSet.getString("ART_SIZE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"qty\":\"");
                        dataString.append((resultSet.getString("ART_PAIR") == null ? "" : resultSet.getString("ART_PAIR")));
                        dataString.append("\",");
                        //
                        dataString.append("\"mrp\":\"");
                        dataString.append((resultSet.getString("UNIT_PRICE") == null ? "" : resultSet.getString("UNIT_PRICE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"discount\":\"");
                        dataString.append((resultSet.getString("TOTAL_DISCOUNT") == null ? "" : resultSet.getString("TOTAL_DISCOUNT")));
                        dataString.append("\",");
                        //
                        dataString.append("\"subtotal\":\"");
                        dataString.append((resultSet.getString("GROSS_VALUE") == null ? "" : resultSet.getString("GROSS_VALUE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"spdiscount\":\"");
                        dataString.append((resultSet.getString("SP_DISCOUNT") == null ? "" : resultSet.getString("SP_DISCOUNT")));
                        dataString.append("\",");
                        //
                        dataString.append("\"subtotalaftersp\":\"");
                        dataString.append((resultSet.getString("GROSS_VALUE_AFTER_SP") == null ? "" : resultSet.getString("GROSS_VALUE_AFTER_SP")));
                        dataString.append("\"");
                        ////
                        dataString.append("}");
                    }

                }

                dataString.append("]");

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return dataString.toString().equalsIgnoreCase("[]") ? "\"\"" : dataString.toString();

    }

    public String ftnOrderDetailsJsonData(HashMap<Object, Object> paramMap) {

        StringBuilder dataString = new StringBuilder();

        StringBuilder orderArticleString = new StringBuilder();

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.customerDetailsQuery(paramMap);

                System.out.println("CustomerDetailsQuery:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                //var j ='[{"name":"test1"},{"id":"2","name":"test2"},{"id":"3","name":"test3"}]';

                dataString.append("[");

                boolean fg = true;

                while (resultSet.next()) {

                    if (fg) {
                        dataString.append("{");
                        ////
                        dataString.append("\"orderdate\":\"");
                        dataString.append((resultSet.getString("ORDER_DATE") == null ? "" : resultSet.getString("ORDER_DATE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"orderno\":\"");
                        dataString.append((resultSet.getString("ORDER_NO") == null ? "" : resultSet.getString("ORDER_NO")));
                        dataString.append("\",");
                        //
                        dataString.append("\"custid\":\"");
                        dataString.append((resultSet.getString("CUSTOMER_ID") == null ? "" : resultSet.getString("CUSTOMER_ID")));
                        dataString.append("\",");
                        //
                        dataString.append("\"contact\":\"");
                        dataString.append((resultSet.getString("CONTACT_NO") == null ? "" : resultSet.getString("CONTACT_NO")));
                        dataString.append("\",");
                        //
                        dataString.append("\"deliverydate\":\"");
                        dataString.append((resultSet.getString("DELIVERY_DATE") == null ? "" : resultSet.getString("DELIVERY_DATE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"ordertime\":\"");
                        dataString.append((resultSet.getString("ORDER_TIME") == null ? "" : resultSet.getString("ORDER_TIME")));
                        dataString.append("\",");
                        //
                        dataString.append("\"shipmentdate\":\"");
                        dataString.append((resultSet.getString("SHIPMENT_DATE") == null ? "" : resultSet.getString("SHIPMENT_DATE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"vatinvno\":\"");
                        dataString.append((resultSet.getString("VAT_INV_NO") == null ? "" : resultSet.getString("VAT_INV_NO")));
                        dataString.append("\",");
                        //
                        dataString.append("\"vatdate\":\"");
                        dataString.append((resultSet.getString("VAT_DATE") == null ? "" : resultSet.getString("VAT_DATE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"invno\":\"");
                        dataString.append((resultSet.getString("INV_NO") == null ? "" : resultSet.getString("INV_NO")));
                        dataString.append("\",");
                        //
                        dataString.append("\"invdate\":\"");
                        dataString.append((resultSet.getString("INV_DATE") == null ? "" : resultSet.getString("INV_DATE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"custname\":\"");
                        dataString.append((resultSet.getString("CUSTOMER_NAME") == null ? "" : resultSet.getString("CUSTOMER_NAME")));
                        dataString.append("\",");
                        //
                        dataString.append("\"emailadd\":\"");
                        dataString.append((resultSet.getString("EMAIL_ADD") == null ? "" : resultSet.getString("EMAIL_ADD")));
                        dataString.append("\",");
                        //
                        dataString.append("\"awb\":\"");
                        dataString.append((resultSet.getString("AWB") == null ? "" : resultSet.getString("AWB")));
                        dataString.append("\",");
                        //
                        dataString.append("\"shipping\":\"");
                        dataString.append((resultSet.getString("SHIPPING_ADDRESS") == null ? "" : resultSet.getString("SHIPPING_ADDRESS")));
                        dataString.append("\"");
                        ////
                        dataString.append("}");
                        fg = false;
                    } else {
                        dataString.append(",{");
                        ////
                        dataString.append("\"orderdate\":\"");
                        dataString.append((resultSet.getString("ORDER_DATE") == null ? "" : resultSet.getString("ORDER_DATE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"orderno\":\"");
                        dataString.append((resultSet.getString("ORDER_NO") == null ? "" : resultSet.getString("ORDER_NO")));
                        dataString.append("\",");
                        //
                        dataString.append("\"custid\":\"");
                        dataString.append((resultSet.getString("CUSTOMER_ID") == null ? "" : resultSet.getString("CUSTOMER_ID")));
                        dataString.append("\",");
                        //
                        dataString.append("\"contact\":\"");
                        dataString.append((resultSet.getString("CONTACT_NO") == null ? "" : resultSet.getString("CONTACT_NO")));
                        dataString.append("\",");
                        //
                        dataString.append("\"deliverydate\":\"");
                        dataString.append((resultSet.getString("DELIVERY_DATE") == null ? "" : resultSet.getString("DELIVERY_DATE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"ordertime\":\"");
                        dataString.append((resultSet.getString("ORDER_TIME") == null ? "" : resultSet.getString("ORDER_TIME")));
                        dataString.append("\",");
                        //
                        dataString.append("\"shipmentdate\":\"");
                        dataString.append((resultSet.getString("SHIPMENT_DATE") == null ? "" : resultSet.getString("SHIPMENT_DATE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"vatinvno\":\"");
                        dataString.append((resultSet.getString("VAT_INV_NO") == null ? "" : resultSet.getString("VAT_INV_NO")));
                        dataString.append("\",");
                        //
                        dataString.append("\"vatdate\":\"");
                        dataString.append((resultSet.getString("VAT_DATE") == null ? "" : resultSet.getString("VAT_DATE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"invno\":\"");
                        dataString.append((resultSet.getString("INV_NO") == null ? "" : resultSet.getString("INV_NO")));
                        dataString.append("\",");
                        //
                        dataString.append("\"invdate\":\"");
                        dataString.append((resultSet.getString("INV_DATE") == null ? "" : resultSet.getString("INV_DATE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"custname\":\"");
                        dataString.append((resultSet.getString("CUSTOMER_NAME") == null ? "" : resultSet.getString("CUSTOMER_NAME")));
                        dataString.append("\",");
                        //
                        dataString.append("\"emailadd\":\"");
                        dataString.append((resultSet.getString("EMAIL_ADD") == null ? "" : resultSet.getString("EMAIL_ADD")));
                        dataString.append("\",");
                        //
                        dataString.append("\"awb\":\"");
                        dataString.append((resultSet.getString("AWB") == null ? "" : resultSet.getString("AWB")));
                        dataString.append("\",");
                        //
                        dataString.append("\"shipping\":\"");
                        dataString.append((resultSet.getString("SHIPPING_ADDRESS") == null ? "" : resultSet.getString("SHIPPING_ADDRESS")));
                        dataString.append("\"");
                        ////
                        dataString.append("}");
                    }

                }

                dataString.append("]");

                orderArticleString.append(ftnCustomerOrderArticleJsonData(connection, paramMap));

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return (dataString.toString().equalsIgnoreCase("[]") ? "\"\"" : dataString.toString())
                + "<SEP>"
                + (orderArticleString.toString().equalsIgnoreCase("[]") ? "\"\"" : orderArticleString.toString());

    }

    public List<FtnPromotion> ftnPromotionList() {

        List<FtnPromotion> tmpInfoList = null;

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.ftnPromotionQuery();

                System.out.println("Query:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                tmpInfoList = new ArrayList<FtnPromotion>();
                //
                FtnPromotion ftnInfo = null;

                while (resultSet.next()) {

                    ftnInfo = new FtnPromotion();
                    //
                    ftnInfo.setPromoId(resultSet.getString("PROMO_ID"));
                    ftnInfo.setPromoName(resultSet.getString("PROMO_NAME"));
                    ftnInfo.setPromoStatus(resultSet.getString("PROMO_STATUS"));
                    ftnInfo.setStartDate(resultSet.getDate("START_DATE"));
                    ftnInfo.setEndDate(resultSet.getDate("END_DATE"));
                    ftnInfo.setStartTime(resultSet.getInt("START_TIME"));
                    ftnInfo.setEndTime(resultSet.getInt("END_TIME"));
                    //
                    tmpInfoList.add(ftnInfo);

                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return tmpInfoList;

    }

    public String promCircularFileProcess(List<FtnPromotion> dataList, String userName) {

        StringBuilder dataString = new StringBuilder();

//        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        Connection connection = DatabaseConnection.connectDB();

        try {
            connection.setAutoCommit(false);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                int record = 0;
                int duplicateRecord = 0;

                int xmlRecord = dataList.size();

                FtnPromotion xmlInfo = null;

                HashMap<Object, Object> paramMap = null;

                String query = "";

                boolean flag = false;

                boolean duplicateCheckFlag = true;

                for (int i = 0; i < xmlRecord; i++) {

                    xmlInfo = dataList.get(i);

                    System.out.print(xmlInfo.getPromoId() + " - ");
                    System.out.print(xmlInfo.getPromoName() + " - ");
                    System.out.print(xmlInfo.getCircularDate() + " - ");
                    System.out.print(xmlInfo.getEffectStartDate() + " - ");
                    System.out.print(xmlInfo.getEffectEndDate() + " - ");
                    System.out.print(xmlInfo.getStartTime() + " - ");
                    System.out.print(xmlInfo.getEndTime() + " - ");
                    System.out.print(xmlInfo.getArtCode() + " - ");
                    System.out.print(xmlInfo.getMrp() + " - ");
                    System.out.print(xmlInfo.getNetSalePriceAfterDis() + " - ");
                    System.out.print(xmlInfo.getDiscountAmount());
                    System.out.println("");

                    paramMap = new HashMap<Object, Object>();
                    paramMap.put("PROMO_ID", xmlInfo.getPromoId());
                    paramMap.put("PROMO_NAME", xmlInfo.getPromoName());
                    paramMap.put("CIRCULAR_DATE", xmlInfo.getCircularDate());
                    paramMap.put("START_DATE", xmlInfo.getEffectStartDate());
                    paramMap.put("END_DATE", xmlInfo.getEffectEndDate());
                    paramMap.put("START_TIME", xmlInfo.getStartTime());
                    paramMap.put("END_TIME", xmlInfo.getEndTime());
                    paramMap.put("ART_CODE", xmlInfo.getArtCode());
                    paramMap.put("MRP", xmlInfo.getMrp());
                    paramMap.put("NET_SALE_PRICE_AFTER_DIS", xmlInfo.getNetSalePriceAfterDis());
                    paramMap.put("DISCOUNT_AMOUNT", xmlInfo.getDiscountAmount());
                    paramMap.put("INSERT_BY", userName);

                    if (duplicateCheckFlag) {

                        // Check duplicate circular

                        query = FtnOrderStatement.circularDuplicateCheckQuery(paramMap);

                        System.out.println("CircularDuplicateCheckQuery:" + query);

                        statement = connection.createStatement();
                        resultSet = statement.executeQuery(query);

                        while (resultSet.next()) {
                            duplicateRecord = resultSet.getInt("TOTAL_CIRCULAR");
                        }

                    }

                    duplicateCheckFlag = false;


                    if (duplicateRecord > 0) {
                        flag = false;
                        dataString.append("Circular file already processed.");
                        break;
                    } else {

                        // insert here

                        query = FtnOrderStatement.circularInsertQuery(paramMap);

                        System.out.println("CircularInsertQuery:" + query);

                        statement = connection.createStatement();
                        record = statement.executeUpdate(query);

                        if (record == 0) {
                            flag = false;
                            dataString.append("Circular does not processed. Please contact administrator.");
                            break;
                        } else {
                            flag = true;
                        }

                    }

                }

                if (flag) {

                    try {
                        connection.commit();
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }

                    dataString.append("Successfully circular file processed.");

                } else {
                    try {
                        connection.rollback();
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return dataString.toString();


    }

    public String ftnCircularArticleJsonData(HashMap<Object, Object> paramMap) {

        StringBuilder dataString = new StringBuilder();

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.circularArticleQuery(paramMap);

                System.out.println("CircularArticleQuery:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                dataString.append("[");

                boolean fg = true;

                while (resultSet.next()) {

                    if (fg) {
                        dataString.append("{");
                        ////
                        dataString.append("\"circular\":\"");
                        dataString.append((resultSet.getString("PROMO_ID") == null ? "" : resultSet.getString("PROMO_ID")));
                        dataString.append("\",");
                        //
                        dataString.append("\"article\":\"");
                        dataString.append((resultSet.getString("ART_CODE") == null ? "" : resultSet.getString("ART_CODE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"oldprice\":\"");
                        dataString.append((resultSet.getString("MRP") == null ? "" : resultSet.getString("MRP")));
                        dataString.append("\",");
                        //
                        dataString.append("\"newprice\":\"");
                        dataString.append((resultSet.getString("NET_SALE_PRICE_AFTER_DIS") == null ? "" : resultSet.getString("NET_SALE_PRICE_AFTER_DIS")));
                        dataString.append("\",");
                        //
                        dataString.append("\"discount\":\"");
                        dataString.append((resultSet.getString("DISCOUNT_AMOUNT") == null ? "" : resultSet.getString("DISCOUNT_AMOUNT")));
                        dataString.append("\"");
                        ////
                        dataString.append("}");
                        fg = false;
                    } else {
                        dataString.append(",{");
                        ////
                        dataString.append("\"circular\":\"");
                        dataString.append((resultSet.getString("PROMO_ID") == null ? "" : resultSet.getString("PROMO_ID")));
                        dataString.append("\",");
                        //
                        dataString.append("\"article\":\"");
                        dataString.append((resultSet.getString("ART_CODE") == null ? "" : resultSet.getString("ART_CODE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"oldprice\":\"");
                        dataString.append((resultSet.getString("MRP") == null ? "" : resultSet.getString("MRP")));
                        dataString.append("\",");
                        //
                        dataString.append("\"newprice\":\"");
                        dataString.append((resultSet.getString("NET_SALE_PRICE_AFTER_DIS") == null ? "" : resultSet.getString("NET_SALE_PRICE_AFTER_DIS")));
                        dataString.append("\",");
                        //
                        dataString.append("\"discount\":\"");
                        dataString.append((resultSet.getString("DISCOUNT_AMOUNT") == null ? "" : resultSet.getString("DISCOUNT_AMOUNT")));
                        dataString.append("\"");
                        ////
                        dataString.append("}");
                    }

                }

                dataString.append("]");



            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return (dataString.toString().equalsIgnoreCase("[]") ? "\"\"" : dataString.toString());


    }

    public String ftnCustomerInvoiceHistoryJsonData(HashMap<Object, Object> paramMap) {

        StringBuilder dataString = new StringBuilder();

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.customerInvoiceHistoryQuery(paramMap);

                System.out.println("Customer Invoice History Query:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                dataString.append("[");

                boolean fg = true;

                while (resultSet.next()) {

                    if (fg) {
                        dataString.append("{");
                        ////
                        dataString.append("\"orderno\":\"");
                        dataString.append((resultSet.getString("ORDER_NO") == null ? "" : resultSet.getString("ORDER_NO")));
                        dataString.append("\",");
                        //
                        dataString.append("\"orderdate\":\"");
                        dataString.append((resultSet.getString("ORDER_DATE") == null ? "" : resultSet.getString("ORDER_DATE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"unitprice\":\"");
                        dataString.append((resultSet.getString("UNIT_PRICE") == null ? "" : resultSet.getString("UNIT_PRICE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"artcode\":\"");
                        dataString.append((resultSet.getString("ART_CODE") == null ? "" : resultSet.getString("ART_CODE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"discount\":\"");
                        dataString.append((resultSet.getString("TOTAL_DISCOUNT") == null ? "" : resultSet.getString("TOTAL_DISCOUNT")));
                        dataString.append("\"");
                        ////
                        dataString.append("}");
                        fg = false;
                    } else {
                        dataString.append(",{");
                        ////
                        dataString.append("\"orderno\":\"");
                        dataString.append((resultSet.getString("ORDER_NO") == null ? "" : resultSet.getString("ORDER_NO")));
                        dataString.append("\",");
                        //
                        dataString.append("\"orderdate\":\"");
                        dataString.append((resultSet.getString("ORDER_DATE") == null ? "" : resultSet.getString("ORDER_DATE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"unitprice\":\"");
                        dataString.append((resultSet.getString("UNIT_PRICE") == null ? "" : resultSet.getString("UNIT_PRICE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"artcode\":\"");
                        dataString.append((resultSet.getString("ART_CODE") == null ? "" : resultSet.getString("ART_CODE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"discount\":\"");
                        dataString.append((resultSet.getString("TOTAL_DISCOUNT") == null ? "" : resultSet.getString("TOTAL_DISCOUNT")));
                        dataString.append("\"");
                        ////
                        dataString.append("}");
                    }

                }

                dataString.append("]");



            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return (dataString.toString().equalsIgnoreCase("[]") ? "\"\"" : dataString.toString());


    }

    public String ftnNewArrivalArticleJsonData() {

        StringBuilder dataString = new StringBuilder();

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.newArrivalArticleQuery();

                System.out.println("New Arrival Article Query:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                dataString.append("[");

                boolean fg = true;

                while (resultSet.next()) {

                    if (fg) {
                        dataString.append("{");
                        ////
                        dataString.append("\"catname\":\"");
                        dataString.append((resultSet.getString("CAT_NAME") == null ? "" : resultSet.getString("CAT_NAME")));
                        dataString.append("\",");
                        //
                        dataString.append("\"artcode\":\"");
                        dataString.append((resultSet.getString("ART_CODE") == null ? "" : resultSet.getString("ART_CODE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"mrp\":\"");
                        dataString.append((resultSet.getString("MRP") == null ? "" : resultSet.getString("MRP")));
                        dataString.append("\"");
                        ////
                        dataString.append("}");
                        fg = false;
                    } else {
                        dataString.append(",{");
                        ////
                        dataString.append("\"catname\":\"");
                        dataString.append((resultSet.getString("CAT_NAME") == null ? "" : resultSet.getString("CAT_NAME")));
                        dataString.append("\",");
                        //
                        dataString.append("\"artcode\":\"");
                        dataString.append((resultSet.getString("ART_CODE") == null ? "" : resultSet.getString("ART_CODE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"mrp\":\"");
                        dataString.append((resultSet.getString("MRP") == null ? "" : resultSet.getString("MRP")));
                        dataString.append("\"");
                        ////
                        dataString.append("}");
                    }

                }

                dataString.append("]");



            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return (dataString.toString().equalsIgnoreCase("[]") ? "\"\"" : dataString.toString());


    }

    public String ftnRemittanceYearJsonData() {

        StringBuilder dataString = new StringBuilder();

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.remittanceYearQuery();

                System.out.println("Remittance Year Query:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                dataString.append("[");

                boolean fg = true;

                while (resultSet.next()) {

                    if (fg) {
                        dataString.append("{");
                        ////
                        dataString.append("\"year\":\"");
                        dataString.append((resultSet.getString("F_YEAR") == null ? "" : resultSet.getString("F_YEAR")));
                        dataString.append("\"");
                        ////
                        dataString.append("}");
                        fg = false;
                    } else {
                        dataString.append(",{");
                        ////
                        dataString.append("\"year\":\"");
                        dataString.append((resultSet.getString("F_YEAR") == null ? "" : resultSet.getString("F_YEAR")));
                        dataString.append("\"");
                        ////
                        dataString.append("}");
                    }

                }

                dataString.append("]");



            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return (dataString.toString().equalsIgnoreCase("[]") ? "\"\"" : dataString.toString());

    }

    public String ftnRemittanceWeekJsonData(HashMap<Object, Object> paramMap) {

        StringBuilder dataString = new StringBuilder();

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.remittanceWeekQuery(paramMap);

                System.out.println("Remittance Week Query:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                dataString.append("[");

                boolean fg = true;

                while (resultSet.next()) {

                    if (fg) {
                        dataString.append("{");
                        ////
                        dataString.append("\"yrweek\":\"");
                        dataString.append((resultSet.getString("WEEK_NO") == null ? "" : resultSet.getString("WEEK_NO")));
                        dataString.append("\"");
                        ////
                        dataString.append("}");
                        fg = false;
                    } else {
                        dataString.append(",{");
                        ////
                        dataString.append("\"yrweek\":\"");
                        dataString.append((resultSet.getString("WEEK_NO") == null ? "" : resultSet.getString("WEEK_NO")));
                        dataString.append("\"");
                        ////
                        dataString.append("}");
                    }

                }

                dataString.append("]");



            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return (dataString.toString().equalsIgnoreCase("[]") ? "\"\"" : dataString.toString());

    }

    public String ftnRemittanceDateJsonData(HashMap<Object, Object> paramMap) {

        StringBuilder dataString = new StringBuilder();

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.remittanceDateQuery(paramMap);

                System.out.println("Remittance Date Query:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                dataString.append("[");

                boolean fg = true;

                while (resultSet.next()) {

                    if (fg) {
                        dataString.append("{");
                        ////
                        dataString.append("\"wkdate\":\"");
                        dataString.append((resultSet.getString("WEEK_DATE") == null ? "" : resultSet.getString("WEEK_DATE")));
                        dataString.append("\"");
                        ////
                        dataString.append("}");
                        fg = false;
                    } else {
                        dataString.append(",{");
                        ////
                        dataString.append("\"wkdate\":\"");
                        dataString.append((resultSet.getString("WEEK_DATE") == null ? "" : resultSet.getString("WEEK_DATE")));
                        dataString.append("\"");
                        ////
                        dataString.append("}");
                    }

                }

                dataString.append("]");



            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return (dataString.toString().equalsIgnoreCase("[]") ? "\"\"" : dataString.toString());

    }

    public String remittanceFileProcess(List<FtnRemittance> dataList, String yr, String wk, String dt, String userName) {

        StringBuilder dataString = new StringBuilder();

//        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        Connection connection = DatabaseConnection.connectDB();

        try {
            connection.setAutoCommit(false);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                int record = 0;
                int duplicateRecord = 0;

                int xmlRecord = dataList.size();

                FtnRemittance xmlInfo = null;

                HashMap<Object, Object> paramMap = null;

                String query = "";

                boolean flag = false;

//                boolean duplicateCheckFlag = true;

                for (int i = 0; i < xmlRecord; i++) {

                    xmlInfo = dataList.get(i);

                    paramMap = new HashMap<Object, Object>();
                    paramMap.put("AWB", xmlInfo.getAwb());
                    paramMap.put("AMOUNT", xmlInfo.getAmount());
                    paramMap.put("MRNO", xmlInfo.getMrno());
                    paramMap.put("MRDATE", xmlInfo.getMrdate());
                    paramMap.put("COLLECTEDAMOUNT", xmlInfo.getCollectedAmount());
                    paramMap.put("PICKUPDATE", xmlInfo.getPickupDate());
                    paramMap.put("SHIPPERNAME", xmlInfo.getShipperName());
                    paramMap.put("REMARKS", xmlInfo.getRemarks());
                    paramMap.put("PROCESS_YEAR", yr);
                    paramMap.put("PROCESS_WEEK", wk);
                    paramMap.put("PROCESS_DATE", dt);
                    paramMap.put("PROCESS_BY", userName);

                    // Check duplicate awb

                    query = FtnOrderStatement.remittanceUpdateQuery(paramMap);

                    System.out.println("RemittanceUpdateQuery:" + query);

                    statement = connection.createStatement();
                    duplicateRecord = statement.executeUpdate(query);

                    if (duplicateRecord == 0) {

                        // insert here

                        query = FtnOrderStatement.remittanceInsertQuery(paramMap);

                        System.out.println("RemittanceInsertQuery:" + query);

                        statement = connection.createStatement();
                        record = statement.executeUpdate(query);

                        if (record == 0) {
                            flag = false;
                            dataString.append("Remittance does not processed. Please contact administrator.");
                            break;
                        } else {
                            flag = true;
                        }

                    } else {
                        flag = true;
                    }

                }

                if (flag) {

                    try {
                        connection.commit();
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }

                    dataString.append("Successfully remittance file processed.");

                } else {
                    try {
                        connection.rollback();
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return dataString.toString();


    }

    public List<FtnRemittance> ftnRemittanceList(HashMap<Object, Object> paramMap) {

        List<FtnRemittance> tmpInfoList = null;

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.remittanceSelectQuery(paramMap);

                System.out.println("Query:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                tmpInfoList = new ArrayList<FtnRemittance>();
                //
                FtnRemittance ftnInfo = null;

                while (resultSet.next()) {

                    ftnInfo = new FtnRemittance();
                    //
                    ftnInfo.setProcessYear(resultSet.getString("PROCESS_YEAR"));
                    ftnInfo.setProcessWeek(resultSet.getString("PROCESS_WEEK"));
                    ftnInfo.setProcessDate(resultSet.getString("PROCESS_DATE"));
                    ftnInfo.setAwb(resultSet.getString("AWB"));
                    ftnInfo.setAmount(resultSet.getString("AMOUNT"));
                    ftnInfo.setMrno(resultSet.getString("MRNO"));
                    ftnInfo.setMrdate(resultSet.getString("MRDATE"));
                    ftnInfo.setCollectedAmount(resultSet.getString("COLLECTEDAMOUNT"));
                    ftnInfo.setPickupDate(resultSet.getString("PICKUPDATE"));
                    ftnInfo.setShipperName(resultSet.getString("SHIPPERNAME"));
                    ftnInfo.setRemarks(resultSet.getString("REMARKS"));
                    ftnInfo.setProcessBy(resultSet.getString("PROCESS_BY"));
                    ftnInfo.setInsertDate(resultSet.getString("INSERT_DATE"));
                    //
                    tmpInfoList.add(ftnInfo);

                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return tmpInfoList;

    }

    public List<FtnRemittance> ftnRemittanceList() {

        List<FtnRemittance> tmpInfoList = null;

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.remittanceSelectQuery();

                System.out.println("Query:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                tmpInfoList = new ArrayList<FtnRemittance>();
                //
                FtnRemittance ftnInfo = null;

                while (resultSet.next()) {

                    ftnInfo = new FtnRemittance();
                    //
                    ftnInfo.setProcessYear(resultSet.getString("PROCESS_YEAR"));
                    ftnInfo.setProcessWeek(resultSet.getString("PROCESS_WEEK"));
                    ftnInfo.setProcessDate(resultSet.getString("PROCESS_DATE"));
                    ftnInfo.setAwb(resultSet.getString("AWB"));
                    ftnInfo.setAmount(resultSet.getString("AMOUNT"));
                    ftnInfo.setMrno(resultSet.getString("MRNO"));
                    ftnInfo.setMrdate(resultSet.getString("MRDATE"));
                    ftnInfo.setCollectedAmount(resultSet.getString("COLLECTEDAMOUNT"));
                    ftnInfo.setPickupDate(resultSet.getString("PICKUPDATE"));
                    ftnInfo.setShipperName(resultSet.getString("SHIPPERNAME"));
                    ftnInfo.setRemarks(resultSet.getString("REMARKS"));
                    ftnInfo.setProcessBy(resultSet.getString("PROCESS_BY"));
                    ftnInfo.setInsertDate(resultSet.getString("INSERT_DATE"));
                    //
                    tmpInfoList.add(ftnInfo);

                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return tmpInfoList;

    }

    public String ftnRemittanceJsonData(HashMap<Object, Object> paramMap) {

        StringBuilder dataString = new StringBuilder();

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = FtnOrderStatement.remittanceSelectQuery(paramMap);

                System.out.println("Query:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                dataString.append("[");

                boolean fg = true;

                while (resultSet.next()) {

                    if (fg) {
                        dataString.append("{");
                        ////
                        dataString.append("\"prodate\":\"");
                        dataString.append((resultSet.getString("PROCESS_DATE") == null ? "" : resultSet.getString("PROCESS_DATE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"awb\":\"");
                        dataString.append((resultSet.getString("AWB") == null ? "" : resultSet.getString("AWB")));
                        dataString.append("\",");
                        //
                        dataString.append("\"amount\":\"");
                        dataString.append((resultSet.getString("AMOUNT") == null ? "" : resultSet.getString("AMOUNT")));
                        dataString.append("\",");
                        //
                        dataString.append("\"mrno\":\"");
                        dataString.append((resultSet.getString("MRNO") == null ? "" : resultSet.getString("MRNO")));
                        dataString.append("\",");
                        //
                        dataString.append("\"mrdate\":\"");
                        dataString.append((resultSet.getString("MRDATE") == null ? "" : resultSet.getString("MRDATE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"collectedamount\":\"");
                        dataString.append((resultSet.getString("COLLECTEDAMOUNT") == null ? "" : resultSet.getString("COLLECTEDAMOUNT")));
                        dataString.append("\",");
                        //
                        dataString.append("\"pickupdate\":\"");
                        dataString.append((resultSet.getString("PICKUPDATE") == null ? "" : resultSet.getString("PICKUPDATE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"shippername\":\"");
                        dataString.append((resultSet.getString("SHIPPERNAME") == null ? "" : resultSet.getString("SHIPPERNAME")));
                        dataString.append("\",");
                        //
                        dataString.append("\"remarks\":\"");
                        dataString.append((resultSet.getString("REMARKS") == null ? "" : resultSet.getString("REMARKS")));
                        dataString.append("\",");
                        //
                        dataString.append("\"processby\":\"");
                        dataString.append((resultSet.getString("PROCESS_BY") == null ? "" : resultSet.getString("PROCESS_BY")));
                        dataString.append("\",");
                        //
                        dataString.append("\"insertdate\":\"");
                        dataString.append((resultSet.getString("INSERT_DATE") == null ? "" : resultSet.getString("INSERT_DATE")));
                        dataString.append("\"");
                        ////
                        dataString.append("}");
                        fg = false;
                    } else {
                        dataString.append(",{");
                        ////
                        dataString.append("\"prodate\":\"");
                        dataString.append((resultSet.getString("PROCESS_DATE") == null ? "" : resultSet.getString("PROCESS_DATE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"awb\":\"");
                        dataString.append((resultSet.getString("AWB") == null ? "" : resultSet.getString("AWB")));
                        dataString.append("\",");
                        //
                        dataString.append("\"amount\":\"");
                        dataString.append((resultSet.getString("AMOUNT") == null ? "" : resultSet.getString("AMOUNT")));
                        dataString.append("\",");
                        //
                        dataString.append("\"mrno\":\"");
                        dataString.append((resultSet.getString("MRNO") == null ? "" : resultSet.getString("MRNO")));
                        dataString.append("\",");
                        //
                        dataString.append("\"mrdate\":\"");
                        dataString.append((resultSet.getString("MRDATE") == null ? "" : resultSet.getString("MRDATE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"collectedamount\":\"");
                        dataString.append((resultSet.getString("COLLECTEDAMOUNT") == null ? "" : resultSet.getString("COLLECTEDAMOUNT")));
                        dataString.append("\",");
                        //
                        dataString.append("\"pickupdate\":\"");
                        dataString.append((resultSet.getString("PICKUPDATE") == null ? "" : resultSet.getString("PICKUPDATE")));
                        dataString.append("\",");
                        //
                        dataString.append("\"shippername\":\"");
                        dataString.append((resultSet.getString("SHIPPERNAME") == null ? "" : resultSet.getString("SHIPPERNAME")));
                        dataString.append("\",");
                        //
                        dataString.append("\"remarks\":\"");
                        dataString.append((resultSet.getString("REMARKS") == null ? "" : resultSet.getString("REMARKS")));
                        dataString.append("\",");
                        //
                        dataString.append("\"processby\":\"");
                        dataString.append((resultSet.getString("PROCESS_BY") == null ? "" : resultSet.getString("PROCESS_BY")));
                        dataString.append("\",");
                        //
                        dataString.append("\"insertdate\":\"");
                        dataString.append((resultSet.getString("INSERT_DATE") == null ? "" : resultSet.getString("INSERT_DATE")));
                        dataString.append("\"");
                        ////
                        dataString.append("}");
                    }

                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return (dataString.toString().equalsIgnoreCase("[]") ? "\"\"" : dataString.toString());

    }
}
