/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.drd.model;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import mis.drd.persistance.*;
import mis.drd.statement.FtnOrderStatement;
import mis.drd.statement.LoginStatement;
import mis.drd.utility.DatabaseConnection;
import mis.drd.utility.EmailSender;
import mis.drd.utility.Encryption;

/**
 *
 * @author MIS
 */
public class LoginModel {

    public List<SvUser> ftnUserList(HashMap<Object, Object> param) {

        List<SvUser> tmpUserInfoList = null;

        Connection connection = DatabaseConnection.connectDB();
        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = LoginStatement.loginQuery(param);

                System.out.println("LoginQuery:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                tmpUserInfoList = new ArrayList<SvUser>();
                //
                SvUser svUserInfo = null;
                SvCompany svCompanyInfo = null;
                SvUserGroup svUserGroupInfo = null;

                while (resultSet.next()) {

                    svUserInfo = new SvUser();
                    //
                    svCompanyInfo = new SvCompany();
                    svCompanyInfo.setComCode(resultSet.getString("COMPANY_CODE"));
                    svCompanyInfo.setComName(resultSet.getString("COMPANY_NAME"));
                    //
                    svUserGroupInfo = new SvUserGroup();
                    svUserGroupInfo.setGroupId(resultSet.getInt("GROUP_ID"));
                    //
                    svUserInfo.setUserPassword(resultSet.getString("USER_PASSWORD"));
                    svUserInfo.setDisplayName(resultSet.getString("DISPLAY_NAME"));
                    svUserInfo.setUserId(resultSet.getInt("USER_ID"));
                    //
                    svUserInfo.setSvCompanyInfo(svCompanyInfo);
                    svUserInfo.setSvUserGroupInfo(svUserGroupInfo);
                    //
                    tmpUserInfoList.add(svUserInfo);

                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return tmpUserInfoList;

    }

    public List<FtnUserLevelMenu> ftnUserLevelMenuList(HashMap<Object, Object> param) {

        List<FtnUserLevelMenu> tmpInfoList = null;

        Connection connection = DatabaseConnection.connectDB();
        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = LoginStatement.groupWiseMenuQuery(param);

                System.out.println("GroupWiseMenuQuery:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                tmpInfoList = new ArrayList<FtnUserLevelMenu>();
                //
                FtnUserLevelMenu info = null;
                FtnMenu rtlMenuInfo = null;

                while (resultSet.next()) {

                    info = new FtnUserLevelMenu();
                    //
                    rtlMenuInfo = new FtnMenu();
                    rtlMenuInfo.setMenuId(resultSet.getString("MENU_ID"));
                    //
                    info.setLevelMenuStatus(resultSet.getString("GROUP_MENU_STATUS").charAt(0));
                    info.setRtlMenuInfo(rtlMenuInfo);
                    //
                    tmpInfoList.add(info);

                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return tmpInfoList;

    }

    public String ftnDuplicateUserCheck(HashMap<Object, Object> paramMap) {

        StringBuilder tempOrderString = new StringBuilder();

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;
        ResultSet resultSet = null;

        if (connection != null) {

            try {

                String query = LoginStatement.checkDuplicateUserQuery(paramMap);

                System.out.println("Check Duplicate User Query:" + query);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);

                tempOrderString.setLength(0);

                while (resultSet.next()) {
                    tempOrderString.append(resultSet.getString("TOTAL_USER"));
                }

            } catch (SQLException ex) {
                ex.printStackTrace();
                tempOrderString.setLength(0);
                tempOrderString.append("-1");
            } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                    tempOrderString.setLength(0);
                    tempOrderString.append("-1");
                }
            }
        }

        return tempOrderString.toString(); // 3

    }

    public String getRandomAlphaNumericNumber(int numOfCharacters) {

        StringBuilder buffer = new StringBuilder();

        Random random = new Random();

        char[] chars = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
            'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
            'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F',
            'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
            'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4',
            '5', '6', '7', '8', '9', '0'};

        for (int i = 0; i < numOfCharacters; i++) {
            buffer.append(chars[random.nextInt(chars.length)]);
        }

        return buffer.toString();

    }

    private String parentUrl() {

        String url = "";

        try {

            Properties properties = new Properties();
            InputStream inputStream = LoginModel.class.getClassLoader().getResourceAsStream("settingString.properties");
            properties.load(inputStream);

            url = properties.getProperty("URL");

        } catch (IOException ex) {
            url = "";
            ex.printStackTrace();
        }

        return url;
    }

    private String[] smtpServerInfo() {

        String[] server = new String[5];

        try {

            Properties properties = new Properties();
            InputStream inputStream = LoginModel.class.getClassLoader().getResourceAsStream("settingString.properties");
            properties.load(inputStream);

            server[0] = properties.getProperty("HOST");
            server[1] = properties.getProperty("PORT");
            server[2] = properties.getProperty("BOUNCE_EMAIL");
            server[3] = properties.getProperty("EMAIL");
            server[4] = properties.getProperty("PASSWORD");

        } catch (IOException ex) {
            server = null;
            ex.printStackTrace();
        }

        return server;
    }

    public String ftnNewUserSave(HashMap<Object, Object> paramMap) {

        StringBuilder tempOrderString = new StringBuilder();

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;
        ResultSet resultSet = null;

        String query = "";

        if (connection != null) {

            String newCustomerCode = "";

            try {

                // Insert customer information
                // Get customer pk id

                String pkQuery = FtnOrderStatement.customerPkQuery(paramMap);

                System.out.println("pkQuery:" + pkQuery);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(pkQuery);

                while (resultSet.next()) {
                    newCustomerCode = resultSet.getString("CUSTOMER_ID") == null ? "" : resultSet.getString("CUSTOMER_ID");
                    break;
                }

                // If customer not available
                if (newCustomerCode.isEmpty()) {
                    String cont = paramMap.get("CONTACT_NO") == null ? "" : paramMap.get("CONTACT_NO").toString();
                    newCustomerCode = cont.substring(cont.length() - 6) + cont.substring(1, 3) + "01";
                } else {
                    String cont = paramMap.get("CONTACT_NO") == null ? "" : paramMap.get("CONTACT_NO").toString();
                    newCustomerCode = cont.substring(cont.length() - 6) + newCustomerCode;
                }

                paramMap.put("CUSTOMER_ID", newCustomerCode);

                query = LoginStatement.customerInsertQuery(paramMap);

                System.out.println("CustomerInsertQuery:" + query);

                statement = connection.createStatement();
                statement.executeUpdate(query);

                // Now insert user information
                // Get user pk id

                String userPkQuery = LoginStatement.userPkQuery();

                System.out.println("UserPkQuery:" + userPkQuery);

                statement = connection.createStatement();
                resultSet = statement.executeQuery(userPkQuery);

                BigDecimal userId = null;

                while (resultSet.next()) {
                    userId = resultSet.getBigDecimal("USER_ID");
                }

                if (userId != null) {

                    paramMap.put("USER_ID", userId);

                    // get random 4 digit number
                    String activationCode = getRandomAlphaNumericNumber(4);
                    paramMap.put("ACTIVATION_CODE", activationCode);
                    // make unique activation link

                    String linkString = "<START>";
                    linkString += "<USER>" + userId + "</USER>";
                    linkString += "<CUSTOMER>" + newCustomerCode + "</CUSTOMER>";
                    linkString += "<CODE>" + activationCode + "</CODE>";
                    linkString += "<END>";

                    System.out.println("linkString:" + linkString);

                    linkString = Encryption.encryption(linkString);

                    System.out.println("linkString:" + linkString);

                    linkString = parentUrl() + linkString;

                    System.out.println("linkString:" + linkString);

                    paramMap.put("ACTIVATION_LINK", linkString);

                    // Mail send to user

                    if (!paramMap.get("USER_EMAIL").toString().isEmpty()) {

                        String[] serverInfo = smtpServerInfo();

                        String host = serverInfo[0];
                        String port = serverInfo[1];
                        String bounceAddr = serverInfo[2];
                        String mailFrom = serverInfo[3];
                        String password = serverInfo[4];

                        // message info
                        String mailTo = paramMap.get("USER_EMAIL").toString();
                        String subject = "Footin user registration";

                        String message = "<p>Dear " + paramMap.get("MIDDLE_NAME").toString() + ",</p>";
                        message += "<p>Thanks for registration. To active your registration please click on below link:</p>";
                        message += "<p><a href=\"" + linkString + "\">To activate user click here</a></p>";
                        message += "<p>&nbsp;</p><p>Regards,</p><p>footin.com.bd</p><p>Tongi, Gazipur</p>";

                        EmailSender.sendEmail(host, port, mailFrom, password, mailTo, subject, message, bounceAddr);

                    }

                    query = LoginStatement.userInsertQuery(paramMap);

                    System.out.println("UserInsertQuery:" + query);

                    connection = DatabaseConnection.connectDB();

                    statement = connection.createStatement();
                    statement.executeUpdate(query);

                    tempOrderString.append("Successfully user created. To activate your user id please check email.");

                } else {
                    tempOrderString.append("User does not created.");
                }



//                tempOrderString.append("Successfully order saved.");
//                tempOrderString.append("\nCustomer Id = ");
//                tempOrderString.append(paramMap.get("CUSTOMER_ID"));
//                tempOrderString.append(".\nOrder Number = ");
//                tempOrderString.append(newOrderNo);


            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return tempOrderString.toString(); // 3

    }

    public String userActivation(HashMap<Object, Object> paramMap) {

        StringBuilder tempOrderString = new StringBuilder();

        tempOrderString.append("0");

        Connection connection = DatabaseConnection.connectDB();

        Statement statement = null;

        if (connection != null) {

            try {

                String query = LoginStatement.userUpdateQuery(paramMap);

                System.out.println("User Update Query:" + query);

                statement = connection.createStatement();
                int update = statement.executeUpdate(query);

                if (update > 0) {
                    tempOrderString.setLength(0);
                    tempOrderString.append("1");
                } else {
                    tempOrderString.setLength(0);
                    tempOrderString.append("0");
                }

            } catch (SQLException ex) {
                ex.printStackTrace();
                tempOrderString.setLength(0);
                tempOrderString.append("0");
            } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                    tempOrderString.setLength(0);
                    tempOrderString.append("0");
                }
            }
        }

        return tempOrderString.toString();

    }
}
