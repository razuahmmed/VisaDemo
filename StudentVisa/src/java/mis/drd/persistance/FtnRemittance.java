package mis.drd.persistance;

public class FtnRemittance {

    private String processYear;
    private String processWeek;
    private String processDate;
    private String logicticNo;
    private String awb;
    private String orderNo;
    private String amount;
    private String mrno;
    private String mrdate;
    private String collectedAmount;
    private String pickupDate;
    private String shipperName;
    private String processBy;
    private String insertDate;
    private String remarks;

    /**
     * @return the processYear
     */
    public String getProcessYear() {
        return processYear;
    }

    /**
     * @param processYear the processYear to set
     */
    public void setProcessYear(String processYear) {
        this.processYear = processYear;
    }

    /**
     * @return the processWeek
     */
    public String getProcessWeek() {
        return processWeek;
    }

    /**
     * @param processWeek the processWeek to set
     */
    public void setProcessWeek(String processWeek) {
        this.processWeek = processWeek;
    }

    /**
     * @return the processDate
     */
    public String getProcessDate() {
        return processDate;
    }

    /**
     * @param processDate the processDate to set
     */
    public void setProcessDate(String processDate) {
        this.processDate = processDate;
    }

    /**
     * @return the logicticNo
     */
    public String getLogicticNo() {
        return logicticNo;
    }

    /**
     * @param logicticNo the logicticNo to set
     */
    public void setLogicticNo(String logicticNo) {
        this.logicticNo = logicticNo;
    }

    /**
     * @return the awb
     */
    public String getAwb() {
        return awb;
    }

    /**
     * @param awb the awb to set
     */
    public void setAwb(String awb) {
        this.awb = awb;
    }

    /**
     * @return the orderNo
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * @param orderNo the orderNo to set
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * @return the mrno
     */
    public String getMrno() {
        return mrno;
    }

    /**
     * @param mrno the mrno to set
     */
    public void setMrno(String mrno) {
        this.mrno = mrno;
    }

    /**
     * @return the mrdate
     */
    public String getMrdate() {
        return mrdate;
    }

    /**
     * @param mrdate the mrdate to set
     */
    public void setMrdate(String mrdate) {
        this.mrdate = mrdate;
    }

    /**
     * @return the collectedAmount
     */
    public String getCollectedAmount() {
        return collectedAmount;
    }

    /**
     * @param collectedAmount the collectedAmount to set
     */
    public void setCollectedAmount(String collectedAmount) {
        this.collectedAmount = collectedAmount;
    }

    /**
     * @return the pickupDate
     */
    public String getPickupDate() {
        return pickupDate;
    }

    /**
     * @param pickupDate the pickupDate to set
     */
    public void setPickupDate(String pickupDate) {
        this.pickupDate = pickupDate;
    }

    /**
     * @return the shipperName
     */
    public String getShipperName() {
        return shipperName;
    }

    /**
     * @param shipperName the shipperName to set
     */
    public void setShipperName(String shipperName) {
        this.shipperName = shipperName;
    }

    /**
     * @return the processBy
     */
    public String getProcessBy() {
        return processBy;
    }

    /**
     * @param processBy the processBy to set
     */
    public void setProcessBy(String processBy) {
        this.processBy = processBy;
    }

    /**
     * @return the insertDate
     */
    public String getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
