package mis.drd.persistance;

import java.util.Date;

public class FtnPromotion {

    private String promoId;
    private String promoName;
    private String promoRemarks;
    private String promoStatus;
    private Date startDate;
    private Date endDate;
    private Integer startTime;
    private Integer endTime;
    private String artCode;
    private String mrp;
    private String costPrice;
    private String salePrice;
    private String vat;
    private String netSalePrice;
    private String discountAmount;
    private String salePriceAfterDis;
    private String netSalePriceAfterDis;
    //
    private String circularDate;
    private String effectStartDate;
    private String effectEndDate;

    /**
     * @return the promoId
     */
    public String getPromoId() {
        return promoId;
    }

    /**
     * @param promoId the promoId to set
     */
    public void setPromoId(String promoId) {
        this.promoId = promoId;
    }

    /**
     * @return the promoName
     */
    public String getPromoName() {
        return promoName;
    }

    /**
     * @param promoName the promoName to set
     */
    public void setPromoName(String promoName) {
        this.promoName = promoName;
    }

    /**
     * @return the promoRemarks
     */
    public String getPromoRemarks() {
        return promoRemarks;
    }

    /**
     * @param promoRemarks the promoRemarks to set
     */
    public void setPromoRemarks(String promoRemarks) {
        this.promoRemarks = promoRemarks;
    }

    /**
     * @return the promoStatus
     */
    public String getPromoStatus() {
        return promoStatus;
    }

    /**
     * @param promoStatus the promoStatus to set
     */
    public void setPromoStatus(String promoStatus) {
        this.promoStatus = promoStatus;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the startTime
     */
    public Integer getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the endTime
     */
    public Integer getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(Integer endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the artCode
     */
    public String getArtCode() {
        return artCode;
    }

    /**
     * @param artCode the artCode to set
     */
    public void setArtCode(String artCode) {
        this.artCode = artCode;
    }

    /**
     * @return the mrp
     */
    public String getMrp() {
        return mrp;
    }

    /**
     * @param mrp the mrp to set
     */
    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    /**
     * @return the costPrice
     */
    public String getCostPrice() {
        return costPrice;
    }

    /**
     * @param costPrice the costPrice to set
     */
    public void setCostPrice(String costPrice) {
        this.costPrice = costPrice;
    }

    /**
     * @return the salePrice
     */
    public String getSalePrice() {
        return salePrice;
    }

    /**
     * @param salePrice the salePrice to set
     */
    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    /**
     * @return the vat
     */
    public String getVat() {
        return vat;
    }

    /**
     * @param vat the vat to set
     */
    public void setVat(String vat) {
        this.vat = vat;
    }

    /**
     * @return the netSalePrice
     */
    public String getNetSalePrice() {
        return netSalePrice;
    }

    /**
     * @param netSalePrice the netSalePrice to set
     */
    public void setNetSalePrice(String netSalePrice) {
        this.netSalePrice = netSalePrice;
    }

    /**
     * @return the discountAmount
     */
    public String getDiscountAmount() {
        return discountAmount;
    }

    /**
     * @param discountAmount the discountAmount to set
     */
    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    /**
     * @return the salePriceAfterDis
     */
    public String getSalePriceAfterDis() {
        return salePriceAfterDis;
    }

    /**
     * @param salePriceAfterDis the salePriceAfterDis to set
     */
    public void setSalePriceAfterDis(String salePriceAfterDis) {
        this.salePriceAfterDis = salePriceAfterDis;
    }

    /**
     * @return the netSalePriceAfterDis
     */
    public String getNetSalePriceAfterDis() {
        return netSalePriceAfterDis;
    }

    /**
     * @param netSalePriceAfterDis the netSalePriceAfterDis to set
     */
    public void setNetSalePriceAfterDis(String netSalePriceAfterDis) {
        this.netSalePriceAfterDis = netSalePriceAfterDis;
    }

    /**
     * @return the circularDate
     */
    public String getCircularDate() {
        return circularDate;
    }

    /**
     * @param circularDate the circularDate to set
     */
    public void setCircularDate(String circularDate) {
        this.circularDate = circularDate;
    }

    /**
     * @return the effectStartDate
     */
    public String getEffectStartDate() {
        return effectStartDate;
    }

    /**
     * @param effectStartDate the effectStartDate to set
     */
    public void setEffectStartDate(String effectStartDate) {
        this.effectStartDate = effectStartDate;
    }

    /**
     * @return the effectEndDate
     */
    public String getEffectEndDate() {
        return effectEndDate;
    }

    /**
     * @param effectEndDate the effectEndDate to set
     */
    public void setEffectEndDate(String effectEndDate) {
        this.effectEndDate = effectEndDate;
    }
}
