/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.drd.persistance;

/**
 *
 * @author MIS
 */
public class FtnPaymentMode {

    private String paymentType;
    private String paymentDescription;
    private String activeStatus;

    /**
     * @return the paymentType
     */
    public String getPaymentType() {
        return paymentType;
    }

    /**
     * @param paymentType the paymentType to set
     */
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    /**
     * @return the paymentDescription
     */
    public String getPaymentDescription() {
        return paymentDescription;
    }

    /**
     * @param paymentDescription the paymentDescription to set
     */
    public void setPaymentDescription(String paymentDescription) {
        this.paymentDescription = paymentDescription;
    }

    /**
     * @return the activeStatus
     */
    public String getActiveStatus() {
        return activeStatus;
    }

    /**
     * @param activeStatus the activeStatus to set
     */
    public void setActiveStatus(String activeStatus) {
        this.activeStatus = activeStatus;
    }
}
