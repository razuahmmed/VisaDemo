package mis.drd.persistance;
// Generated Oct 6, 2013 3:50:13 PM by Hibernate Tools 3.2.1.GA

import java.util.Date;

public class SvUserGroup {

    private Integer groupId;
    private String groupName;
    private String groupType;
    private Character groupStatus;
    private String insertBy;
    private Date insertDate;
    private String updatedBy;
    private Date updatedDate;
    //
    private SvCompany svCompanyInfo;

    /**
     * @return the groupId
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * @param groupId the groupId to set
     */
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @param groupName the groupName to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @return the groupType
     */
    public String getGroupType() {
        return groupType;
    }

    /**
     * @param groupType the groupType to set
     */
    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    /**
     * @return the groupStatus
     */
    public Character getGroupStatus() {
        return groupStatus;
    }

    /**
     * @param groupStatus the groupStatus to set
     */
    public void setGroupStatus(Character groupStatus) {
        this.groupStatus = groupStatus;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public Date getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updatedBy
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * @param updatedBy the updatedBy to set
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * @return the updatedDate
     */
    public Date getUpdatedDate() {
        return updatedDate;
    }

    /**
     * @param updatedDate the updatedDate to set
     */
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    /**
     * @return the svCompanyInfo
     */
    public SvCompany getSvCompanyInfo() {
        return svCompanyInfo;
    }

    /**
     * @param svCompanyInfo the svCompanyInfo to set
     */
    public void setSvCompanyInfo(SvCompany svCompanyInfo) {
        this.svCompanyInfo = svCompanyInfo;
    }
}
