/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.drd.persistance;

import java.util.Date;

/**
 *
 * @author MIS
 */
public class FtnMenu {

    private String menuId;
    private String menuName;
    private String menuTitle;
    private String parentMenu;
    private String menuUrl;
    private String menuType;
    private Character menuStatus;
    private String insertBy;
    private Date insertDate;
    private String updateBy;
    private Date updateDate;


    /**
     * @return the menuId
     */
    public String getMenuId() {
        return menuId;
    }

    /**
     * @param menuId the menuId to set
     */
    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    /**
     * @return the menuName
     */
    public String getMenuName() {
        return menuName;
    }

    /**
     * @param menuName the menuName to set
     */
    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    /**
     * @return the menuTitle
     */
    public String getMenuTitle() {
        return menuTitle;
    }

    /**
     * @param menuTitle the menuTitle to set
     */
    public void setMenuTitle(String menuTitle) {
        this.menuTitle = menuTitle;
    }

    /**
     * @return the parentMenu
     */
    public String getParentMenu() {
        return parentMenu;
    }

    /**
     * @param parentMenu the parentMenu to set
     */
    public void setParentMenu(String parentMenu) {
        this.parentMenu = parentMenu;
    }

    /**
     * @return the menuUrl
     */
    public String getMenuUrl() {
        return menuUrl;
    }

    /**
     * @param menuUrl the menuUrl to set
     */
    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    /**
     * @return the menuType
     */
    public String getMenuType() {
        return menuType;
    }

    /**
     * @param menuType the menuType to set
     */
    public void setMenuType(String menuType) {
        this.menuType = menuType;
    }

    /**
     * @return the menuStatus
     */
    public Character getMenuStatus() {
        return menuStatus;
    }

    /**
     * @param menuStatus the menuStatus to set
     */
    public void setMenuStatus(Character menuStatus) {
        this.menuStatus = menuStatus;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public Date getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
