package mis.drd.persistance;

import java.util.Date;

public class FtnOrderMaster {

    private String store_code;
    private Date order_date;
    private Integer f_year;
    private String week_no;
    private String order_no;
    private String customer_id;
    private String contact_no;
    private String sales_pcode;
    private Date delivery_date;
    private Integer order_time;
    private Date shipment_date;
    private String order_source;
    private String order_status;
    private Date entry_date;
    private Integer user_id;
    private String ip_address;
    private String order_to_cdc_status;
    private Date order_to_cdc_date;
    private String cdc_packing_status;
    private Date cdc_packing_date;
    private String dlv_to_carry_con_status;
    private Date dlv_to_carry_con_date;
    private String cust_rcv_status;
    private Date cust_rcv_ret_date;
    private String payment_status;
    private String payment_amount;
    private Date payment_date;
    private String vat_status;
    private String vat_inv_no;
    private Date vat_date;
    private String inv_no;
    private String inv_status;
    private Date inv_date;
    private String cancel_status;

    /**
     * @return the store_code
     */
    public String getStore_code() {
        return store_code;
    }

    /**
     * @param store_code the store_code to set
     */
    public void setStore_code(String store_code) {
        this.store_code = store_code;
    }

    /**
     * @return the order_date
     */
    public Date getOrder_date() {
        return order_date;
    }

    /**
     * @param order_date the order_date to set
     */
    public void setOrder_date(Date order_date) {
        this.order_date = order_date;
    }

    /**
     * @return the f_year
     */
    public Integer getF_year() {
        return f_year;
    }

    /**
     * @param f_year the f_year to set
     */
    public void setF_year(Integer f_year) {
        this.f_year = f_year;
    }

    /**
     * @return the week_no
     */
    public String getWeek_no() {
        return week_no;
    }

    /**
     * @param week_no the week_no to set
     */
    public void setWeek_no(String week_no) {
        this.week_no = week_no;
    }

    /**
     * @return the order_no
     */
    public String getOrder_no() {
        return order_no;
    }

    /**
     * @param order_no the order_no to set
     */
    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    /**
     * @return the customer_id
     */
    public String getCustomer_id() {
        return customer_id;
    }

    /**
     * @param customer_id the customer_id to set
     */
    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    /**
     * @return the contact_no
     */
    public String getContact_no() {
        return contact_no;
    }

    /**
     * @param contact_no the contact_no to set
     */
    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    /**
     * @return the sales_pcode
     */
    public String getSales_pcode() {
        return sales_pcode;
    }

    /**
     * @param sales_pcode the sales_pcode to set
     */
    public void setSales_pcode(String sales_pcode) {
        this.sales_pcode = sales_pcode;
    }

    /**
     * @return the delivery_date
     */
    public Date getDelivery_date() {
        return delivery_date;
    }

    /**
     * @param delivery_date the delivery_date to set
     */
    public void setDelivery_date(Date delivery_date) {
        this.delivery_date = delivery_date;
    }

    /**
     * @return the order_time
     */
    public Integer getOrder_time() {
        return order_time;
    }

    /**
     * @param order_time the order_time to set
     */
    public void setOrder_time(Integer order_time) {
        this.order_time = order_time;
    }

    /**
     * @return the shipment_date
     */
    public Date getShipment_date() {
        return shipment_date;
    }

    /**
     * @param shipment_date the shipment_date to set
     */
    public void setShipment_date(Date shipment_date) {
        this.shipment_date = shipment_date;
    }

    /**
     * @return the order_source
     */
    public String getOrder_source() {
        return order_source;
    }

    /**
     * @param order_source the order_source to set
     */
    public void setOrder_source(String order_source) {
        this.order_source = order_source;
    }

    /**
     * @return the order_status
     */
    public String getOrder_status() {
        return order_status;
    }

    /**
     * @param order_status the order_status to set
     */
    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    /**
     * @return the entry_date
     */
    public Date getEntry_date() {
        return entry_date;
    }

    /**
     * @param entry_date the entry_date to set
     */
    public void setEntry_date(Date entry_date) {
        this.entry_date = entry_date;
    }

    /**
     * @return the user_id
     */
    public Integer getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the ip_address
     */
    public String getIp_address() {
        return ip_address;
    }

    /**
     * @param ip_address the ip_address to set
     */
    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    /**
     * @return the order_to_cdc_status
     */
    public String getOrder_to_cdc_status() {
        return order_to_cdc_status;
    }

    /**
     * @param order_to_cdc_status the order_to_cdc_status to set
     */
    public void setOrder_to_cdc_status(String order_to_cdc_status) {
        this.order_to_cdc_status = order_to_cdc_status;
    }

    /**
     * @return the order_to_cdc_date
     */
    public Date getOrder_to_cdc_date() {
        return order_to_cdc_date;
    }

    /**
     * @param order_to_cdc_date the order_to_cdc_date to set
     */
    public void setOrder_to_cdc_date(Date order_to_cdc_date) {
        this.order_to_cdc_date = order_to_cdc_date;
    }

    /**
     * @return the cdc_packing_status
     */
    public String getCdc_packing_status() {
        return cdc_packing_status;
    }

    /**
     * @param cdc_packing_status the cdc_packing_status to set
     */
    public void setCdc_packing_status(String cdc_packing_status) {
        this.cdc_packing_status = cdc_packing_status;
    }

    /**
     * @return the cdc_packing_date
     */
    public Date getCdc_packing_date() {
        return cdc_packing_date;
    }

    /**
     * @param cdc_packing_date the cdc_packing_date to set
     */
    public void setCdc_packing_date(Date cdc_packing_date) {
        this.cdc_packing_date = cdc_packing_date;
    }

    /**
     * @return the dlv_to_carry_con_status
     */
    public String getDlv_to_carry_con_status() {
        return dlv_to_carry_con_status;
    }

    /**
     * @param dlv_to_carry_con_status the dlv_to_carry_con_status to set
     */
    public void setDlv_to_carry_con_status(String dlv_to_carry_con_status) {
        this.dlv_to_carry_con_status = dlv_to_carry_con_status;
    }

    /**
     * @return the dlv_to_carry_con_date
     */
    public Date getDlv_to_carry_con_date() {
        return dlv_to_carry_con_date;
    }

    /**
     * @param dlv_to_carry_con_date the dlv_to_carry_con_date to set
     */
    public void setDlv_to_carry_con_date(Date dlv_to_carry_con_date) {
        this.dlv_to_carry_con_date = dlv_to_carry_con_date;
    }

    /**
     * @return the cust_rcv_status
     */
    public String getCust_rcv_status() {
        return cust_rcv_status;
    }

    /**
     * @param cust_rcv_status the cust_rcv_status to set
     */
    public void setCust_rcv_status(String cust_rcv_status) {
        this.cust_rcv_status = cust_rcv_status;
    }

    /**
     * @return the cust_rcv_ret_date
     */
    public Date getCust_rcv_ret_date() {
        return cust_rcv_ret_date;
    }

    /**
     * @param cust_rcv_ret_date the cust_rcv_ret_date to set
     */
    public void setCust_rcv_ret_date(Date cust_rcv_ret_date) {
        this.cust_rcv_ret_date = cust_rcv_ret_date;
    }

    /**
     * @return the payment_status
     */
    public String getPayment_status() {
        return payment_status;
    }

    /**
     * @param payment_status the payment_status to set
     */
    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

    /**
     * @return the payment_amount
     */
    public String getPayment_amount() {
        return payment_amount;
    }

    /**
     * @param payment_amount the payment_amount to set
     */
    public void setPayment_amount(String payment_amount) {
        this.payment_amount = payment_amount;
    }

    /**
     * @return the payment_date
     */
    public Date getPayment_date() {
        return payment_date;
    }

    /**
     * @param payment_date the payment_date to set
     */
    public void setPayment_date(Date payment_date) {
        this.payment_date = payment_date;
    }

    /**
     * @return the vat_status
     */
    public String getVat_status() {
        return vat_status;
    }

    /**
     * @param vat_status the vat_status to set
     */
    public void setVat_status(String vat_status) {
        this.vat_status = vat_status;
    }

    /**
     * @return the vat_date
     */
    public Date getVat_date() {
        return vat_date;
    }

    /**
     * @param vat_date the vat_date to set
     */
    public void setVat_date(Date vat_date) {
        this.vat_date = vat_date;
    }

    /**
     * @return the inv_no
     */
    public String getInv_no() {
        return inv_no;
    }

    /**
     * @param inv_no the inv_no to set
     */
    public void setInv_no(String inv_no) {
        this.inv_no = inv_no;
    }

    /**
     * @return the inv_date
     */
    public Date getInv_date() {
        return inv_date;
    }

    /**
     * @param inv_date the inv_date to set
     */
    public void setInv_date(Date inv_date) {
        this.inv_date = inv_date;
    }

    /**
     * @return the inv_status
     */
    public String getInv_status() {
        return inv_status;
    }

    /**
     * @param inv_status the inv_status to set
     */
    public void setInv_status(String inv_status) {
        this.inv_status = inv_status;
    }

    /**
     * @return the vat_inv_no
     */
    public String getVat_inv_no() {
        return vat_inv_no;
    }

    /**
     * @param vat_inv_no the vat_inv_no to set
     */
    public void setVat_inv_no(String vat_inv_no) {
        this.vat_inv_no = vat_inv_no;
    }

    /**
     * @return the cancel_status
     */
    public String getCancel_status() {
        return cancel_status;
    }

    /**
     * @param cancel_status the cancel_status to set
     */
    public void setCancel_status(String cancel_status) {
        this.cancel_status = cancel_status;
    }

    

}
