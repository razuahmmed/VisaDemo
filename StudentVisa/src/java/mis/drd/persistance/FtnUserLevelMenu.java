package mis.drd.persistance;
// Generated Oct 6, 2013 3:50:13 PM by Hibernate Tools 3.2.1.GA

import java.util.Date;

public class FtnUserLevelMenu {

    private Integer userLevelMenuId;
    private FtnMenu rtlMenuInfo;
    private SvUserGroup rtlUserLevelInfo;
    private Character levelMenuStatus;
    private String insertBy;
    private Date insertDate;
    private String updateBy;
    private Date updateDate;
    private SvCompany rtlCompanyInfo;

    /**
     * @return the userLevelMenuId
     */
    public Integer getUserLevelMenuId() {
        return userLevelMenuId;
    }

    /**
     * @param userLevelMenuId the userLevelMenuId to set
     */
    public void setUserLevelMenuId(Integer userLevelMenuId) {
        this.userLevelMenuId = userLevelMenuId;
    }

    /**
     * @return the rtlMenuInfo
     */
    public FtnMenu getRtlMenuInfo() {
        return rtlMenuInfo;
    }

    /**
     * @param rtlMenuInfo the rtlMenuInfo to set
     */
    public void setRtlMenuInfo(FtnMenu rtlMenuInfo) {
        this.rtlMenuInfo = rtlMenuInfo;
    }

    /**
     * @return the rtlUserLevelInfo
     */
    public SvUserGroup getRtlUserLevelInfo() {
        return rtlUserLevelInfo;
    }

    /**
     * @param rtlUserLevelInfo the rtlUserLevelInfo to set
     */
    public void setRtlUserLevelInfo(SvUserGroup rtlUserLevelInfo) {
        this.rtlUserLevelInfo = rtlUserLevelInfo;
    }

    /**
     * @return the levelMenuStatus
     */
    public Character getLevelMenuStatus() {
        return levelMenuStatus;
    }

    /**
     * @param levelMenuStatus the levelMenuStatus to set
     */
    public void setLevelMenuStatus(Character levelMenuStatus) {
        this.levelMenuStatus = levelMenuStatus;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public Date getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the rtlCompanyInfo
     */
    public SvCompany getRtlCompanyInfo() {
        return rtlCompanyInfo;
    }

    /**
     * @param rtlCompanyInfo the rtlCompanyInfo to set
     */
    public void setRtlCompanyInfo(SvCompany rtlCompanyInfo) {
        this.rtlCompanyInfo = rtlCompanyInfo;
    }
}
