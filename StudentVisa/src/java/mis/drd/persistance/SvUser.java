package mis.drd.persistance;
// Generated Oct 6, 2013 3:50:13 PM by Hibernate Tools 3.2.1.GA

import java.util.Date;

public class SvUser {

    private Integer userId;
    private String userName;
    private String userPassword;
    private Character userStatus;
    private String insertBy;
    private Date insertDate;
    private String updatedBy;
    private Date updatedDate;
    private String displayName;
    //
    private SvCompany svCompanyInfo;
    private SvUserGroup svUserGroupInfo;

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the userPassword
     */
    public String getUserPassword() {
        return userPassword;
    }

    /**
     * @param userPassword the userPassword to set
     */
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    /**
     * @return the userStatus
     */
    public Character getUserStatus() {
        return userStatus;
    }

    /**
     * @param userStatus the userStatus to set
     */
    public void setUserStatus(Character userStatus) {
        this.userStatus = userStatus;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public Date getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updatedBy
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * @param updatedBy the updatedBy to set
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * @return the updatedDate
     */
    public Date getUpdatedDate() {
        return updatedDate;
    }

    /**
     * @param updatedDate the updatedDate to set
     */
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return the svCompanyInfo
     */
    public SvCompany getSvCompanyInfo() {
        return svCompanyInfo;
    }

    /**
     * @param svCompanyInfo the svCompanyInfo to set
     */
    public void setSvCompanyInfo(SvCompany svCompanyInfo) {
        this.svCompanyInfo = svCompanyInfo;
    }

    /**
     * @return the svUserGroupInfo
     */
    public SvUserGroup getSvUserGroupInfo() {
        return svUserGroupInfo;
    }

    /**
     * @param svUserGroupInfo the svUserGroupInfo to set
     */
    public void setSvUserGroupInfo(SvUserGroup svUserGroupInfo) {
        this.svUserGroupInfo = svUserGroupInfo;
    }
}
