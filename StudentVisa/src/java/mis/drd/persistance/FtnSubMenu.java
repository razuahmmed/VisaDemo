package mis.drd.persistance;
// Generated Oct 6, 2013 3:50:13 PM by Hibernate Tools 3.2.1.GA

import java.util.List;

public class FtnSubMenu {

    private String subMenuId;
    private String mainMenuId;
    private String subMenuName;
    private String subMenuTitle;
    private Character subMenuStatus;
    private List<FtnSubChdMenu> rtlSubChdMenuList;

    /**
     * @return the subMenuId
     */
    public String getSubMenuId() {
        return subMenuId;
    }

    /**
     * @param subMenuId the subMenuId to set
     */
    public void setSubMenuId(String subMenuId) {
        this.subMenuId = subMenuId;
    }

    /**
     * @return the mainMenuId
     */
    public String getMainMenuId() {
        return mainMenuId;
    }

    /**
     * @param mainMenuId the mainMenuId to set
     */
    public void setMainMenuId(String mainMenuId) {
        this.mainMenuId = mainMenuId;
    }

    /**
     * @return the subMenuName
     */
    public String getSubMenuName() {
        return subMenuName;
    }

    /**
     * @param subMenuName the subMenuName to set
     */
    public void setSubMenuName(String subMenuName) {
        this.subMenuName = subMenuName;
    }

    /**
     * @return the subMenuTitle
     */
    public String getSubMenuTitle() {
        return subMenuTitle;
    }

    /**
     * @param subMenuTitle the subMenuTitle to set
     */
    public void setSubMenuTitle(String subMenuTitle) {
        this.subMenuTitle = subMenuTitle;
    }

    /**
     * @return the subMenuStatus
     */
    public Character getSubMenuStatus() {
        return subMenuStatus;
    }

    /**
     * @param subMenuStatus the subMenuStatus to set
     */
    public void setSubMenuStatus(Character subMenuStatus) {
        this.subMenuStatus = subMenuStatus;
    }

    /**
     * @return the rtlSubChdMenuList
     */
    public List<FtnSubChdMenu> getRtlSubChdMenuList() {
        return rtlSubChdMenuList;
    }

    /**
     * @param rtlSubChdMenuList the rtlSubChdMenuList to set
     */
    public void setRtlSubChdMenuList(List<FtnSubChdMenu> rtlSubChdMenuList) {
        this.rtlSubChdMenuList = rtlSubChdMenuList;
    }
}
