/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.drd.persistance;

/**
 *
 * @author 3167
 */
public class FtnCalendar {

    private String calDate;
    private String calMonth;
    private String calYear;
    private String calDay;
    private String calFullDate;

    /**
     * @return the calDate
     */
    public String getCalDate() {
        return calDate;
    }

    /**
     * @param calDate the calDate to set
     */
    public void setCalDate(String calDate) {
        this.calDate = calDate;
    }

    /**
     * @return the calMonth
     */
    public String getCalMonth() {
        return calMonth;
    }

    /**
     * @param calMonth the calMonth to set
     */
    public void setCalMonth(String calMonth) {
        this.calMonth = calMonth;
    }

    /**
     * @return the calYear
     */
    public String getCalYear() {
        return calYear;
    }

    /**
     * @param calYear the calYear to set
     */
    public void setCalYear(String calYear) {
        this.calYear = calYear;
    }

    /**
     * @return the calDay
     */
    public String getCalDay() {
        return calDay;
    }

    /**
     * @param calDay the calDay to set
     */
    public void setCalDay(String calDay) {
        this.calDay = calDay;
    }

    /**
     * @return the calFullDate
     */
    public String getCalFullDate() {
        return calFullDate;
    }

    /**
     * @param calFullDate the calFullDate to set
     */
    public void setCalFullDate(String calFullDate) {
        this.calFullDate = calFullDate;
    }
}
