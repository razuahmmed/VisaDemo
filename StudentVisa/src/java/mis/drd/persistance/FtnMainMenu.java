package mis.drd.persistance;
// Generated Oct 6, 2013 3:50:13 PM by Hibernate Tools 3.2.1.GA

import java.util.List;

public class FtnMainMenu {

    private String mainMenuId;
    private String mainMenuName;
    private String mainMenuTitle;
    private Character mainMenuStatus;
    private List<FtnSubMenu> rtlSubMenuList;

    /**
     * @return the mainMenuId
     */
    public String getMainMenuId() {
        return mainMenuId;
    }

    /**
     * @param mainMenuId the mainMenuId to set
     */
    public void setMainMenuId(String mainMenuId) {
        this.mainMenuId = mainMenuId;
    }

    /**
     * @return the mainMenuName
     */
    public String getMainMenuName() {
        return mainMenuName;
    }

    /**
     * @param mainMenuName the mainMenuName to set
     */
    public void setMainMenuName(String mainMenuName) {
        this.mainMenuName = mainMenuName;
    }

    /**
     * @return the mainMenuTitle
     */
    public String getMainMenuTitle() {
        return mainMenuTitle;
    }

    /**
     * @param mainMenuTitle the mainMenuTitle to set
     */
    public void setMainMenuTitle(String mainMenuTitle) {
        this.mainMenuTitle = mainMenuTitle;
    }

    /**
     * @return the mainMenuStatus
     */
    public Character getMainMenuStatus() {
        return mainMenuStatus;
    }

    /**
     * @param mainMenuStatus the mainMenuStatus to set
     */
    public void setMainMenuStatus(Character mainMenuStatus) {
        this.mainMenuStatus = mainMenuStatus;
    }

    /**
     * @return the rtlSubMenuList
     */
    public List<FtnSubMenu> getRtlSubMenuList() {
        return rtlSubMenuList;
    }

    /**
     * @param rtlSubMenuList the rtlSubMenuList to set
     */
    public void setRtlSubMenuList(List<FtnSubMenu> rtlSubMenuList) {
        this.rtlSubMenuList = rtlSubMenuList;
    }
}
