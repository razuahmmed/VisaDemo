package mis.drd.persistance;

import java.util.Date;

public class SvCompany {

    private String comCode;
    private String comName;
    private String comAddress;
    private String comContact;
    //
    private Character comStatus;
    private String insertBy;
    private Date insertDate;
    private String updateBy;
    private Date updateDate;

    /**
     * @return the comCode
     */
    public String getComCode() {
        return comCode;
    }

    /**
     * @param comCode the comCode to set
     */
    public void setComCode(String comCode) {
        this.comCode = comCode;
    }

    /**
     * @return the comName
     */
    public String getComName() {
        return comName;
    }

    /**
     * @param comName the comName to set
     */
    public void setComName(String comName) {
        this.comName = comName;
    }

    /**
     * @return the comAddress
     */
    public String getComAddress() {
        return comAddress;
    }

    /**
     * @param comAddress the comAddress to set
     */
    public void setComAddress(String comAddress) {
        this.comAddress = comAddress;
    }

    /**
     * @return the comContact
     */
    public String getComContact() {
        return comContact;
    }

    /**
     * @param comContact the comContact to set
     */
    public void setComContact(String comContact) {
        this.comContact = comContact;
    }

    /**
     * @return the comStatus
     */
    public Character getComStatus() {
        return comStatus;
    }

    /**
     * @param comStatus the comStatus to set
     */
    public void setComStatus(Character comStatus) {
        this.comStatus = comStatus;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public Date getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
