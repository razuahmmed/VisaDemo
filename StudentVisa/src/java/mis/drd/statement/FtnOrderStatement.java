/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.drd.statement;

import java.util.HashMap;

/**
 *
 * @author MIS
 */
public class FtnOrderStatement {

    //static member holds only one instance of the LoginStatement class.
    private static FtnOrderStatement ftnOrderStatement = null;

    //LoginStatement prevents the instantiation from any other class.
    private FtnOrderStatement() {
    }

    //Now we are providing gloabal point of access.
    public static synchronized FtnOrderStatement getInstance() {

        if (ftnOrderStatement == null) {
            ftnOrderStatement = new FtnOrderStatement();
        }

        return ftnOrderStatement;

    }

    /**
     * Method: All country select query
     * Return: Query string
     */
    public static String countryQuery() {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT DISTINCT COUNTRY_CODE, COUNTRY_NAME FROM FTN_COUNTRY_INFO");

        return queryString.toString();
    }

    public static String paymentModeQuery() {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT DISTINCT PAYMENT_TYPE, PAYMENT_DESC FROM FTN_PAYMENT_MODE WHERE ACTIVE_STATUS = 'Y'");

        return queryString.toString();
    }

    /**
     * Method: Country State query
     * Parameter: HashMap (Country Code)
     * Return: Query string
     */
    public static String countryStateQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT DISTINCT SATE_REGION FROM FTN_COUNTRY_INFO");
        queryString.append(" WHERE COUNTRY_CODE ='");
        queryString.append((param.get("COUNTRY_CODE") == null ? "" : param.get("COUNTRY_CODE").toString()));
        queryString.append("' ORDER BY SATE_REGION ASC");

        return queryString.toString();
    }

    /**
     * Method: Country State query
     * Parameter: HashMap (Country name)
     * Return: Query string
     */
    public static String countryStateQuery2(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT DISTINCT SATE_REGION FROM FTN_COUNTRY_INFO");
        queryString.append(" WHERE COUNTRY_NAME ='");
        queryString.append((param.get("COUNTRY_NAME") == null ? "" : param.get("COUNTRY_NAME").toString()));
        queryString.append("' ORDER BY SATE_REGION ASC");

        return queryString.toString();
    }

    /**
     * Method: Country State query
     * Parameter: HashMap (Country code, state name)
     * Return: Query string
     */
    public static String countryStateCityQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT DISTINCT CITY_NAME FROM FTN_COUNTRY_INFO");
        queryString.append(" WHERE COUNTRY_CODE ='");
        queryString.append((param.get("COUNTRY_CODE") == null ? "" : param.get("COUNTRY_CODE").toString()));
        queryString.append("' AND SATE_REGION ='");
        queryString.append((param.get("SATE_REGION") == null ? "" : param.get("SATE_REGION").toString()));
        queryString.append("' ORDER BY CITY_NAME ASC");

        return queryString.toString();
    }

    /**
     * Method: Country State query
     * Parameter: HashMap (Country name, state name)
     * Return: Query string
     */
    public static String countryStateCityQuery2(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT DISTINCT CITY_NAME FROM FTN_COUNTRY_INFO");
        queryString.append(" WHERE COUNTRY_NAME ='");
        queryString.append((param.get("COUNTRY_NAME") == null ? "" : param.get("COUNTRY_NAME").toString()));
        queryString.append("' AND SATE_REGION ='");
        queryString.append((param.get("SATE_REGION") == null ? "" : param.get("SATE_REGION").toString()));
        queryString.append("' ORDER BY CITY_NAME ASC");

        return queryString.toString();
    }

    /**
     * Method: Search Customer query
     * Parameter: HashMap (Customer Code, Contact Number)
     * Return: Query string
     */
    public static String searchCustomerQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT CUSTOMER_ID, CONTACT_NO, EMAIL_ADD, FIRST_NAME, MIDDLE_NAME, LAST_NAME, POSTAL_CODE,");
        queryString.append(" SURE_NAME, ADD1, ADD2, ADD3, STATE_REGION, CITY, SHIPPING_ADDRESS, GENDER, ALT_CONTACT_NO,");
        queryString.append(" TO_CHAR(DOB,'DD/MM/RRRR') DOB, MARITAL_STATUS, OCCUPATION, RELIGION, CUSTOMER_TYPE, COUNTRY_NAME");
        queryString.append(" FROM FTN_CUSTOMER_INFO WHERE");

        boolean fg = false;

        if (!(param.get("CUSTOMER_ID") == null ? "" : param.get("CUSTOMER_ID").toString()).isEmpty()) {
            queryString.append(" CUSTOMER_ID = '");
            queryString.append((param.get("CUSTOMER_ID") == null ? "" : param.get("CUSTOMER_ID").toString()));
            queryString.append("'");
            fg = true;
        }
        if (!(param.get("CONTACT_NO") == null ? "" : param.get("CONTACT_NO").toString()).isEmpty()) {
            if (fg) {
                queryString.append(" AND");
            }
            queryString.append(" CONTACT_NO = '");
            queryString.append((param.get("CONTACT_NO") == null ? "" : param.get("CONTACT_NO").toString()));
            queryString.append("'");
        }

        return queryString.toString();
    }

    /**
     * Method: Search Article query
     * Parameter: HashMap (Article Code)
     * Return: Query string
     */
    public static String searchArticleQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT A.ART_CODE, A.ART_NAME, A.SUB_CAT_CODE, A.SUB_CAT_NAME,");
        queryString.append(" A.CAT_CODE, A.CAT_NAME, A.BRAND_CODE, A.BRAND_NAME,");
        queryString.append(" A.SIZE_CODE, NVL(A.MRP,0) MRP, NVL(A.COST_PRICE,0) COST_PRICE, NVL(A.SALEPRICE,0) SALEPRICE, NVL(A.VAT,0) VAT, NVL(A.NET_PRICE,0) NET_PRICE,");
        queryString.append(" A.IMGE_URL_1, A.IMGE_URL_2, A.IMGE_URL_3,");
        queryString.append(" B.R1, B.R2, B.R3, B.R4, B.R5, B.R6, B.R7, B.R8, B.R9, B.R10, B.R11, B.R12, B.R13, B.PAIRS,");
        queryString.append(" C.R1 S1, C.R2 S2, C.R3 S3, C.R4 S4, C.R5 S5, C.R6 S6, C.R7 S7, C.R8 S8, C.R9 S9, C.R10 S10, C.R11 S11, C.R12 S12, C.R13 S13");
        queryString.append(" FROM FTN_ART_MAST A, FTN_ART_STOCK B, FTN_SIZE_RANGE C");
        queryString.append(" WHERE A.ART_CODE = B.ART_CODE(+)");
        queryString.append(" AND A.SIZE_CODE = C.SIZE_CODE(+)");
        queryString.append(" AND A.ART_CODE = '");
        queryString.append(param.get("ART_CODE").toString());
        queryString.append("'");

        return queryString.toString();
    }

    /**
     * Method: Customer Temp Order Article query
     * Parameter: HashMap (Customer Contact Number)
     * Return: Query string
     */
    public static String customerTempOrderArticleQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT ART_CODE, ART_DESCRIPTION, ART_SIZE, QTY, MRP, DISCOUNT, SUB_TOTAL, INSERT_DATE,");
        queryString.append(" NVL((SELECT  SUM(SUB_TOTAL)  FROM FTN_TEMP_CUSTOMER_ORDER WHERE CONTACT_NO='");
        queryString.append((param.get("CONTACT_NO") == null ? "" : param.get("CONTACT_NO").toString()));
        queryString.append("'),0) TOTAL_SUB_TOT,");
        queryString.append(" NVL((SELECT SUM(DISCOUNT) FROM FTN_TEMP_CUSTOMER_ORDER WHERE CONTACT_NO='");
        queryString.append((param.get("CONTACT_NO") == null ? "" : param.get("CONTACT_NO").toString()));
        queryString.append("'),0) TOTAL_DISCOUNT");
        queryString.append(" FROM FTN_TEMP_CUSTOMER_ORDER WHERE CONTACT_NO='");
        queryString.append((param.get("CONTACT_NO") == null ? "" : param.get("CONTACT_NO").toString()));
        queryString.append("' ORDER BY INSERT_DATE DESC");


        return queryString.toString();
    }

    /**
     * Method: Customer Temp Order Article Insert query
     * Parameter: HashMap (Customer Contact Number, promotion id, art, art description, size, qty, mrp, discount, subtotal)
     * Return: Query string
     */
    public static String customerTempOrderArticleInsertQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("INSERT INTO FTN_TEMP_CUSTOMER_ORDER (");
        queryString.append("CONTACT_NO, PROMO_ID, ART_CODE, SIZE_CODE, ART_DESCRIPTION, ART_SIZE, QTY, MRP, DISCOUNT,  SUB_TOTAL, INSERT_BY) VALUES ('");
        queryString.append((param.get("CONTACT_NO") == null ? "" : param.get("CONTACT_NO").toString()));
        queryString.append("','");
        queryString.append((param.get("PROMO_ID") == null ? "" : param.get("PROMO_ID").toString()));
        queryString.append("','");
        queryString.append((param.get("ART_CODE") == null ? "" : param.get("ART_CODE").toString()));
        queryString.append("','");
        queryString.append((param.get("SIZE_CODE") == null ? "" : param.get("SIZE_CODE").toString()));
        queryString.append("','");
        queryString.append((param.get("ART_DESCRIPTION") == null ? "" : param.get("ART_DESCRIPTION").toString()));
        queryString.append("','");
        queryString.append((param.get("ART_SIZE") == null ? "" : param.get("ART_SIZE").toString()));
        queryString.append("',");
        queryString.append((param.get("QTY") == null ? "" : param.get("QTY").toString()));
        queryString.append(",");
        queryString.append((param.get("MRP") == null ? "" : param.get("MRP").toString()));
        queryString.append(",");
        queryString.append((param.get("DISCOUNT") == null ? "" : param.get("DISCOUNT").toString()));
        queryString.append(",");
        queryString.append((param.get("SUB_TOTAL") == null ? "" : param.get("SUB_TOTAL").toString()));
        queryString.append(",'");
        queryString.append((param.get("INSERT_BY") == null ? "" : param.get("INSERT_BY").toString()));
        queryString.append("')");

        return queryString.toString();
    }

    /**
     * Method: Customer Temp Order Article Update query
     * Parameter: HashMap (Customer Contact Number, art, art description, size, qty, mrp, discount, subtotal)
     * Return: Query string
     */
    public static String customerTempOrderArticleUpdateQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("UPDATE FTN_TEMP_CUSTOMER_ORDER SET");
        queryString.append(" PROMO_ID = '");
        queryString.append((param.get("PROMO_ID") == null ? "" : param.get("PROMO_ID").toString()));
        queryString.append("', QTY = (NVL(QTY,0) + NVL(");
        queryString.append((param.get("QTY") == null ? "0" : param.get("QTY").toString()));
        queryString.append(", 0)), DISCOUNT = (NVL(DISCOUNT,0) + NVL(");
        queryString.append((param.get("DISCOUNT") == null ? "0" : param.get("DISCOUNT").toString()));
        queryString.append(",0)), SUB_TOTAL = (NVL(SUB_TOTAL,0) + NVL(");
        queryString.append((param.get("SUB_TOTAL") == null ? "0" : param.get("SUB_TOTAL").toString()));
        queryString.append(", 0)) WHERE CONTACT_NO = '");
        queryString.append((param.get("CONTACT_NO") == null ? "" : param.get("CONTACT_NO").toString()));
        queryString.append("' AND ART_CODE = '");
        queryString.append((param.get("ART_CODE") == null ? "" : param.get("ART_CODE").toString()));
        queryString.append("' AND SIZE_CODE = '");
        queryString.append((param.get("SIZE_CODE") == null ? "" : param.get("SIZE_CODE").toString()));
        queryString.append("' AND ART_SIZE = '");
        queryString.append((param.get("ART_SIZE") == null ? "" : param.get("ART_SIZE").toString()));
        queryString.append("'");

        return queryString.toString();
    }

    /**
     * Method: Customer Temp Order Article Update query
     * Parameter: HashMap (Customer Contact Number, article code)
     * Return: Query string
     */
    public static String customerTempOrderArticleDeleteQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("DELETE FROM FTN_TEMP_CUSTOMER_ORDER");
        queryString.append(" WHERE CONTACT_NO = '");
        queryString.append((param.get("CONTACT_NO") == null ? "" : param.get("CONTACT_NO").toString()));
        queryString.append("' AND ART_CODE = '");
        queryString.append((param.get("ART_CODE") == null ? "" : param.get("ART_CODE").toString()));
        queryString.append("' AND ART_SIZE = '");
        queryString.append((param.get("ART_SIZE") == null ? "" : param.get("ART_SIZE").toString()));
        queryString.append("'");

        return queryString.toString();
    }

    /**
     * Method: Customer Temp Order Article Update query
     * Parameter: HashMap (Customer Contact Number, article code)
     * Return: Query string
     */
    public static String customerTempOrderArticleDelete2Query(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("DELETE FROM FTN_TEMP_CUSTOMER_ORDER");
        queryString.append(" WHERE CONTACT_NO = '");
        queryString.append((param.get("CONTACT_NO") == null ? "" : param.get("CONTACT_NO").toString()));
        queryString.append("'");

        return queryString.toString();
    }

    /**
     * Method: Country State query
     * Parameter: HashMap (Country name)
     * Return: Query string
     */
    public static String articlePromotionQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT DISTINCT PROMO_ID, PROMO_NAME, ART_CODE, MRP, DISCOUNT_AMOUNT,");
        queryString.append(" TO_CHAR(START_DATE,'DD/MM/RRRR') START_DATE,  TO_CHAR(START_TIME) START_TIME,");
        queryString.append(" TO_CHAR(END_DATE,'DD/MM/RRRR') END_DATE, TO_CHAR(END_TIME) END_TIME");
        queryString.append(" FROM FTN_PROMOTION WHERE ART_CODE ='");
        queryString.append((param.get("ART_CODE") == null ? "" : param.get("ART_CODE").toString()));
        queryString.append("' AND TO_DATE(SYSDATE,'DD/MM/RRRR') BETWEEN TO_DATE(START_DATE,'DD/MM/RRRR')  AND TO_DATE(END_DATE,'DD/MM/RRRR')");

        return queryString.toString();
    }

    /**
     * Method: Customer Temp Order Article Update query
     * Parameter: HashMap (Customer Contact Number, art, art description, size, qty, mrp, discount, subtotal)
     * Return: Query string
     */
    public static String customerUpdateQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("UPDATE FTN_CUSTOMER_INFO SET");
        queryString.append(" CONTACT_NO = '");
        queryString.append((param.get("CONTACT_NO") == null ? "" : param.get("CONTACT_NO").toString()));
        queryString.append("'");
        //
        queryString.append(", EMAIL_ADD = '");
        queryString.append((param.get("EMAIL_ADD") == null ? "" : param.get("EMAIL_ADD").toString()));
        queryString.append("'");
        //
        queryString.append(", FIRST_NAME = '");
        queryString.append((param.get("FIRST_NAME") == null ? "" : param.get("FIRST_NAME").toString()));
        queryString.append("'");
        //
        queryString.append(", MIDDLE_NAME = '");
        queryString.append((param.get("MIDDLE_NAME") == null ? "" : param.get("MIDDLE_NAME").toString()));
        queryString.append("'");
        //
        queryString.append(", LAST_NAME = '");
        queryString.append((param.get("LAST_NAME") == null ? "" : param.get("LAST_NAME").toString()));
        queryString.append("'");
        //
        queryString.append(", SURE_NAME = '");
        queryString.append((param.get("SURE_NAME") == null ? "" : param.get("SURE_NAME").toString()));
        queryString.append("'");
        //
        queryString.append(", COUNTRY_NAME = '");
        queryString.append((param.get("COUNTRY_NAME") == null ? "" : param.get("COUNTRY_NAME").toString()));
        queryString.append("'");
        //
        queryString.append(", STATE_REGION = '");
        queryString.append((param.get("STATE_REGION") == null ? "" : param.get("STATE_REGION").toString()));
        queryString.append("'");
        //
        queryString.append(", CITY = '");
        queryString.append((param.get("CITY") == null ? "" : param.get("CITY").toString()));
        queryString.append("'");
        //
        queryString.append(", POSTAL_CODE = '");
        queryString.append((param.get("POSTAL_CODE") == null ? "" : param.get("POSTAL_CODE").toString()));
        queryString.append("'");
        //
        queryString.append(", ADD1 = '");
        queryString.append((param.get("ADD1") == null ? "" : param.get("ADD1").toString()));
        queryString.append("'");
        //
        queryString.append(", SHIPPING_ADDRESS = '");
        queryString.append((param.get("SHIPPING_ADDRESS") == null ? "" : param.get("SHIPPING_ADDRESS").toString()));
        queryString.append("'");
        //
        queryString.append(", DOB = TO_DATE('");
        queryString.append((param.get("DOB") == null ? "" : param.get("DOB").toString()));
        queryString.append("','DD/MM/RRRR')");
        //
        queryString.append(", CUSTOMER_TYPE = '");
        queryString.append((param.get("CUSTOMER_TYPE") == null ? "Standard" : param.get("CUSTOMER_TYPE").toString()));
        queryString.append("'");
        //
        queryString.append(", GENDER = '");
        queryString.append((param.get("GENDER") == null ? "" : param.get("GENDER").toString()));
        queryString.append("'");
        //
        queryString.append(", RELIGION = '");
        queryString.append((param.get("RELIGION") == null ? "" : param.get("RELIGION").toString()));
        queryString.append("'");
        // 
        queryString.append(", MARITAL_STATUS = '");
        queryString.append((param.get("MARITAL_STATUS") == null ? "" : param.get("MARITAL_STATUS").toString()));
        queryString.append("'");
        //
        queryString.append(", OCCUPATION = '");
        queryString.append((param.get("OCCUPATION") == null ? "" : param.get("OCCUPATION").toString()));
        queryString.append("'");
        //
        queryString.append(", UPDATE_BY = '");
        queryString.append((param.get("UPDATE_BY") == null ? "" : param.get("UPDATE_BY").toString()));
        queryString.append("'");
        //
        queryString.append(", UPDATE_DATE = SYSDATE");
        //
        queryString.append(" WHERE CUSTOMER_ID = '");
        queryString.append((param.get("CUSTOMER_ID") == null ? "" : param.get("CUSTOMER_ID").toString()));
        queryString.append("'");

        return queryString.toString();

    }

    /**
     * Method: Customer pk query
     * Parameter: HashMap (Contact Number)
     * Return: Query string
     */
    public static String customerPkQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT LPAD(MAX(TO_NUMBER(SUBSTR(CUSTOMER_ID,9,2))) + 1, 2, 0) CUSTOMER_ID");
        queryString.append(" FROM FTN_CUSTOMER_INFO WHERE SUBSTR(CUSTOMER_ID, 0, 8) = SUBSTR('");
        queryString.append((param.get("CONTACT_NO") == null ? "" : param.get("CONTACT_NO").toString()));
        queryString.append("', -6)||SUBSTR('");
        queryString.append((param.get("CONTACT_NO") == null ? "" : param.get("CONTACT_NO").toString()));
        queryString.append("', 2,2)");

        return queryString.toString();
    }

    /**
     * Method: Customer Temp Order Article Insert query
     * Parameter: HashMap (Customer information)
     * Return: Query string
     */
    public static String customerInsertQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("INSERT INTO FTN_CUSTOMER_INFO (");
        queryString.append("CUSTOMER_ID, CONTACT_NO, EMAIL_ADD, FIRST_NAME, MIDDLE_NAME, LAST_NAME,");
        queryString.append("SURE_NAME, COUNTRY_NAME, STATE_REGION, CITY,  POSTAL_CODE, ADD1, SHIPPING_ADDRESS,");
        queryString.append("DOB, CUSTOMER_TYPE, GENDER, RELIGION, MARITAL_STATUS, OCCUPATION, INSERT_BY) VALUES ('");
        queryString.append((param.get("CUSTOMER_ID") == null ? "" : param.get("CUSTOMER_ID").toString()));
        queryString.append("','");
        queryString.append((param.get("CONTACT_NO") == null ? "" : param.get("CONTACT_NO").toString()));
        queryString.append("','");
        queryString.append((param.get("EMAIL_ADD") == null ? "" : param.get("EMAIL_ADD").toString()));
        queryString.append("','");
        queryString.append((param.get("FIRST_NAME") == null ? "" : param.get("FIRST_NAME").toString()));
        queryString.append("','");
        queryString.append((param.get("MIDDLE_NAME") == null ? "" : param.get("MIDDLE_NAME").toString()));
        queryString.append("','");
        queryString.append((param.get("LAST_NAME") == null ? "" : param.get("LAST_NAME").toString()));
        queryString.append("','");
        queryString.append((param.get("SURE_NAME") == null ? "" : param.get("SURE_NAME").toString()));
        queryString.append("','");
        queryString.append((param.get("COUNTRY_NAME") == null ? "" : param.get("COUNTRY_NAME").toString()));
        queryString.append("','");
        queryString.append((param.get("STATE_REGION") == null ? "" : param.get("STATE_REGION").toString()));
        queryString.append("','");
        queryString.append((param.get("CITY") == null ? "" : param.get("CITY").toString()));
        queryString.append("','");
        queryString.append((param.get("POSTAL_CODE") == null ? "" : param.get("POSTAL_CODE").toString()));
        queryString.append("','");
        queryString.append((param.get("ADD1") == null ? "" : param.get("ADD1").toString()));
        queryString.append("','");
        queryString.append((param.get("SHIPPING_ADDRESS") == null ? "" : param.get("SHIPPING_ADDRESS").toString()));
        queryString.append("',TO_DATE('");
        queryString.append((param.get("DOB") == null ? "" : param.get("DOB").toString()));
        queryString.append("','DD/MM/RRRR'),'");
        queryString.append((param.get("CUSTOMER_TYPE") == null ? "Standard" : param.get("CUSTOMER_TYPE").toString()));
        queryString.append("','");
        queryString.append((param.get("GENDER") == null ? "" : param.get("GENDER").toString()));
        queryString.append("','");
        queryString.append((param.get("RELIGION") == null ? "" : param.get("RELIGION").toString()));
        queryString.append("','");
        queryString.append((param.get("MARITAL_STATUS") == null ? "" : param.get("MARITAL_STATUS").toString()));
        queryString.append("','");
        queryString.append((param.get("OCCUPATION") == null ? "" : param.get("OCCUPATION").toString()));
        queryString.append("','");
        queryString.append((param.get("INSERT_BY") == null ? "" : param.get("INSERT_BY").toString()));
        queryString.append("')");

        return queryString.toString();
    }

    /**
     * Method: Order pk query
     * Parameter: HashMap (Contact Number)
     * Return: Query string
     */
    public static String orderPkQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT LPAD(MAX(TO_NUMBER(SUBSTR(ORDER_NO,9,4))) + 1, 4, 0) ORDER_NO");
        queryString.append(" FROM FTN_ORDER_MST WHERE STORE_CODE = '");
        queryString.append(param.get("STORE_CODE").toString());
        queryString.append("' AND TO_DATE(ORDER_DATE,'DD/MM/RRRR') = TO_DATE('");
        queryString.append(param.get("ORDER_DATE").toString());
        queryString.append("','DD/MM/RRRR')");

        return queryString.toString();
    }

    /**
     * Method: Order master query
     * Parameter: HashMap
     * Return: Query string
     */
    public static String orderMasterQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("INSERT INTO FTN_ORDER_MST (");
        queryString.append(" STORE_CODE, ORDER_DATE, DELIVERY_DATE, ORDER_TIME, F_YEAR, WEEK_NO, ORDER_NO, CUSTOMER_ID,");
        if (param.get("ORDER_TO_CDC_STATUS").toString().equalsIgnoreCase("Y")) {
            queryString.append(" ORDER_TO_CDC_STATUS, ORDER_TO_CDC_DATE,");
        }
        queryString.append(" SP_DISCOUNT, PAYMENT_MODE, GROSS_AMOUNT, PROMO_DISCOUNT, NET_PAYMENT,");
        queryString.append(" CONTACT_NO, SALES_PCODE, USER_ID, BIN) VALUES ('");
        queryString.append((param.get("STORE_CODE") == null ? "" : param.get("STORE_CODE").toString()));
        queryString.append("', TO_DATE('");
        queryString.append((param.get("ORDER_DATE") == null ? "" : param.get("ORDER_DATE").toString().replaceAll("-", "/")));
        queryString.append("','dd/MM/rrrr'),");
        queryString.append(" TO_DATE('");
        queryString.append((param.get("ORDER_DATE") == null ? "" : param.get("ORDER_DATE").toString().replaceAll("-", "/")));
        queryString.append("','dd/MM/rrrr'), TO_NUMBER(TO_CHAR(SYSDATE, 'HH24')),");
        queryString.append((param.get("F_YEAR") == null ? "" : param.get("F_YEAR").toString()));
        queryString.append(",'");
        queryString.append((param.get("WEEK_NO") == null ? "" : param.get("WEEK_NO").toString()));
        queryString.append("','");
        queryString.append((param.get("ORDER_NO") == null ? "" : param.get("ORDER_NO").toString()));
        queryString.append("','");
        queryString.append((param.get("CUSTOMER_ID") == null ? "" : param.get("CUSTOMER_ID").toString()));
        queryString.append("'");
        if (param.get("ORDER_TO_CDC_STATUS").toString().equalsIgnoreCase("Y")) {
            queryString.append(",'");
            queryString.append((param.get("ORDER_TO_CDC_STATUS") == null ? "" : param.get("ORDER_TO_CDC_STATUS").toString()));
            queryString.append("',SYSDATE");
        }
        queryString.append(",");
        queryString.append((param.get("SP_DISCOUNT") == null ? "" : param.get("SP_DISCOUNT").toString()));
        queryString.append(",'");
        queryString.append((param.get("PAYMENT_MODE") == null ? "" : param.get("PAYMENT_MODE").toString()));
        queryString.append("',");
        // Total Gross from tem order table
        queryString.append("(SELECT NVL(SUM(NVL(QTY,0)*NVL(MRP,0)),0) GROSS_TOTAL FROM FTN_TEMP_CUSTOMER_ORDER WHERE CONTACT_NO ='");
        queryString.append((param.get("CONTACT_NO") == null ? "" : param.get("CONTACT_NO").toString()));
        queryString.append("'),");
        // Total promo discount from tem order table
        queryString.append("(SELECT NVL(SUM(DISCOUNT),0) DISCOUNT FROM FTN_TEMP_CUSTOMER_ORDER WHERE CONTACT_NO ='");
        queryString.append((param.get("CONTACT_NO") == null ? "" : param.get("CONTACT_NO").toString()));
        queryString.append("'),");
        queryString.append("(SELECT NVL((SUM(NVL(QTY,0)*NVL(MRP,0))-(SUM(DISCOUNT)+ NVL(");
        queryString.append((param.get("SP_DISCOUNT") == null ? "0" : param.get("SP_DISCOUNT").toString()));
        queryString.append(",0))),0) NET_TOTAL FROM FTN_TEMP_CUSTOMER_ORDER WHERE CONTACT_NO ='");
        queryString.append((param.get("CONTACT_NO") == null ? "" : param.get("CONTACT_NO").toString()));
        queryString.append("')");
        queryString.append(",'");
        queryString.append((param.get("CONTACT_NO") == null ? "" : param.get("CONTACT_NO").toString()));
        queryString.append("','");
        queryString.append((param.get("SALES_PCODE") == null ? "" : param.get("SALES_PCODE").toString()));
        queryString.append("','");
        queryString.append((param.get("USER_ID") == null ? "" : param.get("USER_ID").toString()));
        queryString.append("', (SELECT DISTINCT BIN FROM FTN_COMPANY WHERE COM_CODE ='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("'))");

        return queryString.toString();
    }

    /**
     * Method: Order child query
     * Parameter: HashMap
     * Return: Query string
     */
    public static String orderChildInsertQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("INSERT INTO FTN_ORDER_CHD (");
        queryString.append(" ORDER_NO, PROMO_ID, ART_CODE, SIZE_CODE, UNIT_PRICE, COST_PRICE, VAT_AMNT, NET_PRICE,");
        queryString.append(" TOTAL_DISCOUNT, GROSS_VALUE, SP_DISCOUNT, GROSS_VALUE_AFTER_SP,");
        queryString.append(" ORDER_PAIRS, R1, R2, R3, R4, R5, R6, R7, R8, R9, R10, R11, R12, R13)");
        queryString.append("SELECT '");
        queryString.append((param.get("ORDER_NO") == null ? "" : param.get("ORDER_NO").toString()));
        queryString.append("' ORDER_NO, A.PROMO_ID, A.ART_CODE, A.SIZE_CODE,");
        queryString.append(" A.UNIT_PRICE, B.COST_PRICE, B.VAT VAT_AMNT, B.NET_PRICE,");
        queryString.append(" A.TOTAL_DISCOUNT, A.GROSS_VALUE,");
        queryString.append(" CASE WHEN ");
        queryString.append((param.get("SP_DISCOUNT") == null ? "" : param.get("SP_DISCOUNT").toString()));
        queryString.append(" > 0 AND  C.TOTAL_SUB_TOTAL > 0 THEN ROUND((");
        queryString.append((param.get("SP_DISCOUNT") == null ? "" : param.get("SP_DISCOUNT").toString()));
        queryString.append("/C.TOTAL_SUB_TOTAL) * A.GROSS_VALUE) ELSE 0 END  SP_DISCOUNT,");
        queryString.append("CASE WHEN ");
        queryString.append((param.get("SP_DISCOUNT") == null ? "" : param.get("SP_DISCOUNT").toString()));
        queryString.append(" > 0 AND  C.TOTAL_SUB_TOTAL > 0 THEN (A.GROSS_VALUE - ROUND((");
        queryString.append((param.get("SP_DISCOUNT") == null ? "" : param.get("SP_DISCOUNT").toString()));
        queryString.append("/C.TOTAL_SUB_TOTAL) * A.GROSS_VALUE)) ELSE A.GROSS_VALUE END GROSS_VALUE_AFTER_SP,");
        queryString.append(" A.ORDER_PAIRS, A.R1, A.R2, A.R3, A.R4, A.R5, A.R6, A.R7, A.R8, A.R9, A.R10, A.R11, A.R12, A.R13");
        queryString.append(" FROM (SELECT PROMO_ID, ART_CODE, SIZE_CODE, UNIT_PRICE,  SUM(DISCOUNT) TOTAL_DISCOUNT, SUM(SUB_TOTAL) GROSS_VALUE,");
        queryString.append(" NVL(SUM(S1),0) R1, NVL(SUM(S2),0) R2, NVL(SUM(S3),0) R3, NVL(SUM(S4),0) R4, NVL(SUM(S5),0) R5, NVL(SUM(S6),0) R6, NVL(SUM(S7),0) R7,");
        queryString.append(" NVL(SUM(S8),0) R8, NVL(SUM(S9),0) R9, NVL(SUM(S10),0) R10, NVL(SUM(S11),0) R11, NVL(SUM(S12),0) R12, NVL(SUM(S13),0) R13,");
        queryString.append(" (NVL(SUM(S1),0) + NVL(SUM(S2),0) + NVL(SUM(S3),0) + NVL(SUM(S4),0) + NVL(SUM(S5),0) + NVL(SUM(S6),0) + NVL(SUM(S7),0) +");
        queryString.append(" NVL(SUM(S8),0) + NVL(SUM(S9),0) + NVL(SUM(S10),0) + NVL(SUM(S11),0) + NVL(SUM(S12),0) + NVL(SUM(S13),0) ) ORDER_PAIRS");
        queryString.append(" FROM");
        queryString.append(" (");
        queryString.append(" SELECT PROMO_ID, ART_CODE, SIZE_CODE, MRP UNIT_PRICE, DISCOUNT, SUB_TOTAL,");
        queryString.append(" CASE WHEN SIZE_CODE = 'C' AND ART_SIZE = '1' THEN QTY WHEN SIZE_CODE = 'D' AND ART_SIZE = '5' THEN QTY WHEN SIZE_CODE = 'E' AND ART_SIZE IN ('9') THEN QTY WHEN SIZE_CODE IS NULL THEN QTY END S1,");
        queryString.append(" CASE WHEN SIZE_CODE = 'A' AND ART_SIZE = '2' THEN QTY WHEN SIZE_CODE = 'B' AND ART_SIZE = '9' THEN QTY WHEN SIZE_CODE = 'C' AND ART_SIZE = '2' THEN QTY END S2,");
        queryString.append(" CASE WHEN SIZE_CODE = 'A' AND ART_SIZE = '3' THEN QTY WHEN SIZE_CODE = 'D' AND ART_SIZE = '6' THEN QTY END S3,");
        queryString.append(" CASE WHEN SIZE_CODE = 'A' AND ART_SIZE = '4' THEN QTY WHEN SIZE_CODE = 'B' AND ART_SIZE = '10' THEN QTY WHEN SIZE_CODE = 'C' AND ART_SIZE = '3' THEN QTY END S4,");
        queryString.append(" CASE WHEN SIZE_CODE = 'A' AND ART_SIZE = '5' THEN QTY WHEN SIZE_CODE = 'D' AND ART_SIZE = '7' THEN QTY END S5,");
        queryString.append(" CASE WHEN SIZE_CODE = 'B' AND ART_SIZE = '11' THEN QTY WHEN SIZE_CODE = 'C' AND ART_SIZE = '4' THEN QTY END S6,");
        queryString.append(" CASE WHEN SIZE_CODE = 'A' AND ART_SIZE = '6' THEN QTY WHEN SIZE_CODE = 'D' AND ART_SIZE = '8' THEN QTY END S7,");
        queryString.append(" CASE WHEN SIZE_CODE = 'B' AND ART_SIZE = '12' THEN QTY WHEN SIZE_CODE = 'C' AND ART_SIZE = '5' THEN QTY END S8,");
        queryString.append(" CASE WHEN SIZE_CODE = 'A' AND ART_SIZE = '7' THEN QTY WHEN SIZE_CODE = 'D' AND ART_SIZE = '9' THEN QTY END S9,");
        queryString.append(" CASE WHEN SIZE_CODE = 'B' AND ART_SIZE = '13' THEN QTY WHEN SIZE_CODE = 'C' AND ART_SIZE = '6' THEN QTY END S10,");
        queryString.append(" CASE WHEN SIZE_CODE = 'A' AND ART_SIZE = '8' THEN QTY WHEN SIZE_CODE = 'D' AND ART_SIZE = '10' THEN QTY END S11,");
        queryString.append(" CASE WHEN SIZE_CODE = 'B' AND ART_SIZE = '10' THEN QTY WHEN SIZE_CODE = 'C' AND ART_SIZE = '7' THEN QTY END S12,");
        queryString.append(" CASE WHEN SIZE_CODE = 'A' AND ART_SIZE = '9' THEN QTY WHEN SIZE_CODE = 'B' AND ART_SIZE = '2' THEN QTY WHEN SIZE_CODE = 'C' AND ART_SIZE = '8' THEN QTY WHEN SIZE_CODE = 'D' AND ART_SIZE = '11' THEN QTY END S13");
        queryString.append(" FROM  FTN_TEMP_CUSTOMER_ORDER");
        queryString.append(" WHERE  CONTACT_NO ='");
        queryString.append((param.get("CONTACT_NO") == null ? "" : param.get("CONTACT_NO").toString()));
        queryString.append("')");
        queryString.append(" GROUP BY PROMO_ID, ART_CODE, SIZE_CODE, UNIT_PRICE");
        queryString.append(" ) A, FTN_ART_MAST B,");
        queryString.append(" (SELECT SUM(SUB_TOTAL) TOTAL_SUB_TOTAL FROM  FTN_TEMP_CUSTOMER_ORDER WHERE CONTACT_NO ='");
        queryString.append((param.get("CONTACT_NO") == null ? "" : param.get("CONTACT_NO").toString()));
        queryString.append("') C WHERE A.ART_CODE = B.ART_CODE");

        return queryString.toString();
    }

    /**
     * Method: Year week date query
     * Parameter: HashMap
     * Return: Query string
     */
    public static String yearWeekQuery(String dateString) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT F_YEAR, WEEK_NO, TO_CHAR(WEEK_DATE, 'DD/MM/RRRR') WEEK_DATE");
        queryString.append(" FROM FTN_WEEK WHERE TO_DATE(WEEK_DATE, 'DD/MM/RRRR') = TO_DATE('");
        queryString.append(dateString.replaceAll("-", "/"));
        queryString.append("', 'DD/MM/RRRR')");

        return queryString.toString();
    }

    /**
     * Method: All order select query
     * Return: Query string
     */
    public static String ftnOrderQuery() {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT STORE_CODE, ORDER_DATE, F_YEAR, WEEK_NO, ORDER_NO, CUSTOMER_ID, CONTACT_NO,");
        queryString.append(" SALES_PCODE, DELIVERY_DATE, ORDER_TIME, SHIPMENT_DATE, ORDER_SOURCE, ORDER_STATUS,");
        queryString.append(" ENTRY_DATE, USER_ID, IP_ADDRESS, ORDER_TO_CDC_STATUS, ORDER_TO_CDC_DATE,");
        queryString.append(" CDC_PACKING_STATUS, CDC_PACKING_DATE, DLV_TO_CARRY_CON_STATUS, CANCEL_STATUS,");
        queryString.append(" DLV_TO_CARRY_CON_DATE, CUST_RCV_STATUS, CUST_RCV_RET_DATE, VAT_INV_NO, INV_STATUS,");
        queryString.append(" PAYMENT_STATUS, PAYMENT_AMOUNT, PAYMENT_DATE, VAT_STATUS, VAT_DATE, INV_NO, INV_DATE");
        queryString.append(" FROM FTN_ORDER_MST WHERE TO_DATE(ORDER_DATE, 'DD/MM/RRRR') BETWEEN TO_DATE(SYSDATE - 30, 'DD/MM/RRRR')");
        queryString.append(" AND TO_DATE(SYSDATE, 'DD/MM/RRRR') ORDER BY ORDER_NO DESC");


        return queryString.toString();
    }

    /**
     * Method: CDC Status Update query
     * Parameter: HashMap (Customer Contact Number, article code)
     * Return: Query string
     */
    public static String cdcStatusUpdateQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("UPDATE FTN_ORDER_MST SET");
        queryString.append(" ORDER_TO_CDC_STATUS = '");
        queryString.append((param.get("ORDER_TO_CDC_STATUS") == null ? "" : param.get("ORDER_TO_CDC_STATUS").toString()));
        queryString.append("', ORDER_TO_CDC_DATE = SYSDATE");
        queryString.append(" WHERE ORDER_NO = '");
        queryString.append((param.get("ORDER_NO") == null ? "" : param.get("ORDER_NO").toString()));
        queryString.append("'");

        return queryString.toString();
    }

    /**
     * Method: Invoice pk query
     * Parameter: HashMap (Contact Number)
     * Return: Query string
     */
    public static String invoicePkQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT LPAD(MAX(TO_NUMBER(SUBSTR(INV_NO,9,4))) + 1, 4, 0) INVOICE_NO");
        queryString.append(" FROM FTN_ORDER_MST WHERE STORE_CODE = '");
        queryString.append((param.get("STORE_CODE") == null ? "" : param.get("STORE_CODE").toString()));
        queryString.append("' AND TO_DATE(INV_DATE,'DD/MM/RRRR') = TO_DATE(SYSDATE,'DD/MM/RRRR')");

        return queryString.toString();
    }

    public static String invoiceUpdateQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("UPDATE FTN_ORDER_MST SET");
        queryString.append(" USER_ID = '");
        queryString.append(param.get("USER_ID").toString());
        queryString.append("',");
        queryString.append("VAT_DATE = TO_DATE('");
        queryString.append(param.get("VAT_DATE").toString());
        queryString.append("','DD/MM/RRRR'), VAT_STATUS = 'Y',");
        queryString.append("VAT_INV_NO = '");
        queryString.append(param.get("VAT_INV_NO").toString());
        queryString.append("',");
        queryString.append("AWB = '");
        queryString.append(param.get("AWB").toString());
        queryString.append("',");
        queryString.append("INV_NO = '");
        queryString.append(param.get("INV_NO").toString());
        queryString.append("', INV_DATE = SYSDATE, INV_STATUS ='Y'");
        queryString.append(" WHERE ORDER_NO = '");
        queryString.append(param.get("ORDER_NO").toString());
        queryString.append("'");

        return queryString.toString();
    }

    public static String customerDetailsQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT TO_CHAR(A.ORDER_DATE, 'dd/MM/rrrr') ORDER_DATE, A.ORDER_NO, A.CUSTOMER_ID, A.CONTACT_NO, TO_CHAR(A.DELIVERY_DATE, 'dd/MM/rrrr') DELIVERY_DATE, A.ORDER_TIME,");
        queryString.append(" TO_CHAR(A.SHIPMENT_DATE, 'dd/MM/rrrr') SHIPMENT_DATE, A.VAT_INV_NO, TO_CHAR(A.VAT_DATE,'dd/MM/rrrr') VAT_DATE, A.INV_NO, TO_CHAR(A.INV_DATE,'dd/MM/rrrr') INV_DATE,");
        queryString.append(" TRIM(C.FIRST_NAME ||' ' || C.MIDDLE_NAME|| ' ' || C.LAST_NAME || ' ' || C.SURE_NAME) CUSTOMER_NAME,");
        queryString.append(" C.SHIPPING_ADDRESS, EMAIL_ADD, AWB FROM");
        queryString.append(" (SELECT ORDER_DATE, ORDER_NO, CUSTOMER_ID, CONTACT_NO, DELIVERY_DATE, ORDER_TIME,");
        queryString.append(" SHIPMENT_DATE, VAT_INV_NO, VAT_DATE, INV_NO, INV_DATE, AWB");
        queryString.append(" FROM FTN_ORDER_MST WHERE ORDER_NO = '");
        queryString.append((param.get("ORDER_NO") == null ? "" : param.get("ORDER_NO").toString()));
        queryString.append("') A, FTN_CUSTOMER_INFO C WHERE  A.CUSTOMER_ID = C.CUSTOMER_ID");

        return queryString.toString();
    }

    public static String customerOrderArtQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT ART_CODE,  '' ART_DESCRIPTION, ART_SIZE, ART_PAIR, UNIT_PRICE,");
        queryString.append(" CASE WHEN TOTAL_DISCOUNT > 0 AND ART_PAIR > 0 AND ORDER_PAIRS > 0 THEN ROUND((TOTAL_DISCOUNT / ORDER_PAIRS) * ART_PAIR) ELSE 0 END TOTAL_DISCOUNT,");
        queryString.append(" CASE WHEN GROSS_VALUE > 0 AND ART_PAIR > 0 AND ORDER_PAIRS > 0 THEN ROUND((GROSS_VALUE / ORDER_PAIRS) * ART_PAIR) ELSE 0 END GROSS_VALUE,");
        queryString.append(" CASE WHEN SP_DISCOUNT > 0 AND ART_PAIR > 0 AND ORDER_PAIRS > 0 THEN ROUND((SP_DISCOUNT / ORDER_PAIRS) * ART_PAIR) ELSE 0 END SP_DISCOUNT,");
        queryString.append(" CASE WHEN GROSS_VALUE_AFTER_SP > 0 AND ART_PAIR > 0 AND ORDER_PAIRS > 0 THEN ROUND((GROSS_VALUE_AFTER_SP / ORDER_PAIRS) * ART_PAIR) ELSE 0 END GROSS_VALUE_AFTER_SP");
        queryString.append(" FROM");
        queryString.append(" (SELECT ART_CODE, SIZE_CODE, UNIT_PRICE, ORDER_PAIRS, TOTAL_DISCOUNT, GROSS_VALUE, SP_DISCOUNT, GROSS_VALUE_AFTER_SP, ART_SIZE, SUM(ART_PAIR) ART_PAIR");
        queryString.append(" FROM");
        queryString.append(" (SELECT ART_CODE, SIZE_CODE, UNIT_PRICE, ORDER_PAIRS, TOTAL_DISCOUNT, GROSS_VALUE, SP_DISCOUNT, GROSS_VALUE_AFTER_SP,");
        queryString.append(" CASE WHEN SIZE_CODE = 'C' THEN '1' WHEN SIZE_CODE = 'D' THEN '5' WHEN SIZE_CODE = 'E' THEN '9' ELSE 'NA' END ART_SIZE, R1 ART_PAIR");
        queryString.append(" FROM FTN_ORDER_CHD WHERE ORDER_NO = '");
        queryString.append((param.get("ORDER_NO") == null ? "" : param.get("ORDER_NO").toString()));
        queryString.append("' UNION ALL");
        queryString.append(" SELECT  ART_CODE, SIZE_CODE, UNIT_PRICE, ORDER_PAIRS, TOTAL_DISCOUNT, GROSS_VALUE, SP_DISCOUNT, GROSS_VALUE_AFTER_SP,");
        queryString.append(" CASE WHEN SIZE_CODE = 'A' THEN '2' WHEN SIZE_CODE = 'B' THEN '9' WHEN SIZE_CODE = 'C' THEn '2' ELSE 'NA' END ART_SIZE, R2 ART_PAIR");
        queryString.append(" FROM FTN_ORDER_CHD WHERE ORDER_NO = '");
        queryString.append((param.get("ORDER_NO") == null ? "" : param.get("ORDER_NO").toString()));
        queryString.append("' UNION ALL");
        queryString.append(" SELECT ART_CODE, SIZE_CODE, UNIT_PRICE, ORDER_PAIRS, TOTAL_DISCOUNT, GROSS_VALUE, SP_DISCOUNT, GROSS_VALUE_AFTER_SP,");
        queryString.append(" CASE WHEN SIZE_CODE = 'A' THEN '3' WHEN SIZE_CODE = 'D' THEN '6' ELSE 'NA'  END ART_SIZE, R3 ART_PAIR");
        queryString.append(" FROM FTN_ORDER_CHD WHERE ORDER_NO = '");
        queryString.append((param.get("ORDER_NO") == null ? "" : param.get("ORDER_NO").toString()));
        queryString.append("' UNION ALL");
        queryString.append(" SELECT  ART_CODE, SIZE_CODE, UNIT_PRICE, ORDER_PAIRS, TOTAL_DISCOUNT, GROSS_VALUE, SP_DISCOUNT, GROSS_VALUE_AFTER_SP,");
        queryString.append(" CASE WHEN SIZE_CODE = 'A' THEN '4' WHEN SIZE_CODE = 'B' THEN '10'  WHEN SIZE_CODE = 'C' THEN '3'  ELSE 'NA'  END ART_SIZE, R4 ART_PAIR");
        queryString.append(" FROM FTN_ORDER_CHD WHERE ORDER_NO = '");
        queryString.append((param.get("ORDER_NO") == null ? "" : param.get("ORDER_NO").toString()));
        queryString.append("' UNION ALL");
        queryString.append(" SELECT ART_CODE, SIZE_CODE, UNIT_PRICE, ORDER_PAIRS, TOTAL_DISCOUNT, GROSS_VALUE, SP_DISCOUNT, GROSS_VALUE_AFTER_SP,");
        queryString.append(" CASE WHEN SIZE_CODE = 'A' THEN '5' WHEN SIZE_CODE = 'D' THEN '7' ELSE 'NA'  END ART_SIZE, R5 ART_PAIR");
        queryString.append(" FROM FTN_ORDER_CHD WHERE ORDER_NO = '");
        queryString.append((param.get("ORDER_NO") == null ? "" : param.get("ORDER_NO").toString()));
        queryString.append("' UNION ALL");
        queryString.append(" SELECT ART_CODE, SIZE_CODE, UNIT_PRICE, ORDER_PAIRS, TOTAL_DISCOUNT, GROSS_VALUE, SP_DISCOUNT, GROSS_VALUE_AFTER_SP,");
        queryString.append(" CASE WHEN SIZE_CODE = 'B' THEN '11'  WHEN SIZE_CODE = 'C' THEN '4' ELSE 'NA'  END ART_SIZE, R6 ART_PAIR");
        queryString.append(" FROM FTN_ORDER_CHD WHERE ORDER_NO = '");
        queryString.append((param.get("ORDER_NO") == null ? "" : param.get("ORDER_NO").toString()));
        queryString.append("' UNION ALL");
        queryString.append(" SELECT  ART_CODE, SIZE_CODE, UNIT_PRICE, ORDER_PAIRS, TOTAL_DISCOUNT, GROSS_VALUE, SP_DISCOUNT, GROSS_VALUE_AFTER_SP,");
        queryString.append(" CASE WHEN SIZE_CODE = 'A' THEN '6'  WHEN SIZE_CODE = 'D' THEN '8' ELSE 'NA'  END ART_SIZE, R7 ART_PAIR");
        queryString.append(" FROM FTN_ORDER_CHD WHERE ORDER_NO = '");
        queryString.append((param.get("ORDER_NO") == null ? "" : param.get("ORDER_NO").toString()));
        queryString.append("' UNION ALL");
        queryString.append(" SELECT ART_CODE, SIZE_CODE, UNIT_PRICE, ORDER_PAIRS, TOTAL_DISCOUNT, GROSS_VALUE, SP_DISCOUNT, GROSS_VALUE_AFTER_SP,");
        queryString.append(" CASE WHEN SIZE_CODE = 'B' THEN '12' WHEN SIZE_CODE = 'C' THEN '5' ELSE 'NA'  END ART_SIZE, R8 ART_PAIR");
        queryString.append(" FROM FTN_ORDER_CHD WHERE ORDER_NO = '");
        queryString.append((param.get("ORDER_NO") == null ? "" : param.get("ORDER_NO").toString()));
        queryString.append("' UNION ALL");
        queryString.append(" SELECT ART_CODE, SIZE_CODE, UNIT_PRICE, ORDER_PAIRS, TOTAL_DISCOUNT, GROSS_VALUE, SP_DISCOUNT, GROSS_VALUE_AFTER_SP,");
        queryString.append(" CASE WHEN SIZE_CODE = 'A' THEN '7' WHEN SIZE_CODE = 'D' THEN '9' ELSE 'NA'  END ART_SIZE, R9 ART_PAIR");
        queryString.append(" FROM FTN_ORDER_CHD WHERE ORDER_NO = '");
        queryString.append((param.get("ORDER_NO") == null ? "" : param.get("ORDER_NO").toString()));
        queryString.append("' UNION ALL");
        queryString.append(" SELECT ART_CODE, SIZE_CODE, UNIT_PRICE, ORDER_PAIRS, TOTAL_DISCOUNT, GROSS_VALUE, SP_DISCOUNT, GROSS_VALUE_AFTER_SP,");
        queryString.append(" CASE WHEN SIZE_CODE = 'B' THEN '13' WHEN SIZE_CODE = 'C' THEN '6' ELSE 'NA'  END ART_SIZE, R10 ART_PAIR");
        queryString.append(" FROM FTN_ORDER_CHD WHERE ORDER_NO = '");
        queryString.append((param.get("ORDER_NO") == null ? "" : param.get("ORDER_NO").toString()));
        queryString.append("' UNION ALL");
        queryString.append(" SELECT  ART_CODE, SIZE_CODE, UNIT_PRICE, ORDER_PAIRS, TOTAL_DISCOUNT, GROSS_VALUE, SP_DISCOUNT, GROSS_VALUE_AFTER_SP,");
        queryString.append(" CASE WHEN SIZE_CODE = 'A' THEN '8' WHEN SIZE_CODE = 'D' THEN '10' ELSE 'NA'  END ART_SIZE, R11 ART_PAIR");
        queryString.append(" FROM FTN_ORDER_CHD WHERE ORDER_NO = '");
        queryString.append((param.get("ORDER_NO") == null ? "" : param.get("ORDER_NO").toString()));
        queryString.append("' UNION ALL");
        queryString.append(" SELECT  ART_CODE, SIZE_CODE, UNIT_PRICE, ORDER_PAIRS, TOTAL_DISCOUNT, GROSS_VALUE, SP_DISCOUNT, GROSS_VALUE_AFTER_SP,");
        queryString.append(" CASE WHEN SIZE_CODE = 'B' THEN '10' WHEN SIZE_CODE = 'C' THEN '7' ELSE 'NA'  END ART_SIZE, R12 ART_PAIR");
        queryString.append(" FROM FTN_ORDER_CHD WHERE ORDER_NO = '");
        queryString.append((param.get("ORDER_NO") == null ? "" : param.get("ORDER_NO").toString()));
        queryString.append("' UNION ALL");
        queryString.append(" SELECT  ART_CODE, SIZE_CODE, UNIT_PRICE, ORDER_PAIRS, TOTAL_DISCOUNT, GROSS_VALUE, SP_DISCOUNT, GROSS_VALUE_AFTER_SP,");
        queryString.append(" CASE WHEN SIZE_CODE = 'A' THEN '9' WHEN SIZE_CODE = 'B' THEN '2' WHEN SIZE_CODE = 'C' THEN '8'  WHEN SIZE_CODE = 'D' THEN '11' ELSE 'NA'  END ART_SIZE, R13 ART_PAIR");
        queryString.append(" FROM FTN_ORDER_CHD WHERE ORDER_NO = '");
        queryString.append((param.get("ORDER_NO") == null ? "" : param.get("ORDER_NO").toString()));
        queryString.append("') WHERE ART_SIZE NOT IN ('NA') AND ART_PAIR > 0");
        queryString.append(" GROUP BY ART_CODE, SIZE_CODE, UNIT_PRICE, ORDER_PAIRS, TOTAL_DISCOUNT, GROSS_VALUE, SP_DISCOUNT, GROSS_VALUE_AFTER_SP, ART_SIZE");
        queryString.append(" )");


        return queryString.toString();
    }

    /**
     * Method: All order select query
     * Return: Query string
     */
    public static String ftnPromotionQuery() {

        StringBuilder queryString = new StringBuilder();

        queryString.append(" SELECT DISTINCT PROMO_ID, PROMO_NAME, START_DATE, END_DATE, START_TIME, END_TIME,");
        queryString.append(" CASE WHEN PROMO_STATUS ='Y' THEN 'Active' ELSE 'Inactive' END PROMO_STATUS");
        queryString.append(" FROM FTN_PROMOTION ORDER BY END_DATE DESC");

        return queryString.toString();
    }

    public static String orderCancelQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("UPDATE FTN_ORDER_MST SET");
        queryString.append(" CANCEL_REASON = '");
        queryString.append((param.get("CANCEL_REASON") == null ? "" : param.get("CANCEL_REASON").toString()));
        queryString.append("', CANCEL_STATUS = 'Y', CANCEL_BY ='");
        queryString.append((param.get("USER_ID") == null ? "" : param.get("USER_ID").toString()));
        queryString.append("', CANCEL_DATE = SYSDATE");
        queryString.append(" WHERE ORDER_NO = '");
        queryString.append((param.get("ORDER_NO") == null ? "" : param.get("ORDER_NO").toString()));
        queryString.append("'");

        return queryString.toString();
    }

    public static String circularDuplicateCheckQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT COUNT(DISTINCT PROMO_ID) TOTAL_CIRCULAR FROM FTN_PROMOTION WHERE PROMO_ID = '");
        queryString.append((param.get("PROMO_ID") == null ? "" : param.get("PROMO_ID").toString()));
        queryString.append("'");

        return queryString.toString();
    }

    public static String circularInsertQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("INSERT INTO FTN_PROMOTION (");
        queryString.append(" PROMO_ID, PROMO_NAME, CIRCULAR_DATE, START_DATE, END_DATE, START_TIME, END_TIME,");
        queryString.append(" ART_CODE, MRP, NET_SALE_PRICE_AFTER_DIS, DISCOUNT_AMOUNT, INSERT_BY");
        queryString.append(") VALUES (");
        //
        queryString.append("'");
        queryString.append((param.get("PROMO_ID") == null ? "" : param.get("PROMO_ID").toString()));
        queryString.append("',");
        //
        queryString.append("'");
        queryString.append((param.get("PROMO_NAME") == null ? "" : param.get("PROMO_NAME").toString()));
        queryString.append("',");
        //
        queryString.append("TO_DATE('");
        queryString.append((param.get("CIRCULAR_DATE") == null ? "" : param.get("CIRCULAR_DATE").toString()));
        queryString.append("','dd/MM/rrrr'),");
        //
        queryString.append("TO_DATE('");
        queryString.append((param.get("START_DATE") == null ? "" : param.get("START_DATE").toString()));
        queryString.append("','dd/MM/rrrr'),");
        //
        queryString.append("TO_DATE('");
        queryString.append((param.get("END_DATE") == null ? "" : param.get("END_DATE").toString()));
        queryString.append("','dd/MM/rrrr'),");
        //
        queryString.append((param.get("START_TIME") == null ? "" : param.get("START_TIME").toString()));
        queryString.append(",");
        //
        queryString.append((param.get("END_TIME") == null ? "" : param.get("END_TIME").toString()));
        queryString.append(",");
        //
        queryString.append("'");
        queryString.append((param.get("ART_CODE") == null ? "" : param.get("ART_CODE").toString()));
        queryString.append("',");
        //
        queryString.append((param.get("MRP") == null ? "" : param.get("MRP").toString()));
        queryString.append(",");
        //
        queryString.append((param.get("NET_SALE_PRICE_AFTER_DIS") == null ? "" : param.get("NET_SALE_PRICE_AFTER_DIS").toString()));
        queryString.append(",");
        //
        queryString.append((param.get("DISCOUNT_AMOUNT") == null ? "" : param.get("DISCOUNT_AMOUNT").toString()));
        queryString.append(",");
        //
        queryString.append("'");
        queryString.append((param.get("INSERT_BY") == null ? "" : param.get("INSERT_BY").toString()));
        queryString.append("'");
        queryString.append(")");

        return queryString.toString();
    }

    public static String circularArticleQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT PROMO_ID, ART_CODE, MRP, NET_SALE_PRICE_AFTER_DIS, DISCOUNT_AMOUNT");
        queryString.append(" FROM FTN_PROMOTION WHERE PROMO_ID ='");
        queryString.append((param.get("PROMO_ID") == null ? "" : param.get("PROMO_ID").toString()));
        queryString.append("'");

        return queryString.toString();
    }

    public static String customerInvoiceHistoryQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT ORDER_NO, TO_CHAR(ORDER_DATE,'DD-MM-RRRR') ORDER_DATE, ART_CODE, UNIT_PRICE, TOTAL_DISCOUNT");
        queryString.append(" FROM (SELECT A.ORDER_NO, A.ORDER_DATE, B.ART_CODE, B.UNIT_PRICE, B.TOTAL_DISCOUNT");
        queryString.append(" FROM FTN_ORDER_MST A, FTN_ORDER_CHD B WHERE A.ORDER_NO = B.ORDER_NO AND A.INV_STATUS ='Y' AND A.CANCEL_STATUS ='N' AND A.CUSTOMER_ID ='");
        queryString.append((param.get("CUSTOMER_ID") == null ? "" : param.get("CUSTOMER_ID").toString()));
        queryString.append("' ORDER BY A.ORDER_NO DESC) X WHERE ROWNUM <= 5");

        return queryString.toString();
    }

    public static String newArrivalArticleQuery() {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT (CAT_CODE || '-' ||CAT_NAME) CAT_NAME, ART_CODE, MRP FROM FTN_ART_MAST WHERE ART_STATUS = 'A' ORDER BY CAT_CODE, ART_CODE ASC");

        return queryString.toString();
    }
    //

    public static String remittanceYearQuery() {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT DISTINCT F_YEAR FROM");
        queryString.append(" (SELECT A.F_YEAR, A.WEEK_NO, TO_CHAR(A.WEEK_DATE, 'dd/MM/rrrr') WEEK_DATE FROM FTN_WEEK A,");
        queryString.append(" (SELECT F_YEAR, WEEK_NO FROM FTN_WEEK WHERE WEEK_DATE IN (SELECT MAX(WEEK_DATE)+1 FROM FTN_WEEK WHERE FORTNIGHT_WEEK_STATUS = 'Y')) B");
        queryString.append(" WHERE A.F_YEAR = B.F_YEAR AND A.WEEK_NO = B.WEEK_NO AND TO_DATE(A.WEEK_DATE, 'dd/MM/rrrr') <= TO_DATE(SYSDATE, 'dd/MM/rrrr')");
        queryString.append(" ORDER BY A.F_YEAR, A.WEEK_NO, A.WEEK_DATE)");

        return queryString.toString();
    }

    public static String remittanceWeekQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT DISTINCT WEEK_NO FROM");
        queryString.append(" (SELECT A.F_YEAR, A.WEEK_NO, TO_CHAR(A.WEEK_DATE, 'dd/MM/rrrr') WEEK_DATE FROM FTN_WEEK A,");
        queryString.append(" (SELECT F_YEAR, WEEK_NO FROM FTN_WEEK WHERE WEEK_DATE IN (SELECT MAX(WEEK_DATE)+1 FROM FTN_WEEK WHERE FORTNIGHT_WEEK_STATUS = 'Y')) B");
        queryString.append(" WHERE A.F_YEAR = B.F_YEAR AND A.WEEK_NO = B.WEEK_NO AND TO_DATE(A.WEEK_DATE, 'dd/MM/rrrr') <= TO_DATE(SYSDATE, 'dd/MM/rrrr')");
        queryString.append(" ORDER BY A.F_YEAR, A.WEEK_NO, A.WEEK_DATE) WHERE F_YEAR =");
        queryString.append(param.get("F_YEAR").toString());

        return queryString.toString();
    }

    public static String remittanceDateQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT A.F_YEAR, A.WEEK_NO, TO_CHAR(A.WEEK_DATE, 'dd/MM/rrrr') WEEK_DATE FROM FTN_WEEK A,");
        queryString.append(" (SELECT F_YEAR, WEEK_NO FROM FTN_WEEK WHERE WEEK_DATE IN (SELECT MAX(WEEK_DATE)+1 FROM FTN_WEEK WHERE FORTNIGHT_WEEK_STATUS = 'Y')) B");
        queryString.append(" WHERE A.F_YEAR = B.F_YEAR AND A.WEEK_NO = B.WEEK_NO AND TO_DATE(A.WEEK_DATE, 'dd/MM/rrrr') <= TO_DATE(SYSDATE, 'dd/MM/rrrr')");
        queryString.append(" AND A.F_YEAR =");
        queryString.append(param.get("F_YEAR").toString());
        queryString.append(" AND A.WEEK_NO =");
        queryString.append(param.get("WEEK_NO").toString());
        queryString.append(" ORDER BY A.F_YEAR, A.WEEK_NO, A.WEEK_DATE");

        return queryString.toString();
    }

    public static String remittanceUpdateQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("UPDATE FTN_REMITTANCE SET");
        queryString.append(" PROCESS_YEAR = ");
        queryString.append(param.get("PROCESS_YEAR").toString());
        queryString.append(",");
        queryString.append(" PROCESS_WEEK = ");
        queryString.append(param.get("PROCESS_WEEK").toString());
        queryString.append(",");
        queryString.append(" PROCESS_DATE = TO_DATE('");
        queryString.append(param.get("PROCESS_DATE").toString());
        queryString.append("','dd/MM/rrrr'),");
        queryString.append(" AMOUNT = ");
        queryString.append(param.get("AMOUNT").toString());
        queryString.append(",");
        queryString.append(" MRNO = '");
        queryString.append(param.get("MRNO").toString());
        queryString.append("',");
        queryString.append(" MRDATE = TO_DATE('");
        queryString.append(param.get("MRDATE").toString());
        queryString.append("','dd/MM/rrrr'),");
        queryString.append(" COLLECTEDAMOUNT = ");
        queryString.append(param.get("COLLECTEDAMOUNT").toString());
        queryString.append(",");
        queryString.append(" PICKUPDATE = TO_DATE('");
        queryString.append(param.get("PICKUPDATE").toString());
        queryString.append("','dd/MM/rrrr'),");
        queryString.append(" SHIPPERNAME = '");
        queryString.append(param.get("SHIPPERNAME").toString());
        queryString.append("',");
        queryString.append(" REMARKS = '");
        queryString.append(param.get("REMARKS").toString());
        queryString.append("',");
        queryString.append(" PROCESS_BY = '");
        queryString.append(param.get("PROCESS_BY").toString());
        queryString.append("',");
        queryString.append(" INSERT_DATE = SYSDATE");
        queryString.append(" WHERE AWB = '");
        queryString.append(param.get("AWB").toString());
        queryString.append("'");

        return queryString.toString();
    }

    public static String remittanceInsertQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("INSERT INTO FTN_REMITTANCE (");
        queryString.append(" PROCESS_YEAR, PROCESS_WEEK, PROCESS_DATE, AWB, AMOUNT, MRNO, MRDATE,");
        queryString.append(" COLLECTEDAMOUNT, PICKUPDATE, SHIPPERNAME, REMARKS, PROCESS_BY, INSERT_DATE");
        queryString.append(") VALUES (");
        queryString.append(param.get("PROCESS_YEAR").toString());
        queryString.append(",");
        queryString.append(param.get("PROCESS_WEEK").toString());
        queryString.append(",");
        queryString.append(" TO_DATE('");
        queryString.append(param.get("PROCESS_DATE").toString());
        queryString.append("','dd/MM/rrrr'),'");
        queryString.append(param.get("AWB").toString());
        queryString.append("',");
        queryString.append(param.get("AMOUNT").toString());
        queryString.append(",'");
        queryString.append(param.get("MRNO").toString());
        queryString.append("',");
        queryString.append(" TO_DATE('");
        queryString.append(param.get("MRDATE").toString());
        queryString.append("','dd/MM/rrrr'),");
        queryString.append(param.get("COLLECTEDAMOUNT").toString());
        queryString.append(",");
        queryString.append(" TO_DATE('");
        queryString.append(param.get("PICKUPDATE").toString());
        queryString.append("','dd/MM/rrrr'),'");
        queryString.append(param.get("SHIPPERNAME").toString());
        queryString.append("','");
        queryString.append(param.get("REMARKS").toString());
        queryString.append("','");
        queryString.append(param.get("PROCESS_BY").toString());
        queryString.append("',");
        queryString.append("SYSDATE");
        queryString.append(")");

        return queryString.toString();
    }

    public static String remittanceSelectQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT PROCESS_YEAR, PROCESS_WEEK, TO_CHAR(PROCESS_DATE,'dd/MM/rrrr') PROCESS_DATE, AWB, AMOUNT, MRNO, TO_CHAR(MRDATE,'dd/MM/rrrr') MRDATE,");
        queryString.append("COLLECTEDAMOUNT, TO_CHAR(PICKUPDATE,'dd/MM/rrrr') PICKUPDATE, SHIPPERNAME, REMARKS, PROCESS_BY, TO_CHAR(INSERT_DATE,'dd/MM/rrrr') INSERT_DATE");
        queryString.append(" FROM FTN_REMITTANCE WHERE TO_DATE(PROCESS_DATE, 'dd/MM/rrrr') = TO_DATE('");
        queryString.append(param.get("PROCESS_DATE").toString());
        queryString.append("','dd/MM/rrrr')");

        return queryString.toString();
    }

    public static String remittanceSelectQuery() {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT PROCESS_YEAR, PROCESS_WEEK, TO_CHAR(PROCESS_DATE,'dd/MM/rrrr') PROCESS_DATE, AWB, AMOUNT, MRNO, TO_CHAR(MRDATE,'dd/MM/rrrr') MRDATE,");
        queryString.append("COLLECTEDAMOUNT, TO_CHAR(PICKUPDATE,'dd/MM/rrrr') PICKUPDATE, SHIPPERNAME, REMARKS, PROCESS_BY, TO_CHAR(INSERT_DATE,'dd/MM/rrrr') INSERT_DATE");
        queryString.append(" FROM FTN_REMITTANCE WHERE TO_DATE(PROCESS_DATE, 'dd/MM/rrrr') BETWEEN TO_DATE(SYSDATE-30,'dd/MM/rrrr')");
        queryString.append(" AND TO_DATE(SYSDATE,'dd/MM/rrrr')");

        return queryString.toString();
    }
}
