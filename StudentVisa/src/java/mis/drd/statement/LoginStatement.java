/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.drd.statement;

import java.util.HashMap;

/**
 *
 * @author MIS
 */
public class LoginStatement {

    //static member holds only one instance of the LoginStatement class.
    private static LoginStatement loginStatement = null;

    //LoginStatement prevents the instantiation from any other class.
    private LoginStatement() {
    }

    //Now we are providing gloabal point of access.
    public static synchronized LoginStatement getInstance() {

        if (loginStatement == null) {
            loginStatement = new LoginStatement();
        }

        return loginStatement;

    }

    /**
     * Method: Login select query
     * Parameter: HashMap (User name, user password)
     * Return: Query string
     */
    public static String loginQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append(" SELECT A.USER_ID, COALESCE(A.DISPLAY_NAME, A.USER_NAME) DISPLAY_NAME, A.USER_PASSWORD,");
        queryString.append(" A.COMPANY_CODE, C.COMPANY_NAME, B.GROUP_ID");
        queryString.append(" FROM SV_USER A, SV_USER_GROUP B, SV_COMPANY C");
        queryString.append(" WHERE A.COMPANY_CODE=C.COMPANY_CODE");
        queryString.append(" AND A.GROUP_ID = B.GROUP_ID");
        queryString.append(" AND A.USER_STATUS='Y'");
        queryString.append(" AND (A.USER_NAME = '");
        queryString.append(param.get("USER_NAME").toString().replaceAll("'", "''"));
        queryString.append("')");

        return queryString.toString();
    }

    /**
     * Method: Group Wise Menu Query
     * Parameter: HashMap (COM_CODE, user Group / LEVEL_ID)
     * Return: Query string
     */
    public static String groupWiseMenuQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append(" SELECT MENU_ID,");
        queryString.append(" CASE WHEN GROUP_MENU_STATUS=1 THEN 'T' ELSE 'F' END GROUP_MENU_STATUS");
        queryString.append(" FROM");
        queryString.append(" (SELECT MENU_ID, SUM(GROUP_MENU_STATUS) GROUP_MENU_STATUS");
        queryString.append(" FROM");
        queryString.append(" (SELECT MENU_ID, 0 GROUP_MENU_STATUS FROM FTN_MENU");
        queryString.append(" UNION ALL");
        queryString.append(" SELECT MENU_ID, CASE WHEN GROUP_MENU_STATUS='Y' THEN 1 ELSE 0 END GROUP_MENU_STATUS");
        queryString.append(" FROM FTN_USER_GROUP_MENU");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("'");
        queryString.append(" AND USER_GROUP_ID=");
        queryString.append((param.get("USER_GROUP_ID") == null ? "" : param.get("USER_GROUP_ID").toString()));
        queryString.append(" ) GROUP BY MENU_ID)");

        return queryString.toString();
    }

    public static String checkDuplicateUserQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT COUNT(USER_ID) TOTAL_USER FROM FTN_USER WHERE CUSTOMER_ID IN");
        queryString.append(" (SELECT DISTINCT CUSTOMER_ID FROM FTN_CUSTOMER_INFO WHERE CONTACT_NO = '");
        queryString.append(param.get("CONTACT_NO").toString());
        queryString.append("')");

        return queryString.toString();
    }

    public static String customerInsertQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("INSERT INTO FTN_CUSTOMER_INFO (");
        queryString.append("CUSTOMER_ID, CONTACT_NO, EMAIL_ADD, FIRST_NAME, MIDDLE_NAME, LAST_NAME) VALUES ('");
        queryString.append((param.get("CUSTOMER_ID") == null ? "" : param.get("CUSTOMER_ID").toString()));
        queryString.append("','");
        queryString.append((param.get("CONTACT_NO") == null ? "" : param.get("CONTACT_NO").toString()));
        queryString.append("','");
        queryString.append((param.get("EMAIL_ADD") == null ? "" : param.get("EMAIL_ADD").toString()));
        queryString.append("','");
        queryString.append((param.get("FIRST_NAME") == null ? "" : param.get("FIRST_NAME").toString()));
        queryString.append("','");
        queryString.append((param.get("MIDDLE_NAME") == null ? "" : param.get("MIDDLE_NAME").toString()));
        queryString.append("','");
        queryString.append((param.get("LAST_NAME") == null ? "" : param.get("LAST_NAME").toString()));
        queryString.append("')");

        return queryString.toString();
    }

    public static String userPkQuery() {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT (MAX(NVL(USER_ID,0)) + 1) USER_ID FROM FTN_USER");

        return queryString.toString();
    }

    public static String userInsertQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("INSERT INTO FTN_USER (USER_ID,USER_NAME,USER_PASSWORD,USER_GROUP_ID,COM_CODE,DISPLAY_NAME,");
        queryString.append("USER_EMAIL,USER_CONTACT,CUSTOMER_ID,SECURITY_QUESTION,SECURITY_ANSWER,ACTIVATION_CODE,ACTIVATION_LINK)VALUES(");
        queryString.append(param.get("USER_ID").toString());
        queryString.append(",'");
        queryString.append(param.get("USER_NAME").toString());
        queryString.append("','");
        queryString.append(param.get("USER_PASSWORD").toString());
        queryString.append("',");
        queryString.append(param.get("USER_GROUP_ID").toString());
        queryString.append(",");
        queryString.append(param.get("COM_CODE").toString());
        queryString.append(",'");
        queryString.append(param.get("DISPLAY_NAME"));
        queryString.append("','");
        queryString.append(param.get("USER_EMAIL"));
        queryString.append("','");
        queryString.append(param.get("USER_CONTACT"));
        queryString.append("','");
        queryString.append(param.get("CUSTOMER_ID"));
        queryString.append("','");
        queryString.append(param.get("SECURITY_QUESTION"));
        queryString.append("','");
        queryString.append(param.get("SECURITY_ANSWER"));
        queryString.append("','");
        queryString.append(param.get("ACTIVATION_CODE"));
        queryString.append("','");
        queryString.append(param.get("ACTIVATION_LINK"));
        queryString.append("')");

        return queryString.toString();
    }

    public static String userUpdateQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("UPDATE FTN_USER SET ACTIVATION_FLAG = 'Y', ACTIVE_STATUS = 'Y', ACTIVATION_DATE = SYSDATE  WHERE USER_ID =");
        queryString.append(param.get("USER_ID").toString());
        queryString.append(" AND CUSTOMER_ID ='");
        queryString.append(param.get("CUSTOMER_ID"));
        queryString.append("' AND ACTIVATION_FLAG = 'N' AND ACTIVATION_CODE ='");
        queryString.append(param.get("ACTIVATION_CODE").toString());
        queryString.append("'");

        return queryString.toString();
    }
}
