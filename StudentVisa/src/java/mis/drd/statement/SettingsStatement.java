/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.drd.statement;

import java.util.HashMap;

/**
 *
 * @author MIS
 */
public class SettingsStatement {

    //static member holds only one instance of the LoginStatement class.
    private static SettingsStatement settingsStatement = null;

    //LoginStatement prevents the instantiation from any other class.
    private SettingsStatement() {
    }

    //Now we are providing gloabal point of access.
    public static synchronized SettingsStatement getInstance() {

        if (settingsStatement == null) {
            settingsStatement = new SettingsStatement();
        }

        return settingsStatement;

    }

    /*
     * Method: All Company select query
     * Return: Query string
     */
    public static String allCompanyQuery() {

        StringBuilder queryString = new StringBuilder();

        queryString.append(" SELECT COM_CODE, COM_NAME, COM_COUNTRY, COM_REIGON,");
        queryString.append(" COM_MANAGER, COM_EMAIL, COM_CONTACT, COM_FAX, COM_STATUS, COM_BANNER, COM_ADDRESS");
        queryString.append(" FROM RTL_COMPANY");

        return queryString.toString();
    }

    /*
     * Method: All Active Company Query
     * Return: Query string
     */
    public static String allActiveCompanyQuery() {

        StringBuilder queryString = new StringBuilder();

        queryString.append(" SELECT COM_CODE, COM_NAME, COM_COUNTRY, COM_REIGON,");
        queryString.append(" COM_MANAGER, COM_EMAIL, COM_CONTACT, COM_FAX, COM_STATUS, COM_BANNER, COM_ADDRESS");
        queryString.append(" FROM RTL_COMPANY WHERE COM_STATUS = 'Y'");

        return queryString.toString();
    }

    /*
     * Method: Company query
     * Parameter: HashMap (Company Code)
     * Return: Query string
     */
    public static String companyQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append(" SELECT COM_CODE, COM_NAME, COM_COUNTRY, COM_REIGON,");
        queryString.append(" COM_MANAGER, COM_EMAIL, COM_CONTACT, COM_FAX, COM_STATUS, COM_BANNER, COM_ADDRESS");
        queryString.append(" FROM RTL_COMPANY");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("'");

        return queryString.toString();
    }

    /*
     * Method: Company Count Query
     * Parameter: HashMap (Company Code)
     * Return: Query string
     */
    public static String companyCountQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT COUNT(COM_CODE) TOTAL FROM RTL_COMPANY");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("'");

        return queryString.toString();
    }

    /*
     * Method: Company Delete Query
     * Parameter: HashMap (Company Code)
     * Return: Query string
     */
    public static String companyDeleteQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("DELETE FROM RTL_COMPANY");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("'");

        return queryString.toString();
    }

    /*
     * Method: Company Update Query
     * Parameter: HashMap
     * Return: Query string
     */
    public static String companyUpdateQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("UPDATE RTL_COMPANY SET");
        queryString.append(" COM_NAME='");
        queryString.append((param.get("COM_NAME") == null ? "" : param.get("COM_NAME").toString().replaceAll("'", "\''")));
        queryString.append("',");
        queryString.append(" COM_BANNER='");
        queryString.append((param.get("COM_BANNER") == null ? "" : param.get("COM_BANNER").toString().replaceAll("'", "\''")));
        queryString.append("',");
        queryString.append(" COM_ADDRESS='");
        queryString.append((param.get("COM_ADDRESS") == null ? "" : param.get("COM_ADDRESS").toString().replaceAll("'", "\''")));
        queryString.append("',");
        queryString.append(" COM_COUNTRY='");
        queryString.append((param.get("COM_COUNTRY") == null ? "" : param.get("COM_COUNTRY").toString().replaceAll("'", "\''")));
        queryString.append("',");
        queryString.append(" COM_REIGON='");
        queryString.append((param.get("COM_REIGON") == null ? "" : param.get("COM_REIGON").toString().replaceAll("'", "\''")));
        queryString.append("',");
        queryString.append(" COM_MANAGER='");
        queryString.append((param.get("COM_MANAGER") == null ? "" : param.get("COM_MANAGER").toString().replaceAll("'", "\''")));
        queryString.append("',");
        queryString.append(" COM_EMAIL='");
        queryString.append((param.get("COM_EMAIL") == null ? "" : param.get("COM_EMAIL").toString().replaceAll("'", "\''")));
        queryString.append("',");
        queryString.append(" COM_CONTACT='");
        queryString.append((param.get("COM_CONTACT") == null ? "" : param.get("COM_CONTACT").toString().replaceAll("'", "\''")));
        queryString.append("',");
        queryString.append(" COM_FAX='");
        queryString.append((param.get("COM_FAX") == null ? "" : param.get("COM_FAX").toString().replaceAll("'", "\''")));
        queryString.append("',");
        queryString.append(" COM_STATUS='");
        queryString.append((param.get("COM_STATUS") == null ? "" : param.get("COM_STATUS").toString().replaceAll("'", "\''")));
        queryString.append("',");
        queryString.append(" UPDATE_BY='");
        queryString.append((param.get("UPDATE_BY") == null ? "" : param.get("UPDATE_BY").toString().replaceAll("'", "\''")));
        queryString.append("',");
        queryString.append(" UPDATE_DATE=SYSDATE");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("'");

        return queryString.toString();
    }

    /*
     * Method: Company Insert Query
     * Parameter: HashMap
     * Return: Query string
     */
    public static String companyInsertQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("INSERT INTO RTL_COMPANY");
        queryString.append(" (COM_CODE, COM_NAME, COM_BANNER, COM_ADDRESS, COM_COUNTRY, COM_REIGON, COM_MANAGER, COM_EMAIL, COM_CONTACT, COM_FAX, COM_STATUS, INSERT_BY) VALUES (");
        queryString.append("'");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("','");
        queryString.append((param.get("COM_NAME") == null ? "" : param.get("COM_NAME").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("COM_BANNER") == null ? "" : param.get("COM_BANNER").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("COM_ADDRESS") == null ? "" : param.get("COM_ADDRESS").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("COM_COUNTRY") == null ? "" : param.get("COM_COUNTRY").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("COM_REIGON") == null ? "" : param.get("COM_REIGON").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("COM_MANAGER") == null ? "" : param.get("COM_MANAGER").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("COM_EMAIL") == null ? "" : param.get("COM_EMAIL").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("COM_CONTACT") == null ? "" : param.get("COM_CONTACT").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("COM_FAX") == null ? "" : param.get("COM_FAX").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("COM_STATUS") == null ? "" : param.get("COM_STATUS").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("INSERT_BY") == null ? "" : param.get("INSERT_BY").toString().replaceAll("'", "\''")));
        queryString.append("')");

        return queryString.toString();
    }

    /*
     * Method: User Group Select Query
     * Parameter: HashMap
     * Return: Query string
     */
    public static String userGroupSelectQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT LEVEL_ID, LEVEL_NAME, LEVEL_SHORT_NAME, STATUS, LEVEL_DESCRIPTION");
        queryString.append(" FROM RTL_USER_LEVEL");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' ORDER BY LEVEL_NAME ASC");

        return queryString.toString();
    }

    /*
     * Method: Group Short Name Count Query
     * Parameter: HashMap (Short Name, Company Code)
     * Return: Query string
     */
    public static String groupShortNameCountQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT COUNT(LEVEL_SHORT_NAME) TOTAL");
        queryString.append(" FROM RTL_USER_LEVEL");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' AND UPPER(LEVEL_SHORT_NAME)=UPPER('");
        queryString.append((param.get("LEVEL_SHORT_NAME") == null ? "" : param.get("LEVEL_SHORT_NAME").toString()));
        queryString.append("')");

        return queryString.toString();
    }

    /*
     * Method: Group Name Count Query
     * Parameter: HashMap (Short Name, Company Code)
     * Return: Query string
     */
    public static String groupNameCountQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT COUNT(LEVEL_NAME) TOTAL");
        queryString.append(" FROM RTL_USER_LEVEL");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' AND UPPER(LEVEL_NAME)=UPPER('");
        queryString.append((param.get("LEVEL_NAME") == null ? "" : param.get("LEVEL_NAME").toString()));
        queryString.append("')");

        return queryString.toString();
    }

    /*
     * Method: Group Insert Query
     * Parameter: HashMap
     * Return: Query string
     */
    public static String groupInsertQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("INSERT INTO RTL_USER_LEVEL");
        queryString.append(" (LEVEL_ID, LEVEL_NAME, INSERT_BY, COM_CODE, LEVEL_SHORT_NAME, LEVEL_DESCRIPTION) VALUES (");
        queryString.append((param.get("LEVEL_ID") == null ? "" : param.get("LEVEL_ID").toString()));
        queryString.append(",'");
        queryString.append((param.get("LEVEL_NAME") == null ? "" : param.get("LEVEL_NAME").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("INSERT_BY") == null ? "" : param.get("INSERT_BY").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("LEVEL_SHORT_NAME") == null ? "" : param.get("LEVEL_SHORT_NAME").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("LEVEL_DESCRIPTION") == null ? "" : param.get("LEVEL_DESCRIPTION").toString().replaceAll("'", "\''")));
        queryString.append("')");

        return queryString.toString();
    }

    /*
     * Method: Group Update Query
     * Parameter: HashMap
     * Return: Query string
     */
    public static String groupUpdateQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("UPDATE RTL_USER_LEVEL SET");
        queryString.append(" UPDATED_BY='");
        queryString.append((param.get("UPDATED_BY") == null ? "" : param.get("UPDATED_BY").toString().replaceAll("'", "\''")));
        queryString.append("',");
        queryString.append(" STATUS='");
        queryString.append((param.get("STATUS") == null ? "" : param.get("STATUS").toString().replaceAll("'", "\''")));
        queryString.append("', UPDATED_DATE = SYSDATE");
        queryString.append(" WHERE LEVEL_ID=");
        queryString.append((param.get("LEVEL_ID") == null ? "" : param.get("LEVEL_ID").toString().replaceAll("'", "\''")));
        queryString.append(" AND COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString().replaceAll("'", "\''")));
        queryString.append("'");

        return queryString.toString();
    }

    /*
     * Method: User Duplicate Check Query
     * Parameter: HashMap (Company Code)
     * Return: Query string
     */
    public static String userDuplicateCheckQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT COUNT(USER_NAME) TOTAL FROM RTL_USER_INFO");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' AND USER_NAME='");
        queryString.append((param.get("USER_NAME") == null ? "" : param.get("USER_NAME").toString()));
        queryString.append("'");

        return queryString.toString();
    }

    /*
     * Method: User Insert Query
     * Parameter: HashMap
     * Return: Query string
     */
    public static String userInsertQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("INSERT INTO RTL_USER_INFO");
        queryString.append(" (USER_ID, USER_NAME, USER_PASSWORD, DESCRIPTION, STATUS, INSERT_BY, USER_LEVEL_ID,");
        queryString.append(" COM_CODE, DISPLAY_NAME, USER_EMAIL, USER_CONTACT ) VALUES (");
        queryString.append((param.get("USER_ID") == null ? "" : param.get("USER_ID").toString()));
        queryString.append(",'");
        queryString.append((param.get("USER_NAME") == null ? "" : param.get("USER_NAME").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("USER_PASSWORD") == null ? "" : param.get("USER_PASSWORD").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("DESCRIPTION") == null ? "" : param.get("DESCRIPTION").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("STATUS") == null ? "" : param.get("STATUS").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("INSERT_BY") == null ? "" : param.get("INSERT_BY").toString().replaceAll("'", "\''")));
        //
        queryString.append("',");
        queryString.append((param.get("USER_LEVEL_ID") == null ? "" : param.get("USER_LEVEL_ID").toString().replaceAll("'", "\''")));
        queryString.append(",'");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("DISPLAY_NAME") == null ? "" : param.get("DISPLAY_NAME").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("USER_EMAIL") == null ? "" : param.get("USER_EMAIL").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("USER_CONTACT") == null ? "" : param.get("USER_CONTACT").toString().replaceAll("'", "\''")));
        queryString.append("')");

        return queryString.toString();
    }

    /*
     * Method: User Update Query
     * Parameter: HashMap
     * Return: Query string
     */
    public static String userUpdateQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("UPDATE RTL_USER_INFO SET");
        queryString.append(" USER_NAME = '");
        queryString.append((param.get("USER_NAME") == null ? "" : param.get("USER_NAME").toString().replaceAll("'", "\''")));
        queryString.append("', USER_PASSWORD = '");
        queryString.append((param.get("USER_PASSWORD") == null ? "" : param.get("USER_PASSWORD").toString().replaceAll("'", "\''")));
        queryString.append("', DESCRIPTION = '");
        queryString.append((param.get("DESCRIPTION") == null ? "" : param.get("DESCRIPTION").toString().replaceAll("'", "\''")));
        queryString.append("', STATUS = '");
        queryString.append((param.get("STATUS") == null ? "" : param.get("STATUS").toString().replaceAll("'", "\''")));
        queryString.append("', UPDATE_BY = '");
        queryString.append((param.get("UPDATE_BY") == null ? "" : param.get("UPDATE_BY").toString().replaceAll("'", "\''")));
        queryString.append("', UPDATE_DATE = SYSDATE, USER_LEVEL_ID = ");
        queryString.append((param.get("USER_LEVEL_ID") == null ? "" : param.get("USER_LEVEL_ID").toString().replaceAll("'", "\''")));
        queryString.append(", DISPLAY_NAME = '");
        queryString.append((param.get("DISPLAY_NAME") == null ? "" : param.get("DISPLAY_NAME").toString().replaceAll("'", "\''")));
        queryString.append("', USER_EMAIL = '");
        queryString.append((param.get("USER_EMAIL") == null ? "" : param.get("USER_EMAIL").toString().replaceAll("'", "\''")));
        queryString.append("', USER_CONTACT = '");
        queryString.append((param.get("USER_CONTACT") == null ? "" : param.get("USER_CONTACT").toString().replaceAll("'", "\''")));
        queryString.append("' WHERE COM_CODE = '");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString().replaceAll("'", "\''")));
        queryString.append("' AND USER_ID =");
        queryString.append((param.get("USER_ID") == null ? "" : param.get("USER_ID").toString()));

        return queryString.toString();
    }

    /*
     * Method: Select All Active Store Classifiation
     * Parameter: HashMap
     * Return: Query string
     */
    public static String allActiveStoreClassificationSelectQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT COM_CODE, CLASSIFICATION_CODE, CLASSIFICATION_NAME");
        queryString.append(" FROM RTL_STORE_CLASSIFICATION");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' AND ACTIVE_STATUS='Y' ORDER BY CLASSIFICATION_NAME ASC");

        return queryString.toString();
    }

    /*
     * Method: Select All Active Store Behavior Or Status
     * Parameter: HashMap
     * Return: Query string
     */
    public static String allActiveStoreBehaviorSelectQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT COM_CODE, STORE_BEHAVIOR_CODE, STORE_BEHAVIOR_NAME");
        queryString.append(" FROM RTL_STORE_BEHAVIOR");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' AND ACTIVE_STATUS='Y' ORDER BY STORE_BEHAVIOR_NAME ASC");

        return queryString.toString();
    }

    /*
     * Method: Select All Active Store Concept
     * Parameter: HashMap
     * Return: Query string
     */
    public static String allActiveStoreConceptSelectQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT COM_CODE, CONCEPT_CODE, CONCEPT_NAME");
        queryString.append(" FROM RTL_STORE_CONCEPT");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' AND ACTIVE_STATUS='Y' ORDER BY CONCEPT_NAME ASC");

        return queryString.toString();
    }

    /*
     * Method: Select All Active Store Group
     * Parameter: HashMap
     * Return: Query string
     */
    public static String allActiveStoreGroupSelectQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT COM_CODE, STORE_GROUP_CODE, STORE_GROUP_NAME");
        queryString.append(" FROM RTL_STORE_GROUP");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' AND ACTIVE_STATUS='Y' ORDER BY STORE_GROUP_NAME ASC");

        return queryString.toString();
    }
    /*
     * Method: Select All Active Store Type
     * Parameter: HashMap
     * Return: Query string
     */

    public static String allActiveStoreTypeSelectQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT COM_CODE, STORE_TYPE_CODE, STORE_TYPE_NAME");
        queryString.append(" FROM RTL_STORE_TYPE");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' AND ACTIVE_STATUS='Y' ORDER BY STORE_TYPE_NAME ASC");

        return queryString.toString();
    }

    /*
     * Method: Select All Active Area
     * Parameter: HashMap
     * Return: Query string
     */
    public static String allActiveAreaSelectQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT COM_CODE, AREA_CODE, AREA_MANAGER, DESCRIPTION, OPEARTION_CODE, PHONE, EMAIL, AREA_ADDR");
        queryString.append(" FROM RTL_AREA_INFO");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' AND STATUS='Y' ORDER BY AREA_CODE ASC");

        return queryString.toString();
    }

    /*
     * Method: Select All Active District
     * Parameter: HashMap
     * Return: Query string
     */
    public static String allActiveDistrictSelectQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT COM_CODE, DISTRICT_CODE, DISTRICT_NAME");
        queryString.append(" FROM RTL_DISTRICT");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' AND ACTIVE_STATUS='Y' ORDER BY DISTRICT_NAME ASC");

        return queryString.toString();
    }

    /*
     * Method: Store Count Query
     * Parameter: HashMap (Company Code, Store Code)
     * Return: Query string
     */
    public static String storeCountQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT COUNT(STORE_CODE) TOTAL FROM RTL_STORE_INFO");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' AND STORE_CODE='");
        queryString.append((param.get("STORE_CODE") == null ? "" : param.get("STORE_CODE").toString()));
        queryString.append("'");

        return queryString.toString();
    }

    /*
     * Method: Store Insert Query
     * Parameter: HashMap
     * Return: Query string
     */
    public static String storeInsertQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("INSERT INTO RTL_STORE_INFO");
        queryString.append(" (STORE_CODE, STORE_NAME, STORE_MANAGER, STORE_DESCRIPTION, STORE_PHONE, STORE_EMAIL,");
        queryString.append(" STORE_CLASSIFICATION, STORE_ADDRESS, MODEM_NUMBER, SIM_NUMBER, STORE_GROUP, OPEN_WEEK,");
        queryString.append(" OPEN_YEAR, STORE_TYPE, COMM_RATE, STORE_CONCEPT, STOCK_AREA, SELLING_AREA, NEW_COM_REN_STATUS,");
        queryString.append(" NEW_COM_REN_WEEK, NEW_COM_REN_YEAR, SELL_LINE_STORE, AREA_CODE, COM_CODE, INSERT_BY,");
        queryString.append(" DISTRICT_CODE, POSTAL_CODE");
        queryString.append(" ) VALUES (");
        queryString.append("'");
        queryString.append((param.get("STORE_CODE") == null ? "" : param.get("STORE_CODE").toString()));
        queryString.append("','");
        queryString.append((param.get("STORE_NAME") == null ? "" : param.get("STORE_NAME").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("STORE_MANAGER") == null ? "" : param.get("STORE_MANAGER").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("STORE_DESCRIPTION") == null ? "" : param.get("STORE_DESCRIPTION").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("STORE_PHONE") == null ? "" : param.get("STORE_PHONE").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("STORE_EMAIL") == null ? "" : param.get("STORE_EMAIL").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("STORE_CLASSIFICATION") == null ? "" : param.get("STORE_CLASSIFICATION").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("STORE_ADDRESS") == null ? "" : param.get("STORE_ADDRESS").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("MODEM_NUMBER") == null ? "" : param.get("MODEM_NUMBER").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("SIM_NUMBER") == null ? "" : param.get("SIM_NUMBER").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("STORE_GROUP") == null ? "" : param.get("STORE_GROUP").toString().replaceAll("'", "\''")));
        queryString.append("',");
        queryString.append((param.get("OPEN_WEEK") == null ? "" : param.get("OPEN_WEEK").toString().replaceAll("'", "\''")));
        queryString.append(",");
        queryString.append((param.get("OPEN_YEAR") == null ? "" : param.get("OPEN_YEAR").toString().replaceAll("'", "\''")));
        queryString.append(",'");
        queryString.append((param.get("STORE_TYPE") == null ? "" : param.get("STORE_TYPE").toString().replaceAll("'", "\''")));
        queryString.append("',");
        queryString.append((param.get("COMM_RATE") == null ? "" : param.get("COMM_RATE").toString().replaceAll("'", "\''")));
        queryString.append(",'");
        queryString.append((param.get("STORE_CONCEPT") == null ? "" : param.get("STORE_CONCEPT").toString().replaceAll("'", "\''")));
        queryString.append("',");
        queryString.append((param.get("STOCK_AREA") == null ? "" : param.get("STOCK_AREA").toString().replaceAll("'", "\''")));
        queryString.append(",");
        queryString.append((param.get("SELLING_AREA") == null ? "" : param.get("SELLING_AREA").toString().replaceAll("'", "\''")));
        queryString.append(",'");
        queryString.append((param.get("NEW_COM_REN_STATUS") == null ? "" : param.get("NEW_COM_REN_STATUS").toString().replaceAll("'", "\''")));
        queryString.append("',");
        queryString.append((param.get("NEW_COM_REN_WEEK") == null ? "" : param.get("NEW_COM_REN_WEEK").toString().replaceAll("'", "\''")));
        queryString.append(",");
        queryString.append((param.get("NEW_COM_REN_YEAR") == null ? "" : param.get("NEW_COM_REN_YEAR").toString().replaceAll("'", "\''")));
        queryString.append(",'");
        queryString.append((param.get("SELL_LINE_STORE") == null ? "" : param.get("SELL_LINE_STORE").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("AREA_CODE") == null ? "" : param.get("AREA_CODE").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("INSERT_BY") == null ? "" : param.get("INSERT_BY").toString().replaceAll("'", "\''")));
        queryString.append("',");
        queryString.append((param.get("DISTRICT_CODE") == null ? "" : param.get("DISTRICT_CODE").toString().replaceAll("'", "\''")));
        queryString.append(",'");
        queryString.append((param.get("POSTAL_CODE") == null ? "" : param.get("POSTAL_CODE").toString().replaceAll("'", "\''")));
        queryString.append("')");

        return queryString.toString();
    }

    /*
     * Method: Select All Store
     * Parameter: HashMap
     * Return: Query string
     */
    public static String allStoreSelectQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT A.COM_CODE, A.STORE_CODE, A.STORE_NAME, A.STORE_MANAGER,");
        queryString.append(" A.STORE_DESCRIPTION, A.STORE_PHONE, A.STORE_EMAIL,");
        queryString.append(" A.STORE_ADDRESS, A.MODEM_NUMBER, A.SIM_NUMBER,");
        queryString.append(" A.OPEN_WEEK, A.OPEN_YEAR, A.COMM_RATE, A.STOCK_AREA,");
        queryString.append(" A.SELLING_AREA,  A.NEW_COM_REN_WEEK,  A.AREA_CODE,");
        queryString.append(" A.NEW_COM_REN_YEAR, A.SELL_LINE_STORE,  A.POSTAL_CODE,");
        queryString.append(" A.STORE_CLASSIFICATION, B.CLASSIFICATION_NAME, A.STORE_CONCEPT, C.CONCEPT_NAME, D.STORE_BEHAVIOR_NAME,");
        queryString.append(" A.NEW_COM_REN_STATUS, A.STORE_GROUP, E.STORE_GROUP_NAME, A.STORE_TYPE, F.STORE_TYPE_NAME, A.DISTRICT_CODE, G.DISTRICT_NAME");
        queryString.append(" FROM RTL_STORE_INFO A, RTL_STORE_CLASSIFICATION B,");
        queryString.append(" RTL_STORE_CONCEPT C, RTL_STORE_BEHAVIOR D,");
        queryString.append(" RTL_STORE_GROUP E, RTL_STORE_TYPE F, RTL_DISTRICT G");
        queryString.append(" WHERE A.STORE_CLASSIFICATION = B.CLASSIFICATION_CODE");
        queryString.append(" AND A.STORE_CONCEPT =C.CONCEPT_CODE");
        queryString.append(" AND A.NEW_COM_REN_STATUS = D.STORE_BEHAVIOR_CODE");
        queryString.append(" AND A.STORE_GROUP=E.STORE_GROUP_CODE");
        queryString.append(" AND A.STORE_TYPE= F.STORE_TYPE_CODE");
        queryString.append(" AND A.DISTRICT_CODE = G.DISTRICT_CODE");
        queryString.append(" AND A.COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' ORDER BY A.STORE_CODE ASC");

        return queryString.toString();
    }
    /*
     * Method: Select Store Code Details
     * Parameter: HashMap
     * Return: Query string
     */

    public static String storeDetailsSelectQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT A.COM_CODE, A.STORE_CODE, A.STORE_NAME, A.STORE_MANAGER,");
        queryString.append(" A.STORE_DESCRIPTION, A.STORE_PHONE, A.STORE_EMAIL,");
        queryString.append(" A.STORE_ADDRESS, A.MODEM_NUMBER, A.SIM_NUMBER,");
        queryString.append(" A.OPEN_WEEK, A.OPEN_YEAR, A.COMM_RATE, A.STOCK_AREA,");
        queryString.append(" A.SELLING_AREA,  A.NEW_COM_REN_WEEK,  A.AREA_CODE,");
        queryString.append(" A.NEW_COM_REN_YEAR, A.SELL_LINE_STORE,  A.POSTAL_CODE,");
        queryString.append(" A.STORE_CLASSIFICATION, B.CLASSIFICATION_NAME, A.STORE_CONCEPT, C.CONCEPT_NAME, D.STORE_BEHAVIOR_NAME,");
        queryString.append(" A.NEW_COM_REN_STATUS, A.STORE_GROUP, E.STORE_GROUP_NAME, A.STORE_TYPE, F.STORE_TYPE_NAME, A.DISTRICT_CODE, G.DISTRICT_NAME");
        queryString.append(" FROM RTL_STORE_INFO A, RTL_STORE_CLASSIFICATION B,");
        queryString.append(" RTL_STORE_CONCEPT C, RTL_STORE_BEHAVIOR D,");
        queryString.append(" RTL_STORE_GROUP E, RTL_STORE_TYPE F, RTL_DISTRICT G");
        queryString.append(" WHERE A.STORE_CLASSIFICATION = B.CLASSIFICATION_CODE");
        queryString.append(" AND A.STORE_CONCEPT =C.CONCEPT_CODE");
        queryString.append(" AND A.NEW_COM_REN_STATUS = D.STORE_BEHAVIOR_CODE");
        queryString.append(" AND A.STORE_GROUP=E.STORE_GROUP_CODE");
        queryString.append(" AND A.STORE_TYPE= F.STORE_TYPE_CODE");
        queryString.append(" AND A.DISTRICT_CODE = G.DISTRICT_CODE");
        queryString.append(" AND A.COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' AND A.STORE_CODE='");
        queryString.append((param.get("STORE_CODE") == null ? "" : param.get("STORE_CODE").toString()));
        queryString.append("' ORDER BY A.STORE_CODE ASC");

        return queryString.toString();
    }

    /*
     * Method: Store Update Query
     * Parameter: HashMap
     * Return: Query string
     */
    public static String storeUpdateQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("UPDATE RTL_STORE_INFO SET");
        queryString.append(" STORE_NAME = '");
        queryString.append((param.get("STORE_NAME") == null ? "" : param.get("STORE_NAME").toString().replaceAll("'", "\''")));
        queryString.append("', STORE_MANAGER = '");
        queryString.append((param.get("STORE_MANAGER") == null ? "" : param.get("STORE_MANAGER").toString().replaceAll("'", "\''")));
        queryString.append("', STORE_DESCRIPTION = '");
        queryString.append((param.get("STORE_DESCRIPTION") == null ? "" : param.get("STORE_DESCRIPTION").toString().replaceAll("'", "\''")));
        queryString.append("', STORE_PHONE = '");
        queryString.append((param.get("STORE_PHONE") == null ? "" : param.get("STORE_PHONE").toString().replaceAll("'", "\''")));
        queryString.append("', STORE_EMAIL = '");
        queryString.append((param.get("STORE_EMAIL") == null ? "" : param.get("STORE_EMAIL").toString().replaceAll("'", "\''")));
        queryString.append("', STORE_CLASSIFICATION = '");
        queryString.append((param.get("STORE_CLASSIFICATION") == null ? "" : param.get("STORE_CLASSIFICATION").toString().replaceAll("'", "\''")));
        queryString.append("', STORE_ADDRESS = '");
        queryString.append((param.get("STORE_ADDRESS") == null ? "" : param.get("STORE_ADDRESS").toString().replaceAll("'", "\''")));
        queryString.append("', MODEM_NUMBER = '");
        queryString.append((param.get("MODEM_NUMBER") == null ? "" : param.get("MODEM_NUMBER").toString().replaceAll("'", "\''")));
        queryString.append("', SIM_NUMBER = '");
        queryString.append((param.get("SIM_NUMBER") == null ? "" : param.get("SIM_NUMBER").toString().replaceAll("'", "\''")));
        queryString.append("', STORE_GROUP = '");
        queryString.append((param.get("STORE_GROUP") == null ? "" : param.get("STORE_GROUP").toString().replaceAll("'", "\''")));
        queryString.append("', OPEN_WEEK =");
        queryString.append((param.get("OPEN_WEEK") == null ? "" : param.get("OPEN_WEEK").toString().replaceAll("'", "\''")));
        queryString.append(", OPEN_YEAR =");
        queryString.append((param.get("OPEN_YEAR") == null ? "" : param.get("OPEN_YEAR").toString().replaceAll("'", "\''")));
        queryString.append(", STORE_TYPE = '");
        queryString.append((param.get("STORE_TYPE") == null ? "" : param.get("STORE_TYPE").toString().replaceAll("'", "\''")));
        queryString.append("', COMM_RATE = ");
        queryString.append((param.get("COMM_RATE") == null ? "" : param.get("COMM_RATE").toString().replaceAll("'", "\''")));
        queryString.append(", STORE_CONCEPT = '");
        queryString.append((param.get("STORE_CONCEPT") == null ? "" : param.get("STORE_CONCEPT").toString().replaceAll("'", "\''")));
        queryString.append("', STOCK_AREA =");
        queryString.append((param.get("STOCK_AREA") == null ? "" : param.get("STOCK_AREA").toString().replaceAll("'", "\''")));
        queryString.append(", SELLING_AREA=");
        queryString.append((param.get("SELLING_AREA") == null ? "" : param.get("SELLING_AREA").toString().replaceAll("'", "\''")));
        queryString.append(", NEW_COM_REN_STATUS='");
        queryString.append((param.get("NEW_COM_REN_STATUS") == null ? "" : param.get("NEW_COM_REN_STATUS").toString().replaceAll("'", "\''")));
        queryString.append("', NEW_COM_REN_WEEK=");
        queryString.append((param.get("NEW_COM_REN_WEEK") == null ? "" : param.get("NEW_COM_REN_WEEK").toString().replaceAll("'", "\''")));
        queryString.append(", NEW_COM_REN_YEAR=");
        queryString.append((param.get("NEW_COM_REN_YEAR") == null ? "" : param.get("NEW_COM_REN_YEAR").toString().replaceAll("'", "\''")));
        queryString.append(", SELL_LINE_STORE='");
        queryString.append((param.get("SELL_LINE_STORE") == null ? "" : param.get("SELL_LINE_STORE").toString().replaceAll("'", "\''")));
        queryString.append("', AREA_CODE='");
        queryString.append((param.get("AREA_CODE") == null ? "" : param.get("AREA_CODE").toString().replaceAll("'", "\''")));
        queryString.append("', UPDATE_BY='");
        queryString.append((param.get("UPDATE_BY") == null ? "" : param.get("UPDATE_BY").toString().replaceAll("'", "\''")));
        queryString.append("', UPDATE_DATE = SYSDATE");
        queryString.append(", DISTRICT_CODE=");
        queryString.append((param.get("DISTRICT_CODE") == null ? "" : param.get("DISTRICT_CODE").toString().replaceAll("'", "\''")));
        queryString.append(", POSTAL_CODE='");
        queryString.append((param.get("POSTAL_CODE") == null ? "" : param.get("POSTAL_CODE").toString().replaceAll("'", "\''")));
        queryString.append("'");
        queryString.append(" WHERE COM_CODE = '");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' AND STORE_CODE = '");
        queryString.append((param.get("STORE_CODE") == null ? "" : param.get("STORE_CODE").toString()));
        queryString.append("'");

        return queryString.toString();
    }

    /*
     * Method: Select All User
     * Parameter: HashMap
     * Return: Query string
     */
    public static String allUserSelectQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT D.USER_ID, D.COM_CODE, D.LEVEL_ID, D.LEVEL_NAME, D.USER_NAME,");
        queryString.append(" D.DISPLAY_NAME, D.USER_EMAIL, D.USER_CONTACT, D.STATUS, NVL(E.TOTAL_STORE,0) TOTAL_STORE");
        queryString.append(" FROM");
        queryString.append(" (SELECT A.USER_ID, A.COM_CODE, B.LEVEL_ID, B.LEVEL_NAME, A.USER_NAME,");
        queryString.append(" A.DISPLAY_NAME, A.USER_EMAIL, A.USER_CONTACT, A.STATUS");
        queryString.append(" FROM RTL_USER_INFO A, RTL_USER_LEVEL B ");
        queryString.append(" WHERE A.USER_LEVEL_ID = B.LEVEL_ID");
        queryString.append(" AND A.COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' ) D");
        queryString.append(" LEFT JOIN");
        queryString.append(" (SELECT USER_ID,  COUNT(STORE_CODE) TOTAL_STORE ");
        queryString.append(" FROM RTL_USER_VS_STORE ");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' GROUP BY USER_ID) E");
        queryString.append(" ON D.USER_ID = E.USER_ID");
        queryString.append(" ORDER BY UPPER(D.USER_NAME) ASC");

        return queryString.toString();
    }

    /*
     * Method: Select All User
     * Parameter: HashMap
     * Return: Query string
     */
    public static String userDetailsSelectQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT A.USER_ID, A.COM_CODE, B.LEVEL_ID, B.LEVEL_NAME, A.USER_PASSWORD,");
        queryString.append(" A.USER_NAME, A.DISPLAY_NAME, A.USER_EMAIL, A.USER_CONTACT, A.STATUS");
        queryString.append(" FROM RTL_USER_INFO A, RTL_USER_LEVEL B");
        queryString.append(" WHERE A.USER_LEVEL_ID = B.LEVEL_ID");
        queryString.append(" AND A.COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' AND A.USER_ID=");
        queryString.append((param.get("USER_ID") == null ? "" : param.get("USER_ID").toString()));
        queryString.append(" ORDER BY A.USER_NAME ASC");

        return queryString.toString();
    }

    /*
     * Method: Select User All Concept
     * Parameter: HashMap
     * Return: Query string
     */
    public static String userVsConceptSelectQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append(" SELECT C.CONCEPT_CODE CODE, C.CONCEPT_NAME NAME,");
        queryString.append(" CASE WHEN SUM(A.ACTIVE_STATUS) >0 THEN 'Y' ELSE 'N' END ACTIVE_STATUS");
        queryString.append(" FROM");
        queryString.append(" (SELECT STORE_CODE, SUM(ACTIVE_STATUS) ACTIVE_STATUS");
        queryString.append(" FROM");
        queryString.append(" (SELECT STORE_CODE, 0 ACTIVE_STATUS");
        queryString.append(" FROM RTL_STORE_INFO");
        queryString.append(" WHERE NEW_COM_REN_STATUS  NOT IN ('CLOSED')");
        queryString.append(" AND SELL_LINE_STORE = 'Y'");
        queryString.append(" AND COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' UNION ALL");
        queryString.append(" SELECT STORE_CODE, CASE WHEN ACTIVE_STATUS ='Y' THEN 1 ELSE 0 END ACTIVE_STATUS");
        queryString.append(" FROM RTL_USER_VS_STORE ");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' AND USER_ID=");
        queryString.append((param.get("USER_ID") == null ? "" : param.get("USER_ID").toString()));
        queryString.append(") GROUP BY STORE_CODE) A, RTL_STORE_INFO B, RTL_STORE_CONCEPT C");
        queryString.append(" WHERE A.STORE_CODE = B.STORE_CODE");
        queryString.append(" AND B.STORE_CONCEPT = C.CONCEPT_CODE");
        queryString.append(" GROUP By C.CONCEPT_CODE, C.CONCEPT_NAME");
        queryString.append(" ORDER BY C.CONCEPT_CODE");

        return queryString.toString();
    }

    /*
     * Method: Select User All Concept
     * Parameter: HashMap
     * Return: Query string
     */
    public static String userVsAreaSelectQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append(" SELECT C.AREA_CODE CODE, C.AREA_CODE NAME,");
        queryString.append(" CASE WHEN SUM(A.ACTIVE_STATUS) >0 THEN 'Y' ELSE 'N' END ACTIVE_STATUS");
        queryString.append(" FROM");
        queryString.append(" (SELECT STORE_CODE, SUM(ACTIVE_STATUS) ACTIVE_STATUS");
        queryString.append(" FROM");
        queryString.append(" (SELECT STORE_CODE, 0 ACTIVE_STATUS");
        queryString.append(" FROM RTL_STORE_INFO");
        queryString.append(" WHERE NEW_COM_REN_STATUS  NOT IN ('CLOSED')");
        queryString.append(" AND SELL_LINE_STORE = 'Y'");
        queryString.append(" AND COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' UNION ALL");
        queryString.append(" SELECT STORE_CODE, CASE WHEN ACTIVE_STATUS ='Y' THEN 1 ELSE 0 END ACTIVE_STATUS");
        queryString.append(" FROM RTL_USER_VS_STORE ");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' AND USER_ID=");
        queryString.append((param.get("USER_ID") == null ? "" : param.get("USER_ID").toString()));
        queryString.append(") GROUP BY STORE_CODE) A, RTL_STORE_INFO B, RTL_AREA_INFO C");
        queryString.append(" WHERE A.STORE_CODE = B.STORE_CODE");
        queryString.append(" AND B.AREA_CODE = C.AREA_CODE");
        queryString.append(" AND C.STATUS ='Y'");
        queryString.append(" GROUP By C.AREA_CODE");
        queryString.append(" ORDER BY C.AREA_CODE");

        return queryString.toString();
    }

    /*
     * Method: Select User All Store
     * Parameter: HashMap
     * Return: Query string
     */
    public static String userVsStoreSelectQuery(HashMap<Object, Object> param, String conceptCode) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT B.STORE_CONCEPT, B.STORE_CODE, B.STORE_NAME,");
        queryString.append(" CASE WHEN A.ACTIVE_STATUS >0 THEN 'Y' ELSE 'N' END ACTIVE_STATUS");
        queryString.append(" FROM");
        queryString.append(" (SELECT STORE_CODE, SUM(ACTIVE_STATUS) ACTIVE_STATUS");
        queryString.append(" FROM");
        queryString.append(" (SELECT STORE_CODE, 0 ACTIVE_STATUS");
        queryString.append(" FROM RTL_STORE_INFO");
        queryString.append(" WHERE NEW_COM_REN_STATUS  NOT IN ('CLOSED')");
        queryString.append(" AND SELL_LINE_STORE = 'Y'");
        queryString.append(" AND COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' UNION ALL");
        queryString.append(" SELECT STORE_CODE, CASE WHEN ACTIVE_STATUS ='Y' THEN 1 ELSE 0 END ACTIVE_STATUS");
        queryString.append(" FROM RTL_USER_VS_STORE ");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' AND USER_ID=");
        queryString.append((param.get("USER_ID") == null ? "" : param.get("USER_ID").toString()));
        queryString.append(" )GROUP BY STORE_CODE) A, RTL_STORE_INFO B");
        queryString.append(" WHERE A.STORE_CODE = B.STORE_CODE");
        queryString.append(" AND B.STORE_CONCEPT ='");
        queryString.append(conceptCode);
        queryString.append("' ORDER BY B.STORE_CONCEPT, B.STORE_CODE");


        return queryString.toString();
    }

    /*
     * Method: Select User All Store
     * Parameter: HashMap
     * Return: Query string
     */
    public static String userVsStoreSelectQuery2(HashMap<Object, Object> param, String areaCode) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT B.AREA_CODE, B.STORE_CODE, B.STORE_NAME,");
        queryString.append(" CASE WHEN A.ACTIVE_STATUS >0 THEN 'Y' ELSE 'N' END ACTIVE_STATUS");
        queryString.append(" FROM");
        queryString.append(" (SELECT STORE_CODE, SUM(ACTIVE_STATUS) ACTIVE_STATUS");
        queryString.append(" FROM");
        queryString.append(" (SELECT STORE_CODE, 0 ACTIVE_STATUS");
        queryString.append(" FROM RTL_STORE_INFO");
        queryString.append(" WHERE NEW_COM_REN_STATUS  NOT IN ('CLOSED')");
        queryString.append(" AND SELL_LINE_STORE = 'Y'");
        queryString.append(" AND COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' UNION ALL");
        queryString.append(" SELECT STORE_CODE, CASE WHEN ACTIVE_STATUS ='Y' THEN 1 ELSE 0 END ACTIVE_STATUS");
        queryString.append(" FROM RTL_USER_VS_STORE ");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' AND USER_ID=");
        queryString.append((param.get("USER_ID") == null ? "" : param.get("USER_ID").toString()));
        queryString.append(" )GROUP BY STORE_CODE) A, RTL_STORE_INFO B");
        queryString.append(" WHERE A.STORE_CODE = B.STORE_CODE");
        queryString.append(" AND B.AREA_CODE ='");
        queryString.append(areaCode);
        queryString.append("' ORDER BY B.AREA_CODE, B.STORE_CODE");
        //


        return queryString.toString();
    }

    /*
     * Method: User Password Update Query
     * Parameter: HashMap
     * Return: Query string
     */
    public static String userPasswordUpdateQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("UPDATE RTL_USER_INFO SET");
        queryString.append(" USER_PASSWORD ='");
        queryString.append((param.get("USER_PASSWORD") == null ? "" : param.get("USER_PASSWORD").toString().replaceAll("'", "\''")));
        queryString.append("', UPDATE_BY ='");
        queryString.append((param.get("UPDATE_BY") == null ? "" : param.get("UPDATE_BY").toString().replaceAll("'", "\''")));
        queryString.append("', UPDATE_DATE = SYSDATE WHERE USER_ID ='");
        queryString.append((param.get("USER_ID") == null ? "" : param.get("USER_ID").toString()));
        queryString.append("' AND COM_CODE ='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("'");

        return queryString.toString();
    }

    /*
     * Method: All Brand select query
     * Parameter: HashMap
     * Return: Query string
     */
    public static String allBrandQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT COM_CODE, BRAND_CODE, BRAND_NAME, BRAND_SHORT_CODE,");
        queryString.append(" BRAND_DESCRIPTION, BRAND_LOGO_PATH, BRAND_DISTRIBUTION, BRAND_STATUS,");
        queryString.append(" INSERT_BY, INSERT_DATE");
        queryString.append(" FROM RTL_BRAND");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("'");

        return queryString.toString();
    }

    /*
     * Method: All Brand select query
     * Parameter: HashMap
     * Return: Query string
     */
    public static String brandCodeViewQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT COM_CODE, BRAND_CODE, BRAND_NAME, BRAND_SHORT_CODE,");
        queryString.append(" BRAND_DESCRIPTION, BRAND_LOGO_PATH, BRAND_DISTRIBUTION, BRAND_STATUS,");
        queryString.append(" INSERT_BY, INSERT_DATE");
        queryString.append(" FROM RTL_BRAND");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' AND BRAND_CODE ='");
        queryString.append((param.get("BRAND_CODE") == null ? "" : param.get("BRAND_CODE").toString()));
        queryString.append("'");

        return queryString.toString();
    }

    /*
     * Method: Brand Code Count Query
     * Parameter: HashMap (Company Code, Brand Code)
     * Return: Query string
     */
    public static String brandCodeCountQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT COUNT(BRAND_CODE) TOTAL FROM RTL_BRAND");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' AND UPPER(BRAND_CODE) =UPPER('");
        queryString.append((param.get("BRAND_CODE") == null ? "" : param.get("BRAND_CODE").toString()));
        queryString.append("')");

        return queryString.toString();
    }

    /*
     * Method: Brand Name Count Query
     * Parameter: HashMap (Company Code, Brand Name)
     * Return: Query string
     */
    public static String brandNameCountQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT COUNT(BRAND_NAME) TOTAL FROM RTL_BRAND");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' AND UPPER(BRAND_NAME) =UPPER('");
        queryString.append((param.get("BRAND_NAME") == null ? "" : param.get("BRAND_NAME").toString()));
        queryString.append("')");

        return queryString.toString();
    }

    /*
     * Method: Brand Insert Query
     * Parameter: HashMap
     * Return: Query string
     */
    public static String brandInsertQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("INSERT INTO RTL_BRAND");
        queryString.append(" (COM_CODE, BRAND_CODE, BRAND_NAME, BRAND_DESCRIPTION, BRAND_STATUS, INSERT_BY) VALUES (");
        queryString.append("'");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("','");
        queryString.append((param.get("BRAND_CODE") == null ? "" : param.get("BRAND_CODE").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("BRAND_NAME") == null ? "" : param.get("BRAND_NAME").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("BRAND_DESCRIPTION") == null ? "" : param.get("BRAND_DESCRIPTION").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("BRAND_STATUS") == null ? "" : param.get("BRAND_STATUS").toString().replaceAll("'", "\''")));
        queryString.append("','");
        queryString.append((param.get("INSERT_BY") == null ? "" : param.get("INSERT_BY").toString().replaceAll("'", "\''")));
        queryString.append("')");

        return queryString.toString();
    }

    /*
     * Method: Brand Update Query
     * Parameter: HashMap
     * Return: Query string
     */
    public static String brandUpdateQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("UPDATE RTL_BRAND SET");
        queryString.append(" BRAND_DESCRIPTION ='");
        queryString.append((param.get("BRAND_DESCRIPTION") == null ? "" : param.get("BRAND_DESCRIPTION").toString().replaceAll("'", "\''")));
        queryString.append("', BRAND_STATUS ='");
        queryString.append((param.get("BRAND_STATUS") == null ? "" : param.get("BRAND_STATUS").toString().replaceAll("'", "\''")));
        queryString.append("', UPDATE_BY='");
        queryString.append((param.get("UPDATE_BY") == null ? "" : param.get("UPDATE_BY").toString().replaceAll("'", "\''")));
        queryString.append("', UPDATE_DATE = SYSDATE ");
        queryString.append(" WHERE COM_CODE='");
        queryString.append((param.get("COM_CODE") == null ? "" : param.get("COM_CODE").toString()));
        queryString.append("' AND BRAND_CODE='");
        queryString.append((param.get("BRAND_CODE") == null ? "" : param.get("BRAND_CODE").toString().replaceAll("'", "\''")));
        queryString.append("'");

        return queryString.toString();
    }
}
