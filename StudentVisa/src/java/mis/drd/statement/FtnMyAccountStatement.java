/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.drd.statement;

import java.util.HashMap;

/**
 *
 * @author MIS
 */
public class FtnMyAccountStatement {

    //static member holds only one instance of the LoginStatement class.
    private static FtnMyAccountStatement ftnMyAccountStatement = null;

    //LoginStatement prevents the instantiation from any other class.
    private FtnMyAccountStatement() {
    }

    //Now we are providing gloabal point of access.
    public static synchronized FtnMyAccountStatement getInstance() {

        if (ftnMyAccountStatement == null) {
            ftnMyAccountStatement = new FtnMyAccountStatement();
        }

        return ftnMyAccountStatement;

    }

    /**
     * Method: Country State query
     * Parameter: HashMap (Country Code)
     * Return: Query string
     */
    public static String customerMyPointsQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT DISTINCT SATE_REGION FROM FTN_COUNTRY_INFO");
        queryString.append(" WHERE COUNTRY_CODE ='");
        queryString.append((param.get("COUNTRY_CODE") == null ? "" : param.get("COUNTRY_CODE").toString()));
        queryString.append("' ORDER BY SATE_REGION ASC");

        return queryString.toString();
    }

    /**
     * Method: Search Customer query
     * Parameter: HashMap (Customer Code, Contact Number)
     * Return: Query string
     */
    public static String searchCustomerQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT CUSTOMER_ID, CONTACT_NO, EMAIL_ADD, FIRST_NAME, MIDDLE_NAME, LAST_NAME, POSTAL_CODE,");
        queryString.append(" SURE_NAME, ADD1, ADD2, ADD3, STATE_REGION, CITY, SHIPPING_ADDRESS, GENDER, ALT_CONTACT_NO,");
        queryString.append(" TO_CHAR(DOB,'DD/MM/RRRR') DOB, TO_CHAR(MARRIAGE_DATE,'DD/MM/RRRR') MARRIAGE_DATE, MARITAL_STATUS,");
        queryString.append(" OCCUPATION, RELIGION, CUSTOMER_TYPE, COUNTRY_NAME");
        queryString.append(" FROM FTN_CUSTOMER_INFO WHERE  CUSTOMER_ID = '");
        queryString.append(param.get("CUSTOMER_ID").toString());
        queryString.append("'");

        return queryString.toString();
    }

    public static String countryStateCityQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT DISTINCT CITY_NAME FROM FTN_COUNTRY_INFO");
        queryString.append(" WHERE COUNTRY_NAME ='");
        queryString.append((param.get("COUNTRY_NAME") == null ? "" : param.get("COUNTRY_NAME").toString()));
        queryString.append("' AND SATE_REGION ='");
        queryString.append((param.get("SATE_REGION") == null ? "" : param.get("SATE_REGION").toString()));
        queryString.append("' ORDER BY CITY_NAME ASC");

        return queryString.toString();
    }

    public static String countryStateQuery(HashMap<Object, Object> param) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("SELECT DISTINCT SATE_REGION FROM FTN_COUNTRY_INFO");
        queryString.append(" WHERE COUNTRY_NAME ='");
        queryString.append((param.get("COUNTRY_NAME") == null ? "" : param.get("COUNTRY_NAME").toString()));
        queryString.append("' ORDER BY SATE_REGION ASC");

        return queryString.toString();
    }
}
