
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mis.drd.action.ParamUtil;
import mis.drd.utility.DatabaseConnection;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author MIS
 */
public class ReportServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String filename = (String) request.getSession().getAttribute("report");
        String reporttype = (String) request.getSession().getAttribute("format");
        Map parameters = (HashMap) request.getSession().getAttribute("parameters");
        String subReportFilePath = request.getSession().getAttribute("subreport") != null ? (String) request.getSession().getAttribute("subreport") : "";
        request.getSession().removeAttribute("report");
        request.getSession().removeAttribute("format");
        request.getSession().removeAttribute("parameters");


//        InputStream image = (InputStream) getServletContext().getResourceAsStream("/WEB-INF/bata_2.jpg");
        InputStream image = (InputStream) getServletContext().getResourceAsStream("/WEB-INF/logo.png");
        parameters.put("image", image);
        InputStream sub_report = null;
        if (subReportFilePath.length() > 0) {
            sub_report = (InputStream) getServletContext().getResourceAsStream(subReportFilePath);
            parameters.put("sub_report", sub_report);
        }
        Connection conn = null;
        OutputStream ouputStream = null;
        if (ParamUtil.isSet(filename) && ParamUtil.isSet(reporttype) && ParamUtil.isSet(parameters)) {
            try {

                conn = DatabaseConnection.connectDB();

//                Class.forName("oracle.jdbc.driver.OracleDriver");
//                conn = DriverManager.getConnection("jdbc:oracle:thin:@172.20.8.41:1521:orcl", "RETAIL","SYS123");
                if (conn != null) {
                    String path = request.getSession().getServletContext().getRealPath("/");
                    JasperReport jasperReport = JasperCompileManager.compileReport(path + "report/" + filename);
                    if (jasperReport != null) {
                        try {
                            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, conn);

                            ouputStream = response.getOutputStream();

                            JRExporter exporter = null;
                            JExcelApiExporter exporterXLS = null;
                            if ("pdf".equalsIgnoreCase(reporttype)) {
                                response.setContentType("application/pdf");
                                exporter = new JRPdfExporter();
                                exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                                exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, ouputStream);
                            } else if ("rtf".equalsIgnoreCase(reporttype)) {
                                response.setContentType("application/rtf");
                                response.setHeader("Content-Disposition", "inline; filename=\"file.rtf\"");

                                exporter = new JRRtfExporter();
                                exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                                exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, ouputStream);
                            } else if ("html".equalsIgnoreCase(reporttype)) {
                                exporter = new JRHtmlExporter();
                                exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                                exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, ouputStream);
                            } else if ("xls".equalsIgnoreCase(reporttype)) {
                                response.setContentType("application/xls");
                                response.setHeader("Content-Disposition", "inline; filename=\"file.xls\"");

                                exporter = new JExcelApiExporter();
                                exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                                exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, ouputStream);


                            } else if ("csv".equalsIgnoreCase(reporttype)) {
                                response.setContentType("application/csv");
                                response.setHeader("Content-Disposition", "inline; filename=\"file.csv\"");

                                exporter = new JRCsvExporter();
                                exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                                exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, ouputStream);
                            }

                            try {
                                exporter.exportReport();
                            } catch (JRException e) {
                                e.printStackTrace();
                                //   System.out.println(e.getMessage());
                                //throw new ServletException(e);
                            } finally {
                                if (ouputStream != null) {
                                    try {
                                        ouputStream.flush();
                                        ouputStream.close();
                                    } catch (IOException ex) {

                                        Logger.getLogger(ReportServlet.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            // System.out.println(ex.getMessage());
                            Logger.getLogger(ReportServlet.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex.getMessage());
                Logger.getLogger(ReportServlet.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(ReportServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
